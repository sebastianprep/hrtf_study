# README #

This file contains information about the present repository.

Files here are given as open-source under the MIT license.

### What is this repository for? ###

* The present repository contains scripts and programs related to the **verification of FDTD** simulation of acoustic simulations. 
* 1.2 update (June 2020): added solution verification python modules that were used.
* Sebastian Prepelita, 2018

This code was used for the following papers (please cite them if you use this code):

* [Prepeliță S., Gómez Bolaños J., Geronazzo M., Mehra R., Savioja L., "Pinna-related transfer functions and lossless wave equation using finite-difference methods: Verification and asymptotic solution", J. Acoust. Soc. Am., 146(5), pp.3629–3645, (2019)](https://doi.org/10.1121/1.5131245)



* [Prepeliță S., Gómez Bolaños J., Geronazzo M., Mehra R., Savioja L., "Pinna-related transfer functions and lossless wave equation using finite-difference methods: Validation with measurements", J. Acoust. Soc. Am., 147(5), pp.3631-3645, (2020)](https://doi.org/10.1121/10.0001230) 

### How do I get set up? ###

One needs the following: 

*    [**Python 2.7**](https://www.python.org/download/releases/2.7/) with the following packages: [*numpy*](https://www.numpy.org), [*scipy*](https://www.scipy.org), [*hdf5*](https://www.h5py.org), [*matplotlib*](https://matplotlib.org), [*statsmodels*](https://www.statsmodels.org/stable/index.html), [*scikits.bootstrap*](https://pypi.org/project/scikits.bootstrap/)


*    [**ParallelFDTD**](https://github.com/juuli/ParallelFDTD) (and dependencies: [*HDF5 libray (C/C++)*](https://support.hdfgroup.org/downloads/index.html), [*Boost library*](https://www.boost.org), [CUDA SDK](https://developer.nvidia.com/cuda-downloads), *CUDA-aware MPI library* (e.g., [*OpenMPI*](https://www.open-mpi.org), [*MVAPICH2-GDR*](http://mvapich.cse.ohio-state.edu/userguide/gdr/)) )


*    (optional) [**SLURM**](https://slurm.schedmd.com/documentation.html)


### Code verification quick tutorial: ###

In order to verify your code:

1. Have a compiled and running version of ParallelFDTD library.

    ## 
    ## 

2. Edit ``code_verification\MPI_Verification\``[Makefile](https://bitbucket.org/sebastianprep/hrtf_study/src/master/src/code_verification/MPI_Verification/Makefile) to point to the correct locations of the libraries (e.g., compiler name, ParallelFDTD branch and location, Voxelizer library, MPI library, Boost library)

    ## 
    *  Update include paths variables ``$I_PATH_%%%``  
   
    *  Update library paths in variables ``$L_PATH_%%%``  
   
    ## 
    *  Note the ``$PARALEL_FDTD_BRANCH`` variable is used such that the ParallelFDTD folder is built as ``path_to_library\ParallelFDTD-$(PARALEL_FDTD_BRANCH)``  
    ## 
    
3. Compile the code verification from C++ code:

    ## 
    *  **Regular verification**: 
    
        ##  
        *  *make test*
        
            which compiles ``MPI_Verification\src\``[code_Verification_BOX.cpp](https://bitbucket.org/sebastianprep/hrtf_study/src/master/src/code_verification/MPI_Verification/src/code_Verification_BOX.cpp) as ``code_Verification_BOX_prog`` (actual name of program can be modified in ``Makefile``)
   
    ## 
    *  **MPI verification**: 
    
        ##  
        *  *make test_mpi* 
        
            which compiles ``MPI_Verification\src\``[code_Verification_BOX_MPI.cpp as](https://bitbucket.org/sebastianprep/hrtf_study/src/master/src/code_verification/MPI_Verification/src/code_Verification_BOX_MPI.cpp) ``code_Verification_BOX_MPI_prog`` (actual name of program can be modified in ``Makefile``)
    
    ## 
	The branch name agains which the program is compiled is written in a ``make_branch_name.txt`` (actual name of file can be modified in ``Makefile``) file. This can be used at runtime to point to the correct location of the [FDTD library](https://github.com/juuli/ParallelFDTD).

    ## 
    
4. Run the code verification program on your environment with three arguments:

    ## 
    
    *  *./code_Verification_BOX_prog $TEST_MES $TEST_INTERIOR $GPU_PARTITIONS*,
    *  *mpiexec ./code_Verification_BOX_MPI_prog $TEST_MES $TEST_INTERIOR $GPU_PARTITIONS*,
    
        ##  
        ``$TEST_MES`` -> test EXACT SOLUTION. true or false
        
        ``$TEST_INTERIOR`` -> test MANUFACTURED SOLUTION. true or false
        
        ``$GPU_PARTITIONS`` -> integer telling the software how many GPU partitions per MPI node to use. 1,2,3... Code will fail if not enough number of partitions are available.
        
        ##  
        Example:
        
        ``$GPU_PARTITIONS=16`` for 16 partitions per node
        
        *  ``$TEST_MES = true`` for exact solution (``$TEST_INTERIOR`` value does not matter),
        
        *  ``$TEST_MES = false`` and ``$TEST_INTERIOR = true`` for manufactured solution (no boundary update),
        
        *  ``$TEST_MES = false`` and ``$TEST_INTERIOR = false`` for manufactured solution (FULL update including boundary conditions).
        

        ##  
        ##  
        
5. (Optional) Use [**SLURM**](https://slurm.schedmd.com/documentation.html) to schedule the runs.

    ## 

    1.  Edit ``.slrm`` files (i.e., [FDTD_verif.slrm](https://bitbucket.org/sebastianprep/hrtf_study/src/master/src/code_verification/MPI_Verification/FDTD_verif.slrm) and [FDTD_verif_MPI.slrm](https://bitbucket.org/sebastianprep/hrtf_study/src/master/src/code_verification/MPI_Verification/FDTD_verif_MPI.slrm)) to fit your environment needs
    
        ##  
        
        Here, make sure to set the ``#SBATCH`` variables properly and the ``export`` statements for the test case.
    
        ##  
        
    2.  Run batch simulation:
    
        ##  
        
        *  [FDTD_verif.slrm](https://bitbucket.org/sebastianprep/hrtf_study/src/master/src/code_verification/MPI_Verification/FDTD_verif.slrm)
        
            schedules ``code_Verification_BOX_prog`` on a single node.
            
            ##  
            
        *  [FDTD_verif_MPI.slrm](https://bitbucket.org/sebastianprep/hrtf_study/src/master/src/code_verification/MPI_Verification/FDTD_verif_MPI.slrm)
        
            schedules ``code_Verification_BOX_MPI_prog`` on multiple MPI nodes.

    ## 

5. Wait for the program output from the three scenarios at point 4. (.hdf5 files). Then create a folder named ``some_folder`` (e.g., [MPI_AWS_surface_vox_host](https://bitbucket.org/sebastianprep/hrtf_study/src/master/src/code_verification/Results/MPI_AWS_surface_vox_host/)) in the [Results](https://bitbucket.org/sebastianprep/hrtf_study/src/master/src/code_verification/Results/) folder and store the output  and the following subfolders:
    
    ## 

    1.  ``Results\some_folder\MES`` (e.g., [MES](https://bitbucket.org/sebastianprep/hrtf_study/src/master/src/code_verification/Results/MPI_AWS_surface_vox_host/MES/)) -> results from EXACT SOLUTION (i.e., ``$TEST_MES = true``)
       

    2.  ``Results\some_folder\MMS_Boundaries`` (e.g., [MMS_Boundaries](https://bitbucket.org/sebastianprep/hrtf_study/src/master/src/code_verification/Results/MPI_AWS_surface_vox_host/MMS_Boundaries/))  -> results from MANUFACTURED SOLUTION no boundary (i.e., ``$TEST_MES = false`` and ``$TEST_INTERIOR = true``)
       

    3.  ``Results\some_folder\MMS_interior`` (e.g., [MMS_interior](https://bitbucket.org/sebastianprep/hrtf_study/src/master/src/code_verification/Results/MPI_AWS_surface_vox_host/MMS_interior/)) -> results from MANUFACTURED SOLUTION (i.e., ``$TEST_MES = false`` and ``$TEST_INTERIOR = false``)

    ## 

6. Analyze the data in Python:

    ##  
    
    1.  Edit and update ``code_Verification_analysis.py`` to point to ``some_folder`` in the calls: 
    
        ##  
        
            ...(..., reading_dir = "Results" + os.sep + "some_folder")


    
        ##  
        

    2.  Run the python ``code_Verification_analysis.py`` (e.g., *python code_Verification_analysis*)

    ## 


7. If results are like in the paper, you are done - code was verified.

### Who do I talk to? ###

* Sebastian Prepelita