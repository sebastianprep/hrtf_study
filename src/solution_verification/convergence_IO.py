'''
This file is released under the MIT License. See
http://www.opensource.org/licenses/mit-license.php

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Created on Oct 9, 2018

    Function designed for Input/Output functions for data used in convergence studies.

@author: Sebastian Prepelita
'''
import h5py
import numpy
import os
import MPI_IO
import re
import warnings

class RegressionModelData:
    '''
    Class stores data computed based on a regression model. Used to encapsulate data and do IO with it.
    '''
    def __init__(self, N_directions = None, N_grids = None, N_frequencies = None, Model_size = None):
        if (N_directions is None) or (N_grids is None) or (N_frequencies is None) or (Model_size is None):
            self.model_name = "";                                           # String containing the model name - used when writing a .hdf5 to create a separate hdf5 group.
            self.X_matrix = numpy.float64( [[0.0]] );                       # [N_grids, Model_size] The X matrix (exogen or input matrix) - consistent across bins and directions.
            self.dX_vector = numpy.float64( [[0.0]] );                      # [N_grids] The grid spacing vector - consistent across bins and directions.
            self.weights = numpy.float64( [0.0] );                          # [N_grids] Weights used in a Weighted Least Squares (if used).
            self.df = 0.0;                                                  # Float with frequency resolution.
            self.B_samples = 0;                                             # Int with the number of bootstrap samples used.
            self.frequency_vector = numpy.float64( [0.0] );                 # [N_freqs] Vector with the positive frequencies for which data is stored.
            self.final_receiver_direction_list = numpy.float64( [0.0] );    # [N_directions, 2] Vector with directions in spherical coordinate system (azimuht, elevation).
            self.fs_list = numpy.float64( [[0.0]] );                        # [N_grids] The sampling frequencies for all the grids.
            ############################################################################################################################ Data for all directions and frequencies:
            self.Y_ = numpy.float64( [0.0] );                               # [N_dirs, N_freqs, N_grids] The Y array (output) for each bin.
            self.beta = numpy.float64( [[0.0]] );                                       # [N_dirs, N_freqs, Model_size]     The linear regression model parameters.
            self.residuals = numpy.float64( [[0.0]] );                                  # [N_dirs, N_freqs, N_grids]        The residuals.
            self.R2 = numpy.float64( [[0.0]] );                                         # [N_dirs, N_freqs]                 Coefficient of determination
            self.R2_adjusted = numpy.float64( [[0.0]] );                                # [N_dirs, N_freqs]                 Adjusted coefficient of determination
            self.p_values = numpy.float64( [[[0.0]]] );                                 # [N_dirs, N_freqs, Model_size]     P-value for each coefficient of linear regression
            self.f_value = numpy.float64( [[0.0]] );                                    # [N_dirs, N_freqs]                 F-value for the linear regression fit.
            self.asymptotic_solution_average = numpy.float64([[0.0]]);                      # [N_dirs, N_freqs]             Asymptotic solution E[y | x=0] for the regression model.
            self.asymptotic_solution_CI_lower = numpy.float64([[0.0]]);                     # [N_dirs, N_freqs]             Lower bound parametric (i.e., errors NID) confindence interval (alpha = alpha_) for the asymptotic solution.
            self.asymptotic_solution_CI_upper = numpy.float64([[0.0]]);                     # [N_dirs, N_freqs]             UPPER bound parametric (i.e., errors NID) confindence interval (alpha = alpha_) for the asymptotic solution.
            self.asymptotic_solution_CI_lower_BootstrapPairs = numpy.float64([[0.0]]);      # [N_dirs, N_freqs]             Lower bound non-parametric (i.e., errors IID; boostrapping pairs) confindence interval (alpha = alpha_) for the asymptotic solution.
            self.asymptotic_solution_CI_upper_BootstrapPairs = numpy.float64([[0.0]]);      # [N_dirs, N_freqs]             UPPER bound non-parametric (i.e., errors IID; boostrapping pairs) confindence interval (alpha = alpha_) for the asymptotic solution.
            self.asymptotic_solution_CI_lower_BootstrapResiduals = numpy.float64([[0.0]]);  # [N_dirs, N_freqs]             Lower bound non-parametric (i.e., errors IID; boostrapping residuals) confindence interval (alpha = alpha_) for the asymptotic solution.
            self.asymptotic_solution_CI_upper_BootstrapResiduals = numpy.float64([[0.0]]);  # [N_dirs, N_freqs]             UPPER bound non-parametric (i.e., errors IID; boostrapping residuals) confindence interval (alpha = alpha_) for the asymptotic solution.
        else:
            # See details above for what the data means and their sizes:
            self.model_name = "";
            self.X_matrix = numpy.zeros( (N_grids, Model_size), dtype = numpy.float64 );
            self.dX_vector = numpy.zeros( N_grids, dtype = numpy.float64 );
            self.weights = numpy.zeros( N_grids, dtype = numpy.float64 );
            self.df = 0.0;
            self.B_samples = 0;
            self.frequency_vector = numpy.zeros( N_frequencies, dtype = numpy.float64 );
            self.final_receiver_direction_list = numpy.zeros((N_directions,2), dtype = numpy.float64 );
            self.fs_list = numpy.zeros(N_grids, dtype = numpy.float64 );
            ############################################################################################################################ Data for all directions and frequencies:
            self.Y_ = numpy.zeros( (N_directions, N_frequencies, N_grids), dtype = numpy.float64 );
            self.beta = numpy.zeros( (N_directions, N_frequencies, Model_size), dtype = numpy.float64 );
            self.residuals = numpy.zeros( (N_directions, N_frequencies, N_grids), dtype = numpy.float64 );
            self.R2 = numpy.zeros( (N_directions, N_frequencies), dtype = numpy.float64 );
            self.R2_adjusted = numpy.zeros( (N_directions, N_frequencies), dtype = numpy.float64 );
            self.p_values = numpy.zeros( (N_directions, N_frequencies, Model_size), dtype = numpy.float64 );
            self.f_value = numpy.zeros( (N_directions, N_frequencies), dtype = numpy.float64 );
            self.asymptotic_solution_average = numpy.zeros( (N_directions, N_frequencies), dtype = numpy.float64 );
            self.asymptotic_solution_CI_lower = numpy.zeros( (N_directions, N_frequencies), dtype = numpy.float64 );
            self.asymptotic_solution_CI_upper = numpy.zeros( (N_directions, N_frequencies), dtype = numpy.float64 );
            self.asymptotic_solution_CI_lower_BootstrapPairs = numpy.zeros( (N_directions, N_frequencies), dtype = numpy.float64 );
            self.asymptotic_solution_CI_upper_BootstrapPairs = numpy.zeros( (N_directions, N_frequencies), dtype = numpy.float64 );
            self.asymptotic_solution_CI_lower_BootstrapResiduals = numpy.zeros( (N_directions, N_frequencies), dtype = numpy.float64 );
            self.asymptotic_solution_CI_upper_BootstrapResiduals = numpy.zeros( (N_directions, N_frequencies), dtype = numpy.float64 );

def dump_RegressionData_Model(file_dir, file_name, RegressionModeldata_, N_grids, N_dirs, N_freqs):
    '''
    Function dumps data in the RegressionModelData() class by appending to the hdf5 file.
    
    ! Note it appends so manage the deletion of file somewhere upstream!
    
    :param file_dir: The base dir where the data is appended.
    :param file_name: The file name (with or without .hdf5 extension).
    :param RegressionModeldata_: A populated RegressionModelData() class.
    
    :param N_grids: The number of grids. Written only if none exists before.
    :param N_dirs: The number of directions. Written only if none exists before.
    :param N_freqs: The number of frequencies. Written only if none exists before.
    '''
    if file_name[-5::] != '.hdf5':
        file_name = file_name + '.hdf5';
    f = h5py.File ( file_dir + os.sep + file_name, 'a');
    
    # Check if N_xxx exist:
    datasets_info = f.items();
    set_keys = [];
    for i_ in range ( len(datasets_info)):
        if isinstance(datasets_info[i_][1], h5py.Dataset): # .Dataset for datasets...
            set_keys.append(datasets_info[i_][0]);
    if 'N_grids' in set_keys:
        N_grids_file = numpy.asscalar(f['N_grids'][...]);
        if N_grids_file != N_grids:
            raise NameError("Inconsistency in N_grids. In file = " + str(N_grids_file) + " and given as input = " + str(N_grids) );
    else:
        f.create_dataset('N_grids', data = N_grids);
    if 'N_dirs' in set_keys:
        N_dirs_file = numpy.asscalar(f['N_dirs'][...]);
        if N_dirs_file != N_dirs:
            raise NameError("Inconsistency in N_dirs. In file = " + str(N_dirs_file) + " and given as input = " + str(N_dirs) );
    else:
        f.create_dataset('N_dirs', data = N_dirs);
    if 'N_freqs' in set_keys:
        N_freqs_file = numpy.asscalar(f['N_freqs'][...]);
        if N_freqs_file != N_freqs:
            raise NameError("Inconsistency in N_freqs. In file = " + str(N_freqs_file) + " and given as input = " + str(N_freqs) );
    else:
        f.create_dataset('N_freqs', data = N_freqs);
    # Create a group:
    regrData_grp = f.create_group( RegressionModeldata_.model_name );
    regrData_grp.create_dataset("X_matrix", data = RegressionModeldata_.X_matrix );
    regrData_grp.create_dataset("weights", data = RegressionModeldata_.weights );
    regrData_grp.create_dataset("df", data = RegressionModeldata_.df );
    regrData_grp.create_dataset("B_samples", data = RegressionModeldata_.B_samples );
    regrData_grp.create_dataset("frequency_vector", data = RegressionModeldata_.frequency_vector );
    regrData_grp.create_dataset("final_receiver_direction_list", data = RegressionModeldata_.final_receiver_direction_list );
    regrData_grp.create_dataset("fs_list", data = RegressionModeldata_.fs_list );
    regrData_grp.create_dataset("Y_", data = RegressionModeldata_.Y_ );
    regrData_grp.create_dataset("beta", data = RegressionModeldata_.beta );
    regrData_grp.create_dataset("residuals", data = RegressionModeldata_.residuals );
    regrData_grp.create_dataset("R2", data = RegressionModeldata_.R2 );
    regrData_grp.create_dataset("R2_adjusted", data = RegressionModeldata_.R2_adjusted );
    regrData_grp.create_dataset("p_values", data = RegressionModeldata_.p_values );
    regrData_grp.create_dataset("f_value", data = RegressionModeldata_.f_value );
    regrData_grp.create_dataset("asymptotic_solution_average", data = RegressionModeldata_.asymptotic_solution_average );
    regrData_grp.create_dataset("asymptotic_solution_CI_lower", data = RegressionModeldata_.asymptotic_solution_CI_lower );
    regrData_grp.create_dataset("asymptotic_solution_CI_upper", data = RegressionModeldata_.asymptotic_solution_CI_upper );
    regrData_grp.create_dataset("asymptotic_solution_CI_lower_BootstrapPairs", data = RegressionModeldata_.asymptotic_solution_CI_lower_BootstrapPairs );
    regrData_grp.create_dataset("asymptotic_solution_CI_upper_BootstrapPairs", data = RegressionModeldata_.asymptotic_solution_CI_upper_BootstrapPairs );
    regrData_grp.create_dataset("asymptotic_solution_CI_lower_BootstrapResiduals", data = RegressionModeldata_.asymptotic_solution_CI_lower_BootstrapResiduals );
    regrData_grp.create_dataset("asymptotic_solution_CI_upper_BootstrapResiduals", data = RegressionModeldata_.asymptotic_solution_CI_upper_BootstrapResiduals );        
    # Close the file:
    f.close();

def read_RegressionData_Model(file_dir, file_name, model_name = None):
    '''
    Function dumps data in the RegressionModelData() class by appending to the hdf5 file.
    
    ! Note it appends so manage the deletion of file somewhere upstream!
    
    :param file_dir: The base dir where the data is appended.
    :param file_name: The file name (with or without .hdf5 extension).
    :param RegressionModeldata_: A populated RegressionModelData() class.
    '''
    if file_name[-5::] != '.hdf5':
        file_name = file_name + '.hdf5';
    f = h5py.File ( file_dir + os.sep + file_name, 'r');
    if model_name is None:
        raise NameError("read_RegressionData_Model() needs a model name to read a specific group!");
    # Read N_xxx data:
    N_grids = numpy.asscalar(f['N_grids'][...]);
    N_dirs = numpy.asscalar(f['N_dirs'][...]);
    N_freqs = numpy.asscalar(f['N_freqs'][...]);
    
    # Get group keys:
    group_keys = [];
    dataset_info = f.items();
    for i_ in range ( len(dataset_info)):
        if isinstance(dataset_info[i_][1], h5py.Group): # .Dataset for datasets...
            group_keys.append(dataset_info[i_][0]);
    model_name = str(model_name);
    if model_name not in group_keys:
        raise NameError("Group name '" + model_name + "' not in file " + str(file_name));
    RegressionModeldata_ = RegressionModelData(N_directions = N_dirs, N_grids = N_grids, N_frequencies = N_freqs, Model_size = len(f[model_name + '/beta'][...]));
    
    RegressionModeldata_.X_matrix = f[model_name + '/X_matrix'][...];
    RegressionModeldata_.weights = f[model_name + '/weights'][...];
    RegressionModeldata_.df = numpy.asscalar(f[model_name + '/df'][...]);
    RegressionModeldata_.B_samples = numpy.asscalar(f[model_name + '/B_samples'][...]);
    RegressionModeldata_.frequency_vector = f[model_name + '/frequency_vector'][...];
    RegressionModeldata_.final_receiver_direction_list = f[model_name + '/final_receiver_direction_list'][...];
    RegressionModeldata_.fs_list = f[model_name + '/fs_list'][...];
    RegressionModeldata_.Y_ = f[model_name + '/Y_'][...];
    RegressionModeldata_.beta = f[model_name + '/beta'][...];
    RegressionModeldata_.residuals = f[model_name + '/residuals'][...];
    RegressionModeldata_.R2 = f[model_name + '/R2'][...];
    RegressionModeldata_.R2_adjusted = f[model_name + '/R2_adjusted'][...];
    RegressionModeldata_.p_values = f[model_name + '/p_values'][...];
    RegressionModeldata_.f_value = f[model_name + '/f_value'][...];
    RegressionModeldata_.asymptotic_solution_average = f[model_name + '/asymptotic_solution_average'][...];
    RegressionModeldata_.asymptotic_solution_CI_lower = f[model_name + '/asymptotic_solution_CI_lower'][...];
    RegressionModeldata_.asymptotic_solution_CI_upper = f[model_name + '/asymptotic_solution_CI_upper'][...];
    RegressionModeldata_.asymptotic_solution_CI_lower_BootstrapPairs = f[model_name + '/asymptotic_solution_CI_lower_BootstrapPairs'][...];
    RegressionModeldata_.asymptotic_solution_CI_upper_BootstrapPairs = f[model_name + '/asymptotic_solution_CI_upper_BootstrapPairs'][...];
    RegressionModeldata_.asymptotic_solution_CI_lower_BootstrapResiduals = f[model_name + '/asymptotic_solution_CI_lower_BootstrapResiduals'][...];
    RegressionModeldata_.asymptotic_solution_CI_upper_BootstrapResiduals = f[model_name + '/asymptotic_solution_CI_upper_BootstrapResiduals'][...];
    # Close the file:
    f.close();
    return N_grids, N_dirs, N_freqs, RegressionModeldata_;

def get_groups_hdf5(file_dir, file_name):
    '''
    Function returns a list with all the group_keys in the hdf5 file.
    
    :param file_dir:
    :param file_name:
    '''
    if file_name[-5::] != '.hdf5':
        file_name = file_name + '.hdf5';
    f = h5py.File ( file_dir + os.sep + file_name, 'r');
    # Get group keys:
    group_keys = [];
    dataset_info = f.items();
    for i_ in range ( len(dataset_info)):
        if isinstance(dataset_info[i_][1], h5py.Group): # .Dataset for datasets...
            group_keys.append(dataset_info[i_][0]);
    f.close();
    return group_keys;
 
def dump_interpolated_data_hdf5(file_dir, file_name, sim_data, interpolated_SRC_signals, NON_interpolated_signals, interpolated_REC_Only_signals, SRC_interpolation_intended_location_m, direction_list_, Receiver_final_locations_m_list):
    '''
    Function dumps an interpolated signal.
    
    :param file_dir: The base dir where the data is written.
    :param file_name: The file name (with or without .hdf5 extension).
    
    :param sim_data: Simulation data. An MPI_IO.SimData() object.
    
    :param interpolated_SRC_signals: The final interpolated signal (SOURCE + RECEIVER)! Size of (N_directions,sim_length)!
    :param NON_interpolated_signals: This is the most basic simulation: nor receiver nor source gets interpolated (this means SRC at, usually, [0,0,0]) and receiver at the voxel containing the continuous location.
    :param interpolated_REC_Only_signals: Here, the receiver is interpolated but not the source (this means receiver at the desired continuous location while source at, usually, [0,0,0]).
    
    :param interpolation_intended_location_m: The interpolation location!
    :param direction_list_: The directions after receiver interpolation.
    :param Receiver_final_locations_m_list: The final locations for receivers (after interpolation).
    '''
    if file_name[-5::] != '.hdf5':
        file_name = file_name + '.hdf5';
    f = h5py.File ( file_dir + os.sep + file_name, 'w');
    # Dump data:
    f.create_dataset('interpolated_SRC_signals', data = interpolated_SRC_signals);
    f.create_dataset('NON_interpolated_signals', data = NON_interpolated_signals);
    f.create_dataset('interpolated_REC_Only_signals', data = interpolated_REC_Only_signals);
    
    f.create_dataset('SRC_interpolation_intended_location_m', data = SRC_interpolation_intended_location_m);
    f.create_dataset('direction_list_', data = direction_list_);
    f.create_dataset('Receiver_final_locations_m_list', data = Receiver_final_locations_m_list);
    m_sim_data_grp = f.create_group("sim_data");
    m_sim_data_grp.create_dataset("c_sound", data = sim_data.c_sound );
    m_sim_data_grp.create_dataset("fs", data = sim_data.fs );
    m_sim_data_grp.create_dataset("dX", data = sim_data.dX );
    m_sim_data_grp.create_dataset("lambda_sim", data = sim_data.lambda_sim );
    m_sim_data_grp.create_dataset("source_type", data = sim_data.source_type );
    m_sim_data_grp.create_dataset("no_of_receivers_per_direction", data = sim_data.no_of_receivers_per_direction );
    m_sim_data_grp.create_dataset("voxelization_type_string", data = sim_data.voxelization_type_string );
    m_sim_data_grp.create_dataset("n_receiver_directions", data = sim_data.n_receiver_directions );
    m_sim_data_grp.create_dataset("N_samples", data = sim_data.N_samples );
    m_sim_data_grp.create_dataset("use_double", data = sim_data.use_double );
    m_sim_data_grp.create_dataset("reciprocity", data = sim_data.reciprocity );
    # Close the file:
    f.close();
    
def read_dumped_interpolated_data(file_dir, file_name):
    '''
    Function read dumped data for a specific grid (as dumped by dump_interpolated_data_hdf5() function - see that function for all the returned data).
      
    :param file_dir: The base dir where the .hdf5 file resides.
    :param file_name: The file name to be read (with or without .hdf5 extension).
    '''
    if file_name[-5::] != '.hdf5':
        file_name = file_name + '.hdf5';
    f = h5py.File ( file_dir + os.sep + file_name, 'r');
    # Get group keys:
    group_keys = [];
    dataset_info = f.items();
    for i_ in range ( len(dataset_info)):
        if isinstance(dataset_info[i_][1], h5py.Group): # .Dataset for datasets...
            group_keys.append(dataset_info[i_][0]);
    
    interpolated_SRC_signals = f['interpolated_SRC_signals'][...];
    NON_interpolated_signals = f['NON_interpolated_signals'][...];
    interpolated_REC_Only_signals = f['interpolated_REC_Only_signals'][...];
    
    SRC_interpolation_intended_location_m = f['SRC_interpolation_intended_location_m'][...];
    direction_list_array = f['direction_list_'][...];
    # Build a direction list instead of numpy array:
    direction_list_ = [];
    for idx_ in range(len(direction_list_array)):
        direction_list_.append( (direction_list_array[idx_][0],direction_list_array[idx_][1]) );
        
    Receiver_final_locations_m_list = f['Receiver_final_locations_m_list'][...];
    
    sim_data = MPI_IO.SimData();
    if "sim_data" in group_keys:
        sim_data.c_sound = numpy.asscalar(f['sim_data/c_sound'][...]);
        sim_data.fs = numpy.asscalar(f['sim_data/fs'][...]);
        sim_data.dX = numpy.asscalar(f['sim_data/dX'][...]);
        sim_data.lambda_sim = numpy.asscalar(f['sim_data/lambda_sim'][...]);
        sim_data.source_type = f['sim_data/source_type'][...];
        sim_data.no_of_receivers_per_direction = numpy.asscalar(f['sim_data/no_of_receivers_per_direction'][...]);
        sim_data.voxelization_type_string = f['sim_data/voxelization_type_string'][...];
        sim_data.n_receiver_directions = numpy.asscalar(f['sim_data/n_receiver_directions'][...]);
        sim_data.N_samples = numpy.asscalar(f['sim_data/N_samples'][...]);
        sim_data.use_double = numpy.asscalar(f['sim_data/use_double'][...]);
        sim_data.reciprocity = numpy.asscalar(f['sim_data/reciprocity'][...]);
    # Close the file:
    f.close();
    return interpolated_SRC_signals, NON_interpolated_signals, interpolated_REC_Only_signals, SRC_interpolation_intended_location_m, direction_list_, Receiver_final_locations_m_list, sim_data

def dump_PRTF_data_ALL_grids(file_dir, file_name,\
                             interpolate_on_grid, grids_number_list, grids_fs_list, Receiver_final_locations_m_list_BLOCKED, Receiver_final_locations_m_list_FREEFIELD, final_receiver_direction_list, folder_list, final_location_source_BLOCKED_m, final_location_source_FREEFIELD_m, sim_data_list, \
                             PRTF_SRC_REC_interpolated_grid_list, PRTF_NON_interpolated_grid_list, PRTF_interpolated_RecOnly_grid_list, gaussian_window_list, hdf5_modified_time_BLOCKED, hdf5_modified_time_FREEFIELD):
    '''
    Function dumps PRTFs for all grids.
    
    :param file_dir: The directory where the .hdf5 will be written.
    :param file_name: The file name of the .hdf5 file to be written.
    
    :param interpolate_on_grid: int containing the grid number (a convention based on filenames) where interpolation takes place.
    :param grids_number_list: (N_grids) A list containing grid numbers (convention) coming from, e.g., read_grid_data_Interp_SRC_REC() function.
    :param grids_fs_list: (N_grids) A list containing the sampling frequencies in Hz for each grid. Comes from, e.g., read_grid_data_Interp_SRC_REC() function.
    
    :param Receiver_final_locations_m_list_BLOCKED: A list of size (N_dir) containing arrays of size 3 which give the location in m for each receiver at each direction for BLOCKED simulations (e.g., [REC_m_DIR_0_x, REC_m_DIR_0_y, REC_m_DIR_0_z]).
    :param Receiver_final_locations_m_list_FREEFIELD: A list of size (N_dir) containing arrays of size 3 which give the location in m for each receiver at each direction for BLOCKED simulations (e.g., [REC_m_DIR_0_x, REC_m_DIR_0_y, REC_m_DIR_0_z]).
    :param final_receiver_direction_list: (N_dirs) A list containing direction (az, el) with the receiver directions. Comes from, e.g., read_grid_data_Interp_SRC_REC() function.
    
    :param folder_list: A list containing the corresponding directories for each grid number/fs where the data was read.
    
    :ret final_location_source_BLOCKED_m: Array of size 3 contianing the final location in m in Cartesian coordinates (array of the form [SRC_m_x, SRC_m_y, SRC_m_z]) where the source was interpolated for BLOCKED simulation(s).
    :ret final_location_source_FREEFIELD_m: Array of size 3 contianing the final location in m in Cartesian coordinates (array of the form [SRC_m_x, SRC_m_y, SRC_m_z]) where the source was interpolated for FREE-FIELD simulation(s).
    
    :param sim_data_list: A list of size N_grids containing MPI_IO.SimData() objects with simulation-related data for each grid.
    
    :param PRTF_SRC_REC_interpolated_grid_list: A list of size N_grids  containing time response PRTFs for all grids with SRC-REC interpolated data (both BLOCKED and FREE-FIELD). If gaussian_window_list_BLOCKED is not a list of None, then these windows are applied.
    :param PRTF_NON_interpolated_grid_list: A list of size N_grids containing time response PRTFs for all grids without any interpolation (both BLOCKED and FREE-FIELD). If gaussian_window_list_BLOCKED is not a list of None, then these windows are applied.
    :param PRTF_interpolated_RecOnly_grid_list: A list of size N_grids containing time response PRTFs for all grids with only RECeiver interpolated data (both BLOCKED and FREE-FIELD; src is at the closest voxel next to the location to be interpolated). If gaussian_window_list_BLOCKED is not a list of None, then these windows are applied.
    
    :param gaussian_window_list: A list of size N_grids containing the (Gaussian) windows applied to both BLOCKED and FREE-FIELD FDTD simulations. If no windows are applied, this will be a list of None objects.
    
    :param hdf5_modified_time_BLOCKED: An array of size N_grids containing the modified time of the BLOCKED .hdf5 files used when computing the PRTFs. Use time.ctime() to display this in a good format.
    :param hdf5_modified_time_FREEFIELD: An array of size N_grids containing the modified time of the FREEFIELD .hdf5 files used when computing the PRTFs. Use time.ctime() to display this in a good format.
    '''
    if file_name[-5::] != '.hdf5':
        file_name = file_name + '.hdf5';
    f = h5py.File ( file_dir + os.sep + file_name, 'w');
    
    # Write multi-grid data:
    f.create_dataset('interpolate_on_grid', data = interpolate_on_grid);
    f.create_dataset('grids_number_list', data = grids_number_list);
    f.create_dataset('grids_fs_list', data = grids_fs_list);
    f.create_dataset('Receiver_final_locations_m_list_BLOCKED', data = Receiver_final_locations_m_list_BLOCKED);
    f.create_dataset('Receiver_final_locations_m_list_FREEFIELD', data = Receiver_final_locations_m_list_FREEFIELD);
    f.create_dataset('final_receiver_direction_list', data = final_receiver_direction_list);
    f.create_dataset('folder_list', data = folder_list);
    f.create_dataset('final_location_source_BLOCKED_m', data = final_location_source_BLOCKED_m);
    f.create_dataset('final_location_source_FREEFIELD_m', data = final_location_source_FREEFIELD_m);
    # hdf5 data:
    f.create_dataset('hdf5_modified_time_BLOCKED', data = hdf5_modified_time_BLOCKED);
    f.create_dataset('hdf5_modified_time_FREEFIELD', data = hdf5_modified_time_FREEFIELD);
    
    # Now write grid-specific data:
    for grid_idx in range(len(grids_number_list)):
        m_grid_data_grp = f.create_group("G" + str(grids_number_list[grid_idx]));
        m_grid_data_grp.create_dataset("PRTF_SRC_REC_interpolated_grid", data = PRTF_SRC_REC_interpolated_grid_list[grid_idx] );
        m_grid_data_grp.create_dataset("PRTF_NON_interpolated_grid", data = PRTF_NON_interpolated_grid_list[grid_idx] );
        m_grid_data_grp.create_dataset("PRTF_interpolated_RecOnly_grid", data = PRTF_interpolated_RecOnly_grid_list[grid_idx] );
        if gaussian_window_list[grid_idx] is None:
            m_grid_data_grp.create_dataset("gaussian_window", data = 'None' );
        else:
            m_grid_data_grp.create_dataset("gaussian_window", data = gaussian_window_list[grid_idx] );
        m_sim_data_grp = m_grid_data_grp.create_group("sim_data");
        m_sim_data_grp.create_dataset("c_sound", data = sim_data_list[grid_idx].c_sound );
        m_sim_data_grp.create_dataset("fs", data = sim_data_list[grid_idx].fs );
        m_sim_data_grp.create_dataset("dX", data = sim_data_list[grid_idx].dX );
        m_sim_data_grp.create_dataset("lambda_sim", data = sim_data_list[grid_idx].lambda_sim );
        m_sim_data_grp.create_dataset("source_type", data = sim_data_list[grid_idx].source_type );
        m_sim_data_grp.create_dataset("no_of_receivers_per_direction", data = sim_data_list[grid_idx].no_of_receivers_per_direction );
        m_sim_data_grp.create_dataset("voxelization_type_string", data = sim_data_list[grid_idx].voxelization_type_string );
        m_sim_data_grp.create_dataset("n_receiver_directions", data = sim_data_list[grid_idx].n_receiver_directions );
        m_sim_data_grp.create_dataset("N_samples", data = sim_data_list[grid_idx].N_samples );
        m_sim_data_grp.create_dataset("use_double", data = sim_data_list[grid_idx].use_double );
        m_sim_data_grp.create_dataset("reciprocity", data = sim_data_list[grid_idx].reciprocity );
    # Close the file:
    f.close();

def read_PRTF_data_ALL_grids(file_dir, file_name):
    '''
    Function read dumped PRTF data for all grids (as dumped by dump_PRTF_data_ALL_grids() function - see that function for all the returned data).
      
    :param file_dir: The base dir where the .hdf5 file resides.
    :param file_name: The file name to be read (with or without .hdf5 extension).
    '''
    if file_name[-5::] != '.hdf5':
        file_name = file_name + '.hdf5';
    f = h5py.File ( file_dir + os.sep + file_name, 'r');
    
    PRTF_SRC_REC_interpolated_grid_list = [];
    PRTF_NON_interpolated_grid_list = [];
    PRTF_interpolated_RecOnly_grid_list = [];
    gaussian_window_list = [];
    sim_data_list = [];
    
    # Read multi-grid data:
    interpolate_on_grid = int(f['interpolate_on_grid'][...]);
    grids_number_list = (f['grids_number_list'][...]).tolist();
    grids_fs_list = (f['grids_fs_list'][...]).tolist();
    Receiver_final_locations_m_list_BLOCKED = f['Receiver_final_locations_m_list_BLOCKED'][...];
    Receiver_final_locations_m_list_FREEFIELD = f['Receiver_final_locations_m_list_FREEFIELD'][...];
    final_receiver_direction_list = (f['final_receiver_direction_list'][...]).tolist();
    folder_list = (f['folder_list'][...]).tolist();
    final_location_source_BLOCKED_m = f['final_location_source_BLOCKED_m'][...];
    final_location_source_FREEFIELD_m = f['final_location_source_FREEFIELD_m'][...];
    # hdf5 data:
    hdf5_modified_time_BLOCKED = f['hdf5_modified_time_BLOCKED'][...];
    hdf5_modified_time_FREEFIELD = f['hdf5_modified_time_FREEFIELD'][...];
    
    # Get group keys:
    group_keys = [];
    dataset_info = f.items();
    for i_ in range ( len(dataset_info)):
        if isinstance(dataset_info[i_][1], h5py.Group): # .Dataset for datasets...
            group_keys.append(dataset_info[i_][0]);
    idx_ = 0;
    for group_key in group_keys:
        PRTF_SRC_REC_interpolated_grid_list.append( f[str(group_key) + '/PRTF_SRC_REC_interpolated_grid'][...] );
        PRTF_NON_interpolated_grid_list.append( f[str(group_key) + '/PRTF_NON_interpolated_grid'][...] );
        PRTF_interpolated_RecOnly_grid_list.append( f[str(group_key) + '/PRTF_interpolated_RecOnly_grid'][...] );
        gaussian_window_list.append( f[str(group_key) + '/gaussian_window'][...] );
        if str(gaussian_window_list[idx_]) == 'None':
            gaussian_window_list[idx_] == None;
        if re.match(r'G\d*', group_key):
            # Get sub-group keys:
            subgroup_keys = [];
            dataset_info = f[str(group_key) + "/"].items();
            for i_ in range ( len(dataset_info)):
                if isinstance(dataset_info[i_][1], h5py.Group): # .Dataset for datasets...
                    subgroup_keys.append(dataset_info[i_][0]);
            sim_data = MPI_IO.SimData();
            if "sim_data" in subgroup_keys:
                sim_data.c_sound = numpy.asscalar(f[str(group_key) + "/" + 'sim_data/c_sound'][...]);
                sim_data.fs = numpy.asscalar(f[str(group_key) + "/" + 'sim_data/fs'][...]);
                sim_data.dX = numpy.asscalar(f[str(group_key) + "/" + 'sim_data/dX'][...]);
                sim_data.lambda_sim = numpy.asscalar(f[str(group_key) + "/" + 'sim_data/lambda_sim'][...]);
                sim_data.source_type = f[str(group_key) + "/" + 'sim_data/source_type'][...];
                sim_data.no_of_receivers_per_direction = numpy.asscalar(f[str(group_key) + "/" + 'sim_data/no_of_receivers_per_direction'][...]);
                sim_data.voxelization_type_string = f[str(group_key) + "/" + 'sim_data/voxelization_type_string'][...];
                sim_data.n_receiver_directions = numpy.asscalar(f[str(group_key) + "/" + 'sim_data/n_receiver_directions'][...]);
                sim_data.N_samples = numpy.asscalar(f[str(group_key) + "/" + 'sim_data/N_samples'][...]);
                sim_data.use_double = numpy.asscalar(f[str(group_key) + "/" + 'sim_data/use_double'][...]);
                sim_data.reciprocity = numpy.asscalar(f[str(group_key) + "/" + 'sim_data/reciprocity'][...]);
                #print numpy.asscalar(f[str(group_key) + "/" + 'sim_data/fs'][...]);
            sim_data_list.append(sim_data);
        else:
            warnings.warn("Group key '" + str(group_key) + "' not expected in file " + str(file_name) + "!");        
        idx_ = idx_ + 1;

    return interpolate_on_grid, grids_number_list, grids_fs_list, Receiver_final_locations_m_list_BLOCKED, Receiver_final_locations_m_list_FREEFIELD, \
        final_receiver_direction_list, folder_list, final_location_source_BLOCKED_m, final_location_source_FREEFIELD_m, sim_data_list, \
            PRTF_SRC_REC_interpolated_grid_list, PRTF_NON_interpolated_grid_list, PRTF_interpolated_RecOnly_grid_list, gaussian_window_list, \
                hdf5_modified_time_BLOCKED, hdf5_modified_time_FREEFIELD;
    
    f.close();