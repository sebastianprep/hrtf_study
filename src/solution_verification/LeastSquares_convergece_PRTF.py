'''
This file is released under the MIT License. See
http://www.opensource.org/licenses/mit-license.php

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Created on Oct 10, 2018

    This module will be based on convergence_PRTF.py (NOT GIVEN in this repo!) module and will analyze the convergence on more than 3 grids.

    References:
        [Roy 2005] Roy C.J., "Review of code and solution verification procedures for computational simulation", Journal of Computational Physics, 205(1), p131-156, (2005)
        
        [518] Andrews D.W.K., Buchinsky M., "On the number of boostrap repetitions for BC_a  confidence intervals", Econometric Theory, 18, pp.962-984, (2002)
        
        [347] Eca L., Hoekstra M., "A procedure for the estimation of the numerical uncertainty of CFD calculations based on grid refinement studies", Journal of Compuational Physics 262, p104-130, (2014)

@author: Sebastian Prepelita
'''
import os
import re
import numpy
import time
import PRTF_interpolation
import scipy.fftpack
import scipy.linalg
import warnings
from scipy import stats
import math
import convergence_IO
import HRTF_utils
import statsmodels.api as sm
from statsmodels.stats.outliers_influence import summary_table
import scikits.bootstrap as bootstrap
import scipy.optimize
# Plotting:
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
import matplotlib.colors as colors
import matplotlib.cm as cmx
import matplotlib as mpl
#mpl.style.use('classic') # Switch back to classic
# Only change the grid:
mpl.rcParams['grid.color'] = 'k'
mpl.rcParams['grid.linestyle'] = ':'
mpl.rcParams['grid.linewidth'] = 0.5

print_colors = PRTF_interpolation.AnsiColors();

class RegressionModel:
    '''
    Class stores data from a regression model. This is because scikits.bootstrap does not pass other arguments to the function so a
    global variable needs to be defined.
    '''
    def __init__(self):
        self.X_ = numpy.float64( [[0.0]] );                     # The X matrix (exogen array).
        self.beta = numpy.float64( [0.0] );                     # The linear regression model parameters.
        self.residuals = numpy.float64( [0.0] );                # The residuals.
        self.weights = numpy.float64( [0.0] );                  # The weights - in case WLS is used.

def get_N_end(MPI_fs):
    '''
    Function returns the number of samples (to truncate the simulated responses) necessary to achieve the desired frequency resolution. 
    
    For the moment, the function is manual - it can be based on Simulations/Simulate_3D_FDTD.py and HRTF/CIPIC_KEMAR.py.match_sample_frequency
    
    :param MPI_fs: The sampling frequency of the simulation.
    '''
    if MPI_fs == 629760:
        return 3936;
    elif MPI_fs == 692800:
        return 4330;
    elif MPI_fs == 762080:
        return 4763;
    elif MPI_fs == 838240:
        return 5239;
    elif MPI_fs == 922080:
        return 5763;
    elif MPI_fs == 1014240:
        return 6339;
    elif MPI_fs == 1115680:
        return 6973;
    elif MPI_fs == 1227200:
        return 7670;
    else:
        raise NameError("Sampling frequency " + str(MPI_fs) + " not supported!");

def read_grid_data_Interp_SRC_REC( LS_root_directory ):
    '''
    Function reads all the necessary data and interpolates the Receiver and source. Note the function will check for the existence of a dumped .hdf5 file
    which will be created after reading to speed up the reading process (similar to convergence_PRTF.interpolate_Receiver_responses() function).
    
    Function checks consistency between grids!
    
    Function reads data for ALL grids (no filtering!) found and interpolates data on a grid based on the following naming conventions:
    
        DIRECTOR NAMING CONVENTIONS:
            DIR_1) data for each grid should be within a directory inside root_directory and start with G[%grid_no%]_[%fs_Hz%]Hz followed by "_Interpolated" if the result will be
            interpolated or followed by nothing if the interpolation should be done on that grid.
                Where "grid_no" is grid number (simply a convention) and "fs_Hz" is the final sampling frequency in kHz.
                
                ! "%fs_Hz%" will be used to match the .hdf5 files within subdirectories so it is important !
            
            DIR_2) only one directory withouth the "_Interpolated" suffix must exist! An error will be thrown!
            
            SUBDIR_1) Within each grid directory, if directory has an "_Interplated" suffix, then 8 directories for BLOCKED and 8 directories for FREE FIELD are expected 
            in the following format:
                    "BLOCKED [%0-7%]_" followed by some source displacement vector from default source location (e.g.,[0,0,0] means no displacement while [+1,+1,+1] means a diagonal displacement)
                    "FREE FIELD [%0-7%]_" again followed by source displacement vector from default source location
                    
                files_1) Within each subdirectory, .hdf5 data from an MPI simulations are expected. The files should be in the form "R[%mpi_rank%]_[%node_name%]_rec_data_[%fs_Hz%]_[LEFT or CENTER]_..."
    
    :param LS_root_directory: The root directory where the data (for all grids, BLOCKED and FREEFIELD) resides.
    
    :returns:
        :ret interpolate_on_grid: The grid number on which interpolation is done (grid number is a convention encoded in the folder names).
        :ret grids_number_list: A list containing the grid numbers giving also the arrangement of the data in the list. Use this to retrieve simulations for a particular grid (grid number is a convention encoded in the folder names)..
        :ret grids_fs_list: A list containing the sampling frequencies fs in Hz giving also the arrangement of the data in the list. Use this to retrieve simulations for a particular sampling frequency.
        :ret folder_list: A list containing the corresponding directories for each grid number/fs where the data was read.
        
        :ret final_location_source_BLOCKED_m: Array of size N_dir contianing the final location in m in Cartesian coordinates (array of the form [SRC_m_x, SRC_m_y, SRC_m_z]) where the source was interpolated for BLOCKED simulation(s).
        :ret final_location_source_FREEFIELD_m: Array of size N_dir contianing the final location in m in Cartesian coordinates (array of the form [SRC_m_x, SRC_m_y, SRC_m_z]) where the source was interpolated for FREE-FIELD simulation(s).
        
        :ret Receiver_final_locations_m_list_BLOCKED_master: A list of size (N_dir) containing arrays of size 3 which give the location in m for each receiver at each direction for BLOCKED simulations (e.g., [REC_m_DIR_0_x, REC_m_DIR_0_y, REC_m_DIR_0_z]). 
        :ret Receiver_final_locations_m_list_FREEFIELD_master: A list of size (N_dir) containing arrays of size 3 which give the location in m for each receiver at each direction for BLOCKED simulations (e.g., [REC_m_DIR_0_x, REC_m_DIR_0_y, REC_m_DIR_0_z]).
        :ret final_receiver_direction_list: (N_dir) A list containing tuples with the receiver directions (az, el) for the simulation data (both BLOCKED and FREE-FIELD - checked inside). 
                            Simulation data for each grid contains responses for all these directions. Order will be consistent among grids!
    
        :ret interpolated_SRC_signals_BLOCKED_list: List containing SRC-REC INTERPOLATED responses (N_dir x N_samples_grid) for BLOCKED simulation corresponding to each grid in grids_number_list returned variable.
        :ret interpolated_SRC_signals_FREEFIELD_list: List containing SRC-REC INTERPOLATED responses (N_dir x N_samples_grid) for FREEFIELD simulation corresponding to each grid in grids_number_list returned variable.
        
        :ret non_interpolated_signal_BLOCKED_list: List containing responses with no interpolation at all (N_dir x N_samples_grid) for BLOCKED simulation corresponding to each grid in grids_number_list returned variable.
        :ret non_interpolated_signal_FREEFIELD_list: List containing responses with no interpolation at all (N_dir x N_samples_grid) for FREEFIELD simulation corresponding to each grid in grids_number_list returned variable.
        
        :ret interpolated_signal_REC_Only_BLOCKED_list: List containing responses with REC-only interpolation (SRC is at SRC_0 location) of size (N_dir x N_samples_grid) for BLOCKED simulation corresponding to each grid in grids_number_list returned variable.
        :ret interpolated_signal_REC_Only_FREEFIELD_list: List containing responses with REC-only interpolation (SRC is at SRC_0 location) of size (N_dir x N_samples_grid) for FREEFIELD simulation corresponding to each grid in grids_number_list returned variable.
                    Note N_dir is consistent among grids while N_samples_grid depends on grid.
    
        :ret hdf5_modified_time_BLOCKED: Numpy array of size N_grids contains the modified time (as float timestamp - use time.ctime() to print it nicely) for the BLOCKED hdf5 files read/written. 
        :ret hdf5_modified_time_FREEFIELD: Numpy array of size N_grids contains the modified time (as float timestamp - use time.ctime() to print it nicely) for the FREEFIELD hdf5 files read/written.
    '''
    folders_reading_dir = os.listdir(LS_root_directory);
    # Filter to convention DIR_1):
    folders_reading_dir = [f for f in folders_reading_dir if re.match(r'G\d*_\d*Hz.*', f)];
    
    grids_number_list = [];
    grids_fs_list = [];
    folder_list = [];
    interpolate_on_grid = -1;
    for folder_ in folders_reading_dir:
        # Ignore files:
        path_ = os.path.join(LS_root_directory, folder_);
        if os.path.isdir(path_):
            folder_list.append(folder_);
            # Get grid number:
            end_grid = re.search(r'_\d*Hz', folder_).start();
            grid_no = int(folder_[1:end_grid]);
            if grid_no<0:
                raise NameError("Grid number cannot be negative!");
            grids_number_list.append(grid_no);
            # Get sampling frequency:
            end_Hz = re.search(r'Hz', folder_).start();
            MPI_fs = int(folder_[end_grid+1:end_Hz]);
            grids_fs_list.append(MPI_fs);
            # Check if interpolation:
            start_interp = re.search(r'_Interpolated', folder_); #start_interp.start()
            if start_interp is None:
                if interpolate_on_grid != -1:
                    raise NameError("More than on grids is 'non-interpolated' (grid " + str(interpolate_on_grid) + " and now gid " + str(grid_no) + ")! Convention is that only one satisifies this!");
                interpolate_on_grid = grid_no;
            #print str(grid_no) + " -> " + str(MPI_fs) + " " + (" --> Interpolate here!" if grid_no == interpolate_on_grid else "")
    # Check that the interoplate_on_location is done:
    if interpolate_on_grid == -1:
        raise NameError("No grid to interpolate on was found!");
    # Sanity checks:
    if len(grids_number_list) != len(grids_fs_list) or len(grids_number_list) != len(folder_list):
        raise NameError("The resulting lists have different lenghts! Somehting went wrong!");
    #################################################################################################
    # Done with first round of directories. Now reading the actual data for each grid number.
    #### Allocating dummy lists 
    ## Note each element will contain a list with all the locations!
    ##################################################################################
    # Full interpolated signals:
    interpolated_SRC_signals_BLOCKED_list = [None]*len(grids_number_list);  # list of (direction) lists
    interpolated_SRC_signals_FREEFIELD_list = [None]*len(grids_number_list);
    # Completely non-interpolated signals (SRC and REC are located in the voxel containing the continous location or closest to the interpolation location)
    non_interpolated_signal_BLOCKED_list = [None]*len(grids_number_list);   # list of (direction) lists
    non_interpolated_signal_FREEFIELD_list = [None]*len(grids_number_list);
    # Only receivers interpolated:
    interpolated_signal_REC_Only_BLOCKED_list = [None]*len(grids_number_list);   # list of (direction) lists
    interpolated_signal_REC_Only_FREEFIELD_list = [None]*len(grids_number_list);
    # Simulation useful:
    sim_data_BLOCKED_list = [None]*len(grids_number_list);
    sim_data_FREEFIELD_list = [None]*len(grids_number_list);
    hdf5_modified_time_BLOCKED = numpy.zeros( len(grids_number_list) );
    hdf5_modified_time_FREEFIELD = numpy.zeros( len(grids_number_list) );
    #===========================================================================
    # First read the grid giving interpolation coordinates:
    #===========================================================================
    print "" + print_colors.FG_CYAN + "Reading " + print_colors.BG_MAGENTA  +"Grid " + str(interpolate_on_grid) + print_colors.END + print_colors.FG_CYAN + " on which interpolation will be done!" + print_colors.END + ""
    # Get data:
    idx_toInterp_grid = grids_number_list.index(interpolate_on_grid);
    grid_no = grids_number_list[idx_toInterp_grid];
    MPI_fs = grids_fs_list[idx_toInterp_grid];
    folder_name = folder_list[idx_toInterp_grid];
    N_end = get_N_end(MPI_fs = MPI_fs);
    # Read original Grid - BLOCKED:
    ####################################
    interpolated_SRC_signals_BLOCKED, non_interpolated_signal_BLOCKED, interpolated_signal_REC_Only_BLOCKED, \
        direction_list_BLOCKED, Receiver_final_locations_m_list_BLOCKED, sim_data_BLOCKED, SRC_interpolation_intended_location_BLOCKED_m, \
            displace_src_vox_default, _, hdf5_BLOCKED_file_modified_TS = read_SRC_REC_single_grid_data_BLOCKED(LS_root_directory = LS_root_directory, single_Grid_folder_name = folder_name, 
                                                                                         grid_no = grid_no, interpolate_on_grid_no = interpolate_on_grid,
                                                                                         MPI_fs = MPI_fs, N_end_ = N_end, 
                                                                                         interpolate_source_flag = False, interpolation_intended_location_m = None);
    # Assign data:
    interpolated_SRC_signals_BLOCKED_list[idx_toInterp_grid] = interpolated_SRC_signals_BLOCKED;
    non_interpolated_signal_BLOCKED_list[idx_toInterp_grid] = non_interpolated_signal_BLOCKED;
    interpolated_signal_REC_Only_BLOCKED_list[idx_toInterp_grid] = interpolated_signal_REC_Only_BLOCKED;
    sim_data_BLOCKED_list[idx_toInterp_grid] = sim_data_BLOCKED;
    hdf5_modified_time_BLOCKED[idx_toInterp_grid] = hdf5_BLOCKED_file_modified_TS;
    direction_list_BLOCKED_master = direction_list_BLOCKED;
    Receiver_final_locations_m_list_BLOCKED_master = Receiver_final_locations_m_list_BLOCKED;
    final_location_source_BLOCKED = SRC_interpolation_intended_location_BLOCKED_m;
    # Read original Grid - FREE FIELD:
    ####################################
    interpolated_SRC_signals_FREEFIELD, non_interpolated_signal_FREEFIELD, interpolated_signal_REC_Only_FREEFIELD, \
        direction_list_FREEFIELD, Receiver_final_locations_m_list_FREEFIELD, sim_data_FREEFIELD, SRC_interpolation_intended_location_FREEFIELD_m, \
            displace_src_vox_default, _, hdf5_FREEFIELD_file_modified_TS = read_SRC_REC_single_grid_data_FREEFIELD(LS_root_directory = LS_root_directory, single_Grid_folder_name = folder_name, 
                                                                                         grid_no = grid_no, interpolate_on_grid_no = interpolate_on_grid,
                                                                                         MPI_fs = MPI_fs, N_end_ = N_end, 
                                                                                         interpolate_source_flag = False, interpolation_intended_location_m = None);
    # Assign data:
    interpolated_SRC_signals_FREEFIELD_list[idx_toInterp_grid] = interpolated_SRC_signals_FREEFIELD;
    non_interpolated_signal_FREEFIELD_list[idx_toInterp_grid] = non_interpolated_signal_FREEFIELD;
    interpolated_signal_REC_Only_FREEFIELD_list[idx_toInterp_grid] = interpolated_signal_REC_Only_FREEFIELD;
    sim_data_FREEFIELD_list[idx_toInterp_grid] = sim_data_FREEFIELD;
    hdf5_modified_time_FREEFIELD[idx_toInterp_grid] = hdf5_FREEFIELD_file_modified_TS;
    direction_list_FREEFIELD_master = direction_list_FREEFIELD;
    # Check consistency of direction list between BLOCKED and FREE-FIELD:
    for dir_idx in range(len(direction_list_BLOCKED_master)):
        # Compare azimuth:
        if direction_list_BLOCKED_master[dir_idx][0] != direction_list_FREEFIELD_master[dir_idx][0]:
            print "BLOCKED direction [" + str(dir_idx) + "] = " + str(direction_list_BLOCKED_master[dir_idx]);
            print "FREEFIELD direction [" + str(dir_idx) + "] = " + str(direction_list_FREEFIELD_master[dir_idx]);
            raise NameError("Azimuth mismatch between BLOCKED and FREE field master direction lists!");
        if direction_list_BLOCKED_master[dir_idx][1] != direction_list_FREEFIELD_master[dir_idx][1]:
            print "BLOCKED direction [" + str(dir_idx) + "] = " + str(direction_list_BLOCKED_master[dir_idx]);
            print "FREEFIELD direction [" + str(dir_idx) + "] = " + str(direction_list_FREEFIELD_master[dir_idx]);
            raise NameError("ELEVATION mismatch between BLOCKED and FREE field master direction lists!");
    final_receiver_direction_list = direction_list_BLOCKED_master;
    Receiver_final_locations_m_list_FREEFIELD_master = Receiver_final_locations_m_list_FREEFIELD;
    final_location_source_FREEFIELD = SRC_interpolation_intended_location_FREEFIELD_m;
    #===========================================================================
    # Now the other grids and interpolate:
    #===========================================================================
    for grid_idx_ in range(len(grids_number_list)):
        # Get data:
        grid_no = grids_number_list[grid_idx_];
        MPI_fs = grids_fs_list[grid_idx_];
        folder_name = folder_list[grid_idx_];
        N_end = get_N_end(MPI_fs = MPI_fs);
        # Read only grids that need interpolation now:
        if grid_no != interpolate_on_grid:
            print "" + print_colors.FG_CYAN + "Reading " + print_colors.BG_MAGENTA  +"Grid " + str(grid_no) + print_colors.END + print_colors.FG_CYAN + " [fs = "+ str(MPI_fs) + " Hz, N_end = #" + str(N_end) + "] from dir \\" + str(folder_name) + "\\ to interpolate on " + print_colors.BG_WHITE + "G" + str(interpolate_on_grid) + "!" + print_colors.END + ""
            # Reading BLOCKED & Interpolate:
            ####################################
            interpolated_SRC_signals_BLOCKED, non_interpolated_signal_BLOCKED, interpolated_signal_REC_Only_BLOCKED, \
                direction_list_BLOCKED, Receiver_final_locations_m_list_BLOCKED, sim_data_BLOCKED, SRC_interpolation_intended_location_BLOCKED_m, \
                    displace_src_vox_default, _, hdf5_BLOCKED_file_modified_TS = read_SRC_REC_single_grid_data_BLOCKED(LS_root_directory = LS_root_directory, single_Grid_folder_name = folder_name, 
                                                                                                 grid_no = grid_no, interpolate_on_grid_no = interpolate_on_grid,
                                                                                                 MPI_fs = MPI_fs, N_end_ = N_end, 
                                                                                                 interpolate_source_flag = True, interpolation_intended_location_m = final_location_source_BLOCKED);
            print "\t\t" + "BLOCKED checks against GRID " + str(grids_number_list[idx_toInterp_grid]) + " [fs = " + str(grids_fs_list[idx_toInterp_grid]) + "]:"
            # Check Receiver consistency between grids:
            check_receiver_directions_and_intended_locations(direction_list = direction_list_BLOCKED_master, direction_list_ = direction_list_BLOCKED, 
                                                             intended_locations_m = Receiver_final_locations_m_list_BLOCKED_master, intended_locations_m_ = Receiver_final_locations_m_list_BLOCKED, 
                                                             file_name = "GRID " + str(grid_no) + " versus GRID " + str(interpolate_on_grid) + " (BLOCKED)");
            # Using deprecated code that does checks on 3 grids. Only 2 grids are enoug. Using: Grid G_current, G_current, G_original checks BLOCKED:
            check_consistency_grids(c_sound_G1 = sim_data_BLOCKED.c_sound, c_sound_G2 = sim_data_BLOCKED.c_sound, c_sound_G3 = sim_data_BLOCKED_list[idx_toInterp_grid].c_sound,
                                    lambda_sim_G1 = sim_data_BLOCKED.lambda_sim, lambda_sim_G2 = sim_data_BLOCKED.lambda_sim, lambda_sim_G3 = sim_data_BLOCKED_list[idx_toInterp_grid].lambda_sim,
                                    source_type_G1 = sim_data_BLOCKED.source_type, source_type_G2 = sim_data_BLOCKED.source_type, source_type_G3 = sim_data_BLOCKED_list[idx_toInterp_grid].source_type,
                                    no_of_receivers_per_direction_G1 = sim_data_BLOCKED.no_of_receivers_per_direction, no_of_receivers_per_direction_G2 = sim_data_BLOCKED.no_of_receivers_per_direction, no_of_receivers_per_direction_G3 = sim_data_BLOCKED_list[idx_toInterp_grid].no_of_receivers_per_direction, 
                                    voxelization_type_string_G1 = sim_data_BLOCKED.voxelization_type_string, voxelization_type_string_G2 = sim_data_BLOCKED.voxelization_type_string, voxelization_type_string_G3 = sim_data_BLOCKED_list[idx_toInterp_grid].voxelization_type_string,
                                    n_rec_G1 = sim_data_BLOCKED.n_receiver_directions, n_rec_G2 = sim_data_BLOCKED.n_receiver_directions, n_rec_G3 = sim_data_BLOCKED_list[idx_toInterp_grid].n_receiver_directions, fs_G1 = sim_data_BLOCKED.fs, fs_G2 = sim_data_BLOCKED.fs, fs_G3 = sim_data_BLOCKED_list[idx_toInterp_grid].fs, \
                                    N_s_G1 = sim_data_BLOCKED.N_samples, N_s_G2 = sim_data_BLOCKED.N_samples, N_s_G3 = sim_data_BLOCKED_list[idx_toInterp_grid].N_samples);
            # Assign data:
            interpolated_SRC_signals_BLOCKED_list[grid_idx_] = interpolated_SRC_signals_BLOCKED;
            non_interpolated_signal_BLOCKED_list[grid_idx_] = non_interpolated_signal_BLOCKED;
            interpolated_signal_REC_Only_BLOCKED_list[grid_idx_] = interpolated_signal_REC_Only_BLOCKED;
            sim_data_BLOCKED_list[grid_idx_] = sim_data_BLOCKED;
            hdf5_modified_time_BLOCKED[grid_idx_] = hdf5_BLOCKED_file_modified_TS;
            # Reading FREE-FIELD & Interpolate:
            ####################################
            interpolated_SRC_signals_FREEFIELD, non_interpolated_signal_FREEFIELD, interpolated_signal_REC_Only_FREEFIELD, \
                direction_list_FREEFIELD, Receiver_final_locations_m_list_FREEFIELD, sim_data_FREEFIELD, SRC_interpolation_intended_location_FREEFIELD_m, \
                    displace_src_vox_default, _, hdf5_FREEFIELD_file_modified_TS = read_SRC_REC_single_grid_data_FREEFIELD(LS_root_directory = LS_root_directory, single_Grid_folder_name = folder_name, 
                                                                                                 grid_no = grid_no, interpolate_on_grid_no = interpolate_on_grid,
                                                                                                 MPI_fs = MPI_fs, N_end_ = N_end, 
                                                                                                 interpolate_source_flag = True, interpolation_intended_location_m = final_location_source_FREEFIELD);
            print "\t\t" + "FREEFIELD checks against GRID " + str(grids_number_list[idx_toInterp_grid]) + " [fs = " + str(grids_fs_list[idx_toInterp_grid]) + "]:"
            # Check Receiver consistency between grids:
            check_receiver_directions_and_intended_locations(direction_list = direction_list_FREEFIELD_master, direction_list_ = direction_list_FREEFIELD, 
                                                             intended_locations_m = Receiver_final_locations_m_list_FREEFIELD_master, intended_locations_m_ = Receiver_final_locations_m_list_FREEFIELD, 
                                                             file_name = "GRID " + str(grid_no) + " versus GRID " + str(interpolate_on_grid) + " (FREEFIELD)");
            # Using deprecated code that does checks on 3 grids. Only 2 grids are enoug. Using: Grid G_current, G_current, G_original checks FREEFIELD:
            check_consistency_grids(c_sound_G1 = sim_data_FREEFIELD.c_sound, c_sound_G2 = sim_data_FREEFIELD.c_sound, c_sound_G3 = sim_data_FREEFIELD_list[idx_toInterp_grid].c_sound,
                                    lambda_sim_G1 = sim_data_FREEFIELD.lambda_sim, lambda_sim_G2 = sim_data_FREEFIELD.lambda_sim, lambda_sim_G3 = sim_data_FREEFIELD_list[idx_toInterp_grid].lambda_sim,
                                    source_type_G1 = sim_data_FREEFIELD.source_type, source_type_G2 = sim_data_FREEFIELD.source_type, source_type_G3 = sim_data_FREEFIELD_list[idx_toInterp_grid].source_type,
                                    no_of_receivers_per_direction_G1 = sim_data_FREEFIELD.no_of_receivers_per_direction, no_of_receivers_per_direction_G2 = sim_data_FREEFIELD.no_of_receivers_per_direction, no_of_receivers_per_direction_G3 = sim_data_FREEFIELD_list[idx_toInterp_grid].no_of_receivers_per_direction, 
                                    voxelization_type_string_G1 = sim_data_FREEFIELD.voxelization_type_string, voxelization_type_string_G2 = sim_data_FREEFIELD.voxelization_type_string, voxelization_type_string_G3 = sim_data_FREEFIELD_list[idx_toInterp_grid].voxelization_type_string,
                                    n_rec_G1 = sim_data_FREEFIELD.n_receiver_directions, n_rec_G2 = sim_data_FREEFIELD.n_receiver_directions, n_rec_G3 = sim_data_FREEFIELD_list[idx_toInterp_grid].n_receiver_directions, fs_G1 = sim_data_FREEFIELD.fs, fs_G2 = sim_data_FREEFIELD.fs, fs_G3 = sim_data_FREEFIELD_list[idx_toInterp_grid].fs, \
                                    N_s_G1 = sim_data_FREEFIELD.N_samples, N_s_G2 = sim_data_FREEFIELD.N_samples, N_s_G3 = sim_data_FREEFIELD_list[idx_toInterp_grid].N_samples);
            # Assign data:
            interpolated_SRC_signals_FREEFIELD_list[grid_idx_] = interpolated_SRC_signals_FREEFIELD;
            non_interpolated_signal_FREEFIELD_list[grid_idx_] = non_interpolated_signal_FREEFIELD;
            interpolated_signal_REC_Only_FREEFIELD_list[grid_idx_] = interpolated_signal_REC_Only_FREEFIELD;
            sim_data_FREEFIELD_list[grid_idx_] = sim_data_FREEFIELD;
            hdf5_modified_time_FREEFIELD[grid_idx_] = hdf5_FREEFIELD_file_modified_TS;
    #===========================================================================
    # Return data:
    #===========================================================================
    # Renames:
    final_location_source_BLOCKED_m = final_location_source_BLOCKED;
    final_location_source_FREEFIELD_m = final_location_source_FREEFIELD;
    return interpolate_on_grid, grids_number_list, grids_fs_list, folder_list, \
        final_location_source_BLOCKED_m, final_location_source_FREEFIELD_m, \
            Receiver_final_locations_m_list_BLOCKED_master, Receiver_final_locations_m_list_FREEFIELD_master, final_receiver_direction_list, \
                interpolated_SRC_signals_BLOCKED_list, interpolated_SRC_signals_FREEFIELD_list,\
                    non_interpolated_signal_BLOCKED_list, non_interpolated_signal_FREEFIELD_list,\
                        interpolated_signal_REC_Only_BLOCKED_list, interpolated_signal_REC_Only_FREEFIELD_list, \
                            sim_data_BLOCKED_list, sim_data_FREEFIELD_list, hdf5_modified_time_BLOCKED, hdf5_modified_time_FREEFIELD;

def read_SRC_REC_single_grid_data_BLOCKED( LS_root_directory, single_Grid_folder_name, grid_no, interpolate_on_grid_no, MPI_fs, N_end_, interpolate_source_flag = False, interpolation_intended_location_m = None ):
    '''
    Function goes through all the directories and reads/interpolates source/receivers for BLOCKED data (Pinna simulations). Receiver/source interpolation is trilinear.
    
    Highly similar to read_SRC_REC_single_grid_data_FREEFIELD().
    
    Function calls the source interpolation function which checks that the interpolation location is within the other source locations! 
    
    Function also searches for a .hdf5 file and reads or writes it if available (based on convergence_PRTF.interpolate_Receiver_responses() function ).
    
    :param LS_root_directory: The parent directory with directories with all the grids
    :param single_Grid_folder_name: The directory where responses for a grid are found (if interpolat_source_flag, then multiple results for different source locations are needed).
    :param grid_no: The grid index (just a convention used in printing/reporting).
    :param interpolate_on_grid_no: The grid index where interpolation happens (just a convention used in printing/reporting).
    :param MPI_fs: int. The sampling frequency for the current grid. Used in matching .hdf5 files!
    :param N_end_: int. Gives the number of samples for this grid (this is used to trim the IRs in order to achieve a certain frequency resolution).
    
    :param interpolate_source_flag: Boolean. If True, source interpolation is done. Otherwise only receiver interpolation is done (for, e.g., the grid on which interpolation is done).
    :param interpolation_intended_location_m: The location where source interpolation happens. Only matters when interpolat_source_flag is True.
    
    :returns:
        :ret interpolated_SRC_signals_BLOCKED: (N_dir x N_end_) List with the fully interpolated signal (SRC & Receiver) for each direction. If not interpolate_source_flag, then only receivers are interpolated so this will be identical to interpolated_signal_REC_Only_BLOCKED.
        :ret non_interpolated_signal_BLOCKED: (N_dir x N_end_) List with the raw signal where neither source or receiver is interpolated - for each direction.
        :ret interpolated_signal_REC_Only_BLOCKED: (N_dir x N_end_) List with the only receivers are interpolated, with source is at (displcement) location _0 (whatever that may be - usually, [0,0,0]) - for each direction.
         
        :ret direction_list_BLOCKED: (N_dir) The list of directions - you will get a signal for each direction in the previous three returned variables.
        :ret Receiver_final_locations_m_list_BLOCKED: (N_dir) A list containing numpy.arrays of size 3: the continuous Cartesian locations in [m] where each Receiver (for each direction in direction_list_BLOCKED) will be interpolated.
        
        :ret sim_data_BLOCKED: An MPI_IO.SimData() object containing data regarding the simulation.
        :ret SRC_interpolation_intended_location_BLOCKED_m: Contains the final location in m in Cartesian coordinates (array of the form [SRC_x, SRC_y, SRC_z]) where the source needs to be interpolated. If interpolate_source_flag is False, then the continuous location of the source in the current simulation is used.
        :ret displace_src_vox_default: A tuple containing the displacement for the idx 0 for source (e.g., (0,1,0) for a directory like "BLOCKED 0_[0,+1,0]").
        :ret found_hdf5_flag: Boolean. Lets you know if the function found a hdf5 file with the data.
    '''
    if interpolate_source_flag and (interpolation_intended_location_m is None or len(interpolation_intended_location_m) != 3):
        raise NameError("If interpolation is needed, parameter interpolation_intended_location_m cannot be None!");
    ###########################
    # Get source location list:
    src_folders_reading_dir = os.listdir(LS_root_directory + os.sep + single_Grid_folder_name);
    src_folders_reading_dir = [f for f in src_folders_reading_dir if re.match(r'BLOCKED \d*_.*[\+\-]*\d,[\+\-]*\d,[\+\-]*\d.', f)];
    src_list = [];
    full_path_src_list = [];
    src_idx_list = [];
    for folder_ in src_folders_reading_dir:
        # Ignore files:
        path_ = os.path.join(LS_root_directory, single_Grid_folder_name, folder_);
        if os.path.isdir(path_):
            start_src_loc = re.search(r'_.*', folder_).start();
            src_idx = int(folder_[8:start_src_loc]);
            src_idx_list.append(src_idx)
            src_list.append(folder_[start_src_loc+1::]);
            full_path_src_list.append( os.path.join(LS_root_directory, single_Grid_folder_name, folder_))
    # Checks:
    if len(src_list) != len(src_idx_list):
        raise NameError("Lists have different lengths - something is wrong.");
    if interpolate_source_flag:
        if len(src_list)!= 8:
            raise NameError("For source interpolation, we need 8 grids for different source location for trilinear interpolation!");
        for i_ in range(8):
            if i_ not in src_idx_list:
                raise NameError("Convention is BLOCKED [src_idx]_..., where src_idx must be 0,1,2,3,4,5,6,7 for trilinear interpolation to work! Could not find " + str(i_) + " src_idx!");
    # Get default source location:
    def_src_location = src_list[src_idx_list.index(0)];
    src_vox_x_default, src_vox_y_default, src_vox_z_default = def_src_location[1:-1].split(",");
    # To int:
    src_vox_x_default = int(src_vox_x_default); src_vox_y_default = int(src_vox_y_default); src_vox_z_default = int(src_vox_z_default);
    displace_src_vox_default = (src_vox_x_default, src_vox_y_default, src_vox_z_default);
    
    found_hdf5_flag = False;
    hdf5_BLOCKED_file_modified_TS = None; # Contains the modified time of the read .hdf5!
    # Check HDF5 for BLOCKED:
    hdf5_fn = single_Grid_folder_name + "_" + "BLOCKED_REC_Interpolated_data" + ".hdf5";
    if os.path.exists( LS_root_directory + os.sep + hdf5_fn):
        print "\t" + print_colors.FG_CYAN + "Found .hdf5 data GRID " + print_colors.BG_MAGENTA + str(grid_no) + print_colors.END + print_colors.FG_CYAN + " (BLOCKED): " + print_colors.FG_YELLOW + hdf5_fn + print_colors.END +  "";
        interpolated_SRC_signals_BLOCKED, non_interpolated_signal_BLOCKED, interpolated_signal_REC_Only_BLOCKED,\
            SRC_interpolation_intended_location_BLOCKED_m, direction_list_BLOCKED, \
                Receiver_final_locations_m_list_BLOCKED, sim_data_BLOCKED = convergence_IO.read_dumped_interpolated_data(file_dir = LS_root_directory, file_name = hdf5_fn);
        hdf5_BLOCKED_file_modified_TS = os.path.getmtime( os.path.join(LS_root_directory, hdf5_fn) );
        found_hdf5_flag = True;
    else:
        print "\t" + print_colors.FG_CYAN + "hdf5 not found -> Looping GRID " + print_colors.BG_MAGENTA + str(grid_no) + print_colors.END + print_colors.FG_CYAN + " (BLOCKED):" + print_colors.END + "";
        # !! Remember Receiver_interpolated_signal_BLOCKED will have shape of (N_src_directories, N_directions, N_samples)
        src_data_list_BLOCKED, direction_list_BLOCKED, Receiver_final_locations_m_list_BLOCKED,\
          Receiver_interpolated_signal_BLOCKED, Receiver_non_interpolated_signal_BLOCKED,\
            N_directories_BLOCKED, sim_data_BLOCKED =  interpolate_Receiver_responses(MPI_fs_ = MPI_fs, list_src_roots = full_path_src_list, 
                                                                                                        MPI_ear_ = "LEFT", verbose_flag = True, N_end = N_end_);
        # Get non-interpolated data:
        found_non_interp = False;
        for i_ in range(len(src_data_list_BLOCKED)):
            if numpy.array_equal(src_data_list_BLOCKED[i_].displace_source_by_vox, numpy.int32((src_vox_x_default,src_vox_y_default,src_vox_z_default))):
                if found_non_interp:
                    raise NameError("Duplicate non-interpolation location for G" + str(grid_no) + " BLOCKED!");
                non_interpolated_signal_BLOCKED = Receiver_non_interpolated_signal_BLOCKED[i_];
                interpolated_signal_REC_Only_BLOCKED = Receiver_interpolated_signal_BLOCKED[i_];
                found_non_interp = True;
        if not found_non_interp:
            raise NameError("Default non-interpolation location not found for G" + str(grid_no) + " BLOCKED!");
        # Interpolate source:
        if interpolate_source_flag:
            print "\t" + print_colors.FG_CYAN + print_colors.BG_MAGENTA + "G" + str(grid_no) + print_colors.END + print_colors.FG_CYAN + " SOURCE " + print_colors.BG_YELLOW + "Interpolation" +print_colors.END+print_colors.FG_CYAN+ " on " + print_colors.BG_YELLOW + "G" + str(interpolate_on_grid_no) +print_colors.END+print_colors.FG_CYAN+ " (BLOCKED):" + print_colors.END + ""
            # This one checks that the interpolation location is between the other locations!:
            interpolated_SRC_signals_BLOCKED = PRTF_interpolation.trilinear_interpolate_Source_time(sim_data = sim_data_BLOCKED, 
                                                                                                   src_data_list_in = src_data_list_BLOCKED, 
                                                                                                   sim_data_Int_Rec = Receiver_interpolated_signal_BLOCKED, 
                                                                                                   interpolation_intended_location_m = interpolation_intended_location_m, 
                                                                                                   verbose_flag_ = False, plot_interpolation_flag = False);
            print "\t   " + print_colors.FG_CYAN + "Interpolation done for GRID " + print_colors.BG_MAGENTA + str(grid_no) + print_colors.END + print_colors.FG_CYAN + " (BLOCKED):" + print_colors.END + "";
        else:
            print "\t   " + print_colors.FG_BLUE + " No SOURCE Interpolation for this (BLOCKED) grid.... "+ print_colors.END + "";
            interpolation_intended_location_m = src_data_list_BLOCKED[0].final_location_m; # Use this one for src interpolation continuous location!
            interpolated_SRC_signals_BLOCKED = numpy.copy(interpolated_signal_REC_Only_BLOCKED);
        SRC_interpolation_intended_location_BLOCKED_m = numpy.copy(interpolation_intended_location_m); # Different name for the same thing...
        
        print "\t" + print_colors.FG_CYAN + "dumping .hdf5 data GRID " + print_colors.BG_MAGENTA + str(grid_no) + print_colors.END + print_colors.FG_CYAN + " (BLOCKED):  " + print_colors.FG_YELLOW + hdf5_fn + print_colors.END +  "";    
        convergence_IO.dump_interpolated_data_hdf5(file_dir = LS_root_directory, file_name = hdf5_fn, 
                                                   sim_data = sim_data_BLOCKED,
                                                   interpolated_SRC_signals = interpolated_SRC_signals_BLOCKED,
                                                   NON_interpolated_signals = non_interpolated_signal_BLOCKED, 
                                                   interpolated_REC_Only_signals = interpolated_signal_REC_Only_BLOCKED, 
                                                   SRC_interpolation_intended_location_m = interpolation_intended_location_m, 
                                                   direction_list_ = direction_list_BLOCKED, 
                                                   Receiver_final_locations_m_list = Receiver_final_locations_m_list_BLOCKED);
        hdf5_BLOCKED_file_modified_TS = os.path.getmtime( os.path.join(LS_root_directory, hdf5_fn) ); # Get the modified time-stamp!
        print "\t" + print_colors.FG_CYAN + "   DONE." + print_colors.END + "";
    return interpolated_SRC_signals_BLOCKED, non_interpolated_signal_BLOCKED, interpolated_signal_REC_Only_BLOCKED, direction_list_BLOCKED, Receiver_final_locations_m_list_BLOCKED, sim_data_BLOCKED, SRC_interpolation_intended_location_BLOCKED_m, displace_src_vox_default, found_hdf5_flag, hdf5_BLOCKED_file_modified_TS;

def read_SRC_REC_single_grid_data_FREEFIELD( LS_root_directory, single_Grid_folder_name, grid_no, interpolate_on_grid_no, MPI_fs, N_end_, interpolate_source_flag = False, interpolation_intended_location_m = None ):
    '''
    Function goes through all the directories and reads/interpolates source/receivers for FREEFIELD data (Pinna simulations). Receiver/source interpolation is trilinear.
    
    Highly similar to read_SRC_REC_single_grid_data_BLOCKED().
    
    Function calls the source interpolation function which checks that the interpolation location is within the other source locations!
    
    Function also searches for a .hdf5 file and reads or writes it if available (based on convergence_PRTF.interpolate_Receiver_responses() function ).
    
    :param LS_root_directory: The parent directory with directories with all the grids
    :param single_Grid_folder_name: The directory where responses for a grid are found (if interpolat_source_flag, then multiple results for different source locations are needed).
    :param grid_no: The grid index (just a convention used in printing/reporting).
    :param interpolate_on_grid_no: The grid index where interpolation happens (just a convention used in printing/reporting).
    :param MPI_fs: int. The sampling frequency for the current grid. Used in matching .hdf5 files!
    :param N_end_: int. Gives the number of samples for this grid (this is used to trim the IRs in order to achieve a certain frequency resolution).
    
    :param interpolate_source_flag: Boolean. If True, source interpolation is done. Otherwise only receiver interpolation is done (for, e.g., the grid on which interpolation is done).
    :param interpolation_intended_location_m: The location where source interpolation happens. Only matters when interpolat_source_flag is True.
    
    :returns:
        :ret interpolated_SRC_signals_FREEFIELD: (N_dir x N_end_) List with the fully interpolated signal (SRC & Receiver) for each direction. If not interpolate_source_flag, then only receivers are interpolated so this will be identical to interpolated_signal_REC_Only_FREEFIELD.
        :ret non_interpolated_signal_FREEFIELD: (N_dir x N_end_) List with the raw signal where neither source or receiver is interpolated - for each direction.
        :ret interpolated_signal_REC_Only_FREEFIELD: (N_dir x N_end_) List with the only receivers are interpolated, with source is at (displcement) location _0 (whatever that may be - usually, [0,0,0]) - for each direction.
         
        :ret direction_list_FREEFIELD: (N_dir) The list of directions - you will get a signal for each direction in the previous three returned variables.
        :ret Receiver_final_locations_m_list_FREEFIELD: (N_dir) A list containing numpy.arrays of size 3: the continuous Cartesian locations in [m] where each Receiver (for each direction in direction_list_FREEFIELD) will be interpolated.
        
        :ret sim_data_FREEFIELD: An MPI_IO.SimData() object containing data regarding the simulation.
        :ret SRC_interpolation_intended_location_FREEFIELD_m: Contains the final location in m in Cartesian coordinates (array of the form [SRC_x, SRC_y, SRC_z]) where the source needs to be interpolated. If interpolate_source_flag is False, then the continuous location of the source in the current simulation is used.
        :ret displace_src_vox_default: A tuple containing the displacement for the idx 0 for source (e.g., (0,1,0) for a directory like "FREE FIELD 0_[0,+1,0]").
        :ret found_hdf5_flag: Boolean. Lets you know if the function found a hdf5 file with the data.
    '''
    if interpolate_source_flag and (interpolation_intended_location_m is None or len(interpolation_intended_location_m) != 3):
        raise NameError("If interpolation is needed, parameter interpolation_intended_location_m cannot be None!");
    ###########################
    # Get source location list:
    src_folders_reading_dir = os.listdir(LS_root_directory + os.sep + single_Grid_folder_name);
    src_folders_reading_dir = [f for f in src_folders_reading_dir if re.match(r'FREE FIELD \d*_.*[\+\-]*\d,[\+\-]*\d,[\+\-]*\d.', f)];
    src_list = [];
    full_path_src_list = [];
    src_idx_list = [];
    for folder_ in src_folders_reading_dir:
        # Ignore files:
        path_ = os.path.join(LS_root_directory, single_Grid_folder_name, folder_);
        if os.path.isdir(path_):
            start_src_loc = re.search(r'_.*', folder_).start();
            src_idx = int(folder_[11:start_src_loc]);
            src_idx_list.append(src_idx)
            src_list.append(folder_[start_src_loc+1::]);
            full_path_src_list.append( os.path.join(LS_root_directory, single_Grid_folder_name, folder_))
    # Checks:
    if len(src_list) != len(src_idx_list):
        raise NameError("Lists have different lengths - something is wrong.");
    if interpolate_source_flag:
        if len(src_list)!= 8:
            raise NameError("For source interpolation, we need 8 grids for different source location for trilinear interpolation!");
        for i_ in range(8):
            if i_ not in src_idx_list:
                raise NameError("Convention is FREE FIELD [src_idx]_..., where src_idx must be 0,1,2,3,4,5,6,7 for trilinear interpolation to work! Could not find " + str(i_) + " src_idx!");
    # Get default source location:
    def_src_location = src_list[src_idx_list.index(0)];
    src_vox_x_default, src_vox_y_default, src_vox_z_default = def_src_location[1:-1].split(",");
    # To int:
    src_vox_x_default = int(src_vox_x_default); src_vox_y_default = int(src_vox_y_default); src_vox_z_default = int(src_vox_z_default);
    displace_src_vox_default = (src_vox_x_default, src_vox_y_default, src_vox_z_default);
    
    found_hdf5_flag = False;
    hdf5_FREEFIELD_file_modified_TS = None; # Contains the modified time of the read .hdf5!
    # Check HDF5 for FREEFIELD:
    hdf5_fn = single_Grid_folder_name + "_" + "FREEFIELD_REC_Interpolated_data" + ".hdf5";
    if os.path.exists( LS_root_directory + os.sep + hdf5_fn):
        print "\t" + print_colors.FG_CYAN + "Found .hdf5 data GRID " + print_colors.BG_MAGENTA + str(grid_no) + print_colors.END + print_colors.FG_CYAN + " (FREE-FIELD): " + print_colors.FG_YELLOW + hdf5_fn + print_colors.END +  "";
        interpolated_SRC_signals_FREEFIELD, non_interpolated_signal_FREEFIELD, interpolated_signal_REC_Only_FREEFIELD,\
            SRC_interpolation_intended_location_FREEFIELD_m, direction_list_FREEFIELD, \
                Receiver_final_locations_m_list_FREEFIELD, sim_data_FREEFIELD = convergence_IO.read_dumped_interpolated_data(file_dir = LS_root_directory, file_name = hdf5_fn);
        hdf5_FREEFIELD_file_modified_TS = os.path.getmtime( os.path.join(LS_root_directory, hdf5_fn) );
        #print "Debug, FREEFIELD - " + str(hdf5_fn) + ", m_time = " + str(hdf5_FREEFIELD_file_modified_TS)
        #print "\t" + str(time.ctime(hdf5_FREEFIELD_file_modified_TS) )
        found_hdf5_flag = True;
    else:
        print "\t" + print_colors.FG_CYAN + "hdf5 not found -> Looping GRID " + print_colors.BG_MAGENTA + str(grid_no) + print_colors.END + print_colors.FG_CYAN + " (FREE-FIELD):" + print_colors.END + "";
        # !! Remember Receiver_interpolated_signal_FREEFIELD will have shape of (N_src_directories, N_directions, N_samples)
        src_data_list_FREEFIELD, direction_list_FREEFIELD, Receiver_final_locations_m_list_FREEFIELD,\
          Receiver_interpolated_signal_FREEFIELD, Receiver_non_interpolated_signal_FREEFIELD,\
            N_directories_FREEFIELD, sim_data_FREEFIELD =  interpolate_Receiver_responses(MPI_fs_ = MPI_fs, list_src_roots = full_path_src_list, 
                                                                                                        MPI_ear_ = "CENTER", verbose_flag = True, N_end = N_end_);
        # Get non-interpolated data:
        found_non_interp = False;
        for i_ in range(len(src_data_list_FREEFIELD)):
            if numpy.array_equal(src_data_list_FREEFIELD[i_].displace_source_by_vox, numpy.int32((src_vox_x_default,src_vox_y_default,src_vox_z_default))):
                if found_non_interp:
                    raise NameError("Duplicate non-interpolation location for G" + str(grid_no) + " FREE-FIELD!");
                non_interpolated_signal_FREEFIELD = Receiver_non_interpolated_signal_FREEFIELD[i_];
                interpolated_signal_REC_Only_FREEFIELD = Receiver_interpolated_signal_FREEFIELD[i_];
                found_non_interp = True;
        if not found_non_interp:
            raise NameError("Default non-interpolation location not found for G" + str(grid_no) + " FREE-FIELD!");
        # Interpolate source:
        if interpolate_source_flag:
            print "\t" + print_colors.FG_CYAN + print_colors.BG_MAGENTA + "G" + str(grid_no) + print_colors.END + print_colors.FG_CYAN + " SOURCE " + print_colors.BG_YELLOW + "Interpolation" +print_colors.END+print_colors.FG_CYAN+ " on " + print_colors.BG_YELLOW + "G" + str(interpolate_on_grid_no) +print_colors.END+print_colors.FG_CYAN+ " (FREE-FIELD):" + print_colors.END + ""
            # This one checks that the interpolation location is between the other locations!:
            interpolated_SRC_signals_FREEFIELD = PRTF_interpolation.trilinear_interpolate_Source_time(sim_data = sim_data_FREEFIELD, 
                                                                                                   src_data_list_in = src_data_list_FREEFIELD, 
                                                                                                   sim_data_Int_Rec = Receiver_interpolated_signal_FREEFIELD, 
                                                                                                   interpolation_intended_location_m = interpolation_intended_location_m, 
                                                                                                   verbose_flag_ = False, plot_interpolation_flag = False);
            print "\t   " + print_colors.FG_CYAN + "Interpolation done for GRID " + print_colors.BG_MAGENTA + str(grid_no) + print_colors.END + print_colors.FG_CYAN + " (FREEFIELD):" + print_colors.END + "";
        else:
            print "\t   " + print_colors.FG_BLUE + " No SOURCE Interpolation for this (FREEFIELD) grid.... "+ print_colors.END + "";
            interpolation_intended_location_m = src_data_list_FREEFIELD[0].final_location_m; # Use this one for src interpolation continuous location!
            interpolated_SRC_signals_FREEFIELD = numpy.copy(interpolated_signal_REC_Only_FREEFIELD);
        SRC_interpolation_intended_location_FREEFIELD_m = numpy.copy(interpolation_intended_location_m); # Different name for the same thing...
        
        print "\t" + print_colors.FG_CYAN + "dumping .hdf5 data GRID " + print_colors.BG_MAGENTA + str(grid_no) + print_colors.END + print_colors.FG_CYAN + " (FREE-FIELD):  " + print_colors.FG_YELLOW + hdf5_fn + print_colors.END +  "";    
        convergence_IO.dump_interpolated_data_hdf5(file_dir = LS_root_directory, file_name = hdf5_fn, 
                                                   sim_data = sim_data_FREEFIELD,
                                                   interpolated_SRC_signals = interpolated_SRC_signals_FREEFIELD,
                                                   NON_interpolated_signals = non_interpolated_signal_FREEFIELD, 
                                                   interpolated_REC_Only_signals = interpolated_signal_REC_Only_FREEFIELD, 
                                                   SRC_interpolation_intended_location_m = interpolation_intended_location_m, 
                                                   direction_list_ = direction_list_FREEFIELD, 
                                                   Receiver_final_locations_m_list = Receiver_final_locations_m_list_FREEFIELD);
        hdf5_FREEFIELD_file_modified_TS = os.path.getmtime( os.path.join(LS_root_directory, hdf5_fn) ); # Get the modified time-stamp!
        print "\t" + print_colors.FG_CYAN + "   DONE." + print_colors.END + "";
    return interpolated_SRC_signals_FREEFIELD, non_interpolated_signal_FREEFIELD, interpolated_signal_REC_Only_FREEFIELD, direction_list_FREEFIELD, Receiver_final_locations_m_list_FREEFIELD, sim_data_FREEFIELD, SRC_interpolation_intended_location_FREEFIELD_m, displace_src_vox_default, found_hdf5_flag, hdf5_FREEFIELD_file_modified_TS;

def apply_Gaussian_window_to_list(grids_number_list, grids_fs_list, receiver_direction_list, signal_list):
    '''
    Function applies a Gaussian window to the time responses in the signal_list.
    
    :param grids_number_list: (N_grids) A list containing grid numbers (convention) coming from, e.g., read_grid_data_Interp_SRC_REC() function.
    :param grids_fs_list: (N_grids) A list containing the sampling frequencies in Hz for each grid. Comes from, e.g., read_grid_data_Interp_SRC_REC() function.
    :param receiver_direction_list: (N_dirs) A list containing direction (az, el) with the receiver directions. Comes from, e.g., read_grid_data_Interp_SRC_REC() function.
    
    :param signal_list: A list containing FDTD responses for each direction for each grid (i.e., N_grids x (N_dir x N_samples) ). Comes from read_grid_data_Interp_SRC_REC() function.
                    Each member of the list should be numpy array of size (N_dir x N_samples).
    
    :return gaussian_window_list: A list with all the Gaussian windows applied to each grid (and direction).
    '''
    gaussian_window_list = [];
    total_time_sim_s = get_total_simulation_time_ELEC_Pinna();
    if len(grids_number_list) != len(signal_list):
        raise NameError("The grid list has different size than the responses list!");
    for grid_idx in range(len(grids_number_list)):
        if len(signal_list[grid_idx]) != len(receiver_direction_list):
            raise NameError("Grid " + str(grids_number_list[grid_idx]) + " does not have the same number of directions (" + str(len(signal_list[grid_idx])) + ") as in the dir_list (" + str(len(receiver_direction_list)) + ")" );
        MPI_fs = grids_fs_list[grid_idx];
        t_samples_ = numpy.float64(range(len( signal_list[grid_idx][0] )))*(1.0/MPI_fs); #Take first direction
        for direction_idx in range(len(receiver_direction_list)):
            window_Gaussian_Grid = get_Gaussian_window(
                                    t_samples = t_samples_,
                                    t_sim = total_time_sim_s );
            signal_list[grid_idx][direction_idx] = signal_list[grid_idx][direction_idx] * window_Gaussian_Grid;
        gaussian_window_list.append(window_Gaussian_Grid);
    return gaussian_window_list;

def apply_Gaussian_window_to_lists(grids_number_list, grids_fs_list, receiver_direction_list, signal_list1, signal_list2, signal_list3):
    '''
    Function calls for multiple lists (to reduce code lines). Three lists (idea is to be used with BLOCKED vs FREEFIELD).
    
        See apply_Gaussian_window_to_list() for all the details.
    '''
    gaussian_window_list1 = apply_Gaussian_window_to_list(grids_number_list = grids_number_list, grids_fs_list = grids_fs_list, receiver_direction_list = receiver_direction_list, signal_list = signal_list1);
    gaussian_window_list2 = apply_Gaussian_window_to_list(grids_number_list = grids_number_list, grids_fs_list = grids_fs_list, receiver_direction_list = receiver_direction_list, signal_list = signal_list2);
    gaussian_window_list3 = apply_Gaussian_window_to_list(grids_number_list = grids_number_list, grids_fs_list = grids_fs_list, receiver_direction_list = receiver_direction_list, signal_list = signal_list3);
    return gaussian_window_list1, gaussian_window_list2, gaussian_window_list3

def build_PRTF_grids(grids_number_list, grids_fs_list, final_receiver_direction_list, \
                     interpolated_SRC_signals_BLOCKED_list, interpolated_SRC_signals_FREEFIELD_list, \
                     non_interpolated_signal_BLOCKED_list, non_interpolated_signal_FREEFIELD_list, \
                     interpolated_signal_REC_Only_BLOCKED_list, interpolated_signal_REC_Only_FREEFIELD_list, \
                     apply_Gaussian_window = True):
    '''
    Function builds PRTFs ( SRC-REC interpolated, Receiver-only interpolated PRTFs and no interpolation) based on the BLOCKED and FREE-FIELD simulations.
    
    :param grids_number_list: (N_grids) A list containing grid numbers (convention) coming from, e.g., read_grid_data_Interp_SRC_REC() function.
    :param grids_fs_list: (N_grids) A list containing the sampling frequencies in Hz for each grid. Comes from, e.g., read_grid_data_Interp_SRC_REC() function.
    :param final_receiver_direction_list: (N_dirs) A list containing direction (az, el) with the receiver directions. Comes from, e.g., read_grid_data_Interp_SRC_REC() function.
    
    FDTD data: remember that each member of the lists below should be numpy array of size (N_dir x N_samples). They all should come from read_grid_data_Interp_SRC_REC() function.
    
    :param interpolated_SRC_signals_BLOCKED_list: A list containing FDTD responses for each direction for each grid (i.e., N_grids x (N_dir x N_samples) ). Full SRC-REC interpolation for BLOCKED.
    :param interpolated_SRC_signals_FREEFIELD_list: A list containing FDTD responses for each direction for each grid (i.e., N_grids x (N_dir x N_samples) ). Full SRC-REC interpolation for FREEFIELD.
    :param non_interpolated_signal_BLOCKED_list: A list containing FDTD responses for each direction for each grid (i.e., N_grids x (N_dir x N_samples) ). NO interpolation interpolation for BLOCKED.
    :param non_interpolated_signal_FREEFIELD_list: A list containing FDTD responses for each direction for each grid (i.e., N_grids x (N_dir x N_samples) ). NO interpolation interpolation for FREEFIELD.
    :param interpolated_signal_REC_Only_BLOCKED_list: A list containing FDTD responses for each direction for each grid (i.e., N_grids x (N_dir x N_samples) ). Only receiver interpolation (src at a voxel close to the location) for BLOCKED.
    :param interpolated_signal_REC_Only_FREEFIELD_list: A list containing FDTD responses for each direction for each grid (i.e., N_grids x (N_dir x N_samples) ). Only receiver interpolation (src at a voxel close to the location) for FREEFIELD.
    
    :param apply_Gaussian_window: Boolean. If True, a Gaussian window is SAMPLED and applied to the time-signals. See convergence_PRTF.get_Gaussian_window() for the Gaussian window implementation.
    
    :return:
        :ret PRTF_SRC_REC_interpolated_grid_list: List containing the PRTFs (SRC and REC interpolation FDTD simulations (BLOCKED and FREEFIELD) ) for each grid and all directions. For each grid, a numpy array of size (N_dirs x N_samples) is created which contains the PRTFs.
        :ret PRTF_NON_interpolated_grid_list: List containing the PRTFs (no interpolation in FDTD simulations (BLOCKED and FREEFIELD) ) for each grid and all directions. For each grid, a numpy array of size (N_dirs x N_samples) is created which contains the PRTFs.
        :ret PRTF_interpolated_RecOnly_grid_list: List containing the PRTFs (only REC are interpolated in FDTD simulations (BLOCKED and FREEFIELD) ) for each grid and all directions. For each grid, a numpy array of size (N_dirs x N_samples) is created which contains the PRTFs.
                Here, the source is located at a voxel close to the interpolation location.
        :ret gaussian_window_list_BLOCKED: A list with all the Gaussian windows applied to each grid (and direction) - BLOCKED simulations.
        :ret gaussian_window_list_FREEFIELD: A list with all the Gaussian windows applied to each grid (and direction) - FREEFIELD simulations.
    '''
    N_grids = len(grids_number_list);
    if apply_Gaussian_window:
        # Apply Gaussian window (BLOCKED):
        gaussian_window_list_BLOCKED, _, _ = apply_Gaussian_window_to_lists(grids_number_list = grids_number_list, grids_fs_list = grids_fs_list, receiver_direction_list = final_receiver_direction_list, 
                                                              signal_list1 = interpolated_SRC_signals_BLOCKED_list,
                                                              signal_list2 = non_interpolated_signal_BLOCKED_list,
                                                              signal_list3 = interpolated_signal_REC_Only_BLOCKED_list);
        # Apply Gaussian window (FREE FIELD):
        gaussian_window_list_FREEFIELD, _, _ = apply_Gaussian_window_to_lists(grids_number_list = grids_number_list, grids_fs_list = grids_fs_list, receiver_direction_list = final_receiver_direction_list, 
                                                              signal_list1 = interpolated_SRC_signals_FREEFIELD_list,
                                                              signal_list2 = non_interpolated_signal_FREEFIELD_list,
                                                              signal_list3 = interpolated_signal_REC_Only_FREEFIELD_list);
    else:
        gaussian_window_list_BLOCKED = [None]*N_grids;
        gaussian_window_list_FREEFIELD = [None]*N_grids;
    # Build PRTFs:
    N_dirs = len(final_receiver_direction_list);
    PRTF_SRC_REC_interpolated_grid_list = [None]*N_grids;
    PRTF_NON_interpolated_grid_list = [None]*N_grids;
    PRTF_interpolated_RecOnly_grid_list = [None]*N_grids;
    for grid_idx in range( N_grids ):
        # Interpolated PRTFs:
        PRTF_Grid = numpy.zeros(numpy.shape(interpolated_SRC_signals_BLOCKED_list[grid_idx]));
        for direction_idx in range( N_dirs ):
            PRTF_Grid[direction_idx, :], _ = HRTF_utils.process_HRTF_simulations(
                                            ear_simulation = interpolated_SRC_signals_BLOCKED_list[grid_idx][direction_idx], middle_of_head_simulation = interpolated_SRC_signals_FREEFIELD_list[grid_idx][direction_idx], 
                                            sampling_frequency = grids_fs_list[grid_idx], 
                                            downsampling_factor = 1,
                                            cap_to_samples_start = 0, 
                                            cap_to_samples_end = None, 
                                            low_pass_filter_flag = False, 
                                            low_pass_filter_length = 150, 
                                            cut_off_freq_Hz = 15000, 
                                            circularly_shift_by_samples = 0,
                                            use_polyphase_resampling_flag = False, # Not implemented yet...
                                            upsample_factor = 1,
                                            downsample_factor = 1,
                                            time_window = 'rectangular');
        PRTF_SRC_REC_interpolated_grid_list[grid_idx] = PRTF_Grid;
        # NON-Interpolated PRTFs:
        PRTF_Grid_NI = numpy.zeros(numpy.shape(non_interpolated_signal_BLOCKED_list[grid_idx]));
        for direction_idx in range( N_dirs ):
            PRTF_Grid_NI[direction_idx, :], _ = HRTF_utils.process_HRTF_simulations(
                                            ear_simulation = non_interpolated_signal_BLOCKED_list[grid_idx][direction_idx], middle_of_head_simulation = non_interpolated_signal_FREEFIELD_list[grid_idx][direction_idx], 
                                            sampling_frequency = grids_fs_list[grid_idx], 
                                            downsampling_factor = 1,
                                            cap_to_samples_start = 0, 
                                            cap_to_samples_end = None, 
                                            low_pass_filter_flag = False, 
                                            low_pass_filter_length = 150, 
                                            cut_off_freq_Hz = 15000, 
                                            circularly_shift_by_samples = 0,
                                            use_polyphase_resampling_flag = False, # Not implemented yet...
                                            upsample_factor = 1,
                                            downsample_factor = 1,
                                            time_window = 'rectangular');
        PRTF_NON_interpolated_grid_list[grid_idx] = PRTF_Grid_NI;
        # Rec-only Interpolated PRTFs:
        PRTF_Grid_Ro = numpy.zeros(numpy.shape(interpolated_signal_REC_Only_BLOCKED_list[grid_idx]));
        for direction_idx in range( N_dirs ):
            PRTF_Grid_Ro[direction_idx, :], _ = HRTF_utils.process_HRTF_simulations(
                                            ear_simulation = interpolated_signal_REC_Only_BLOCKED_list[grid_idx][direction_idx], middle_of_head_simulation = interpolated_signal_REC_Only_FREEFIELD_list[grid_idx][direction_idx], 
                                            sampling_frequency = grids_fs_list[grid_idx], 
                                            downsampling_factor = 1,
                                            cap_to_samples_start = 0, 
                                            cap_to_samples_end = None, 
                                            low_pass_filter_flag = False, 
                                            low_pass_filter_length = 150, 
                                            cut_off_freq_Hz = 15000, 
                                            circularly_shift_by_samples = 0,
                                            use_polyphase_resampling_flag = False, # Not implemented yet...
                                            upsample_factor = 1,
                                            downsample_factor = 1,
                                            time_window = 'rectangular');
        PRTF_interpolated_RecOnly_grid_list[grid_idx] = PRTF_Grid_Ro;
    return PRTF_SRC_REC_interpolated_grid_list, PRTF_NON_interpolated_grid_list, PRTF_interpolated_RecOnly_grid_list, gaussian_window_list_BLOCKED, gaussian_window_list_FREEFIELD;

def read_PRTF_data(LS_root_directory):
    '''
    Function gets the PRTF data from an hdf5 file directly to save time! 
    
    If an hdf5 file is found:
        1) Function checks first that all the .hdf5 files from BLOCKED/FREEFIELD exists for all the grids in LS_root_directory. 
        2) Then function checks that the modified times on the BLOCKED/FREEFIELD hdf5 files match the one in the hdf5 file! 
    
    :param LS_root_directory: The root directory where the data (for all grids, BLOCKED and FREEFIELD) resides.
    
    :return PRTFdata from hdf5 (see also return list from read_grid_data_Interp_SRC_REC() and from build_PRTF_grids() )
        :ret interpolate_on_grid: int. Gives the logical number of the grid where interpolation occurs (this is based on the folder naming convention and then written in the hdf5 - see read_grid_data_Interp_SRC_REC() ) 
        :ret grids_number_list: A list containing of size N_dir the grid numbers (again, logical grid number as found in folder names and then written in hdf5).
        :ret grids_fs_list: A list of int of size N_dir containing the sampling frequencies (corresponds to grids_number_list).
        :ret Receiver_final_locations_m_list_BLOCKED: A list of size (N_dir) containing arrays of size 3 which give the location in m for each receiver at each direction for BLOCKED simulations (e.g., [REC_m_DIR_0_x, REC_m_DIR_0_y, REC_m_DIR_0_z]). 
        :ret Receiver_final_locations_m_list_FREEFIELD: Same as Receiver_final_locations_m_list_BLOCKED but for the FREEFIELD simulations. 
        :ret final_receiver_direction_list: (N_dir) A list containing tuples with the receiver directions (az, el) for the simulation data (both BLOCKED and FREE-FIELD - already checked).
                                            Simulation data for each grid contains responses for all these directions. Order will be consistent among grids!
        :ret folder_list: A list containing the corresponding directories for each grid number/fs where the data was read.
        :ret final_location_source_BLOCKED_m: Array of size N_dir contianing the final location in m in Cartesian coordinates (array of the form [SRC_m_x, SRC_m_y, SRC_m_z]) where the source was interpolated for BLOCKED simulation(s).
        :ret final_location_source_FREEFIELD_m: Same as final_location_source_BLOCKED_m but for the FREEFIELD simulation.
        :ret sim_data_list: A list of size N_grids containing MPI_IO.SimData() objects with simulation-related data for each grid.
        
        :ret PRTF_SRC_REC_interpolated_grid_list: List of size N_dir containing the PRTFs (SRC and REC interpolation FDTD simulations (BLOCKED and FREEFIELD) ) for each grid and all directions. For each grid, a numpy array of size (N_dirs x N_samples) is created which contains the PRTFs.
        :ret PRTF_NON_interpolated_grid_list: List of size N_dir containing the PRTFs (no interpolation in FDTD simulations (BLOCKED and FREEFIELD) ) for each grid and all directions. For each grid, a numpy array of size (N_dirs x N_samples) is created which contains the PRTFs.
        :ret PRTF_interpolated_RecOnly_grid_list: List of size N_dir containing the PRTFs (only REC are interpolated in FDTD simulations (BLOCKED and FREEFIELD) ) for each grid and all directions. For each grid, a numpy array of size (N_dirs x N_samples) is created which contains the PRTFs.
                                                Here, the source is located at a voxel close to the interpolation location.
        :ret gaussian_window_list: A list with all the Gaussian windows applied to each grid (and direction) - both BLOCKED and FREEFIELD simulations.
        :ret hdf5_modified_time_BLOCKED: An array of size N_grids containing the modified time of the BLOCKED .hdf5 files used when computing the PRTFs. Use time.ctime() to display this in a good format.
        :ret hdf5_modified_time_FREEFIELD: An array of size N_grids containing the modified time of the FREEFIELD .hdf5 files used when computing the PRTFs. Use time.ctime() to display this in a good format.
    '''
    hdf5_fn = "PRTF_data_ALL_grids.hdf5";
    if os.path.exists( LS_root_directory + os.sep + hdf5_fn):
        print "\t" + print_colors.BG_GREEN + "hdf5 file found with all the PRTF data... " + print_colors.END + " checking consistency..." + "";
        # Read data:
        interpolate_on_grid, grids_number_list, grids_fs_list, Receiver_final_locations_m_list_BLOCKED, Receiver_final_locations_m_list_FREEFIELD, \
            final_receiver_direction_list, folder_list, final_location_source_BLOCKED_m, final_location_source_FREEFIELD_m, sim_data_list, \
                PRTF_SRC_REC_interpolated_grid_list, PRTF_NON_interpolated_grid_list, PRTF_interpolated_RecOnly_grid_list, gaussian_window_list, \
                    hdf5_modified_time_BLOCKED, hdf5_modified_time_FREEFIELD = convergence_IO.read_PRTF_data_ALL_grids(file_dir = LS_root_directory, file_name = hdf5_fn);
        # Check 1) - check all folders are in the hdf5 file:
        #########################################################
        folders_reading_dir = os.listdir(LS_root_directory);
        folders_reading_dir = [f for f in folders_reading_dir if re.match(r'G\d*_\d*Hz.*', f)];
        for folder_ in folders_reading_dir:
            # Ignore files:
            path_ = os.path.join(LS_root_directory, folder_);
            if os.path.isdir(path_):
                if folder_ not in folder_list:
                    raise NameError("Foleder " + str(folder_) + " not in the folder list in the PRTF hdf5 file! List inisde = " + str(folder_list) );
        # Check 2) - timestamps are okay:
        #########################################################
        # BLOCKED:
        #################
        folders_reading_dir = os.listdir(LS_root_directory);
        folders_reading_dir = [f for f in folders_reading_dir if re.match(r'G\d*_\d*Hz.*_BLOCKED_.*.hdf5', f)];
        if len(folders_reading_dir) != len(hdf5_modified_time_BLOCKED):
            print str(len(folders_reading_dir));
            print str(len(hdf5_modified_time_BLOCKED));
            raise NameError("In the root folder, there is a different number of BLOCKED .hdf5 files than what there was when the PRTF .hdf5 was done.");
        for blocked_file in folders_reading_dir:
            current_TS = os.path.getmtime( os.path.join(LS_root_directory, blocked_file) );
            # Get grid number:
            end_grid = re.search(r'_\d*Hz_', blocked_file).start();
            grid_no = int(blocked_file[1:end_grid]);
            if grid_no<0:
                raise NameError("Grid number (BLOCKED) cannot be negative!");
            # find index:
            idx_grid = grids_number_list.index(grid_no);
            if int(current_TS) != int(hdf5_modified_time_BLOCKED[idx_grid]):
                print "Current time stamp = " + str(time.ctime(current_TS));
                #print "\t" + str(current_TS)
                print "PRTF .hdft time stamp = " + str(time.ctime(hdf5_modified_time_BLOCKED[idx_grid]));
                #print "\t" + str(hdf5_modified_time_BLOCKED[idx_grid])
                raise NameError("Unmatching modified date for BLOCKED GRID " + str(grid_no) + " and .hdf5 file " + str(blocked_file));
        # FREE FIELD:
        #################
        folders_reading_dir = os.listdir(LS_root_directory);
        folders_reading_dir = [f for f in folders_reading_dir if re.match(r'G\d*_\d*Hz.*_FREEFIELD_.*.hdf5', f)];
        if len(folders_reading_dir) != len(hdf5_modified_time_FREEFIELD):
            print str(len(folders_reading_dir));
            print str(len(hdf5_modified_time_FREEFIELD));
            raise NameError("In the root folder, there is a different number of FREEFIELD .hdf5 files than what there was when the PRTF .hdf5 was done.");
        for ff_file in folders_reading_dir:
            current_TS = os.path.getmtime( os.path.join(LS_root_directory, ff_file) );
            # Get grid number:
            end_grid = re.search(r'_\d*Hz_', ff_file).start();
            grid_no = int(ff_file[1:end_grid]);
            if grid_no<0:
                raise NameError("Grid number (FREEFIELD) cannot be negative!");
            # find index:
            idx_grid = grids_number_list.index(grid_no);
            if int(current_TS) != int(hdf5_modified_time_FREEFIELD[idx_grid]):
                print "Current time stamp = " + str(time.ctime(current_TS));
                print "PRTF .hdft time stamp = " + str(time.ctime(hdf5_modified_time_FREEFIELD[idx_grid]));
                raise NameError("Unmatching modified date for FREEFIELD GRID " + str(grid_no) + " and .hdf5 file " + str(blocked_file));
        print "\t Checks " + print_colors.BG_GREEN + "OK. Returning... " + print_colors.END + "";
    else:
        print "\t" + print_colors.FG_CYAN + "hdf5 file not found. Reading .HDF5 data for all grids..." + print_colors.END + "";
        interpolate_on_grid, grids_number_list, grids_fs_list, folder_list, \
        final_location_source_BLOCKED_m, final_location_source_FREEFIELD_m, \
            Receiver_final_locations_m_list_BLOCKED, Receiver_final_locations_m_list_FREEFIELD, final_receiver_direction_list, \
                interpolated_SRC_signals_BLOCKED_list, interpolated_SRC_signals_FREEFIELD_list,\
                    non_interpolated_signal_BLOCKED_list, non_interpolated_signal_FREEFIELD_list,\
                        interpolated_signal_REC_Only_BLOCKED_list, interpolated_signal_REC_Only_FREEFIELD_list, \
                            sim_data_BLOCKED_list, sim_data_FREEFIELD_list, hdf5_modified_time_BLOCKED, hdf5_modified_time_FREEFIELD = read_grid_data_Interp_SRC_REC(LS_root_directory = root_dir_);
        print "\t" + print_colors.FG_CYAN + "Computing PRTFs..." + print_colors.END + "";
        PRTF_SRC_REC_interpolated_grid_list, PRTF_NON_interpolated_grid_list, PRTF_interpolated_RecOnly_grid_list, \
            gaussian_window_list_BLOCKED, gaussian_window_list_FREEFIELD = build_PRTF_grids(
                                                                        grids_number_list = grids_number_list, grids_fs_list = grids_fs_list, final_receiver_direction_list = final_receiver_direction_list,
                                                                        interpolated_SRC_signals_BLOCKED_list = interpolated_SRC_signals_BLOCKED_list, interpolated_SRC_signals_FREEFIELD_list = interpolated_SRC_signals_FREEFIELD_list, 
                                                                        non_interpolated_signal_BLOCKED_list = non_interpolated_signal_BLOCKED_list, non_interpolated_signal_FREEFIELD_list = non_interpolated_signal_FREEFIELD_list, 
                                                                        interpolated_signal_REC_Only_BLOCKED_list = interpolated_signal_REC_Only_BLOCKED_list, interpolated_signal_REC_Only_FREEFIELD_list = interpolated_signal_REC_Only_FREEFIELD_list,
                                                                        apply_Gaussian_window = True
                                                                        );
        # Compare windows:
        for grid_idx in range(len(gaussian_window_list_BLOCKED)):
            if not (gaussian_window_list_BLOCKED[grid_idx] == gaussian_window_list_FREEFIELD[grid_idx]).all():
                raise NameError("Windows applied to the time domain are different for BLOCKED and FREE-FIELD for grid G" + str(grids_number_list[grid_idx]) + "!");
        print "\t" + print_colors.FG_CYAN + "Dumping PRTF data..." + print_colors.END + "";
        
        convergence_IO.dump_PRTF_data_ALL_grids(file_dir = LS_root_directory, file_name = hdf5_fn,
                                                interpolate_on_grid = interpolate_on_grid, grids_number_list = grids_number_list, grids_fs_list = grids_fs_list, 
                                                Receiver_final_locations_m_list_BLOCKED = Receiver_final_locations_m_list_BLOCKED, Receiver_final_locations_m_list_FREEFIELD = Receiver_final_locations_m_list_FREEFIELD, final_receiver_direction_list = final_receiver_direction_list, 
                                                folder_list = folder_list, final_location_source_BLOCKED_m = final_location_source_BLOCKED_m, final_location_source_FREEFIELD_m = final_location_source_FREEFIELD_m, 
                                                sim_data_list = sim_data_BLOCKED_list,
                                                PRTF_SRC_REC_interpolated_grid_list = PRTF_SRC_REC_interpolated_grid_list, 
                                                PRTF_NON_interpolated_grid_list = PRTF_NON_interpolated_grid_list, 
                                                PRTF_interpolated_RecOnly_grid_list = PRTF_interpolated_RecOnly_grid_list,
                                                gaussian_window_list = gaussian_window_list_BLOCKED, 
                                                hdf5_modified_time_BLOCKED = hdf5_modified_time_BLOCKED, hdf5_modified_time_FREEFIELD = hdf5_modified_time_FREEFIELD);                                 
        sim_data_list = sim_data_BLOCKED_list; # Renaming...
        gaussian_window_list = gaussian_window_list_BLOCKED;
    return interpolate_on_grid, grids_number_list, grids_fs_list, Receiver_final_locations_m_list_BLOCKED, Receiver_final_locations_m_list_FREEFIELD, \
            final_receiver_direction_list, folder_list, final_location_source_BLOCKED_m, final_location_source_FREEFIELD_m, sim_data_list, \
                PRTF_SRC_REC_interpolated_grid_list, PRTF_NON_interpolated_grid_list, PRTF_interpolated_RecOnly_grid_list, gaussian_window_list, \
                    hdf5_modified_time_BLOCKED, hdf5_modified_time_FREEFIELD;

def prepare_PRTF_data(final_receiver_direction_list, grids_number_list, grids_fs_list, PRTF_SRC_REC_interpolated_grid_list, PRTF_NON_interpolated_grid_list, PRTF_interpolated_RecOnly_grid_list, f_max_ = 20E3):
    '''
    Function checks and prepares data for all grids, all directions and all frequencies. 
    
    
    These parameters all come from read_PRTF_data() function:
    
    :param final_receiver_direction_list: A list of size N_dir containing tuples with the receiver directions (az, el) for the simulation data (both BLOCKED and FREE-FIELD - already checked).
                                            Simulation data for each grid contains responses for all these directions. Order will be consistent among grids!
    :param grids_number_list: A list of size N_dir containing the grid numbers (again, logical grid number as found in folder names and then written in hdf5 - see read_grid_data_Interp_SRC_REC() ).
    :param grids_fs_list: A list of int of size N_dir containing the sampling frequencies (corresponds to grids_number_list).
    :param PRTF_SRC_REC_interpolated_grid_list: List of size N_dir containing the PRTFs (SRC and REC interpolation FDTD simulations (BLOCKED and FREEFIELD) ) for each grid and all directions. For each grid, a numpy array of size (N_dirs x N_samples) is created which contains the PRTFs.
    :param PRTF_NON_interpolated_grid_list: List of size N_dir containing the PRTFs (no interpolation in FDTD simulations (BLOCKED and FREEFIELD) ) for each grid and all directions. For each grid, a numpy array of size (N_dirs x N_samples) is created which contains the PRTFs.
    :param PRTF_interpolated_RecOnly_grid_list: List of size N_dir containing the PRTFs (only REC are interpolated in FDTD simulations (BLOCKED and FREEFIELD) ) for each grid and all directions. For each grid, a numpy array of size (N_dirs x N_samples) is created which contains the PRTFs.
                                                Here, the source is located at a voxel close to the interpolation location.
    
    Extra parameter:
    :param f_max_: Maximal frequency where analysis will be conducted. Note that it might be too large than the maximal frequency as read by the read_PRTF_data() function and an error will occur.
    
    :returns:
        :ret N_grids: int with the total number of grids from input. Helper variable.
        :ret N_dirs: int with the number of directions. Helper variable.
        :ret N_freqs: int with the total number of frequency bins (positive frequency) between DC and f_max_ for which PRTF data was computed. Helper variable.
        :ret df: float with frequency resolution. Checks are done that this will be consistent between grids.
        :ret frequency_vector: A numpy.array with the frequency in Hz corresponding to all the frequency bins in the returned data. These bins are consistent between grids - e.g., frequency_vector[grid_0][direction_0][f_idx] = frequency_vector[grid_i][direction_j][f_idx].
        :ret PRTF_ALL_GRIDS_interpolated: Numpy array (numpy.complex128) of size [N_grids, N_dirs, N_freqs] with the PRTF for both source and receiver interpolation.   
        :ret PRTF_ALL_GRIDS_non_interpolated: Numpy array (numpy.complex128) of size [N_grids, N_dirs, N_freqs] with the PRTF for no interpolation in FDTD simulations (either BLOCKED or FREEFIELD) 
        :ret PRTF_ALL_GRIDS_Rec_only_interp: Numpy array (numpy.complex128) of size [N_grids, N_dirs, N_freqs] with the PRTF where only REC are interpolated in FDTD simulations (both BLOCKED and FREEFIELD).
                                              Here, the source is located at a voxel close to the interpolation location.
    '''
    # Helper data:
    N_dirs = len(final_receiver_direction_list);
    N_grids = len(PRTF_SRC_REC_interpolated_grid_list);
    df = (1.0*grids_fs_list[0]) / len(PRTF_SRC_REC_interpolated_grid_list[0][0]);
    frequency_vector = scipy.fftpack.fftfreq(len(PRTF_SRC_REC_interpolated_grid_list[0][0])) * grids_fs_list[0]; # Get frequency vector based on, e.g., grid_idx=0, dir_idx=0
    # a) CHECKS:
    ################
    for grid_idx in range(1,N_grids):
        # Check N_dirs consistency:
        if len(PRTF_SRC_REC_interpolated_grid_list[grid_idx]) != N_dirs or len(PRTF_SRC_REC_interpolated_grid_list[grid_idx]) != N_dirs or len(PRTF_NON_interpolated_grid_list[grid_idx]) != N_dirs or len(PRTF_interpolated_RecOnly_grid_list[grid_idx]) != N_dirs:
            raise NameError("Inconsistency in number of directions! The lengths of input lists must be the same!");
        # Check length consistency:
        if len(PRTF_SRC_REC_interpolated_grid_list[grid_idx][0]) != len(PRTF_SRC_REC_interpolated_grid_list[grid_idx][0]) or len(PRTF_SRC_REC_interpolated_grid_list[grid_idx][0]) != len(PRTF_NON_interpolated_grid_list[grid_idx][0]):
            raise NameError("FDTD simulation lengths are inconsistent!");
        # Check frequency resolution consistency:
        df_grid = (1.0*grids_fs_list[grid_idx]) / len(PRTF_SRC_REC_interpolated_grid_list[grid_idx][0]);
        if df != df_grid:
            raise NameError("Frequency resolution inconsistency! First grid has df = " + str(df) + " and current grid has df_grid=" + str(df_grid));
    # b) Prepare PRTF frqeuency data:
    #################################
    numpy.set_printoptions(threshold=numpy.inf)
    bool_ = numpy.all([frequency_vector>=0, frequency_vector<=f_max_],0);
    frequency_vector = frequency_vector[bool_];
    N_freqs = len(frequency_vector);
    # Allocate vectors:
    PRTF_ALL_GRIDS_interpolated = numpy.zeros( (N_grids, N_dirs, N_freqs), dtype = numpy.complex128);
    PRTF_ALL_GRIDS_non_interpolated = numpy.zeros( (N_grids, N_dirs, N_freqs), dtype = numpy.complex128);
    PRTF_ALL_GRIDS_Rec_only_interp = numpy.zeros( (N_grids, N_dirs, N_freqs), dtype = numpy.complex128);
    for grid_idx in range(N_grids):
        MPI_fs = grids_fs_list[grid_idx];
        freq_PRIR = scipy.fftpack.fftfreq(len(PRTF_SRC_REC_interpolated_grid_list[grid_idx][0])) * MPI_fs;
        bool_grid = numpy.all([freq_PRIR>=0, freq_PRIR<=f_max_],0);
        freq_PRIR = freq_PRIR[bool_grid];
        df_grid = (1.0*grids_fs_list[grid_idx]) / len(PRTF_SRC_REC_interpolated_grid_list[grid_idx][0]);
        check_frequency_vectors_2grids(freq_PRIR_G1 = frequency_vector, freq_PRIR_G2 = freq_PRIR, dF1 = df, dF2 = df_grid, grid_no = grids_number_list[grid_idx]);
        for dir_idx in range(N_dirs):
            # Working on normalized temporal grids (due to frequency division)
            spectrum_PRIR_interp =  scipy.fftpack.fft( PRTF_SRC_REC_interpolated_grid_list[grid_idx][dir_idx] );
            spectrum_PRIR_non_interp =  scipy.fftpack.fft( PRTF_NON_interpolated_grid_list[grid_idx][dir_idx] );
            spectrum_PRIR_Rec_only_interp =  scipy.fftpack.fft( PRTF_interpolated_RecOnly_grid_list[grid_idx][dir_idx] ); 
            # Assign:
            PRTF_ALL_GRIDS_interpolated[grid_idx,dir_idx,:] = spectrum_PRIR_interp[bool_grid];
            PRTF_ALL_GRIDS_non_interpolated[grid_idx,dir_idx,:] = spectrum_PRIR_non_interp[bool_grid];
            PRTF_ALL_GRIDS_Rec_only_interp[grid_idx,dir_idx,:] = spectrum_PRIR_Rec_only_interp[bool_grid];
    return N_grids, N_dirs, N_freqs, df, frequency_vector, PRTF_ALL_GRIDS_interpolated, PRTF_ALL_GRIDS_non_interpolated, PRTF_ALL_GRIDS_Rec_only_interp;

def model_asymptotic_power_law(theta, x):
    '''
    Function computes the model's function according to asymptotic expansion:
        
        p_FDTD = y0 + e_p*(dX)^q
        
        where:
            y0 = asymptotic solution
            e_p = principal error term
            q = the power with wich the error decreases
    
        Following parameter order as in https://docs.scipy.org/doc/scipy/reference/tutorial/optimize.html (note scipy.optimize.leastsq seems to have different ordering).
                                                Accessed at 19 Oct 2018.
    
    :param x: Input data (independent variable).
    :param theta: Model parameter vector.
        y0 = theta[0]  # asymptotic solution
        e_p = theta[1] # principal error term
        q = theta[2]   # the power with wich the error decreases
    '''
    return 1.0*theta[0] + theta[1]*numpy.power(x, theta[2]);

def model_1st_AND_2nd_order(theta, x):
    '''
    Function computes the model's function according to asymptotic expansion:
        
        p_FDTD = y0 + beta1*dX + beta2*(dX)^2
        
        where:
            y0 = asymptotic solution
            beta1 = principal error term
            beta2 = error term corresponding to the quadratic form.
            
    :param x: Input data (independent variable).
    :param theta: Model parameter vector.
        y0 = theta[0]  # asymptotic solution
        beta1 = theta[1] # principal error term
        beta2 = theta[2] 
    '''
    return 1.0*theta[0] + theta[1]*x + theta[2]*numpy.power(x, 2.0)

def model_1st_AND_2nd_order_xM(theta, x):
    '''
    Same as model_1st_AND_2nd_order() but x given in matrix form. 
    '''
    if x.ndim == 1:
        res = 1.0*theta[0] + numpy.dot(theta[1::], x);
    else:
        res = numpy.zeros(len(x[:,0]));
        for idx in range(len(res)):
            res[idx] = 1.0*theta[0] + numpy.dot(theta[1::], x[idx,:]);
    return res;

def model_1st_AND_minus1_order(theta, x):
    '''
    Function computes the model's function according to asymptotic expansion:
        
        p_FDTD = y0 + beta1*dX + beta2*(dX)^(-1)
        
        where:
            y0 = asymptotic solution
            beta1 = principal error term
            beta2 = error term corresponding to the divergent form.
            
    :param x: Input data (independent variable).
    :param theta: Model parameter vector.
        y0 = theta[0]  # asymptotic solution
        beta1 = theta[1] # principal error term
        beta2 = theta[2] 
    '''
    return 1.0*theta[0] + theta[1]*x + theta[2]*numpy.power(x, -1.0);

def residual_asymptotic_power_law(theta, x, y):
    '''
    Function computes the residuals which will be the minimization function for the least-squares fit ( scipy.optimize.least_squares seems to 
    square this result and sum).
    
        Following parameter order as in https://docs.scipy.org/doc/scipy/reference/tutorial/optimize.html (note scipy.optimize.leastsq seems to have different ordering).
                                                Accessed at 19 Oct 2018.
                                                
    :param x: Input data (independent variable).
    :param theta: Model parameter vector.
    :param y: Dependent variable (i.e., output).
    '''
    return model_asymptotic_power_law(theta, x) - y;

def residual_weighted_asymptotic_power_law(theta, x, y):
    '''
    Function computes the residuals which will be the minimization function for the least-squares fit ( scipy.optimize.least_squares seems to 
    square this result and sum) but weighted according to the weights proposed by [Eca L, Hoekstra M., "A procedure for the estimation of the numerical uncertainty of 
    CFD calculations based on grid refinement studies", Journal of Compuational Physics, 262, pp.104-130, (2014)].
    
        Following parameter order as in https://docs.scipy.org/doc/scipy/reference/tutorial/optimize.html (note scipy.optimize.leastsq seems to have different ordering).
                                                Accessed at 19 Oct 2018.
                                                
    :param x: Input data (independent variable).
    :param theta: Model parameter vector.
    :param y: Dependent variable (i.e., output).
    '''
    # Get weights (see [Eca & Hoekstra 2014]):
    w = numpy.sqrt(1.0/x / numpy.sum(1.0/x)); 
    return w*(model_asymptotic_power_law(theta, x) - y);

def jacobian_power_law(theta, x, y):
    '''
    Function computes the Jacobian for the power law in model_asymptotic_power_law.
    
        Following https://docs.scipy.org/doc/scipy/reference/tutorial/optimize.html [Accessed at 19 Oct 2018].
    
    :param theta: Model parameter vector.
        y0 = theta[0]  # asymptotic solution
        e_p = theta[1] # principal error term
        q = theta[2]   # the power with wich the error decreases
    :param x: Input data (independent variable).
    :param y: Dependent variable (i.e., output).
    '''
    J = numpy.empty((x.size, theta.size));
    J[:,0] = numpy.ones(x.size); # df/d y0(xi) = 1.0
    J[:,1] = numpy.power(x, theta[2]); # df/d e_p(xi) = x^q
    J[:,2] = theta[1] * numpy.power(x, theta[2]) * numpy.log(x); # df/d q(xi) = e_p*x^q*ln(x)
    return J

def get_function_value(q_obs, x_vector, y_obs, w_vector = None ):
    '''
    Function evaluates the non-linear equation from Eca and Hoekstra, Appendix B.1 by sweeping the parameter q_obs.
    
        References: 
            [Eca L., Hoekstra M., "A procedure for the estimation of the numerical uncertainty of CFD calculations based on grid refinement studies", Journal of Compuational Physics 262, p104-130, (2014)]
            [Xing T., Stern F., "Comment on 'A procedure for the estimation of the numerical uncertainty of CFD calculations based on grid refinement studies' (L. Eca and H. Moekstra, Journal of Computational Physics 262 (2014) 104-130)", Journal Of Computational Physics 301, p 484-486, (2015)]
            [Eca L., Hoekstra M., "Reply to comment on 'A procedure for the estimation of the numerical uncertainty of CFD calculations based on grid refinement studies' (L. Eca and H. Moekstra, Journal of Computational Physics 262 (2014) 104-130)", Journal of Computational Physics 301, p 487-488, (2015)]
    
    :param q_obs: The q_obs in the power law: p = p_0 + e_p * (dX)^(q_obs) 
    :param x_vector: The vector with grids.
    :param y_obs: The calculated FDTD values.
    :param w_vector: The weights vector. If None, the non-weighted values are returned.
    '''
    N_grids = 1.0*len(x_vector);
    x_power = numpy.power(x_vector, q_obs );
    x_2power = numpy.power(x_vector, 2.0*q_obs );
    ln_x_ = numpy.log(x_vector);
    if w_vector is None:
        # Non-weighted approach:
        y_sum = numpy.sum(y_obs);
        x_power_sum = numpy.sum(x_power);
        x_2power_sum = numpy.sum(x_2power);
        # First, get alpha (principal error term):
        alpha = ( numpy.sum(y_obs*x_power) - y_sum*x_power_sum/N_grids ) / (x_2power_sum - x_power_sum*x_power_sum/N_grids);
        # Get asymptotic solution p0:
        y0 = (y_sum - alpha*x_power_sum)/N_grids;
        # Now evaluate the equation:
        eval_ = numpy.sum(y_obs*x_power*ln_x_) - y0*numpy.sum(x_power*ln_x_) - alpha*numpy.sum(x_2power*ln_x_);
    else:
        # Weighted approach:
        y_w_sum = numpy.sum(w_vector*y_obs);
        x_w_power_sum = numpy.sum(w_vector*x_power);
        x_w_2power_sum = numpy.sum(w_vector*x_2power);
        # First, get alpha (principal error term):
        alpha = ( numpy.sum(w_vector*y_obs*x_power) - y_w_sum*x_w_power_sum/N_grids ) / (x_w_2power_sum - x_w_power_sum*x_w_power_sum/N_grids);
        # Get asymptotic solution p0:
        y0 = (y_w_sum - alpha*x_w_power_sum)/N_grids;
        # Now evaluate the equation:
        eval_ = numpy.sum(w_vector*y_obs*x_power*ln_x_) - y0*numpy.sum(w_vector*x_power*ln_x_) - alpha*numpy.sum(w_vector*x_2power*ln_x_);
    return eval_;

def get_q_obs_leastSq(dX_vector, y_vector, q_min = -3.0, q_max = 3.0, q_step = 0.005 ):
    '''
    Function calculates q_obs based on equations in Appendix B.1 form [Eca and Hoekstra (2014)] paper.
    
    It does so by sweepeing q_obs from q_min to q_max in q_step steps ( evaluating get_function_value() ) and returning zero-crossings 
    for no and with weights (as defined by [Eca and Hoekstra (2014)]).
        Zero also counts when finding zero crossing: zero-crossing based on https://stackoverflow.com/questions/3843017/efficiently-detect-sign-changes-in-python.
    
    :param dX_vector: The vector with grids (independent variable).
    :param y_vector:The calculated FDTD values (dependent variable).
    :param q_min: Minimum value where q_obs is searched.
    :param q_max: Maximum value where q_obs is searched.
    :param q_step: The step of the zero-crossing search.
    
    :returns:
        2 vectors with q_obs as found with zero-crossing. The two vectors are not necessarily of equal length.
    '''
    q_obs_vector = numpy.arange(q_min, q_max, q_step);
    result_ = numpy.ones( len(q_obs_vector) );
    result_weighted = numpy.ones( len(q_obs_vector) );
    weights_eca = (1.0/dX_vector) / (numpy.sum(1.0/dX_vector));
    idx_ = 0;
    for q_obs_ in q_obs_vector:
        result_[idx_] = get_function_value(q_obs = q_obs_, x_vector = dX_vector, y_obs = y_vector, w_vector = None );
        result_weighted[idx_] = get_function_value(q_obs = q_obs_, x_vector = dX_vector, y_obs = y_vector, w_vector = weights_eca );
        idx_= idx_ + 1;
    # Zero crossing:
    zero_crossings = numpy.where(numpy.diff(numpy.signbit(result_)))[0];
    zero_crossings_wighted = numpy.where(numpy.diff(numpy.signbit(result_weighted)))[0];
    q_zero_crossing =  q_obs_vector[zero_crossings];
    q_zero_crossing_weights =  q_obs_vector[zero_crossings_wighted];
    return q_zero_crossing, q_zero_crossing_weights;

def plot_q_obs_methods(N_freqs, frequency_vector, PRTF_ALL_GRIDS_SRC_REC_interpolated, refinement_ratio, dir_idx = 0, q_step = 0.005):
    '''
    Function plots q_obs for various methods to obtain it.
    
    :param dir_idx: The direction index for which the plot is done. dir_idx<N_dirs.
    :param N_freqs: The number of frequencies in the analysis.
    :param frequency_vector: The frequency vector in Hz.
    :param PRTF_ALL_GRIDS_SRC_REC_interpolated: The PRTFs for which these are calculated. Of size [Ndirs, N_freq, N_grids].
    :param refinement_ratio: The grid refinement ratio ( refinement_ratio = x_(i+1) / x_i ) in the convergence study.
    
    :param q_step: The step of the zero-crossing search to find q_obs from the paper from Eca and Hoekstra (2014). 
    '''
    q_obs_eca_zero_crossing = [None] * N_freqs;
    q_obs_eca_zero_crossing_weighted = [None] * N_freqs;
    q_obs_Box_Tidwell = numpy.zeros(N_freqs);
    q_obs_Box_Tidwell_weighted = numpy.zeros(N_freqs);
    
    plt.figure(1);
    plt.title("q_obs based on 3 grids. dir_idx = " + str(dir_idx));
    plt.figure(2);
    plt.title("q_obs based on Minimization of least squares. dir_idx = " + str(dir_idx));
    for freq_idx in range(0,N_freqs):
        print "[" + str(freq_idx) + "] f = " + str(frequency_vector[freq_idx]);
        PRTF_vector = numpy.abs(PRTF_ALL_GRIDS_SRC_REC_interpolated[:, dir_idx, freq_idx]);
        PRTF_vector_complex = PRTF_ALL_GRIDS_SRC_REC_interpolated[:, dir_idx, freq_idx];
        #=======================================================================
        # Eca and Hoekstra:
        #=======================================================================
        q_zero_crossing, q_zero_crossing_weights =  get_q_obs_leastSq(dX_vector = dX_vector, y_vector = PRTF_vector, q_min = -10.0, q_max = 10.0, q_step = q_step );
        q_obs_eca_zero_crossing[freq_idx] = q_zero_crossing.tolist();
        q_obs_eca_zero_crossing_weighted[freq_idx] = q_zero_crossing_weights.tolist();
        #=======================================================================
        # Regular r123:
        #=======================================================================
        q_3Grids, q_f_all = get_q_obs_r123(y_obs = PRTF_vector, r=refinement_ratio, N_grids = N_grids);
        # On the complex values:
        q_3Grids_complex, q_f_all_complex = get_q_obs_r123_abs(y_obs = PRTF_vector_complex, r=refinement_ratio, N_grids = N_grids);
        #=======================================================================
        # Box-Tidwell:
        #=======================================================================
        alpha_, _ = BoxTidwell(y_in = PRTF_vector, x_in = dX_vector, alpha0 = 1.0);
        q_obs_Box_Tidwell[freq_idx] = alpha_;
        alpha_, _ = BoxTidwell_weighted(y_in = PRTF_vector, x_in = dX_vector, weights_ = (1.0/dX_vector) / (numpy.sum(1.0/dX_vector)), alpha0 = 1.0);
        q_obs_Box_Tidwell_weighted[freq_idx] = alpha_;
        #=======================================================================
        # Plot:
        #=======================================================================
        plt.figure(1);
        bool_vector = q_3Grids != 666.66;
        bool_vector_complex = q_3Grids_complex != 666.66;
        f_ = numpy.ones(4)*frequency_vector[freq_idx];
        if freq_idx == 0:
            # Regular:
            plt.scatter(f_[bool_vector], q_3Grids[bool_vector], marker = 'o', color = 'm', label = 'All q_obs - r123');
            plt.scatter(numpy.ones(2)*frequency_vector[freq_idx], q_f_all, marker = '*', color = 'r', label = 'q_obs r123 farthest all');
            # Complex:
            plt.scatter(f_[bool_vector_complex], q_3Grids_complex[bool_vector_complex], marker = 'o', color = 'b', label = 'All q_obs - r123 [complex]');
            plt.scatter(numpy.ones(2)*frequency_vector[freq_idx], q_f_all_complex, marker = '*', color = 'g', label = 'q_obs r123 farthest all [complex]');
        else:
            # Regular:
            plt.scatter(f_[bool_vector], q_3Grids[bool_vector], marker = 'o', color = 'm');
            plt.scatter(numpy.ones(2)*frequency_vector[freq_idx], q_f_all, marker = '*', color = 'r');
            # Complex:
            plt.scatter(f_[bool_vector_complex], q_3Grids_complex[bool_vector_complex], marker = 'o', color = 'b');
            plt.scatter(numpy.ones(2)*frequency_vector[freq_idx], q_f_all_complex, marker = '*', color = 'g');
        plt.xlabel('f [Hz]');
        plt.ylabel(r'$q_{obs}$ [symmetrical log]');
        plt.grid(True, which = 'both');
        plt.legend(loc='best', borderaxespad=1., fancybox=True, shadow=False);
        plt.xlim(0.0,20000.0);
        #plt.ylim(-3.0,3.0);
        plt.yscale('symlog');
        
        plt.figure(2);
        if freq_idx == 0:
            plt.scatter(numpy.ones( len(q_obs_eca_zero_crossing[freq_idx]) )*frequency_vector[freq_idx], q_obs_eca_zero_crossing[freq_idx], s = 5.0*numpy.ones(len(q_obs_eca_zero_crossing[freq_idx])), marker = 'o', color = 'b', label = 'Eca & Hoekstra vanilla');
            plt.scatter(numpy.ones( len(q_obs_eca_zero_crossing_weighted[freq_idx]) )*frequency_vector[freq_idx], q_obs_eca_zero_crossing_weighted[freq_idx], s = 5.0*numpy.ones(len(q_obs_eca_zero_crossing_weighted[freq_idx])), marker = 'o', color = 'g', label = 'Eca & Hoekstra weighted');
        else:
            plt.scatter(numpy.ones( len(q_obs_eca_zero_crossing[freq_idx]) )*frequency_vector[freq_idx], q_obs_eca_zero_crossing[freq_idx], s = 5.0*numpy.ones(len(q_obs_eca_zero_crossing[freq_idx])), marker = 'o', color = 'b');
            plt.scatter(numpy.ones( len(q_obs_eca_zero_crossing_weighted[freq_idx]) )*frequency_vector[freq_idx], q_obs_eca_zero_crossing_weighted[freq_idx], s = 5.0*numpy.ones(len(q_obs_eca_zero_crossing_weighted[freq_idx])), marker = 'o', color = 'g');
        plt.xlabel('f [Hz]');
        plt.ylabel(r'$q_{obs}$ from [Eca and Hoekstra (2014)]');
        plt.grid(True, which = 'both');
        plt.xlim(0.0,20000.0);
        
        plt.yscale('symlog');
    # Box-Tidwell plot:
    plt.figure(2);
    plt.scatter(frequency_vector[q_obs_Box_Tidwell is not None], q_obs_Box_Tidwell[q_obs_Box_Tidwell is not None], marker = '.', color = 'r', label = 'Box-Tidwell iteration');
    plt.scatter(frequency_vector[q_obs_Box_Tidwell_weighted is not None], q_obs_Box_Tidwell_weighted[q_obs_Box_Tidwell_weighted is not None], marker = 'x', color = 'm', label = 'Box-Tidwell iteration [weighted]');
    
    plt.legend(loc='best', borderaxespad=1., fancybox=True, shadow=False);
    plt.ylim(-10.1,10.1);
    plt.show();

def my_transcedental_eq(X, E_32, E_21, r_12, r_23):
    '''
    Function gives the following transcedental equation to recover q_obs for non-constant grid refinement ratios[Roy 2005]: 
    
    (P_3 - P_2) / (r_23^q - 1) = r_12^q* (P_2 - P_1) / (r_12^q - 1);
    
    Based on the example from [https://stackoverflow.com/questions/43051995/solving-a-system-of-transcendental-equations-with-python/43052679]
    
    :param X: The input
    :param E_32: The difference between results on grid 3 and grid 2: E_32 = P_3 - P_2
    :param E_21: The difference between results on grid 2 and grid 1: E_12 = P_2 - P_1
    :param r_12: The refinement ratio between grid 1 and grid 2: h_1=h_2/r_12
    :param r_23: The refinement ratio between grid 2 and grid 3: h_2=h_3/r_23
    '''
    return ( (1.0*E_32) / (r_23**X-1.0) - (r_12**X) * (1.0*E_21) / (r_12**X-1.0) );

def get_q_obs_r123_transcedental(y_obs, N_grids, fs_vector):
    '''
    Function obtains q_obs based on three grids - consecutive or at +- 2 index. 
    Compared to get_q_obs_r123() function, q_obs found assuming non-constant grid-refinement,
    via solving a transcedental equation - see [Roy 2005].
    
    :param y_obs: The observed vector - with absolute value (i.e., |FDTD| values for all the 6 grids).
    :param N_grids: The number of grids in the convergence.
    
    :param fs_vector: A vector with the sampling frequencies used used (consistent with the order of y_obs vector). 
            Grid refinement ratio will be computed based on this vector.
    '''
    # m1) based on 3 consecutive grids and average (seems to behave better as it is less sensitive if G3,G5,G7 are outliers):
    q_3Grids = numpy.zeros(N_grids - 2);
    q_3Grids_neg = numpy.zeros(N_grids - 2);
    for grid_idx in range(N_grids - 2):
        r_12 = 1.0*fs_vector[grid_idx+1]/fs_vector[grid_idx+2];
        r_23 = 1.0*fs_vector[grid_idx]/fs_vector[grid_idx+1];
        E_32 = 1.0*y_obs[grid_idx+2] - 1.0*y_obs[grid_idx+1];
        E_21 = 1.0*y_obs[grid_idx+1] - 1.0*y_obs[grid_idx];
        res_ = scipy.optimize.root(my_transcedental_eq, x0 = 1.0, args=(E_32, E_21, r_12, r_23));
        res_neg = scipy.optimize.root(my_transcedental_eq, x0 = -1.0, args=(E_32, E_21, r_12, r_23));
        if len(res_.x) == 0:
            print "No sol found..."
        if len(res_.x) >1:
            raise NameError("Multiple roots found for G[" + str(grid_idx) + "]!");
        q_3Grids[grid_idx] = res_.x[0];
        q_3Grids_neg[grid_idx] = res_neg.x[0];
        if math.isnan(q_3Grids[grid_idx]):
            q_3Grids[grid_idx] = 666.66;
    # m1.1) Same, but grids are more separated:
    r_12_furthest_1 = 1.0*fs_vector[2]/fs_vector[4];
    r_23_furthest_1 = 1.0*fs_vector[0]/fs_vector[2];
    E_32_furthest_1 = 1.0*y_obs[4] - 1.0*y_obs[2];
    E_21_furthest_1 = 1.0*y_obs[2] - 1.0*y_obs[0];
    res_furthest_1 = scipy.optimize.root(my_transcedental_eq, x0 = 1.0, args=(E_32_furthest_1, E_21_furthest_1, r_12_furthest_1, r_23_furthest_1));
    res_furthest_1_neg = scipy.optimize.root(my_transcedental_eq, x0 = -1.0, args=(E_32_furthest_1, E_21_furthest_1, r_12_furthest_1, r_23_furthest_1));
    if len(res_furthest_1.x) == 0:
        print "No sol found..."
    if len(res_furthest_1.x) >1:
        raise NameError("Multiple roots found for res_furthest_1!");
    q_furtherst_1 = res_furthest_1.x[0];
    q_furtherst_1_neg = res_furthest_1_neg.x[0];
    r_12_furthest_2 = 1.0*fs_vector[3]/fs_vector[5];
    r_23_furthest_2 = 1.0*fs_vector[1]/fs_vector[3];
    E_32_furthest_2 = 1.0*y_obs[5] - 1.0*y_obs[3];
    E_21_furthest_2 = 1.0*y_obs[3] - 1.0*y_obs[1];
    res_furthest_2 = scipy.optimize.root(my_transcedental_eq, x0 = 1.0, args=(E_32_furthest_2, E_21_furthest_2, r_12_furthest_2, r_23_furthest_2));
    res_furthest_2_neg = scipy.optimize.root(my_transcedental_eq, x0 = -1.0, args=(E_32_furthest_2, E_21_furthest_2, r_12_furthest_2, r_23_furthest_2));
    if len(res_furthest_2.x) == 0:
        print "No sol found..."
    if len(res_furthest_2.x) >1:
        raise NameError("Multiple roots found for res_furthest_2!");
    q_furtherst_2 = res_furthest_2.x[0];
    q_furtherst_2_neg = res_furthest_2_neg.x[0];
    q_farhtest_all = numpy.float64([q_furtherst_1, q_furtherst_2, q_furtherst_1_neg, q_furtherst_2_neg]);
    q_3Grids = numpy.hstack((q_3Grids, q_3Grids_neg));
    return q_3Grids, q_farhtest_all;

def get_q_obs_r123_abs_transcedental(y_obs, N_grids, fs_vector = None):
    '''
    Same as get_q_obs_r123() but computing the absolute value - can be used on complex input too. 
    Compared to get_q_obs_r123_abs() function, q_obs found assuming non-constant grid-refinement, 
    via solving a transcedental equation - see [Roy 2005].
    
    See get_q_obs_r123() for more parameters.
    :param y_obs: The observed COMPLEX vector (i.e., FDTD values for all the 6 grids).
    '''
    # m1) based on 3 consecutive grids and average (seems to behave better as it is less sensitive if G3,G5,G7 are outliers):
    q_3Grids = numpy.zeros(N_grids - 2);
    q_3Grids_neg = numpy.zeros(N_grids - 2);
    for grid_idx in range(N_grids - 2):
        r_12 = 1.0*fs_vector[grid_idx+1]/fs_vector[grid_idx+2];
        r_23 = 1.0*fs_vector[grid_idx]/fs_vector[grid_idx+1];
        E_32 = numpy.abs(1.0*y_obs[grid_idx+2] - 1.0*y_obs[grid_idx+1]);
        E_21 = numpy.abs(1.0*y_obs[grid_idx+1] - 1.0*y_obs[grid_idx]);
        res_ = scipy.optimize.root(my_transcedental_eq, x0 = 1.0, args=(E_32, E_21, r_12, r_23));
        res_neg = scipy.optimize.root(my_transcedental_eq, x0 = -1.0, args=(E_32, E_21, r_12, r_23));
        if len(res_.x) >1:
            raise NameError("Multiple roots found for G[" + str(grid_idx) + "]!");
        q_3Grids[grid_idx] = res_.x[0];
        q_3Grids_neg[grid_idx] = res_neg.x[0];
        if math.isnan(q_3Grids[grid_idx]):
            q_3Grids[grid_idx] = 666.66;
    # m1.1) Same, but grids are more separated:
    r_12_furthest_1 = 1.0*fs_vector[2]/fs_vector[4];
    r_23_furthest_1 = 1.0*fs_vector[0]/fs_vector[2];
    E_32_furthest_1 = numpy.abs(1.0*y_obs[4] - 1.0*y_obs[2]);
    E_21_furthest_1 = numpy.abs(1.0*y_obs[2] - 1.0*y_obs[0]);
    res_furthest_1 = scipy.optimize.root(my_transcedental_eq, x0 = 1.0, args=(E_32_furthest_1, E_21_furthest_1, r_12_furthest_1, r_23_furthest_1));
    res_furthest_1_neg = scipy.optimize.root(my_transcedental_eq, x0 = -1.0, args=(E_32_furthest_1, E_21_furthest_1, r_12_furthest_1, r_23_furthest_1));
    if len(res_furthest_1.x) >1:
        raise NameError("Multiple roots found for res_furthest_1!");
    q_furtherst_1 = res_furthest_1.x[0];
    q_furtherst_1_neg = res_furthest_1_neg.x[0];
    r_12_furthest_2 = 1.0*fs_vector[3]/fs_vector[5];
    r_23_furthest_2 = 1.0*fs_vector[1]/fs_vector[3];
    E_32_furthest_2 = numpy.abs(1.0*y_obs[5] - 1.0*y_obs[3]);
    E_21_furthest_2 = numpy.abs(1.0*y_obs[3] - 1.0*y_obs[1]);
    res_furthest_2 = scipy.optimize.root(my_transcedental_eq, x0 = 1.0, args=(E_32_furthest_2, E_21_furthest_2, r_12_furthest_2, r_23_furthest_2));
    res_furthest_2_neg = scipy.optimize.root(my_transcedental_eq, x0 = -1.0, args=(E_32_furthest_2, E_21_furthest_2, r_12_furthest_2, r_23_furthest_2));
    if len(res_furthest_2.x) >1:
        raise NameError("Multiple roots found for res_furthest_2!");
    q_furtherst_2 = res_furthest_2.x[0];
    q_furtherst_2_neg = res_furthest_2_neg.x[0];
    q_3Grids = numpy.hstack((q_3Grids, q_3Grids_neg));
    q_farhtest_all = numpy.float64([q_furtherst_1, q_furtherst_2, q_furtherst_1_neg, q_furtherst_2_neg]);
    return q_3Grids, q_farhtest_all;

def get_q_obs_r123(y_obs, r, N_grids, fs_vector = None):
    '''
    Function obtains q_obs based on three grids - consecutive or at +- 2 index.
    
    :param y_obs: The observed vector - with absolute value (i.e., |FDTD| values for all the 6 grids).
    :param r: The grid refinement ratio ( r = x_(i+1) / x_i ).
    :param N_grids: The number of grids in the convergence.
    
    :param fs_vector: A vector with the sampling frequencies used used. If not None, the average r value is used as refinement ratio. Otherwise r is used.
    
    :returns:
        :ret q_3Grids: q_obs based on 3 consecutive grids.
        :ret q_farhtest_all: q_obs based on 3 every-other grids.
        :ret R_3Grids: R=(f_1-f_2)/(f_2-f_3) based on 3 consecutive grids. Used to assess monotonic convergence from [347].
        :ret R_farhtest_all: R=(f_1-f_2)/(f_2-f_3) based on 3 every-other grids. Used to assess monotonic convergence from [347].
    '''
    if fs_vector is None:
        # m1) based on 3 consecutive grids and average (seems to behave better as it is less sensitive if G3,G5,G7 are outliers):
        q_3Grids = numpy.zeros(N_grids - 2);
        R_3Grids = numpy.zeros(N_grids - 2);
        for grid_idx in range(N_grids - 2):
            q_3Grids[grid_idx] = numpy.log( (1.0*y_obs[grid_idx+2] - 1.0*y_obs[grid_idx+1])/(1.0*y_obs[grid_idx+1] - 1.0*y_obs[grid_idx]) ) / numpy.log(r);
            R_3Grids[grid_idx] = (-1.0*y_obs[grid_idx+1] + 1.0*y_obs[grid_idx])/(-1.0*y_obs[grid_idx+2] + 1.0*y_obs[grid_idx+1]);
            if math.isnan(q_3Grids[grid_idx]):
                q_3Grids[grid_idx] = 666.66;
        # m1.1) Same, but grids are more separated:
        q_furtherst_1 = numpy.log( (1.0*y_obs[4] - 1.0*y_obs[2])/(1.0*y_obs[2] - 1.0*y_obs[0]) ) / numpy.log(r) / 2.0;
        q_furtherst_2 = numpy.log( (1.0*y_obs[5] - 1.0*y_obs[3])/(1.0*y_obs[3] - 1.0*y_obs[1]) ) / numpy.log(r) / 2.0;
        R_furtherst_1 = (-1.0*y_obs[2] + 1.0*y_obs[0])/(-1.0*y_obs[4] + 1.0*y_obs[2]);
        R_furtherst_2 = (-1.0*y_obs[3] + 1.0*y_obs[1])/(-1.0*y_obs[5] + 1.0*y_obs[3]);
        q_farhtest_all = numpy.float64([q_furtherst_1, q_furtherst_2]);
        R_farhtest_all = numpy.float64([R_furtherst_1, R_furtherst_2]);
    else:
        # m1) based on 3 consecutive grids and average (seems to behave better as it is less sensitive if G3,G5,G7 are outliers):
        q_3Grids = numpy.zeros(N_grids - 2);
        R_3Grids = numpy.zeros(N_grids - 2);
        for grid_idx in range(N_grids - 2):
            r_12 = 1.0*fs_vector[grid_idx+1]/fs_vector[grid_idx+2];
            r_23 = 1.0*fs_vector[grid_idx]/fs_vector[grid_idx+1];
            r = (r_12 + r_23) / 2.0; # AVERAGE
            #r = r_12;
            #r = r_23;
            print "\t" + str(r)
            q_3Grids[grid_idx] = numpy.log( (1.0*y_obs[grid_idx+2] - 1.0*y_obs[grid_idx+1])/(1.0*y_obs[grid_idx+1] - 1.0*y_obs[grid_idx]) ) / numpy.log(r);
            R_3Grids[grid_idx] = (-1.0*y_obs[grid_idx+1] + 1.0*y_obs[grid_idx])/(-1.0*y_obs[grid_idx+2] + 1.0*y_obs[grid_idx+1]);
            if math.isnan(q_3Grids[grid_idx]):
                q_3Grids[grid_idx] = 666.66;
        # m1.1) Same, but grids are more separated:
        r_12_furthest_1 = 1.0*fs_vector[2]/fs_vector[4];
        r_23_furthest_1 = 1.0*fs_vector[0]/fs_vector[2];
        r = (r_12_furthest_1 + r_23_furthest_1) / 2.0;# AVERAGE
        #r = r_12_furthest_1;
        #r = r_23_furthest_1;
        q_furtherst_1 = numpy.log( (1.0*y_obs[4] - 1.0*y_obs[2])/(1.0*y_obs[2] - 1.0*y_obs[0]) ) / numpy.log(r);
        R_furtherst_1 = (-1.0*y_obs[2] + 1.0*y_obs[0])/(-1.0*y_obs[4] + 1.0*y_obs[2]);
        r_12_furthest_2 = 1.0*fs_vector[3]/fs_vector[5];
        r_23_furthest_2 = 1.0*fs_vector[1]/fs_vector[3];
        r = (r_12_furthest_2 + r_23_furthest_2) / 2.0;# AVERAGE
        #r = r_12_furthest_2;
        #r = r_23_furthest_2;
        q_furtherst_2 = numpy.log( (1.0*y_obs[5] - 1.0*y_obs[3])/(1.0*y_obs[3] - 1.0*y_obs[1]) ) / numpy.log(r);
        R_furtherst_2 = (-1.0*y_obs[3] + 1.0*y_obs[1])/(-1.0*y_obs[5] + 1.0*y_obs[3]);
        q_farhtest_all = numpy.float64([q_furtherst_1, q_furtherst_2]);
        R_farhtest_all = numpy.float64([R_furtherst_1, R_furtherst_2]);
    return q_3Grids, q_farhtest_all, R_3Grids, R_farhtest_all;

def get_q_obs_r123_abs(y_obs, r, N_grids, fs_vector = None):
    '''
    Same as get_q_obs_r123() but computing the absolute value - can be used on complex input too. 
    
    See get_q_obs_r123() for more parameters.
    :param y_obs: The observed COMPLEX vector (i.e., FDTD values for all the 6 grids).
    '''
    if fs_vector is None:
        # m1) based on 3 consecutive grids and average (seems to behave better as it is less sensitive if G3,G5,G7 are outliers):
        q_3Grids = numpy.zeros(N_grids - 2);
        R_3Grids = numpy.zeros(N_grids - 2);
        for grid_idx in range(N_grids - 2):
            q_3Grids[grid_idx] = numpy.log( numpy.abs(1.0*y_obs[grid_idx+2] - 1.0*y_obs[grid_idx+1])/numpy.abs(1.0*y_obs[grid_idx+1] - 1.0*y_obs[grid_idx]) ) / numpy.log(r);
            R_3Grids[grid_idx] = numpy.abs(-1.0*y_obs[grid_idx+1] + 1.0*y_obs[grid_idx])/numpy.abs(-1.0*y_obs[grid_idx+2] + 1.0*y_obs[grid_idx+1]);
            if math.isnan(q_3Grids[grid_idx]):
                q_3Grids[grid_idx] = 666.66;
        # m1.1) Same, but grids are more separated:
        q_furtherst_1 = numpy.log( numpy.abs(1.0*y_obs[4] - 1.0*y_obs[2])/numpy.abs(1.0*y_obs[2] - 1.0*y_obs[0]) ) / numpy.log(r) / 2.0;
        q_furtherst_2 = numpy.log( numpy.abs(1.0*y_obs[5] - 1.0*y_obs[3])/numpy.abs(1.0*y_obs[3] - 1.0*y_obs[1]) ) / numpy.log(r) / 2.0;
        R_furtherst_1 = numpy.abs(-1.0*y_obs[2] + 1.0*y_obs[0]) /numpy.abs(-1.0*y_obs[4] + 1.0*y_obs[2]);
        R_furtherst_2 = numpy.abs(-1.0*y_obs[3] + 1.0*y_obs[1]) /numpy.abs(-1.0*y_obs[5] + 1.0*y_obs[3]);
        q_farhtest_all = numpy.float64([q_furtherst_1, q_furtherst_2]);
        R_farhtest_all = numpy.float64([R_furtherst_1, R_furtherst_2]);
    else:
        # m1) based on 3 consecutive grids and average (seems to behave better as it is less sensitive if G3,G5,G7 are outliers):
        q_3Grids = numpy.zeros(N_grids - 2);
        R_3Grids = numpy.zeros(N_grids - 2);
        for grid_idx in range(N_grids - 2):
            r_12 = 1.0*fs_vector[grid_idx+1]/fs_vector[grid_idx+2];
            r_23 = 1.0*fs_vector[grid_idx]/fs_vector[grid_idx+1];
            r = (r_12 + r_23) / 2.0;# AVERAGE
            #r = r_12;
            #r = r_23;
            q_3Grids[grid_idx] = numpy.log( numpy.abs(1.0*y_obs[grid_idx+2] - 1.0*y_obs[grid_idx+1])/numpy.abs(1.0*y_obs[grid_idx+1] - 1.0*y_obs[grid_idx]) ) / numpy.log(r);
            R_3Grids[grid_idx] = numpy.abs(-1.0*y_obs[grid_idx+1] + 1.0*y_obs[grid_idx])/numpy.abs(-1.0*y_obs[grid_idx+2] + 1.0*y_obs[grid_idx+1]);
            if math.isnan(q_3Grids[grid_idx]):
                q_3Grids[grid_idx] = 666.66;
        # m1.1) Same, but grids are more separated:
        r_12_furthest_1 = 1.0*fs_vector[2]/fs_vector[4];
        r_23_furthest_1 = 1.0*fs_vector[0]/fs_vector[2];
        r = (r_12_furthest_1 + r_23_furthest_1) / 2.0;# AVERAGE
        #r = r_12_furthest_1;
        #r = r_23_furthest_1;
        q_furtherst_1 = numpy.log( numpy.abs(1.0*y_obs[4] - 1.0*y_obs[2])/numpy.abs(1.0*y_obs[2] - 1.0*y_obs[0]) ) / numpy.log(r);
        R_furtherst_1 = numpy.abs(-1.0*y_obs[2] + 1.0*y_obs[0])/numpy.abs(-1.0*y_obs[4] + 1.0*y_obs[2]);
        r_12_furthest_2 = 1.0*fs_vector[3]/fs_vector[5];
        r_23_furthest_2 = 1.0*fs_vector[1]/fs_vector[3];
        r = (r_12_furthest_2 + r_23_furthest_2) / 2.0;# AVERAGE
        #r = r_12_furthest_2;
        #r = r_23_furthest_2;
        q_furtherst_2 = numpy.log( numpy.abs(1.0*y_obs[5] - 1.0*y_obs[3])/numpy.abs(1.0*y_obs[3] - 1.0*y_obs[1]) ) / numpy.log(r);
        R_furtherst_2 = numpy.abs(-1.0*y_obs[3] + 1.0*y_obs[1])/numpy.abs(-1.0*y_obs[5] + 1.0*y_obs[3]);
        q_farhtest_all = numpy.float64([q_furtherst_1, q_furtherst_2]);
        R_farhtest_all = numpy.float64([R_furtherst_1, R_furtherst_2]);
    return q_3Grids, q_farhtest_all, R_3Grids, R_farhtest_all;

def get_LeastSquares_Parameters_1st_AND_2nd_order(x_vector, y_obs, w_vector = None):
    '''
    Function calculates the model parameters for 1st and 2nd order convergence, i.e.:
        
        y[i] = beta_0 + beta1*x[i] + beta2*(x[i])^2
    
    It basically resolves the usual matrix equation Ax=b.
    
    See [Eca L., Hoekstra M., "A procedure for the estimation of the numerical uncertainty of CFD calculations based on grid refinement studies", Journal of Compuational Physics 262, p104-130, (2014)].
    
    :param x_vector: The vector with grid values (input or independent variable).
    :param y_obs: The FDTD calculation (or similar) - output or dependent variable.
    :param w_vector: Vector to weight the least squares. If None, non-weigted version will be calculated (i.e., w[i] = 1.0).
    
    :returns theta_12: Parameter vector for the model when the sum of weighted squares of the residuals are minimized.
        theta_12[0] = beta_0 = asymptotic solution
        theta_12[1] = beta_1 = principal error term for the first order convergence
        theta_12[2] = beta_2 = principal error term for the second order convergence
    '''
    if len(x_vector) != len(y_obs):
        print "#x = " + str(len(x_vector));
        print "#y = " + str(len(y_obs));
        raise NameError("Input length is different than output length...");
    if w_vector is None:
        w_vector = numpy.ones(len(x_vector));
    # Build matrix:
    A = numpy.zeros((3,3));
    # Row 1:
    A[0,0] = numpy.sum(w_vector);
    A[0,1] = numpy.sum(w_vector*x_vector);
    A[0,2] = numpy.sum(w_vector* numpy.power(x_vector,2.0));
    # Row 2:
    A[1,0] = A[0,1];
    A[1,1] = A[0,2];
    A[1,2] = numpy.sum(w_vector* numpy.power(x_vector,3.0));
    # Row 3:
    A[2,0] = A[1,1];
    A[2,1] = A[1,2];
    A[2,2] = numpy.sum(w_vector* numpy.power(x_vector,4.0));
    # The b vector:
    b = numpy.zeros(3)
    b[0] = numpy.sum(w_vector*y_obs);
    b[1] = numpy.sum(w_vector*y_obs*x_vector);
    b[2] = numpy.sum(w_vector*y_obs*numpy.power(x_vector,2.0));
    theta_12 = numpy.dot(scipy.linalg.inv(A), b);
    residuals = y_obs - model_1st_AND_2nd_order(theta = theta_12, x = x_vector);
    SS_res = numpy.sum( numpy.power(residuals,2.0) ) ;
    SS_tot = numpy.sum( numpy.power(y_obs - numpy.average(y_obs),2.0) );
    R2 = 1.0 - SS_res/SS_tot;#(numpy.power(theta_1[1],2.0)*numpy.sum( numpy.power(x_vector - numpy.average(x_vector), 2.0) )) / (numpy.sum( numpy.power(y_obs,2.0) ))
    return theta_12, R2, SS_res, SS_tot

def model_1st_order(theta, x):
    '''
    Function computes the model's function according to asymptotic expansion:
        
        p_FDTD = y0 + beta1*dX
        
        where:
            y0 = asymptotic solution
            beta1 = principal error term (slope of fit)
            
    :param x: Input data (independent variable).
    :param theta: Model parameter vector.
        y0 = theta[0]  # asymptotic solution
        beta1 = theta[1] # principal error term 
    '''
    return 1.0*theta[0] + theta[1]*x;

def get_CI_MeanOutput_1st_order(theta, x_vector, x0, y_obs, alpha = 0.05):
    '''
    Function calculates the CI on the average for an ordinary least squares prediction for the model p_FDTD = y0 + beta1*dX.
    
        Reference: [Montgomery D.C., Peck E.A., "Introduction to Linear Regression Analysis." 4th. Ed., John Wiley & Sons, (2006), pp.31-31].
    
    This can also be used in prediction, with caution.
    
    :param theta: The model parameter vector:
                y0 = theta[0]  # asymptotic solution
                beta1 = theta[1] # principal error term 
    :param x0: The value at which the interval is evaluated.
    :param x_vector: The vector with independent variables.
    :param y_obs: The vector with the observations (dependent variable).
    :param alpha: The confidence. The 100*(1.0-alpha)[%] interval will be calculated.
    
    :returns:
        :ret lower_bound: The lower bound for the average of the model ( 100*(1.0-alpha)[%] confidence interval ).
        :ret upper_bound: The upper bound for the average of the model ( 100*(1.0-alpha)[%] confidence interval ).
        :ret ms_Residuals: The standard error of the residuals.
        :ret variance_estimate: The matrix with estimated covariance. The standard errors are sqrt(variance_estimate) - the standard errors for model parameters are on the diagonal.
    '''
    residual_vector = y_obs - model_1st_order(theta, x_vector);
    N_obs = len(residual_vector); # number of measurements
    p = len(theta);             # number of parameters
    #fits_ = model_1st_order(theta, x);
    # Calculate the means squared error of the residuals (unbiased estimator of sigma^2, the real variance of the error in the model):
    ms_Residuals = numpy.sum( numpy.power(residual_vector,2.0) )/(1.0*(N_obs - p));
    x_avg = numpy.average(x_vector);
    S_xx = numpy.sum( numpy.power(x_vector - x_avg,2.0) );
    #Get t-value:
    (t_minus, t_plus) = stats.t.interval(alpha = (1.0 - alpha), df = N_obs - p);
    # Calculate full bound:
    lower_bound = model_1st_order(theta, x0) + t_minus*numpy.sqrt(ms_Residuals*(1.0/N_obs + numpy.power(1.0*x0-x_avg,2.0)/S_xx));
    upper_bound = model_1st_order(theta, x0) +  t_plus*numpy.sqrt(ms_Residuals*(1.0/N_obs + numpy.power(1.0*x0-x_avg,2.0)/S_xx));
    # Get approximate of covariance matrix (does not compute well!):
    X___ = numpy.ones((N_obs, p));
    if p==2:
        # The way to calculate for linear model[Montgomery D.C., Peck E.A., "Introduction to Linear Regression Analysis." 4th. Ed., John Wiley & Sons, (2006), pp.23]:
        #standard_errors_beta0 = ms_Residuals*(1.0/N_obs + x_avg**2/S_xx);
        #standard_errors_beta1 = ms_Residuals/S_xx;
        X___[:,1] = x_vector;
    else:
        for idx_ in range(1,p):
            X___[:,idx_] = x_vector[idx_-1,:];
    # Covariance matrix:
    C = scipy.linalg.inv( numpy.dot(X___.T, X___) ); # Inv also works...
    # Note statsmodel does this: pinv_X___, _ = pinv_extended(X___); C_pinv = numpy.dot(pinv_X___, pinv_X___.T); where X__ might be whitened for WLS()
    variance_estimate = ms_Residuals*C;
    return lower_bound, upper_bound, ms_Residuals, variance_estimate;

def get_CI_MeanOutput_MultiVariateRegression(theta, model_function, x_vector, x0, y_obs, alpha = 0.05):
    '''
    Function calculates the CI on the average for a multivariate least squares prediction for the data (x_i,y_i) fitted by a linear regression model. 
    
        Reference: [Montgomery D.C., Peck E.A., "Introduction to Linear Regression Analysis." 4th. Ed., John Wiley & Sons, (2006), pp.93-96].
    
        Here, the notation from [Montgomery] corresponds to:
            n corresponds here to N_obs
            p corresponds here to k
    
        Function designed for 1 observation per input. 
    
    This can also be used in prediction, with caution.
    
    :param theta: The model parameter vector:
                y0 = beta0 = theta[0]    # asymptotic solution
                beta1 = theta[1]         # principal error term for first term in the regression model
                ...
                betap = theta[p]         # principal error term
                 
    :param model_function: A function in the form model_function(theta, x_vector) which calculates the output given the input x_vector and model parameters theta.
    :param x0: The multi-dimensional value at which the interval is evaluated. Note x0[0] = 1 according to [Montgomery p94].
    :param x_vector: The vector with independent variables (no constant vector is present).
    :param y_obs: The vector with the observations (dependent variable).
    :param alpha: The confidence. The 100*(1.0-alpha)[%] interval will be calculated.
    
    :returns:
        :ret lower_bound: The lower bound for the average of the model ( 100*(1.0-alpha)[%] confidence interval ).
        :ret upper_bound: The upper bound for the average of the model ( 100*(1.0-alpha)[%] confidence interval ).
        :ret ms_Residuals: The standard error of the residuals.
        :ret Cov: The covariance matrix of the model.
    '''
    residual_vector = y_obs - model_function(theta, x_vector);
    N_obs = len(residual_vector);   # number of measurements
    k = len(theta);                 # number of parameters of the model
    x0 = numpy.asarray(x0);
    if len(x0) != k:
        raise NameError("x0 has a different length(" + str(x0) + ") than k (" + str(k) + ") - needs a 1 at the first location");
    # Get approximate of covariance matrix (does not compute well!):
    X___ = numpy.ones((N_obs, k));
    if k==2:
        X___[:,1] = x_vector;
    else:
        for idx_ in range(1,k):
            X___[:,idx_] = x_vector[:,idx_-1];
    # Calculate the means squared error of the residuals - from [Mongomery p81]:
    ms_Residuals = ( numpy.dot(y_obs.T, y_obs) - numpy.dot(numpy.dot(theta.T,X___.T), y_obs) ) / (1.0*(N_obs - k));
    # Manual: 
    #ms_Residuals = numpy.sum( numpy.power(residual_vector,2.0) )/(1.0*(N_obs - k));
    Cov = scipy.linalg.inv( numpy.dot(X___.T, X___) );
    #Get t-value:
    (t_minus, t_plus) = stats.t.interval(alpha = (1.0 - alpha), df = N_obs - k);
    # Calculate full bound:
    lower_bound = model_function(theta, x0[1::]) + t_minus*numpy.sqrt(ms_Residuals*( numpy.dot(numpy.dot(x0.T,Cov),x0) ));
    upper_bound = model_function(theta, x0[1::]) +  t_plus*numpy.sqrt(ms_Residuals*( numpy.dot(numpy.dot(x0.T,Cov),x0) ));
    return lower_bound, upper_bound, ms_Residuals, Cov;

def get_CI_MeanOutput_MultiVariateRegression_statsModel_deprecated(theta, y_value_at_x0, x0, y_obs, mse_resid, Cov, alpha = 0.05):
    '''
    Function calculates the CI on the average for a multivariate least squares prediction for the data (x_i,y_i) fitted by a linear regression model. 
    
        Reference: [Montgomery D.C., Peck E.A., "Introduction to Linear Regression Analysis." 4th. Ed., John Wiley & Sons, (2006), pp.93-96].
    
        Here, the notation from [Montgomery] corresponds to:
            n corresponds here to N_obs
            p corresponds here to k
    
        Function designed for 1 observation per input. 
        
    Tested OK with CI from univariate linear regression and manually calculating the CI as in get_CI_MeanOutput_MultiVariateRegression()!
    Tested OK with the CI (within the observation interval) coming directly from statsmodel data (based on [https://stackoverflow.com/questions/17559408/confidence-and-prediction-intervals-with-statsmodels])
                            st, data, ss2 = summary_table(results_12, alpha=0.05);
                            predict_mean_ci_low, predict_mean_ci_upp = data[:, 4:6].T;
    
    This can also be used in prediction, with caution.
    
    :param theta: The model parameter vector:
                y0 = beta0 = theta[0]    # asymptotic solution
                beta1 = theta[1]         # principal error term for first term in the regression model
                ...
                betap = theta[p]         # principal error term
                 
    :param y_value_at_x0: The model evaluated at x0.
    :param x0: The multi-dimensional value at which the interval is evaluated. Note x0[0] = 1 according to [Montgomery p94].
    :param y_obs: The vector with the observations (dependent variable).
    :param mse_resid: Mean square error of residuals (as coming from, e.g., results.mse_resid).
    :param Cov: The covariance matrix (as returned, e.g., from results.normalized_cov_params from statsmodel). It is numpy.pinv( numpy.dot(X_matrix.T, X_matrix) ).
    :param alpha: The confidence. The 100*(1.0-alpha)[%] interval will be calculated.
    
    :returns:
        :ret lower_bound: The lower bound for the average of the model ( 100*(1.0-alpha)[%] confidence interval ).
        :ret upper_bound: The upper bound for the average of the model ( 100*(1.0-alpha)[%] confidence interval ).
    '''
    N_obs = len(y_obs);   # number of measurements
    k = len(theta);                 # number of parameters of the model
    x0 = numpy.asarray(x0);
    #Get t-value:
    (t_minus, t_plus) = stats.t.interval(alpha = (1.0 - alpha), df = 1.0*N_obs - k);
    # Calculate full bound:
    lower_bound = y_value_at_x0 + t_minus*numpy.sqrt(mse_resid*( numpy.dot(numpy.dot(x0.T,Cov),x0) ));
    upper_bound = y_value_at_x0 +  t_plus*numpy.sqrt(mse_resid*( numpy.dot(numpy.dot(x0.T,Cov),x0) ));
    return lower_bound, upper_bound;

def get_CI_MeanOutput_MultiVariateRegression_statsModel(results_, x0, alpha = 0.05):
    '''
    Final version of CI of the mean of the model for results given by statsmodel. 
    
    :param results_: Results given by stats model. E.g., sm.OLS(endog = y_, exog = X_ ) and results_ = model.fit();.
    :param x0: Point where evaluation occurs. In the form [1.0, x_1, x_2...], i.e., it needs a 1.0 at the first location.
    :param alpha: Significance level.
    '''
    #Get t-value:
    (t_minus, t_plus) = stats.t.interval(alpha = (1.0 - alpha), df = len(results_.resid) - len(x0));
    # Calculate full bound:
    y_value_at_x0 = numpy.dot(results_.params.T, x0)
    lower_bound = y_value_at_x0 + t_minus*numpy.sqrt(results_.mse_resid*( numpy.dot(numpy.dot(x0.T, results_.normalized_cov_params),x0) ));
    upper_bound = y_value_at_x0 +  t_plus*numpy.sqrt(results_.mse_resid*( numpy.dot(numpy.dot(x0.T, results_.normalized_cov_params),x0) ));
    return lower_bound, upper_bound;

def get_LeastSquares_Parameters_1st_order(x_vector, y_obs, w_vector = None):
    '''
    Function calculates the model parameters for 1st order convergence:
        
        y[i] = beta_0 + beta1*x[i]
    
    This is the usual ordinary least-squares algorithm.
     
    :param x_vector: The vector with grid values (input or independent variable).
    :param y_obs: The FDTD calculation (or similar) - output or dependent variable.
    :param w_vector: Vector to weight the least squares. If None, non-weigted version will be calculated (i.e., w[i] = 1.0).
    
    :returns theta_1: Parameter vector for the model when the sum of weighted squares of the residuals are minimized.
        theta_1[0] = beta_0 = asymptotic solution (intercept)
        theta_1[1] = beta_1 = principal error term for the first order convergence (slope)
        :ret R2: the coefficient of determination.
        :ret R2_adj: the adjusted coefficient of determination (see [Mongomery p83]).
        :ret SS_res: The sums of sqares of the residuals.
        :ret SS_tot: The sum of squares of the output data (the total variability in the output data)
    '''
    if len(x_vector) != len(y_obs):
        print "#x = " + str(len(x_vector));
        print "#y = " + str(len(y_obs));
        raise NameError("Input length is different than output length...");
    if w_vector is None:
        w_vector = numpy.ones(len(x_vector));
    # Build matrix:
    A = numpy.zeros((2,2));
    # Row 1:
    A[0,0] = numpy.sum(w_vector);
    A[0,1] = numpy.sum(w_vector*x_vector);
    # Row 2:
    A[1,0] = A[0,1];
    A[1,1] = numpy.sum(w_vector* numpy.power(x_vector,2.0));
    # The b vector:
    b = numpy.zeros(2)
    b[0] = numpy.sum(w_vector*y_obs);
    b[1] = numpy.sum(w_vector*y_obs*x_vector);
    theta_1 = numpy.dot(scipy.linalg.inv(A), b);
    residuals = y_obs - model_1st_order(theta_1,x_vector);
    SS_res = numpy.sum( numpy.power(residuals,2.0) ) ;
    SS_tot = numpy.sum( numpy.power(y_obs - numpy.average(y_obs),2.0) );
    R2 = 1.0 - SS_res/SS_tot;#(numpy.power(theta_1[1],2.0)*numpy.sum( numpy.power(x_vector - numpy.average(x_vector), 2.0) )) / (numpy.sum( numpy.power(y_obs,2.0) ))
    R2_adj = 1.0 - (SS_res/(len(y_obs)- 2.0)) / (SS_tot/(len(y_obs)- 1.0) );     
    return theta_1, R2, R2_adj, SS_res, SS_tot

def BoxTidwell(y_in, x_in, alpha0 = 1.0, max_alpha = 100.0, rtol = 1E-3, max_N_steps= 1000):
    '''
    Function executes Box-Tidwell procedure[1], implemented based on section 5.4.2 from [2 pp.174-176].
    
    The function finds alpha for a fit like:
    
    y = beta0 + beta1*x^(alpha)
    
    Tested OK for:
        -) Example in [3 p74,p76]: BoxTidwell(y_in = numpy.asarray([10.0, 11.0, 16.0, 18.0, 25.0, 24.0, 26.0, 29.0, 31.0, 31.0, 34.0, 35.0, 36.0]), 
                                              x_in = numpy.asarray([5.0,  10.0,  8.0, 13.0, 15.0, 18.0, 22.0, 21.0, 19.0, 26.0, 25.0, 29.0, 28.0 ]), 
                                              alpha0 = 1.0);
        -) Examples given by [1]:
                y = 10.0+x^0.5:     BoxTidwell(y_in = 10.0+numpy.sqrt(dX_vector), x_in = dX_vector, alpha0 = 2.0);
                y = 10.0+x^-1:      BoxTidwell(y_in = 10.0+1.0/dX_vector, x_in = dX_vector, alpha0 = 1.0);
                y = 10.0+x^-2:      BoxTidwell(y_in = 10.0+numpy.power(dX_vector,-2.0), x_in = dX_vector, alpha0 = 1.0);
                y = 10.0+ln(x):     BoxTidwell(y_in = 10.0+numpy.log(dX_vector), x_in = dX_vector, alpha0 = 1.0);
    
    References:
        [1] Box G.E.P., Tidwell P.W., "Transformation of the Independent Variables", Technometrics, 4(4), (1962)
        [2] Mongomery D.C., Peck E.A., Bining G.G., "Introduction to Linear Regression Analysis", 4th ed, (2006)
        [3] Ryan T.P., "Modern Regression Methods", 1st ed, John Wiley and Sons, (1997)
        [4] car R package - source code for boxTidwell ( https://rdrr.io/cran/car/src/R/boxTidwell.R )
            To cite a book for this, cite [5] Fox J., Weisberg S., "An R Companion to applied regression", 2nd Edition, SAGE, (2011) - section 6.4.2 pp.312-314
            
    :param y_in: The dependent variable vector (observations).
    :param x_in: The independent variable vector.
    :param alpha0: The initial guess for alpha. [2 p174] recommends alpha0 = 1.0.
    :param max_alpha: If |alpha| is greater than this, it means the algorithm is diverging and returns Nan.
    
    :param rtol: Relative change in alpha which we accept as good enough.
    :param max_N_steps: Maximum number of steps.
    '''
    # Do checks first:
    if (x_in<0.0).any():
        raise NameError("Input (x) must be positive!");
    alpha_vector = numpy.zeros(max_N_steps+1);
    alpha_vector[0] = alpha0;
    alpha = alpha0;
    x1 = x_in;
    step_idx = 0;
    while (step_idx<max_N_steps and (numpy.abs((alpha - alpha_vector[step_idx-1])/(alpha + rtol)) > rtol) ):
        #print "\t[" + str(step_idx) + "] -  alpha = " + str(alpha);
        # STEP:
        ########################
        # 1) fit y=beta0 + beta1*x^alpha
        x1 = numpy.power(x_in, alpha);
        if numpy.abs(alpha)>max_alpha:
            warnings.warn("Box-Tidwell is diverging! Returning Nones...")
            #return float('NaN'), float('NaN');
            return None, None;
        X_step1 = sm.add_constant( x1 );
        model_step1 = sm.OLS(endog = y_in,  exog = X_step1 );
        results_step1 = model_step1.fit();
        beta1 = results_step1.params[1];
        # 2) define w=x^alpha*ln(x^alpha)=x'*ln(x') [2 p176,p176]
        w = x1*numpy.log( x1 );
        # 3) fit y=beta2+beta3*x^(alpha) + gamma*w
        X_step3 = sm.add_constant(numpy.column_stack( (w, x1)) );
        model_step3 = sm.OLS(endog = y_in,  exog = X_step3 );
        results_step3 = model_step3.fit();
        gamma = results_step3.params[1];
        # 4) Update alpha (note the update is inspired from [4,5] which incorporates alpha_previous in alpha for 
        #    when x1 is calculated as x1 = (x^(alpha_previous))^(alpha) - see comment in [3 p76]): 
        alpha = alpha*(1.0 + gamma / beta1);
        step_idx = step_idx +1;
        alpha_vector[step_idx] = alpha;
    # Reshape:
    alpha_vector = alpha_vector[0:step_idx];
    return alpha, alpha_vector;

def BoxTidwell_weighted(y_in, x_in, weights_, alpha0 = 1.0, max_alpha = 20.0, rtol = 1E-3, max_N_steps= 1000):
    '''
    Same as BoxTidwell(), but using weighted least squares.
    
    See BoxTidwell() for more details.
    
    :param weights_: The weights used in the WLS. 
    '''
    # Do checks first:
    if (x_in<0.0).any():
        raise NameError("Input (x) must be positive!");
    alpha_vector = numpy.zeros(max_N_steps+1);
    alpha_vector[0] = alpha0;
    alpha = alpha0;
    x1 = x_in;
    step_idx = 0;
    while (step_idx<max_N_steps and (numpy.abs((alpha - alpha_vector[step_idx-1])/(alpha + rtol)) > rtol) ):
        #print "\t[" + str(step_idx) + "] -  alpha = " + str(alpha);
        # STEP:
        ########################
        # 1) fit y=beta0 + beta1*x^alpha
        x1 = numpy.power(x_in, alpha);
        if numpy.abs(alpha)>max_alpha:
            warnings.warn("Box-Tidwell is diverging! Returning Nones...")
            #return float('NaN'), float('NaN');
            return None, None;
        X_step1 = sm.add_constant( x1 );
        model_step1 = sm.WLS(endog = y_in,  exog = X_step1, weights = weights_ );
        results_step1 = model_step1.fit();
        beta1 = results_step1.params[1];
        # 2) define w=x^alpha*ln(x^alpha)=x'*ln(x') [2 p176,p176]
        w = x1*numpy.log( x1 );
        # 3) fit y=beta2+beta3*x^(alpha) + gamma*w
        X_step3 = sm.add_constant(numpy.column_stack( (w, x1)) );
        model_step3 = sm.WLS(endog = y_in,  exog = X_step3, weights = weights_ );
        results_step3 = model_step3.fit();
        gamma = results_step3.params[1];
        # 4) Update alpha (note the update is inspired from [4] which incorporates alpha_previous in alpha for 
        #    when x1 is calculated as x1 = (x^(alpha_previous))^(alpha) - see comment in [3 p76]): 
        alpha = alpha*(1.0 + gamma / beta1);
        step_idx = step_idx +1;
        alpha_vector[step_idx] = alpha;
    # Reshape:
    alpha_vector = alpha_vector[0:step_idx];
    return alpha, alpha_vector;

def BootstrapPairs_get_asymptotic_solution_1stOrderRegression( data_in ):
    '''
    Function returns the asymptotic solution (y0) for a first-order or second-order linear regression. Function designed to be used with 
    boostrapping and BCA estimates. Here, paris are bootstrapped.
    
    Assumed model is y=y0+alpha*dX or similar (e.g., a second order model works just as fine y=y0+alpha*dX^2).
    
    :param data_in: The data of size (N_samples, 2) which must contain dX vector in the first column and the y_obs in the second column.
    
    :returns: The asymptotic solution y0 for this model.
    '''
    # Check if all data are the same (this can happen due to sampling and since we have a small number of samples):
    if numpy.all(data_in[:,0] == data_in[0,0]):
        return data_in[0,1]; # Just return any y - no regression needed
    dX__ = data_in[:,0];
    y_obs = data_in[:,1];
    X__1 = sm.add_constant(dX__);
    model = sm.OLS(endog = y_obs,  exog = X__1 );
    results__1 = model.fit(method="pinv");
    # Calculate asymptotic solution:
    x_0 = numpy.float64([1.0, 0.0])
    return numpy.dot(results__1.params.T, x_0);

def BootstrapPairs_Weighted_get_asymptotic_solution_1stOrderRegression( data_in ):
    '''
    Same as BootstrapPairs_get_asymptotic_solution_1stOrderRegression() but now
    for a weighted least squares. 
    
    See BootstrapPairs_get_asymptotic_solution_1stOrderRegression() for more details.
    '''
    # Check if all data are the same (this can happen due to sampling and since we have a small number of samples):
    if numpy.all(data_in[:,0] == data_in[0,0]):
        return data_in[0,1]; # Just return any y - no regression needed
    dX__ = data_in[:,0];
    # Build weights vector according to Eca and Hoekstra (2014):
    weights_ = (1.0/dX__) / (numpy.sum(1.0/dX__));
    y_obs = data_in[:,1];
    X__1 = sm.add_constant(dX__);
    model = sm.WLS(endog = y_obs,  exog = X__1, weights = weights_ );
    results__1 = model.fit(method="pinv");
    # Calculate asymptotic solution:
    x_0 = numpy.float64([1.0, 0.0])
    return numpy.dot(results__1.params.T, x_0);

def BootstrapPairs_Weighted_get_asymptotic_solution_2ndOrderRegression( data_in ):
    '''
    Same as BootstrapPairs_Weighted_get_asymptotic_solution_1stOrderRegression() 
    but for second order regression model - the weights needs adjustment.
    '''
    # Check if all data are the same (this can happen due to sampling and since we have a small number of samples):
    if numpy.all(data_in[:,0] == data_in[0,0]):
        return data_in[0,1]; # Just return any y - no regression needed
    dX__ = data_in[:,0];
    X_vec = numpy.sqrt(dX__);
    # Build weights vector according to Eca and Hoekstra (2014):
    weights_ = (1.0/X_vec) / (numpy.sum(1.0/X_vec));
    y_obs = data_in[:,1];
    X__1 = sm.add_constant(dX__);
    model = sm.WLS(endog = y_obs,  exog = X__1, weights = weights_ );
    results__1 = model.fit(method="pinv");
    # Calculate asymptotic solution:
    x_0 = numpy.float64([1.0, 0.0])
    return numpy.dot(results__1.params.T, x_0);

def BootstrapPairs_get_asymptotic_solution_12OrderRegression( data_in ):
    '''
    Function returns the asymptotic solution (y0) for a first and second-order linear regression. Function designed to be used with 
    boostrapping and BCA estimates. Here, paris are bootstrapped.
    
    Assumed model is y=y0+beta1*dX++beta2*dX^2.
    
    :param data_in: The data of size (N_samples, 2) which must contain dX vector in the first column and the y_obs in the second column.
    
    :returns: The asymptotic solution y0 for thei model. 
    '''
    # Check if all data are the same (this can happen due to sampling and since we have a small number of samples):
    if numpy.all(data_in[:,0] == data_in[0,0]):
        return data_in[0,1]; # Just return any y - no regression needed
    dX__12 = data_in[:,0:2];
    y_obs_12 = data_in[:,2];
    X__12 = sm.add_constant(dX__12);
    model = sm.OLS(endog = y_obs_12,  exog = X__12 );
    results__1 = model.fit(method="pinv");
    # Calculate asymptotic solution:
    x_0 = numpy.float64([1.0, 0.0, 0.0])
    return numpy.dot(results__1.params.T, x_0);

def BootstrapPairs_Weighted_get_asymptotic_solution_12OrderRegression( data_in ):
    '''
    Same as BootstrapPairs_get_asymptotic_solution_12OrderRegression() but now
    for a weighted least squares. 
    
    See BootstrapPairs_get_asymptotic_solution_12OrderRegression() for more details.
    '''
    # Check if all data are the same (this can happen due to sampling and since we have a small number of samples):
    if numpy.all(data_in[:,0] == data_in[0,0]):
        return data_in[0,1]; # Just return any y - no regression needed
    dX__12 = data_in[:,0:2];
    dX__ = data_in[:,0];
    # Build weights vector according to Eca and Hoekstra (2014):
    weights_ = (1.0/dX__) / (numpy.sum(1.0/dX__));
    y_obs_12 = data_in[:,2];
    X__12 = sm.add_constant(dX__12);
    model_12w = sm.WLS(endog = y_obs_12,  exog = X__12, weights = weights_ );
    results__12w = model_12w.fit(method="pinv");
    # Calculate asymptotic solution:
    x_0 = numpy.float64([1.0, 0.0, 0.0])
    return numpy.dot(results__12w.params.T, x_0);

def BootstrapResiduals_get_asymptotic_solution( data_in ):
    '''
    Function returns the asymptotic solution (y0) for a linear regression model (due to scikit.bootstrap way of doing things, the regression model
    must be a global class). Function designed to be used with boostrapping and BCA estimates. Here, residuals are bootstrapped.
    
    Assumed model is y=y0+model*x.
    
    Reference: 
        [512] Montgomery D.C., Peck E.A., "Introduction to Linear Regression Analysis." 4th. Ed., John Wiley & Sons, pp.494, (2006)
        [2] https://stackoverflow.com/questions/33678543/finding-indices-of-matches-of-one-array-in-another-array, @12 Nov 2018
    
    :param data_in: The bootstrapped residual vector.
    
    :returns: The asymptotic solution y0 for this model.
    '''
    global Class_results;
    # Find indexes - based on [2]
    sort_idx = Class_results.residuals.argsort();
    indexes_ = sort_idx[numpy.searchsorted(Class_results.residuals, data_in, sorter = sort_idx)];
    #print "[" + str(i__) + "] DEBUG - data_in (residuals):" + str(data_in);
    #print "\t\t DEBUG - data_in (INDEXES):" + str(indexes_);
    # Build x0:
    x_0 = numpy.zeros( len(Class_results.beta) );
    x_0[0] = 1.0;
    # Find if Jackknife sample or bootstrap sample (it can be done generically without if...):
    if len(data_in) == len(Class_results.residuals) - 1:
        Y_ = numpy.dot(Class_results.X_[indexes_], Class_results.beta) + data_in;
        model = sm.OLS(endog = Y_,  exog = Class_results.X_[indexes_] );
        results__ = model.fit(method="pinv");
    else:
        Y_ = numpy.dot(Class_results.X_, Class_results.beta) + data_in;
        model = sm.OLS(endog = Y_,  exog = Class_results.X_ );
        results__ = model.fit(method="pinv");
    return numpy.dot(results__.params.T, x_0);

def BootstrapResiduals_Weighted_get_asymptotic_solution( data_in ):
    '''
    Same as BootstrapResiduals_get_asymptotic_solution() but for a weighted least squares. 
    
    Weights are passed from the global Class_results class.
    
    See BootstrapResiduals_get_asymptotic_solution() for more information.
    '''
    global Class_results;
    # Find indexes - based on [2]
    sort_idx = Class_results.residuals.argsort();
    indexes_ = sort_idx[numpy.searchsorted(Class_results.residuals, data_in, sorter = sort_idx)];
    #print "[" + str(i__) + "] DEBUG - data_in (residuals):" + str(data_in);
    #print "\t\t DEBUG - data_in (INDEXES):" + str(indexes_);
    # Build x0:
    x_0 = numpy.zeros( len(Class_results.beta) );
    x_0[0] = 1.0;
    # Find if Jackknife sample or bootstrap sample (it can be done generically without if...):
    if len(data_in) == len(Class_results.residuals) - 1:
        Y_ = numpy.dot(Class_results.X_[indexes_], Class_results.beta) + data_in;
        model_w = sm.WLS(endog = Y_,  exog = Class_results.X_[indexes_], weights = Class_results.weights[indexes_] );
        results__w = model_w.fit(method="pinv");
    else:
        Y_ = numpy.dot(Class_results.X_, Class_results.beta) + data_in;
        model_w = sm.WLS(endog = Y_,  exog = Class_results.X_, weights = Class_results.weights );
        results__w = model_w.fit(method="pinv");
    return numpy.dot(results__w.params.T, x_0);

def get_ALL_Regression_data(root_dir_, f_max_ = 24E3+1E-3, B_samples = 5000, alpha_ = 0.05, verbose_flag = False):
    '''
    Function retrieves all data for the regression models. If data does not exist, it calculates the data.
    
    :param root_dir_: The root directory where the file with the regression data is searched.
    :param f_max_: The maximum frequency where the analysis is conducted.
    :param B_samples: The number of Bootstrap samples.
    :param alpha_: The confidence level for confidence interval (CI) building.
    :param verbose_flag: Boolean. If True, status is printed.
    '''
    hdf5_fn = "Regression_data_ALL.hdf5";
    models = ["1st order model: y0 + C1*dX", "1st order model [WEIGHTED]: y0 + C1*dX", 
              "2nd order model: y0 + C2*dX^2", "2nd order model [WEIGHTED]: y0 + C2*dX^2", 
              "1-2 order model: y0 + C1*dX + C2*dX^2", "1-2 order model [WEIGHTED]: y0 + C1*dX + C2*dX^2", 
              "m1 order model: y0 + C1*dX^(-1)", "m1 order model [WEIGHTED]: y0 + C1*dX^(-1)",
              "1m1 order model: y0 + C1*dX + C2*dX^(-1)", "1m1 order model [WEIGHTED]: y0 + C1*dX + C2*dX^(-1)",
              ################################# Non Interpolated data:###########################################
              "NON-INTERP 1st order model: y0 + C1*dX", "NON-INTERP 1st order model [WEIGHTED]: y0 + C1*dX",
              "REC-ONLY-INTERP 1st order model: y0 + C1*dX", "REC-ONLY-INTERP 1st order model [WEIGHTED]: y0 + C1*dX"
              ];
    groups_ok = [False, False, False, False, False, False, False, False, False, False, False, False, False, False]; # If group is found in hdf5...
    #===========================================================================
    # Reading data:
    #===========================================================================
    if os.path.exists( root_dir_ + os.sep + hdf5_fn):
        print "" + print_colors.FG_GREEN + "hdf5 file found ... " + print_colors.END + " Checking groups..." + "";
        # Check that all models are inside:
        groups_hdf5 = convergence_IO.get_groups_hdf5(file_dir = root_dir_, file_name = hdf5_fn);
        ############################################# 1st oder:
        if models[0] in groups_hdf5:
            groups_ok[0] = True;
            print "" + print_colors.BG_GREEN + "1st order REGRESSION DATA hdf5 found ... " + print_colors.END + " Reading Regression data..." + "";
            N_grids, N_dirs, N_freqs, Data_1st_order = convergence_IO.read_RegressionData_Model(file_dir = root_dir_, file_name = hdf5_fn, model_name = models[0]);
        else:
            print "" + print_colors.BG_RED + "1st order REGRESSION DATA not found... " + print_colors.END + " Reading PRTF data and writing..." + "";
        ############################################# 1st oder [WEIGHTED]:
        if models[1] in groups_hdf5:
            groups_ok[1] = True;
            print "" + print_colors.BG_GREEN + "1st order [WEIGHTED] REGRESSION DATA hdf5 found ... " + print_colors.END + " Reading Regression data..." + "";
            N_grids_, N_dirs_, N_freqs_, Data_1st_order_w = convergence_IO.read_RegressionData_Model(file_dir = root_dir_, file_name = hdf5_fn, model_name = models[1]);
        else:
            print "" + print_colors.BG_RED + "1st order [WEIGHTED] REGRESSION DATA not found... " + print_colors.END + " Reading PRTF data and writing..." + "";
        ############################################# 2nd oder:
        if models[2] in groups_hdf5:
            groups_ok[2] = True;
            print "" + print_colors.BG_GREEN + "2nd order REGRESSION DATA hdf5 found ... " + print_colors.END + " Reading Regression data..." + "";
            N_grids_, N_dirs_, N_freqs_, Data_2nd_order = convergence_IO.read_RegressionData_Model(file_dir = root_dir_, file_name = hdf5_fn, model_name = models[2]);
        else:
            print "" + print_colors.BG_RED + "2nd order REGRESSION DATA not found... " + print_colors.END + " Reading PRTF data and writing..." + "";
        ############################################# 2nd oder [WEIGHTED]:
        if models[3] in groups_hdf5:
            groups_ok[3] = True;
            print "" + print_colors.BG_GREEN + "2nd order [WEIGHTED] REGRESSION DATA hdf5 found ... " + print_colors.END + " Reading Regression data..." + "";
            N_grids_, N_dirs_, N_freqs_, Data_2nd_order_w = convergence_IO.read_RegressionData_Model(file_dir = root_dir_, file_name = hdf5_fn, model_name = models[3]);
        else:
            print "" + print_colors.BG_RED + "2nd order [WEIGHTED] REGRESSION DATA not found... " + print_colors.END + " Reading PRTF data and writing..." + "";
        ############################################# 1-2 oder:
        if models[4] in groups_hdf5:
            groups_ok[4] = True;
            print "" + print_colors.BG_GREEN + "1-2 order REGRESSION DATA hdf5 found ... " + print_colors.END + " Reading Regression data..." + "";
            N_grids_, N_dirs_, N_freqs_, Data_12_order = convergence_IO.read_RegressionData_Model(file_dir = root_dir_, file_name = hdf5_fn, model_name = models[4]);
        else:
            print "" + print_colors.BG_RED + "1-2 order REGRESSION DATA not found... " + print_colors.END + " Reading PRTF data and writing..." + "";
        ############################################# 2nd oder [WEIGHTED]:
        if models[5] in groups_hdf5:
            groups_ok[5] = True;
            print "" + print_colors.BG_GREEN + "1-2 order [WEIGHTED] REGRESSION DATA hdf5 found ... " + print_colors.END + " Reading Regression data..." + "";
            N_grids_, N_dirs_, N_freqs_, Data_12_order_w = convergence_IO.read_RegressionData_Model(file_dir = root_dir_, file_name = hdf5_fn, model_name = models[5]);
        else:
            print "" + print_colors.BG_RED + "1-2 order [WEIGHTED] REGRESSION DATA not found... " + print_colors.END + " Reading PRTF data and writing..." + "";
        ############################################# -1 oder:
        if models[6] in groups_hdf5:
            groups_ok[6] = True;
            print "" + print_colors.BG_GREEN + "-1 order REGRESSION DATA hdf5 found ... " + print_colors.END + " Reading Regression data..." + "";
            N_grids_, N_dirs_, N_freqs_, Data_m1_order = convergence_IO.read_RegressionData_Model(file_dir = root_dir_, file_name = hdf5_fn, model_name = models[6]);
        else:
            print "" + print_colors.BG_RED + "-1 order REGRESSION DATA not found... " + print_colors.END + " Reading PRTF data and writing..." + "";
        ############################################# -1 oder [WEIGHTED] :
        if models[7] in groups_hdf5:
            groups_ok[7] = True;
            print "" + print_colors.BG_GREEN + "-1 order [WEIGHTED] REGRESSION DATA hdf5 found ... " + print_colors.END + " Reading Regression data..." + "";
            N_grids_, N_dirs_, N_freqs_, Data_m1_order_w = convergence_IO.read_RegressionData_Model(file_dir = root_dir_, file_name = hdf5_fn, model_name = models[7]);
        else:
            print "" + print_colors.BG_RED + "-1 order [WEIGHTED] REGRESSION DATA not found... " + print_colors.END + " Reading PRTF data and writing..." + "";
        ############################################# 1,-1 oder:
        if models[8] in groups_hdf5:
            groups_ok[8] = True;
            print "" + print_colors.BG_GREEN + "1,-1 order REGRESSION DATA hdf5 found ... " + print_colors.END + " Reading Regression data..." + "";
            N_grids_, N_dirs_, N_freqs_, Data_1m1_order = convergence_IO.read_RegressionData_Model(file_dir = root_dir_, file_name = hdf5_fn, model_name = models[8]);
        else:
            print "" + print_colors.BG_RED + "1,-1 order REGRESSION DATA not found... " + print_colors.END + " Reading PRTF data and writing..." + "";
        ############################################# 1,-1 oder [WEIGHTED] :
        if models[9] in groups_hdf5:
            groups_ok[9] = True;
            print "" + print_colors.BG_GREEN + "1,-1 order [WEIGHTED] REGRESSION DATA hdf5 found ... " + print_colors.END + " Reading Regression data..." + "";
            N_grids_, N_dirs_, N_freqs_, Data_1m1_order_w = convergence_IO.read_RegressionData_Model(file_dir = root_dir_, file_name = hdf5_fn, model_name = models[9]);
        else:
            print "" + print_colors.BG_RED + "1,-1 order [WEIGHTED] REGRESSION DATA not found... " + print_colors.END + " Reading PRTF data and writing..." + "";
        ############################################# NON-INTERP 1 oder:
        if models[10] in groups_hdf5:
            groups_ok[10] = True;
            print "" + print_colors.BG_GREEN + "NON-INTERP 1 order REGRESSION DATA hdf5 found ... " + print_colors.END + " Reading Regression data..." + "";
            N_grids_, N_dirs_, N_freqs_, Data_1st_order_NONINTERP = convergence_IO.read_RegressionData_Model(file_dir = root_dir_, file_name = hdf5_fn, model_name = models[10]);
        else:
            print "" + print_colors.BG_RED + "NON-INTERP 1 order REGRESSION DATA not found... " + print_colors.END + " Reading PRTF data and writing..." + "";
        ############################################# NON-INTERP 1 oder [WEIGHTED] :
        if models[11] in groups_hdf5:
            groups_ok[11] = True;
            print "" + print_colors.BG_GREEN + "NON-INTERP 1 order [WEIGHTED] REGRESSION DATA hdf5 found ... " + print_colors.END + " Reading Regression data..." + "";
            N_grids_, N_dirs_, N_freqs_, Data_1st_order_w_NONINTERP = convergence_IO.read_RegressionData_Model(file_dir = root_dir_, file_name = hdf5_fn, model_name = models[11]);
        else:
            print "" + print_colors.BG_RED + "NON-INTERP 1 order [WEIGHTED] REGRESSION DATA not found... " + print_colors.END + " Reading PRTF data and writing..." + "";
        ############################################# REC-ONLY-INTERP 1 oder:
        if models[12] in groups_hdf5:
            groups_ok[12] = True;
            print "" + print_colors.BG_GREEN + "REC-ONLY-INTERP 1 order REGRESSION DATA hdf5 found ... " + print_colors.END + " Reading Regression data..." + "";
            N_grids_, N_dirs_, N_freqs_, Data_1st_order_REC_INTERP = convergence_IO.read_RegressionData_Model(file_dir = root_dir_, file_name = hdf5_fn, model_name = models[12]);
        else:
            print "" + print_colors.BG_RED + "REC-ONLY-INTERP 1 order REGRESSION DATA not found... " + print_colors.END + " Reading PRTF data and writing..." + "";
        ############################################# REC-ONLY-INTERP 1 oder [WEIGHTED] :
        if models[13] in groups_hdf5:
            groups_ok[13] = True;
            print "" + print_colors.BG_GREEN + "REC-ONLY-INTERP 1 order [WEIGHTED] REGRESSION DATA hdf5 found ... " + print_colors.END + " Reading Regression data..." + "";
            N_grids_, N_dirs_, N_freqs_, Data_1st_order_w_REC_INTERP = convergence_IO.read_RegressionData_Model(file_dir = root_dir_, file_name = hdf5_fn, model_name = models[13]);
        else:
            print "" + print_colors.BG_RED + "REC-ONLY-INTERP 1 order [WEIGHTED] REGRESSION DATA not found... " + print_colors.END + " Reading PRTF data and writing..." + "";        
    #===========================================================================
    # Building data:
    #===========================================================================
    if not all(groups_ok):
        interpolate_on_grid, grids_number_list, grids_fs_list, Receiver_final_locations_m_list_BLOCKED, Receiver_final_locations_m_list_FREEFIELD, \
                final_receiver_direction_list, folder_list, final_location_source_BLOCKED_m, final_location_source_FREEFIELD_m, sim_data_list, \
                    PRTF_SRC_REC_interpolated_grid_list, PRTF_NON_interpolated_grid_list, PRTF_interpolated_RecOnly_grid_list, gaussian_window_list, \
                        _, _ = read_PRTF_data(LS_root_directory = root_dir_);
        # Get and prepare data:
        N_grids, N_dirs, N_freqs, df, frequency_vector, \
            PRTF_ALL_GRIDS_SRC_REC_interpolated, PRTF_ALL_GRIDS_non_interpolated, PRTF_ALL_GRIDS_Rec_only_interp = prepare_PRTF_data(final_receiver_direction_list = final_receiver_direction_list,
                                                                                                                             grids_number_list = grids_number_list, 
                                                                                                                             grids_fs_list =grids_fs_list, 
                                                                                                                             PRTF_SRC_REC_interpolated_grid_list = PRTF_SRC_REC_interpolated_grid_list,
                                                                                                                             PRTF_NON_interpolated_grid_list =  PRTF_NON_interpolated_grid_list, PRTF_interpolated_RecOnly_grid_list = PRTF_interpolated_RecOnly_grid_list, 
                                                                                                                             f_max_ = f_max_);
        # Prerequisites:
        dX_vector = numpy.zeros(N_grids, dtype = numpy.float64);
        fs_vector = numpy.zeros(N_grids, dtype = numpy.float64);
        for grid_idx in range(N_grids):
            dX_vector[grid_idx] = sim_data_list[grid_idx].dX;
            fs_vector[grid_idx] = sim_data_list[grid_idx].fs;
        print "DATA READING COMPLETE... Building REGRESSION DATA...."
        ############################################################################################################################# 1st oder:
        if not groups_ok[0]:
            model_ = models[0];
            if verbose_flag:
                print print_colors.FG_CYAN + "MODEL 1st order - GETTING REGRESSION DATA" + print_colors.END;
            Data_1st_order = get_Regression_Data_1stOrder(N_grids = N_grids, N_dirs = N_dirs, N_freqs = N_freqs, df = df, frequency_vector = frequency_vector, dX_vector = dX_vector,
                                        final_receiver_direction_list = final_receiver_direction_list, fs_list = fs_vector, 
                                        PRTF_ALL_GRIDS = PRTF_ALL_GRIDS_SRC_REC_interpolated, B_samples = B_samples, alpha_ = alpha_,
                                        verbose_flag = verbose_flag);
            if verbose_flag:
                print print_colors.FG_CYAN + "MODEL 1st order - dumping DATA ...." + print_colors.END;
            convergence_IO.dump_RegressionData_Model(file_dir = root_dir_, file_name = "Regression_data_ALL.hdf5",
                                                     N_grids = N_grids, N_dirs = N_dirs, N_freqs = N_freqs, 
                                                     RegressionModeldata_ = Data_1st_order);
        ############################################################################################################################# 1st oder [WEIGHTED]:
        if not groups_ok[1]:
            model_ = models[1];
            if verbose_flag:
                print print_colors.FG_CYAN + "MODEL 1st order, WEIGHTED - GETTING REGRESSION DATA" + print_colors.END;
            Data_1st_order_w = get_Regression_Data_1stOrder_Weighted(N_grids = N_grids, N_dirs = N_dirs, N_freqs = N_freqs, df = df, frequency_vector = frequency_vector, dX_vector = dX_vector,
                                        final_receiver_direction_list = final_receiver_direction_list, fs_list = fs_vector, 
                                        PRTF_ALL_GRIDS = PRTF_ALL_GRIDS_SRC_REC_interpolated, B_samples = B_samples, alpha_ = alpha_,
                                        verbose_flag = verbose_flag);
            if verbose_flag:
                print print_colors.FG_CYAN + "MODEL 1st order, WEIGHTED - dumping DATA ...." + print_colors.END;
            convergence_IO.dump_RegressionData_Model(file_dir = root_dir_, file_name = "Regression_data_ALL.hdf5",
                                                     N_grids = N_grids, N_dirs = N_dirs, N_freqs = N_freqs, 
                                                     RegressionModeldata_ = Data_1st_order_w);
        ############################################################################################################################# 2nd oder:
        if not groups_ok[2]:
            model_ = models[2];
            if verbose_flag:
                print print_colors.FG_CYAN + "MODEL 2nd order - GETTING REGRESSION DATA" + print_colors.END;
            Data_2nd_order = get_Regression_Data_2ndOrder(N_grids = N_grids, N_dirs = N_dirs, N_freqs = N_freqs, df = df, frequency_vector = frequency_vector, dX_vector = dX_vector,
                                        final_receiver_direction_list = final_receiver_direction_list, fs_list = fs_vector, 
                                        PRTF_ALL_GRIDS = PRTF_ALL_GRIDS_SRC_REC_interpolated, B_samples = B_samples, alpha_ = alpha_,
                                        verbose_flag = verbose_flag);
            if verbose_flag:
                print print_colors.FG_CYAN + "MODEL 2nd order - dumping DATA ...." + print_colors.END;
            convergence_IO.dump_RegressionData_Model(file_dir = root_dir_, file_name = "Regression_data_ALL.hdf5",
                                                     N_grids = N_grids, N_dirs = N_dirs, N_freqs = N_freqs, 
                                                     RegressionModeldata_ = Data_2nd_order);
        ############################################################################################################################# 2nd oder [WEIGHTED]:
        if not groups_ok[3]:
            model_ = models[3];
            if verbose_flag:
                print print_colors.FG_CYAN + "MODEL 2nd order, WEIGHTED - GETTING REGRESSION DATA" + print_colors.END;
            Data_2nd_order_w = get_Regression_Data_2ndOrder_Weighted(N_grids = N_grids, N_dirs = N_dirs, N_freqs = N_freqs, df = df, frequency_vector = frequency_vector, dX_vector = dX_vector,
                                        final_receiver_direction_list = final_receiver_direction_list, fs_list = fs_vector, 
                                        PRTF_ALL_GRIDS = PRTF_ALL_GRIDS_SRC_REC_interpolated, B_samples = B_samples, alpha_ = alpha_,
                                        verbose_flag = verbose_flag);
            if verbose_flag:
                print print_colors.FG_CYAN + "MODEL 2nd order, WEIGHTED - dumping DATA ...." + print_colors.END;
            convergence_IO.dump_RegressionData_Model(file_dir = root_dir_, file_name = "Regression_data_ALL.hdf5",
                                                     N_grids = N_grids, N_dirs = N_dirs, N_freqs = N_freqs, 
                                                     RegressionModeldata_ = Data_2nd_order_w);
        ############################################################################################################################# 1-2 oder:
        if not groups_ok[4]:
            model_ = models[4];
            if verbose_flag:
                print print_colors.FG_CYAN + "MODEL 1-2 order - GETTING REGRESSION DATA" + print_colors.END;
            Data_12_order = get_Regression_Data_12_Order(N_grids = N_grids, N_dirs = N_dirs, N_freqs = N_freqs, df = df, frequency_vector = frequency_vector, dX_vector = dX_vector,
                                        final_receiver_direction_list = final_receiver_direction_list, fs_list = fs_vector, 
                                        PRTF_ALL_GRIDS = PRTF_ALL_GRIDS_SRC_REC_interpolated, B_samples = B_samples, alpha_ = alpha_,
                                        verbose_flag = verbose_flag);
            if verbose_flag:
                print print_colors.FG_CYAN + "MODEL 1-2 order - dumping DATA ...." + print_colors.END;
            convergence_IO.dump_RegressionData_Model(file_dir = root_dir_, file_name = "Regression_data_ALL.hdf5",
                                                     N_grids = N_grids, N_dirs = N_dirs, N_freqs = N_freqs, 
                                                     RegressionModeldata_ = Data_12_order);
        ############################################################################################################################# 1-2 oder [WEIGHTED]:
        if not groups_ok[5]:
            model_ = models[5];
            if verbose_flag:
                print print_colors.FG_CYAN + "MODEL 1-2 order, WEIGHTED - GETTING REGRESSION DATA" + print_colors.END;
            Data_12_order_w = get_Regression_Data_12_Order_Weighted(N_grids = N_grids, N_dirs = N_dirs, N_freqs = N_freqs, df = df, frequency_vector = frequency_vector, dX_vector = dX_vector,
                                        final_receiver_direction_list = final_receiver_direction_list, fs_list = fs_vector, 
                                        PRTF_ALL_GRIDS = PRTF_ALL_GRIDS_SRC_REC_interpolated, B_samples = B_samples, alpha_ = alpha_,
                                        verbose_flag = verbose_flag);
            if verbose_flag:
                print print_colors.FG_CYAN + "MODEL 1-2 order, WEIGHTED - dumping DATA ...." + print_colors.END;
            convergence_IO.dump_RegressionData_Model(file_dir = root_dir_, file_name = "Regression_data_ALL.hdf5",
                                                     N_grids = N_grids, N_dirs = N_dirs, N_freqs = N_freqs, 
                                                     RegressionModeldata_ = Data_12_order_w);
        ############################################################################################################################# -1 oder:
        if not groups_ok[6]:
            model_ = models[6];
            if verbose_flag:
                print print_colors.FG_CYAN + "MODEL -1 order - GETTING REGRESSION DATA" + print_colors.END;
            Data_m1_order = get_Regression_Data_m1Order(N_grids = N_grids, N_dirs = N_dirs, N_freqs = N_freqs, df = df, frequency_vector = frequency_vector, dX_vector = dX_vector,
                                        final_receiver_direction_list = final_receiver_direction_list, fs_list = fs_vector, 
                                        PRTF_ALL_GRIDS = PRTF_ALL_GRIDS_SRC_REC_interpolated, B_samples = B_samples, alpha_ = alpha_,
                                        verbose_flag = verbose_flag);
            if verbose_flag:
                print print_colors.FG_CYAN + "MODEL -1 order - dumping DATA ...." + print_colors.END;
            convergence_IO.dump_RegressionData_Model(file_dir = root_dir_, file_name = "Regression_data_ALL.hdf5",
                                                     N_grids = N_grids, N_dirs = N_dirs, N_freqs = N_freqs, 
                                                     RegressionModeldata_ = Data_m1_order);
        ############################################################################################################################# -1 oder [WEIGHTED]:
        if not groups_ok[7]:
            model_ = models[7];
            if verbose_flag:
                print print_colors.FG_CYAN + "MODEL -1 order, WEIGHTED - GETTING REGRESSION DATA" + print_colors.END;
            Data_m1_order_w = get_Regression_Data_m1Order_Weighted(N_grids = N_grids, N_dirs = N_dirs, N_freqs = N_freqs, df = df, frequency_vector = frequency_vector, dX_vector = dX_vector,
                                        final_receiver_direction_list = final_receiver_direction_list, fs_list = fs_vector, 
                                        PRTF_ALL_GRIDS = PRTF_ALL_GRIDS_SRC_REC_interpolated, B_samples = B_samples, alpha_ = alpha_,
                                        verbose_flag = verbose_flag);
            if verbose_flag:
                print print_colors.FG_CYAN + "MODEL -1 order, WEIGHTED - dumping DATA ...." + print_colors.END;
            convergence_IO.dump_RegressionData_Model(file_dir = root_dir_, file_name = "Regression_data_ALL.hdf5",
                                                     N_grids = N_grids, N_dirs = N_dirs, N_freqs = N_freqs, 
                                                     RegressionModeldata_ = Data_m1_order_w);
        ############################################################################################################################# 1,-1 oder:
        if not groups_ok[8]:
            model_ = models[8];
            if verbose_flag:
                print print_colors.FG_CYAN + "MODEL 1,-1 order - GETTING REGRESSION DATA" + print_colors.END;
            Data_1m1_order = get_Regression_Data_1m1Order(N_grids = N_grids, N_dirs = N_dirs, N_freqs = N_freqs, df = df, frequency_vector = frequency_vector, dX_vector = dX_vector,
                                        final_receiver_direction_list = final_receiver_direction_list, fs_list = fs_vector, 
                                        PRTF_ALL_GRIDS = PRTF_ALL_GRIDS_SRC_REC_interpolated, B_samples = B_samples, alpha_ = alpha_,
                                        verbose_flag = verbose_flag);
            if verbose_flag:
                print print_colors.FG_CYAN + "MODEL 1,-1 order - dumping DATA ...." + print_colors.END;
            convergence_IO.dump_RegressionData_Model(file_dir = root_dir_, file_name = "Regression_data_ALL.hdf5",
                                                     N_grids = N_grids, N_dirs = N_dirs, N_freqs = N_freqs, 
                                                     RegressionModeldata_ = Data_1m1_order);
        ############################################################################################################################# 1,-1 oder [WEIGHTED]:
        if not groups_ok[9]:
            model_ = models[9];
            if verbose_flag:
                print print_colors.FG_CYAN + "MODEL 1,-1 order, WEIGHTED - GETTING REGRESSION DATA" + print_colors.END;
            Data_1m1_order_w = get_Regression_Data_1m1Order_Weighted(N_grids = N_grids, N_dirs = N_dirs, N_freqs = N_freqs, df = df, frequency_vector = frequency_vector, dX_vector = dX_vector,
                                        final_receiver_direction_list = final_receiver_direction_list, fs_list = fs_vector, 
                                        PRTF_ALL_GRIDS = PRTF_ALL_GRIDS_SRC_REC_interpolated, B_samples = B_samples, alpha_ = alpha_,
                                        verbose_flag = verbose_flag);
            if verbose_flag:
                print print_colors.FG_CYAN + "MODEL 1,-1 order, WEIGHTED - dumping DATA ...." + print_colors.END;
            convergence_IO.dump_RegressionData_Model(file_dir = root_dir_, file_name = "Regression_data_ALL.hdf5",
                                                     N_grids = N_grids, N_dirs = N_dirs, N_freqs = N_freqs, 
                                                     RegressionModeldata_ = Data_1m1_order_w);
        ############################################################################################################################# NON-INTERP 1 oder:
        if not groups_ok[10]:
            model_ = models[10];
            if verbose_flag:
                print print_colors.FG_CYAN + "NON-INTERP MODEL 1 order - GETTING REGRESSION DATA" + print_colors.END;
            Data_1st_order_NONINTERP = get_Regression_Data_1stOrder(N_grids = N_grids, N_dirs = N_dirs, N_freqs = N_freqs, df = df, frequency_vector = frequency_vector, dX_vector = dX_vector,
                                        final_receiver_direction_list = final_receiver_direction_list, fs_list = fs_vector, 
                                        PRTF_ALL_GRIDS = PRTF_ALL_GRIDS_non_interpolated, B_samples = B_samples, alpha_ = alpha_,
                                        model_name = model_,
                                        verbose_flag = verbose_flag);
            if verbose_flag:
                print print_colors.FG_CYAN + "NON-INTERP MODEL 1 order - dumping DATA ...." + print_colors.END;
            convergence_IO.dump_RegressionData_Model(file_dir = root_dir_, file_name = "Regression_data_ALL.hdf5",
                                                     N_grids = N_grids, N_dirs = N_dirs, N_freqs = N_freqs, 
                                                     RegressionModeldata_ = Data_1st_order_NONINTERP);
        ############################################################################################################################# NON-INTERP 1 oder [WEIGHTED]:
        if not groups_ok[11]:
            model_ = models[11];
            if verbose_flag:
                print print_colors.FG_CYAN + "NON-INTERP MODEL 1 order, WEIGHTED - GETTING REGRESSION DATA" + print_colors.END;
            Data_1st_order_w_NONINTERP = get_Regression_Data_1stOrder_Weighted(N_grids = N_grids, N_dirs = N_dirs, N_freqs = N_freqs, df = df, frequency_vector = frequency_vector, dX_vector = dX_vector,
                                        final_receiver_direction_list = final_receiver_direction_list, fs_list = fs_vector, 
                                        PRTF_ALL_GRIDS = PRTF_ALL_GRIDS_non_interpolated, B_samples = B_samples, alpha_ = alpha_,
                                        model_name = model_,
                                        verbose_flag = verbose_flag);
            if verbose_flag:
                print print_colors.FG_CYAN + "NON-INTERP MODEL 1 order, WEIGHTED - dumping DATA ...." + print_colors.END;
            convergence_IO.dump_RegressionData_Model(file_dir = root_dir_, file_name = "Regression_data_ALL.hdf5",
                                                     N_grids = N_grids, N_dirs = N_dirs, N_freqs = N_freqs, 
                                                     RegressionModeldata_ = Data_1st_order_w_NONINTERP);
        ############################################################################################################################# REC-ONLY-INTERP 1 oder:
        if not groups_ok[12]:
            model_ = models[12];
            if verbose_flag:
                print print_colors.FG_CYAN + "REC-ONLY-INTERP MODEL 1 order - GETTING REGRESSION DATA" + print_colors.END;
            Data_1st_order_REC_INTERP = get_Regression_Data_1stOrder(N_grids = N_grids, N_dirs = N_dirs, N_freqs = N_freqs, df = df, frequency_vector = frequency_vector, dX_vector = dX_vector,
                                        final_receiver_direction_list = final_receiver_direction_list, fs_list = fs_vector, 
                                        PRTF_ALL_GRIDS = PRTF_ALL_GRIDS_Rec_only_interp, B_samples = B_samples, alpha_ = alpha_,
                                        model_name = model_,
                                        verbose_flag = verbose_flag);
            if verbose_flag:
                print print_colors.FG_CYAN + "REC-ONLY-INTERP MODEL 1 order - dumping DATA ...." + print_colors.END;
            convergence_IO.dump_RegressionData_Model(file_dir = root_dir_, file_name = "Regression_data_ALL.hdf5",
                                                     N_grids = N_grids, N_dirs = N_dirs, N_freqs = N_freqs, 
                                                     RegressionModeldata_ = Data_1st_order_REC_INTERP);
        ############################################################################################################################# REC-ONLY-INTERP 1 oder [WEIGHTED]:
        if not groups_ok[13]:
            model_ = models[13];
            if verbose_flag:
                print print_colors.FG_CYAN + "REC-ONLY-INTERP MODEL 1 order, WEIGHTED - GETTING REGRESSION DATA" + print_colors.END;
            Data_1st_order_w_REC_INTERP = get_Regression_Data_1stOrder_Weighted(N_grids = N_grids, N_dirs = N_dirs, N_freqs = N_freqs, df = df, frequency_vector = frequency_vector, dX_vector = dX_vector,
                                        final_receiver_direction_list = final_receiver_direction_list, fs_list = fs_vector, 
                                        PRTF_ALL_GRIDS = PRTF_ALL_GRIDS_Rec_only_interp, B_samples = B_samples, alpha_ = alpha_,
                                        model_name = model_,
                                        verbose_flag = verbose_flag);
            if verbose_flag:
                print print_colors.FG_CYAN + "REC-ONLY-INTERP MODEL 1 order, WEIGHTED - dumping DATA ...." + print_colors.END;
            convergence_IO.dump_RegressionData_Model(file_dir = root_dir_, file_name = "Regression_data_ALL.hdf5",
                                                     N_grids = N_grids, N_dirs = N_dirs, N_freqs = N_freqs, 
                                                     RegressionModeldata_ = Data_1st_order_w_REC_INTERP);
    if verbose_flag:
        print print_colors.FG_CYAN + "Returning Regression data..." + print_colors.END;
    return N_grids, N_dirs, N_freqs, Data_1st_order, Data_1st_order_w, Data_2nd_order, Data_2nd_order_w, Data_12_order, Data_12_order_w, \
            Data_m1_order, Data_m1_order_w, Data_1m1_order, Data_1m1_order_w, Data_1st_order_NONINTERP, Data_1st_order_w_NONINTERP, Data_1st_order_REC_INTERP, Data_1st_order_w_REC_INTERP;

def get_Regression_Data_1stOrder(N_grids, N_dirs, N_freqs, df, frequency_vector, dX_vector, final_receiver_direction_list, fs_list, PRTF_ALL_GRIDS, B_samples, alpha_ = 0.05, model_name = "1st order model: y0 + C1*dX", verbose_flag = False):
    '''
    Function retrieves the relevant regression data from PRTF_ALL_GRIDS input for a first order regression model:
    
        y = y0 + beta1*dX;
    
    The input data comes from prepare_PRTF_data() function.
    
    For regression, the function uses statsmodel[Seabold S., Perktold J., "Statsmodels: Econometric and Statistical Modeling with Python", Proc. Of the 9th python in science conf., 
    (2010)] package. Results were double-checked to match manual regression fits.
    For bootstrapping, function uses scikits.bootstrap package.
    
    :param N_grids: int with the total number of grids from input.
    :param N_dirs: int with the number of directions.
    :param N_freqs: int with the total number of frequency bins (positive frequency) between DC and f_max_ for which PRTF data was computed.
    :param df: float with frequency resolution. Function assumes checks are already done.
    :param frequency_vector: A numpy.array with the frequency in Hz corresponding to all the frequency bins in the returned data. These bins are consistent between grids.
    
    :param dX_vector: A numpy.array (float64) of size N_grids which contain the grid spacing dX (in m) for each grid.
    :param final_receiver_direction_list: (N_dir) A list containing tuples with the receiver directions (az, el) for the simulation data (both BLOCKED and FREE-FIELD - already checked).
    :param fs_list: A numpy.array (float64) of size N_grids which contain the sampling frequency (in Hz) for each grid.
    :param PRTF_ALL_GRIDS: Numpy array (numpy.complex128) of size [N_grids, N_dirs, N_freqs] with the PRTF for which regression data is computed.
    
    :param B_samples: The number of bootstrap samples used in bootstrapping.
    :param alpha_: The confidence level (two-tailed) for the confidence interval.
    
    :param model_name: The model name. Used to write a hdf5 group.
    
    :param verbose_flag: Boolean. If True, status is printed.
    
    :returns: A converge_IO.RegressionModelData() class with all the data.   
    '''
    global Class_results;
    Data_1st_order = convergence_IO.RegressionModelData(N_directions = N_dirs, N_grids = N_grids, N_frequencies = N_freqs, Model_size = 2);
    # Populate basic data:
    Data_1st_order.model_name = model_name;
    Data_1st_order.df = df;
    Data_1st_order.B_samples = B_samples; 
    Data_1st_order.frequency_vector = frequency_vector;
    Data_1st_order.dX_vector = dX_vector;
    Data_1st_order.final_receiver_direction_list = final_receiver_direction_list;
    Data_1st_order.fs_list = fs_list;
    Data_1st_order.weights = numpy.ones(N_grids, dtype = numpy.float64);
    
    #for dir_idx in range(0,1):
    for dir_idx in range(N_dirs):
        if verbose_flag:
            print "VERBOSE: 1st order model - DIRECTION = " + str(final_receiver_direction_list[dir_idx]);
        for freq_idx in range(0,N_freqs):
            if verbose_flag:
                print "\tVERBOSE: 1st order model [d_idx = " + str(dir_idx) + "] - f = " + str(frequency_vector[freq_idx]);
            # a) Get PRTF data:
            PRTF_vector = numpy.abs(PRTF_ALL_GRIDS[:, dir_idx, freq_idx]);
            #===================================================================
            # 1st order model:
            #===================================================================
            X__ = sm.add_constant(dX_vector);
            model = sm.OLS(endog = PRTF_vector,  exog = X__ );
            results_1 = model.fit(method="pinv");
            Data_1st_order.X_matrix = X__; 
            Data_1st_order.Y_[dir_idx, freq_idx, :] = PRTF_vector;
            Data_1st_order.asymptotic_solution_average[dir_idx, freq_idx] = numpy.dot( results_1.params.T, numpy.array([1.0, 0.0], dtype = numpy.float64));
            Data_1st_order.beta[dir_idx, freq_idx, :] = results_1.params;
            Data_1st_order.residuals[dir_idx, freq_idx, :] = results_1.resid;
            Data_1st_order.R2[dir_idx, freq_idx] = results_1.rsquared;
            Data_1st_order.R2_adjusted[dir_idx, freq_idx] = results_1.rsquared_adj;# Checked with values from Python package...
            Data_1st_order.p_values[dir_idx, freq_idx, :] = results_1.pvalues; # Use to test significance level: if results_1.pvalues[0] < significance_level:...
            Data_1st_order.f_value[dir_idx, freq_idx] = results_1.fvalue;
            #################### CIs:
            data_BCA_1st = numpy.hstack( (numpy.asarray([dX_vector]).T, numpy.asarray([PRTF_vector]).T) );
            Data_1st_order.asymptotic_solution_CI_lower[dir_idx, freq_idx], Data_1st_order.asymptotic_solution_CI_upper[dir_idx, freq_idx] = get_CI_MeanOutput_MultiVariateRegression_statsModel(results_ = results_1, x0 = numpy.float64([1.0, 0.0]), alpha = alpha_);
            # Bootstrap pairs:
            CIs_BCA_1st = bootstrap.ci(data=data_BCA_1st, statfunction=BootstrapPairs_get_asymptotic_solution_1stOrderRegression, alpha=alpha_, n_samples=B_samples, method='bca');
            Data_1st_order.asymptotic_solution_CI_lower_BootstrapPairs[dir_idx, freq_idx] = CIs_BCA_1st[0];
            Data_1st_order.asymptotic_solution_CI_upper_BootstrapPairs[dir_idx, freq_idx] = CIs_BCA_1st[1]; 
            # Bootstrap residuals:
            # Build the class for bootstrapping residuals (BCA) - for a global variable to be passed to the function:
            Class_results_1 = RegressionModel();
            Class_results_1.X_ = numpy.copy(X__);
            Class_results_1.beta = numpy.copy(results_1.params);
            Class_results_1.residuals = numpy.copy(results_1.resid);
            Class_results = Class_results_1;
            #####
            CIs_BCA_1st_Resid = bootstrap.ci(data=numpy.copy(results_1.resid), statfunction=BootstrapResiduals_get_asymptotic_solution, alpha=alpha_, n_samples=B_samples, method='bca');
            Data_1st_order.asymptotic_solution_CI_lower_BootstrapResiduals[dir_idx, freq_idx] = CIs_BCA_1st_Resid[0];
            Data_1st_order.asymptotic_solution_CI_upper_BootstrapResiduals[dir_idx, freq_idx] = CIs_BCA_1st_Resid[1];
    return Data_1st_order;

def get_Regression_Data_1stOrder_Weighted(N_grids, N_dirs, N_freqs, df, frequency_vector, dX_vector, final_receiver_direction_list, fs_list, PRTF_ALL_GRIDS, B_samples, alpha_ = 0.05, model_name = "1st order model [WEIGHTED]: y0 + C1*dX", verbose_flag = False):
    '''
    Same as get_Regression_Data_1stOrder(), but for weighted data - weights according to Eca and Hoekstra (2014).
    
    See get_Regression_Data_1stOrder() for all the input data.
    
    :returns: A converge_IO.RegressionModelData() class with all the data.   
    '''
    global Class_results;
    Data_1st_order_w = convergence_IO.RegressionModelData(N_directions = N_dirs, N_grids = N_grids, N_frequencies = N_freqs, Model_size = 2);
    # Populate basic data:
    Data_1st_order_w.model_name = model_name;
    Data_1st_order_w.df = df;
    Data_1st_order_w.B_samples = B_samples; 
    Data_1st_order_w.frequency_vector = frequency_vector;
    Data_1st_order_w.dX_vector = dX_vector;
    Data_1st_order_w.final_receiver_direction_list = final_receiver_direction_list;
    Data_1st_order_w.fs_list = fs_list;
    Data_1st_order_w.weights = (1.0/dX_vector) / (numpy.sum(1.0/dX_vector));# Build weights for weighted least_squares according to Eca and Hoekstra (2014):
    
    #for dir_idx in range(0,1):
    for dir_idx in range(N_dirs):
        if verbose_flag:
            print "VERBOSE: 1st order model, WEIGHTED - DIRECTION = " + str(final_receiver_direction_list[dir_idx]);
        for freq_idx in range(0,N_freqs):
            if verbose_flag:
                print "\tVERBOSE: 1st order model, WEIGHTED [d_idx = " + str(dir_idx) + "] - f = " + str(frequency_vector[freq_idx]);
            # a) Get PRTF data:
            PRTF_vector = numpy.abs(PRTF_ALL_GRIDS[:, dir_idx, freq_idx]);
            #===================================================================
            # 1st order model:
            #===================================================================
            X__ = sm.add_constant(dX_vector);
            model_w = sm.WLS(endog = PRTF_vector,  exog = X__, weights = Data_1st_order_w.weights );
            results_1w = model_w.fit(method="pinv");
            Data_1st_order_w.X_matrix = X__; 
            Data_1st_order_w.Y_[dir_idx, freq_idx, :] = PRTF_vector;
            Data_1st_order_w.asymptotic_solution_average[dir_idx, freq_idx] = numpy.dot( results_1w.params.T, numpy.array([1.0, 0.0], dtype = numpy.float64));
            Data_1st_order_w.beta[dir_idx, freq_idx, :] = results_1w.params;
            Data_1st_order_w.residuals[dir_idx, freq_idx, :] = results_1w.resid;
            Data_1st_order_w.R2[dir_idx, freq_idx] = results_1w.rsquared;
            Data_1st_order_w.R2_adjusted[dir_idx, freq_idx] = results_1w.rsquared_adj;# Checked with values from Python package...
            Data_1st_order_w.p_values[dir_idx, freq_idx, :] = results_1w.pvalues; # Use to test significance level: if results_1w.pvalues[0] < significance_level:...
            Data_1st_order_w.f_value[dir_idx, freq_idx] = results_1w.fvalue;
            #################### CIs:
            data_BCA_1st = numpy.hstack( (numpy.asarray([dX_vector]).T, numpy.asarray([PRTF_vector]).T) );
            Data_1st_order_w.asymptotic_solution_CI_lower[dir_idx, freq_idx], Data_1st_order_w.asymptotic_solution_CI_upper[dir_idx, freq_idx] = get_CI_MeanOutput_MultiVariateRegression_statsModel(results_ = results_1w, x0 = numpy.float64([1.0, 0.0]), alpha = alpha_);
            # Bootstrap pairs:
            CIs_BCA_1st_w = bootstrap.ci(data=data_BCA_1st, statfunction=BootstrapPairs_Weighted_get_asymptotic_solution_1stOrderRegression, alpha=alpha_, n_samples=B_samples, method='bca');
            Data_1st_order_w.asymptotic_solution_CI_lower_BootstrapPairs[dir_idx, freq_idx] = CIs_BCA_1st_w[0];
            Data_1st_order_w.asymptotic_solution_CI_upper_BootstrapPairs[dir_idx, freq_idx] = CIs_BCA_1st_w[1]; 
            # Bootstrap residuals:
            # Build the class for bootstrapping residuals (BCA) - for a global variable to be passed to the function:
            Class_results_1w = RegressionModel();
            Class_results_1w.X_ = numpy.copy(X__);
            Class_results_1w.beta = numpy.copy(results_1w.params);
            Class_results_1w.residuals = numpy.copy(results_1w.resid);
            Class_results_1w.weights = numpy.copy(Data_1st_order_w.weights);
            Class_results = Class_results_1w;
            #####
            CIs_BCA_1st_Resid_w = bootstrap.ci(data=numpy.copy(results_1w.resid), statfunction=BootstrapResiduals_Weighted_get_asymptotic_solution, alpha=alpha_, n_samples=B_samples, method='bca');
            Data_1st_order_w.asymptotic_solution_CI_lower_BootstrapResiduals[dir_idx, freq_idx] = CIs_BCA_1st_Resid_w[0];
            Data_1st_order_w.asymptotic_solution_CI_upper_BootstrapResiduals[dir_idx, freq_idx] = CIs_BCA_1st_Resid_w[1];
    return Data_1st_order_w;

def get_Regression_Data_2ndOrder(N_grids, N_dirs, N_freqs, df, frequency_vector, dX_vector, final_receiver_direction_list, fs_list, PRTF_ALL_GRIDS, B_samples, alpha_ = 0.05, model_name = "2nd order model: y0 + C2*dX^2", verbose_flag = False):
    '''
    Function retrieves the relevant regression data from PRTF_ALL_GRIDS input for a second order regression model:
    
        y = y0 + beta1*dX^2;
    
    The input data comes from prepare_PRTF_data() function.
    
    For regression, the function uses statsmodel[Seabold S., Perktold J., "Statsmodels: Econometric and Statistical Modeling with Python", Proc. Of the 9th python in science conf., 
    (2010)] package. Results were double-checked to match manual regression fits.
    For bootstrapping, function uses scikits.bootstrap package.
    
    :param N_grids: int with the total number of grids from input.
    :param N_dirs: int with the number of directions.
    :param N_freqs: int with the total number of frequency bins (positive frequency) between DC and f_max_ for which PRTF data was computed.
    :param df: float with frequency resolution. Function assumes checks are already done.
    :param frequency_vector: A numpy.array with the frequency in Hz corresponding to all the frequency bins in the returned data. These bins are consistent between grids.
    
    :param dX_vector: A numpy.array (float64) of size N_grids which contain the grid spacing dX (in m) for each grid.
    :param final_receiver_direction_list: (N_dir) A list containing tuples with the receiver directions (az, el) for the simulation data (both BLOCKED and FREE-FIELD - already checked).
    :param fs_list: A numpy.array (float64) of size N_grids which contain the sampling frequency (in Hz) for each grid.
    :param PRTF_ALL_GRIDS: Numpy array (numpy.complex128) of size [N_grids, N_dirs, N_freqs] with the PRTF for which regression data is computed.
    
    :param B_samples: The number of bootstrap samples used in bootstrapping.
    :param alpha_: The confidence level (two-tailed) for the confidence interval.
    
    :param model_name: The model name. Used to write a hdf5 group.
    
    :param verbose_flag: Boolean. If True, status is printed.
    
    :returns: A converge_IO.RegressionModelData() class with all the data.   
    '''
    global Class_results;
    Data_2nd_order = convergence_IO.RegressionModelData(N_directions = N_dirs, N_grids = N_grids, N_frequencies = N_freqs, Model_size = 2);
    # Populate basic data:
    Data_2nd_order.model_name = model_name;
    Data_2nd_order.df = df;
    Data_2nd_order.B_samples = B_samples; 
    Data_2nd_order.frequency_vector = frequency_vector;
    Data_2nd_order.dX_vector = dX_vector;
    Data_2nd_order.final_receiver_direction_list = final_receiver_direction_list;
    Data_2nd_order.fs_list = fs_list;
    Data_2nd_order.weights = numpy.ones(N_grids, dtype = numpy.float64);
    
    #for dir_idx in range(0,1):
    for dir_idx in range(N_dirs):
        if verbose_flag:
            print "VERBOSE: 2nd order model - DIRECTION = " + str(final_receiver_direction_list[dir_idx]);
        for freq_idx in range(0,N_freqs):
            if verbose_flag:
                print "\tVERBOSE: 2nd order model [d_idx = " + str(dir_idx) + "] - f = " + str(frequency_vector[freq_idx]);
            # a) Get PRTF data:
            PRTF_vector = numpy.abs(PRTF_ALL_GRIDS[:, dir_idx, freq_idx]);
            #===================================================================
            # 1st order model:
            #===================================================================
            X__ = sm.add_constant(dX_vector**2);
            model_2 = sm.OLS(endog = PRTF_vector,  exog = X__ );
            results_2 = model_2.fit(method="pinv");
            Data_2nd_order.X_matrix = X__; 
            Data_2nd_order.Y_[dir_idx, freq_idx, :] = PRTF_vector;
            Data_2nd_order.asymptotic_solution_average[dir_idx, freq_idx] = numpy.dot( results_2.params.T, numpy.array([1.0, 0.0], dtype = numpy.float64));
            Data_2nd_order.beta[dir_idx, freq_idx, :] = results_2.params;
            Data_2nd_order.residuals[dir_idx, freq_idx, :] = results_2.resid;
            Data_2nd_order.R2[dir_idx, freq_idx] = results_2.rsquared;
            Data_2nd_order.R2_adjusted[dir_idx, freq_idx] = results_2.rsquared_adj;# Checked with values from Python package...
            Data_2nd_order.p_values[dir_idx, freq_idx, :] = results_2.pvalues; # Use to test significance level: if results_2.pvalues[0] < significance_level:...
            Data_2nd_order.f_value[dir_idx, freq_idx] = results_2.fvalue;
            #################### CIs:
            data_BCA_2nd = numpy.hstack( (numpy.asarray([dX_vector**2]).T, numpy.asarray([PRTF_vector]).T) );
            Data_2nd_order.asymptotic_solution_CI_lower[dir_idx, freq_idx], Data_2nd_order.asymptotic_solution_CI_upper[dir_idx, freq_idx] = get_CI_MeanOutput_MultiVariateRegression_statsModel(results_ = results_2, x0 = numpy.float64([1.0, 0.0]), alpha = alpha_);
            # Bootstrap pairs:
            CIs_BCA_2nd = bootstrap.ci(data=data_BCA_2nd, statfunction=BootstrapPairs_get_asymptotic_solution_1stOrderRegression, alpha=alpha_, n_samples=B_samples, method='bca');
            Data_2nd_order.asymptotic_solution_CI_lower_BootstrapPairs[dir_idx, freq_idx] = CIs_BCA_2nd[0];
            Data_2nd_order.asymptotic_solution_CI_upper_BootstrapPairs[dir_idx, freq_idx] = CIs_BCA_2nd[1]; 
            # Bootstrap residuals:
            # Build the class for bootstrapping residuals (BCA) - for a global variable to be passed to the function:
            Class_results_2 = RegressionModel();
            Class_results_2.X_ = numpy.copy(X__);
            Class_results_2.beta = numpy.copy(results_2.params);
            Class_results_2.residuals = numpy.copy(results_2.resid);
            Class_results = Class_results_2;
            #####
            CIs_BCA_2nd_Resid = bootstrap.ci(data=numpy.copy(results_2.resid), statfunction=BootstrapResiduals_get_asymptotic_solution, alpha=alpha_, n_samples=B_samples, method='bca');
            Data_2nd_order.asymptotic_solution_CI_lower_BootstrapResiduals[dir_idx, freq_idx] = CIs_BCA_2nd_Resid[0];
            Data_2nd_order.asymptotic_solution_CI_upper_BootstrapResiduals[dir_idx, freq_idx] = CIs_BCA_2nd_Resid[1];
    return Data_2nd_order;

def get_Regression_Data_2ndOrder_Weighted(N_grids, N_dirs, N_freqs, df, frequency_vector, dX_vector, final_receiver_direction_list, fs_list, PRTF_ALL_GRIDS, B_samples, alpha_ = 0.05, model_name = "2nd order model [WEIGHTED]: y0 + C2*dX^2", verbose_flag = False):
    '''
    Same as get_Regression_Data_2ndOrder(), but for weighted data - weights according to Eca and Hoekstra (2014).
    
    See get_Regression_Data_2ndOrder() for all the input data.
    
    :returns: A converge_IO.RegressionModelData() class with all the data.   
    '''
    global Class_results;
    Data_2nd_order_w = convergence_IO.RegressionModelData(N_directions = N_dirs, N_grids = N_grids, N_frequencies = N_freqs, Model_size = 2);
    # Populate basic data:
    Data_2nd_order_w.model_name = model_name;
    Data_2nd_order_w.df = df;
    Data_2nd_order_w.B_samples = B_samples; 
    Data_2nd_order_w.frequency_vector = frequency_vector;
    Data_2nd_order_w.dX_vector = dX_vector;
    Data_2nd_order_w.final_receiver_direction_list = final_receiver_direction_list;
    Data_2nd_order_w.fs_list = fs_list;
    Data_2nd_order_w.weights = (1.0/dX_vector) / (numpy.sum(1.0/dX_vector));# Build weights for weighted least_squares according to Eca and Hoekstra (2014):
    
    #for dir_idx in range(0,1):
    for dir_idx in range(N_dirs):
        if verbose_flag:
            print "VERBOSE: 2nd order model, WEIGHTED - DIRECTION = " + str(final_receiver_direction_list[dir_idx]);
        for freq_idx in range(0,N_freqs):
            if verbose_flag:
                print "\tVERBOSE: 2nd order model, WEIGHTED [d_idx = " + str(dir_idx) + "] - f = " + str(frequency_vector[freq_idx]);
            # a) Get PRTF data:
            PRTF_vector = numpy.abs(PRTF_ALL_GRIDS[:, dir_idx, freq_idx]);
            #===================================================================
            # 1st order model:
            #===================================================================
            X__ = sm.add_constant(dX_vector**2);
            model_2w = sm.WLS(endog = PRTF_vector,  exog = X__, weights = Data_2nd_order_w.weights );
            results_2w = model_2w.fit(method="pinv");
            Data_2nd_order_w.X_matrix = X__; 
            Data_2nd_order_w.Y_[dir_idx, freq_idx, :] = PRTF_vector;
            Data_2nd_order_w.asymptotic_solution_average[dir_idx, freq_idx] = numpy.dot( results_2w.params.T, numpy.array([1.0, 0.0], dtype = numpy.float64));
            Data_2nd_order_w.beta[dir_idx, freq_idx, :] = results_2w.params;
            Data_2nd_order_w.residuals[dir_idx, freq_idx, :] = results_2w.resid;
            Data_2nd_order_w.R2[dir_idx, freq_idx] = results_2w.rsquared;
            Data_2nd_order_w.R2_adjusted[dir_idx, freq_idx] = results_2w.rsquared_adj;# Checked with values from Python package...
            Data_2nd_order_w.p_values[dir_idx, freq_idx, :] = results_2w.pvalues; # Use to test significance level: if results_2w.pvalues[0] < significance_level:...
            Data_2nd_order_w.f_value[dir_idx, freq_idx] = results_2w.fvalue;
            #################### CIs:
            data_BCA_2nd = numpy.hstack( (numpy.asarray([dX_vector**2]).T, numpy.asarray([PRTF_vector]).T) );
            Data_2nd_order_w.asymptotic_solution_CI_lower[dir_idx, freq_idx], Data_2nd_order_w.asymptotic_solution_CI_upper[dir_idx, freq_idx] = get_CI_MeanOutput_MultiVariateRegression_statsModel(results_ = results_2w, x0 = numpy.float64([1.0, 0.0]), alpha = alpha_);
            # Bootstrap pairs:
            CIs_BCA_2nd_w = bootstrap.ci(data=data_BCA_2nd, statfunction=BootstrapPairs_Weighted_get_asymptotic_solution_2ndOrderRegression, alpha=alpha_, n_samples=B_samples, method='bca');
            Data_2nd_order_w.asymptotic_solution_CI_lower_BootstrapPairs[dir_idx, freq_idx] = CIs_BCA_2nd_w[0];
            Data_2nd_order_w.asymptotic_solution_CI_upper_BootstrapPairs[dir_idx, freq_idx] = CIs_BCA_2nd_w[1]; 
            # Bootstrap residuals:
            # Build the class for bootstrapping residuals (BCA) - for a global variable to be passed to the function:
            Class_results_2w = RegressionModel();
            Class_results_2w.X_ = numpy.copy(X__);
            Class_results_2w.beta = numpy.copy(results_2w.params);
            Class_results_2w.residuals = numpy.copy(results_2w.resid);
            Class_results_2w.weights = numpy.copy(Data_2nd_order_w.weights);
            Class_results = Class_results_2w;
            #####
            CIs_BCA_2nd_Resid = bootstrap.ci(data=numpy.copy(results_2w.resid), statfunction=BootstrapResiduals_Weighted_get_asymptotic_solution, alpha=alpha_, n_samples=B_samples, method='bca');
            Data_2nd_order_w.asymptotic_solution_CI_lower_BootstrapResiduals[dir_idx, freq_idx] = CIs_BCA_2nd_Resid[0];
            Data_2nd_order_w.asymptotic_solution_CI_upper_BootstrapResiduals[dir_idx, freq_idx] = CIs_BCA_2nd_Resid[1];
    return Data_2nd_order_w;

def get_Regression_Data_12_Order(N_grids, N_dirs, N_freqs, df, frequency_vector, dX_vector, final_receiver_direction_list, fs_list, PRTF_ALL_GRIDS, B_samples, alpha_ = 0.05, model_name = "1-2 order model: y0 + C1*dX + C2*dX^2", verbose_flag = False):
    '''
    Function retrieves the relevant regression data from PRTF_ALL_GRIDS input for a first and second order regression model:
    
        y = y0 + beta1*dX + beta2*dX^2;
    
    The input data comes from prepare_PRTF_data() function.
    
    For regression, the function uses statsmodel[Seabold S., Perktold J., "Statsmodels: Econometric and Statistical Modeling with Python", Proc. Of the 9th python in science conf., 
    (2010)] package. Results were double-checked to match manual regression fits.
    For bootstrapping, function uses scikits.bootstrap package.
    
    :param N_grids: int with the total number of grids from input.
    :param N_dirs: int with the number of directions.
    :param N_freqs: int with the total number of frequency bins (positive frequency) between DC and f_max_ for which PRTF data was computed.
    :param df: float with frequency resolution. Function assumes checks are already done.
    :param frequency_vector: A numpy.array with the frequency in Hz corresponding to all the frequency bins in the returned data. These bins are consistent between grids.
    
    :param dX_vector: A numpy.array (float64) of size N_grids which contain the grid spacing dX (in m) for each grid.
    :param final_receiver_direction_list: (N_dir) A list containing tuples with the receiver directions (az, el) for the simulation data (both BLOCKED and FREE-FIELD - already checked).
    :param fs_list: A numpy.array (float64) of size N_grids which contain the sampling frequency (in Hz) for each grid.
    :param PRTF_ALL_GRIDS: Numpy array (numpy.complex128) of size [N_grids, N_dirs, N_freqs] with the PRTF for which regression data is computed.
    
    :param B_samples: The number of bootstrap samples used in bootstrapping.
    :param alpha_: The confidence level (two-tailed) for the confidence interval.
    
    :param model_name: The model name. Used to write a hdf5 group.
    
    :param verbose_flag: Boolean. If True, status is printed.
    
    :returns: A converge_IO.RegressionModelData() class with all the data.   
    '''
    global Class_results;
    Data_12_order = convergence_IO.RegressionModelData(N_directions = N_dirs, N_grids = N_grids, N_frequencies = N_freqs, Model_size = 3);
    # Populate basic data:
    Data_12_order.model_name = model_name;
    Data_12_order.df = df;
    Data_12_order.B_samples = B_samples; 
    Data_12_order.frequency_vector = frequency_vector;
    Data_12_order.dX_vector = dX_vector;
    Data_12_order.final_receiver_direction_list = final_receiver_direction_list;
    Data_12_order.fs_list = fs_list;
    Data_12_order.weights = numpy.ones(N_grids, dtype = numpy.float64);
    
    #for dir_idx in range(0,1):
    for dir_idx in range(N_dirs):
        if verbose_flag:
            print "VERBOSE: 1-2 order model - DIRECTION = " + str(final_receiver_direction_list[dir_idx]);
        for freq_idx in range(0,N_freqs):
            if verbose_flag:
                print "\tVERBOSE: 1-2 order model [d_idx = " + str(dir_idx) + "] - f = " + str(frequency_vector[freq_idx]);
            # a) Get PRTF data:
            PRTF_vector = numpy.abs(PRTF_ALL_GRIDS[:, dir_idx, freq_idx]);
            #===================================================================
            # 1st order model:
            #===================================================================
            X__ = sm.add_constant(numpy.column_stack((dX_vector, dX_vector**2)));
            model_12 = sm.OLS(endog = PRTF_vector,  exog = X__ );
            results_12 = model_12.fit(method="pinv");
            Data_12_order.X_matrix = X__; 
            Data_12_order.Y_[dir_idx, freq_idx, :] = PRTF_vector;
            Data_12_order.asymptotic_solution_average[dir_idx, freq_idx] = numpy.dot( results_12.params.T, numpy.array([1.0, 0.0, 0.0], dtype = numpy.float64));
            Data_12_order.beta[dir_idx, freq_idx, :] = results_12.params;
            Data_12_order.residuals[dir_idx, freq_idx, :] = results_12.resid;
            Data_12_order.R2[dir_idx, freq_idx] = results_12.rsquared;
            Data_12_order.R2_adjusted[dir_idx, freq_idx] = results_12.rsquared_adj;# Checked with values from Python package...
            Data_12_order.p_values[dir_idx, freq_idx, :] = results_12.pvalues; # Use to test significance level: if results_12.pvalues[0] < significance_level:...
            Data_12_order.f_value[dir_idx, freq_idx] = results_12.fvalue;
            #################### CIs:
            data_BCA_12 = numpy.hstack( (numpy.column_stack((dX_vector, dX_vector**2)), numpy.asarray([PRTF_vector]).T) );
            Data_12_order.asymptotic_solution_CI_lower[dir_idx, freq_idx], Data_12_order.asymptotic_solution_CI_upper[dir_idx, freq_idx] = get_CI_MeanOutput_MultiVariateRegression_statsModel(results_ = results_12, x0 = numpy.float64([1.0, 0.0, 0.0]), alpha = alpha_);
            # Bootstrap pairs:
            CIs_BCA_12 = bootstrap.ci(data=data_BCA_12, statfunction=BootstrapPairs_get_asymptotic_solution_12OrderRegression, alpha=alpha_, n_samples=B_samples, method='bca');
            Data_12_order.asymptotic_solution_CI_lower_BootstrapPairs[dir_idx, freq_idx] = CIs_BCA_12[0];
            Data_12_order.asymptotic_solution_CI_upper_BootstrapPairs[dir_idx, freq_idx] = CIs_BCA_12[1]; 
            # Bootstrap residuals:
            # Build the class for bootstrapping residuals (BCA) - for a global variable to be passed to the function:
            Class_results_12 = RegressionModel();
            Class_results_12.X_ = numpy.copy(X__);
            Class_results_12.beta = numpy.copy(results_12.params);
            Class_results_12.residuals = numpy.copy(results_12.resid);
            Class_results = Class_results_12;
            #####
            CIs_BCA_12_Resid = bootstrap.ci(data=numpy.copy(results_12.resid), statfunction=BootstrapResiduals_get_asymptotic_solution, alpha=alpha_, n_samples=B_samples, method='bca');
            Data_12_order.asymptotic_solution_CI_lower_BootstrapResiduals[dir_idx, freq_idx] = CIs_BCA_12_Resid[0];
            Data_12_order.asymptotic_solution_CI_upper_BootstrapResiduals[dir_idx, freq_idx] = CIs_BCA_12_Resid[1];
    return Data_12_order;

def get_Regression_Data_12_Order_Weighted(N_grids, N_dirs, N_freqs, df, frequency_vector, dX_vector, final_receiver_direction_list, fs_list, PRTF_ALL_GRIDS, B_samples, alpha_ = 0.05, model_name = "1-2 order model [WEIGHTED]: y0 + C1*dX + C2*dX^2", verbose_flag = False):
    '''
    Same as get_Regression_Data_12_Order(), but for weighted data - weights according to Eca and Hoekstra (2014).
    
    See get_Regression_Data_12_Order() for all the input data.
    
    :returns: A converge_IO.RegressionModelData() class with all the data.   
    '''
    global Class_results;
    Data_12_order_w = convergence_IO.RegressionModelData(N_directions = N_dirs, N_grids = N_grids, N_frequencies = N_freqs, Model_size = 3);
    # Populate basic data:
    Data_12_order_w.model_name = model_name;
    Data_12_order_w.df = df;
    Data_12_order_w.B_samples = B_samples; 
    Data_12_order_w.frequency_vector = frequency_vector;
    Data_12_order_w.dX_vector = dX_vector;
    Data_12_order_w.final_receiver_direction_list = final_receiver_direction_list;
    Data_12_order_w.fs_list = fs_list;
    Data_12_order_w.weights = (1.0/dX_vector) / (numpy.sum(1.0/dX_vector));# Build weights for weighted least_squares according to Eca and Hoekstra (2014):
    
    #for dir_idx in range(0,1):
    for dir_idx in range(N_dirs):
        if verbose_flag:
            print "VERBOSE: 1-2 order model, WEIGTHED - DIRECTION = " + str(final_receiver_direction_list[dir_idx]);
        for freq_idx in range(0,N_freqs):
            if verbose_flag:
                print "\tVERBOSE: 1-2 order model, WEIGTHED [d_idx = " + str(dir_idx) + "] - f = " + str(frequency_vector[freq_idx]);
            # a) Get PRTF data:
            PRTF_vector = numpy.abs(PRTF_ALL_GRIDS[:, dir_idx, freq_idx]);
            #===================================================================
            # 1st order model:
            #===================================================================
            X__ = sm.add_constant(numpy.column_stack((dX_vector, dX_vector**2)));
            model_12_w = sm.WLS(endog = PRTF_vector,  exog = X__, weights = Data_12_order_w.weights);
            results_12_w = model_12_w.fit(method="pinv");
            Data_12_order_w.X_matrix = X__; 
            Data_12_order_w.Y_[dir_idx, freq_idx, :] = PRTF_vector;
            Data_12_order_w.asymptotic_solution_average[dir_idx, freq_idx] = numpy.dot( results_12_w.params.T, numpy.array([1.0, 0.0, 0.0], dtype = numpy.float64));
            Data_12_order_w.beta[dir_idx, freq_idx, :] = results_12_w.params;
            Data_12_order_w.residuals[dir_idx, freq_idx, :] = results_12_w.resid;
            Data_12_order_w.R2[dir_idx, freq_idx] = results_12_w.rsquared;
            Data_12_order_w.R2_adjusted[dir_idx, freq_idx] = results_12_w.rsquared_adj;# Checked with values from Python package...
            Data_12_order_w.p_values[dir_idx, freq_idx, :] = results_12_w.pvalues; # Use to test significance level: if results_12_w.pvalues[0] < significance_level:...
            Data_12_order_w.f_value[dir_idx, freq_idx] = results_12_w.fvalue;
            #################### CIs:
            data_BCA_12 = numpy.hstack( (numpy.column_stack((dX_vector, dX_vector**2)), numpy.asarray([PRTF_vector]).T) );
            Data_12_order_w.asymptotic_solution_CI_lower[dir_idx, freq_idx], Data_12_order_w.asymptotic_solution_CI_upper[dir_idx, freq_idx] = get_CI_MeanOutput_MultiVariateRegression_statsModel(results_ = results_12_w, x0 = numpy.float64([1.0, 0.0, 0.0]), alpha = alpha_);
            # Bootstrap pairs:
            CIs_BCA_12_w = bootstrap.ci(data=data_BCA_12, statfunction=BootstrapPairs_Weighted_get_asymptotic_solution_12OrderRegression, alpha=alpha_, n_samples=B_samples, method='bca');
            Data_12_order_w.asymptotic_solution_CI_lower_BootstrapPairs[dir_idx, freq_idx] = CIs_BCA_12_w[0];
            Data_12_order_w.asymptotic_solution_CI_upper_BootstrapPairs[dir_idx, freq_idx] = CIs_BCA_12_w[1]; 
            # Bootstrap residuals:
            # Build the class for bootstrapping residuals (BCA) - for a global variable to be passed to the function:
            Class_results_12_w = RegressionModel();
            Class_results_12_w.X_ = numpy.copy(X__);
            Class_results_12_w.beta = numpy.copy(results_12_w.params);
            Class_results_12_w.residuals = numpy.copy(results_12_w.resid);
            Class_results_12_w.weights = numpy.copy(Data_12_order_w.weights);
            Class_results = Class_results_12_w;
            #####
            CIs_BCA_12_Resid_w = bootstrap.ci(data=numpy.copy(results_12_w.resid), statfunction=BootstrapResiduals_Weighted_get_asymptotic_solution, alpha=alpha_, n_samples=B_samples, method='bca');
            Data_12_order_w.asymptotic_solution_CI_lower_BootstrapResiduals[dir_idx, freq_idx] = CIs_BCA_12_Resid_w[0];
            Data_12_order_w.asymptotic_solution_CI_upper_BootstrapResiduals[dir_idx, freq_idx] = CIs_BCA_12_Resid_w[1];
    return Data_12_order_w;

def get_Regression_Data_m1Order(N_grids, N_dirs, N_freqs, df, frequency_vector, dX_vector, final_receiver_direction_list, fs_list, PRTF_ALL_GRIDS, B_samples, alpha_ = 0.05, model_name = "m1 order model: y0 + C1*dX^(-1)", verbose_flag = False):
    '''
    Function retrieves the relevant regression data from PRTF_ALL_GRIDS input for a divergence regression model:
    
        y = y0 + beta1*dX^(-1);
    
    !! Note that this model cannot predict the asymptotic solution at dX = 0 since it diverges. As such, no CIs are calculated as they do not make any sense.
        The 'asymptotic solution' is computed as y0 but it is incorrect!
    
    The input data comes from prepare_PRTF_data() function.
    
    For regression, the function uses statsmodel[Seabold S., Perktold J., "Statsmodels: Econometric and Statistical Modeling with Python", Proc. Of the 9th python in science conf., 
    (2010)] package. Results were double-checked to match manual regression fits.
    For bootstrapping, function uses scikits.bootstrap package.
    
    :param N_grids: int with the total number of grids from input.
    :param N_dirs: int with the number of directions.
    :param N_freqs: int with the total number of frequency bins (positive frequency) between DC and f_max_ for which PRTF data was computed.
    :param df: float with frequency resolution. Function assumes checks are already done.
    :param frequency_vector: A numpy.array with the frequency in Hz corresponding to all the frequency bins in the returned data. These bins are consistent between grids.
    
    :param dX_vector: A numpy.array (float64) of size N_grids which contain the grid spacing dX (in m) for each grid.
    :param final_receiver_direction_list: (N_dir) A list containing tuples with the receiver directions (az, el) for the simulation data (both BLOCKED and FREE-FIELD - already checked).
    :param fs_list: A numpy.array (float64) of size N_grids which contain the sampling frequency (in Hz) for each grid.
    :param PRTF_ALL_GRIDS: Numpy array (numpy.complex128) of size [N_grids, N_dirs, N_freqs] with the PRTF for which regression data is computed.
    
    :param B_samples: The number of bootstrap samples used in bootstrapping.
    :param alpha_: The confidence level (two-tailed) for the confidence interval.
    
    :param model_name: The model name. Used to write a hdf5 group.
    
    :param verbose_flag: Boolean. If True, status is printed.
    
    :returns: A converge_IO.RegressionModelData() class with all the data.   
    '''
    global Class_results;
    Data_m1_order = convergence_IO.RegressionModelData(N_directions = N_dirs, N_grids = N_grids, N_frequencies = N_freqs, Model_size = 2);
    # Populate basic data:
    Data_m1_order.model_name = model_name;
    Data_m1_order.df = df;
    Data_m1_order.B_samples = B_samples; 
    Data_m1_order.frequency_vector = frequency_vector;
    Data_m1_order.dX_vector = dX_vector;
    Data_m1_order.final_receiver_direction_list = final_receiver_direction_list;
    Data_m1_order.fs_list = fs_list;
    Data_m1_order.weights = numpy.ones(N_grids, dtype = numpy.float64);
    
    #for dir_idx in range(0,1):
    for dir_idx in range(N_dirs):
        if verbose_flag:
            print "VERBOSE: -1 order model - DIRECTION = " + str(final_receiver_direction_list[dir_idx]);
        for freq_idx in range(0,N_freqs):
            if verbose_flag:
                print "\tVERBOSE: -1 order model [d_idx = " + str(dir_idx) + "] - f = " + str(frequency_vector[freq_idx]);
            # a) Get PRTF data:
            PRTF_vector = numpy.abs(PRTF_ALL_GRIDS[:, dir_idx, freq_idx]);
            #===================================================================
            # 1st order model:
            #===================================================================
            X__ = sm.add_constant( 1.0/dX_vector );
            model_m1 = sm.OLS(endog = PRTF_vector,  exog = X__ );
            results_m1 = model_m1.fit(method="pinv");
            Data_m1_order.X_matrix = X__; 
            Data_m1_order.Y_[dir_idx, freq_idx, :] = PRTF_vector;
            Data_m1_order.asymptotic_solution_average[dir_idx, freq_idx] = numpy.dot( results_m1.params.T, numpy.array([1.0, 0.0], dtype = numpy.float64)); # This is incorrect!
            Data_m1_order.beta[dir_idx, freq_idx, :] = results_m1.params;
            Data_m1_order.residuals[dir_idx, freq_idx, :] = results_m1.resid;
            Data_m1_order.R2[dir_idx, freq_idx] = results_m1.rsquared;
            Data_m1_order.R2_adjusted[dir_idx, freq_idx] = results_m1.rsquared_adj;# Checked with values from Python package...
            Data_m1_order.p_values[dir_idx, freq_idx, :] = results_m1.pvalues; # Use to test significance level: if results_m1.pvalues[0] < significance_level:...
            Data_m1_order.f_value[dir_idx, freq_idx] = results_m1.fvalue;
            #################### CIs:
            # Data makes no sense to have CIs for the asymptotic solution.
    return Data_m1_order;

def get_Regression_Data_m1Order_Weighted(N_grids, N_dirs, N_freqs, df, frequency_vector, dX_vector, final_receiver_direction_list, fs_list, PRTF_ALL_GRIDS, B_samples, alpha_ = 0.05, model_name = "m1 order model [WEIGHTED]: y0 + C1*dX^(-1)", verbose_flag = False):
    '''
    Same as get_Regression_Data_m1Order() but for weighted results.
    
    Again, there is no asymptotic solution in this case since solution is diverging.
    
    See more in get_Regression_Data_m1Order().
    '''
    global Class_results;
    Data_m1_order_w = convergence_IO.RegressionModelData(N_directions = N_dirs, N_grids = N_grids, N_frequencies = N_freqs, Model_size = 2);
    # Populate basic data:
    Data_m1_order_w.model_name = model_name;
    Data_m1_order_w.df = df;
    Data_m1_order_w.B_samples = B_samples; 
    Data_m1_order_w.frequency_vector = frequency_vector;
    Data_m1_order_w.dX_vector = dX_vector;
    Data_m1_order_w.final_receiver_direction_list = final_receiver_direction_list;
    Data_m1_order_w.fs_list = fs_list;
    Data_m1_order_w.weights = (1.0/dX_vector) / (numpy.sum(1.0/dX_vector));# Build weights for weighted least_squares according to Eca and Hoekstra (2014):
    
    #for dir_idx in range(0,1):
    for dir_idx in range(N_dirs):
        if verbose_flag:
            print "VERBOSE: -1 order model, WEIGHTED - DIRECTION = " + str(final_receiver_direction_list[dir_idx]);
        for freq_idx in range(0,N_freqs):
            if verbose_flag:
                print "\tVERBOSE: -1 order model, WEIGHTED [d_idx = " + str(dir_idx) + "] - f = " + str(frequency_vector[freq_idx]);
            # a) Get PRTF data:
            PRTF_vector = numpy.abs(PRTF_ALL_GRIDS[:, dir_idx, freq_idx]);
            #===================================================================
            # 1st order model:
            #===================================================================
            X__ = sm.add_constant( 1.0/dX_vector );
            model_m1 = sm.WLS(endog = PRTF_vector,  exog = X__ , weights = Data_m1_order_w.weights);
            results_m1 = model_m1.fit(method="pinv");
            Data_m1_order_w.X_matrix = X__; 
            Data_m1_order_w.Y_[dir_idx, freq_idx, :] = PRTF_vector;
            Data_m1_order_w.asymptotic_solution_average[dir_idx, freq_idx] = numpy.dot( results_m1.params.T, numpy.array([1.0, 0.0], dtype = numpy.float64)); # This is incorrect!
            Data_m1_order_w.beta[dir_idx, freq_idx, :] = results_m1.params;
            Data_m1_order_w.residuals[dir_idx, freq_idx, :] = results_m1.resid;
            Data_m1_order_w.R2[dir_idx, freq_idx] = results_m1.rsquared;
            Data_m1_order_w.R2_adjusted[dir_idx, freq_idx] = results_m1.rsquared_adj;# Checked with values from Python package...
            Data_m1_order_w.p_values[dir_idx, freq_idx, :] = results_m1.pvalues; # Use to test significance level: if results_m1.pvalues[0] < significance_level:...
            Data_m1_order_w.f_value[dir_idx, freq_idx] = results_m1.fvalue;
            #################### CIs:
            # Data makes no sense to have CIs for the asymptotic solution.
    return Data_m1_order_w;

def get_Regression_Data_1m1Order(N_grids, N_dirs, N_freqs, df, frequency_vector, dX_vector, final_receiver_direction_list, fs_list, PRTF_ALL_GRIDS, B_samples, alpha_ = 0.05, model_name = "1m1 order model: y0 + C1*dX + C2*dX^(-1)", verbose_flag = False):
    '''
    Function retrieves the relevant regression data from PRTF_ALL_GRIDS input for a regression model of the form:
    
        y = y0 + beta1*dX^(-1) + beta2*dX^(-1);
    
    !! Note that this model cannot predict the asymptotic solution at dX = 0 since it diverges. As such, no CIs are calculated as they do not make any sense.
        The 'asymptotic solution' is computed as y0 but it is incorrect!
    
    The input data comes from prepare_PRTF_data() function.
    
    For regression, the function uses statsmodel[Seabold S., Perktold J., "Statsmodels: Econometric and Statistical Modeling with Python", Proc. Of the 9th python in science conf., 
    (2010)] package. Results were double-checked to match manual regression fits.
    For bootstrapping, function uses scikits.bootstrap package.
    
    :param N_grids: int with the total number of grids from input.
    :param N_dirs: int with the number of directions.
    :param N_freqs: int with the total number of frequency bins (positive frequency) between DC and f_max_ for which PRTF data was computed.
    :param df: float with frequency resolution. Function assumes checks are already done.
    :param frequency_vector: A numpy.array with the frequency in Hz corresponding to all the frequency bins in the returned data. These bins are consistent between grids.
    
    :param dX_vector: A numpy.array (float64) of size N_grids which contain the grid spacing dX (in m) for each grid.
    :param final_receiver_direction_list: (N_dir) A list containing tuples with the receiver directions (az, el) for the simulation data (both BLOCKED and FREE-FIELD - already checked).
    :param fs_list: A numpy.array (float64) of size N_grids which contain the sampling frequency (in Hz) for each grid.
    :param PRTF_ALL_GRIDS: Numpy array (numpy.complex128) of size [N_grids, N_dirs, N_freqs] with the PRTF for which regression data is computed.
    
    :param B_samples: The number of bootstrap samples used in bootstrapping.
    :param alpha_: The confidence level (two-tailed) for the confidence interval.
    
    :param model_name: The model name. Used to write a hdf5 group.
    
    :param verbose_flag: Boolean. If True, status is printed.
    
    :returns: A converge_IO.RegressionModelData() class with all the data.   
    '''
    global Class_results;
    Data_1m1_order = convergence_IO.RegressionModelData(N_directions = N_dirs, N_grids = N_grids, N_frequencies = N_freqs, Model_size = 3);
    # Populate basic data:
    Data_1m1_order.model_name = model_name;
    Data_1m1_order.df = df;
    Data_1m1_order.B_samples = B_samples; 
    Data_1m1_order.frequency_vector = frequency_vector;
    Data_1m1_order.dX_vector = dX_vector;
    Data_1m1_order.final_receiver_direction_list = final_receiver_direction_list;
    Data_1m1_order.fs_list = fs_list;
    Data_1m1_order.weights = numpy.ones(N_grids, dtype = numpy.float64);
    
    #for dir_idx in range(0,1):
    for dir_idx in range(N_dirs):
        if verbose_flag:
            print "VERBOSE: 1,-1 order model - DIRECTION = " + str(final_receiver_direction_list[dir_idx]);
        for freq_idx in range(0,N_freqs):
            if verbose_flag:
                print "\tVERBOSE: 1,-1 order model [d_idx = " + str(dir_idx) + "] - f = " + str(frequency_vector[freq_idx]);
            # a) Get PRTF data:
            PRTF_vector = numpy.abs(PRTF_ALL_GRIDS[:, dir_idx, freq_idx]);
            #===================================================================
            # 1st order model:
            #===================================================================
            X__ = sm.add_constant(numpy.column_stack((dX_vector, 1.0/dX_vector)));
            model_1m1 = sm.OLS(endog = PRTF_vector,  exog = X__ );
            results_1m1 = model_1m1.fit(method="pinv");
            Data_1m1_order.X_matrix = X__; 
            Data_1m1_order.Y_[dir_idx, freq_idx, :] = PRTF_vector;
            Data_1m1_order.asymptotic_solution_average[dir_idx, freq_idx] = numpy.dot( results_1m1.params.T, numpy.array([1.0, 0.0, 0.0], dtype = numpy.float64)); # This is incorrect!
            Data_1m1_order.beta[dir_idx, freq_idx, :] = results_1m1.params;
            Data_1m1_order.residuals[dir_idx, freq_idx, :] = results_1m1.resid;
            Data_1m1_order.R2[dir_idx, freq_idx] = results_1m1.rsquared;
            Data_1m1_order.R2_adjusted[dir_idx, freq_idx] = results_1m1.rsquared_adj;# Checked with values from Python package...
            Data_1m1_order.p_values[dir_idx, freq_idx, :] = results_1m1.pvalues; # Use to test significance level: if results_1m1.pvalues[0] < significance_level:...
            Data_1m1_order.f_value[dir_idx, freq_idx] = results_1m1.fvalue;
            #################### CIs:
            # Data makes no sense to have CIs for the asymptotic solution.
    return Data_1m1_order;

def get_Regression_Data_1m1Order_Weighted(N_grids, N_dirs, N_freqs, df, frequency_vector, dX_vector, final_receiver_direction_list, fs_list, PRTF_ALL_GRIDS, B_samples, alpha_ = 0.05, model_name = "1m1 order model [WEIGHTED]: y0 + C1*dX + C2*dX^(-1)", verbose_flag = False):
    '''
    Same as get_Regression_Data_1m1Order() but for weighted results.
    
    Again, there is no asymptotic solution in this case since solution is diverging.
    
    See more in get_Regression_Data_1m1Order().
    '''
    global Class_results;
    Data_1m1_order_w = convergence_IO.RegressionModelData(N_directions = N_dirs, N_grids = N_grids, N_frequencies = N_freqs, Model_size = 3);
    # Populate basic data:
    Data_1m1_order_w.model_name = model_name;
    Data_1m1_order_w.df = df;
    Data_1m1_order_w.B_samples = B_samples; 
    Data_1m1_order_w.frequency_vector = frequency_vector;
    Data_1m1_order_w.dX_vector = dX_vector;
    Data_1m1_order_w.final_receiver_direction_list = final_receiver_direction_list;
    Data_1m1_order_w.fs_list = fs_list;
    Data_1m1_order_w.weights = (1.0/dX_vector) / (numpy.sum(1.0/dX_vector));# Build weights for weighted least_squares according to Eca and Hoekstra (2014):
    
    #for dir_idx in range(0,1):
    for dir_idx in range(N_dirs):
        if verbose_flag:
            print "VERBOSE: 1,-1 order model, WEIGHTED - DIRECTION = " + str(final_receiver_direction_list[dir_idx]);
        for freq_idx in range(0,N_freqs):
            if verbose_flag:
                print "\tVERBOSE: 1,-1 order model, WEIGHTED [d_idx = " + str(dir_idx) + "] - f = " + str(frequency_vector[freq_idx]);
            # a) Get PRTF data:
            PRTF_vector = numpy.abs(PRTF_ALL_GRIDS[:, dir_idx, freq_idx]);
            #===================================================================
            # 1st order model:
            #===================================================================
            X__ = sm.add_constant(numpy.column_stack((dX_vector, 1.0/dX_vector)));
            model_m1 = sm.WLS(endog = PRTF_vector,  exog = X__ , weights = Data_1m1_order_w.weights);
            results_m1 = model_m1.fit(method="pinv");
            Data_1m1_order_w.X_matrix = X__; 
            Data_1m1_order_w.Y_[dir_idx, freq_idx, :] = PRTF_vector;
            Data_1m1_order_w.asymptotic_solution_average[dir_idx, freq_idx] = numpy.dot( results_m1.params.T, numpy.array([1.0, 0.0, 0.0], dtype = numpy.float64)); # This is incorrect!
            Data_1m1_order_w.beta[dir_idx, freq_idx, :] = results_m1.params;
            Data_1m1_order_w.residuals[dir_idx, freq_idx, :] = results_m1.resid;
            Data_1m1_order_w.R2[dir_idx, freq_idx] = results_m1.rsquared;
            Data_1m1_order_w.R2_adjusted[dir_idx, freq_idx] = results_m1.rsquared_adj;# Checked with values from Python package...
            Data_1m1_order_w.p_values[dir_idx, freq_idx, :] = results_m1.pvalues; # Use to test significance level: if results_m1.pvalues[0] < significance_level:...
            Data_1m1_order_w.f_value[dir_idx, freq_idx] = results_m1.fvalue;
            #################### CIs:
            # Data makes no sense to have CIs for the asymptotic solution.
    return Data_1m1_order_w;

def plot_p_values_12Order(Data_12_order, N_dirs, N_freqs):
    '''
    Function plots the percentage that the second order term is relevan (i.e., p<0.005 for beta2) for all directions and 1-2 order model:
    
        y = y0 + beta1*dX + beta2*dX^2
    
    :param Data_12_order: A convergence_IO.RegressionModelData class with data for the 1-2 order model.
    :param N_dirs: The total number of directions.
    :param N_freqs: The total number of frequencies.
    '''
    sig_level_ = 0.005;
    percentage_frequency = numpy.zeros(N_freqs, dtype = numpy.int32);
    count_global = 0;
    count_global_1st = 0;
    count_global_cst = 0;
    count_global_F = 0;
    i_ = 0; 
    for dir_idx in range(N_dirs):
        print "DIR " + str(dir_idx);
        for freq_idx in range(N_freqs):
            if Data_12_order.p_values[dir_idx, freq_idx, 2] <= sig_level_:
                percentage_frequency[freq_idx] = percentage_frequency[freq_idx] + 1;
                count_global = count_global + 1;
            if Data_12_order.p_values[dir_idx, freq_idx, 1] <= sig_level_:
                count_global_1st = count_global_1st + 1;
            if Data_12_order.p_values[dir_idx, freq_idx, 0] <= sig_level_:
                count_global_cst = count_global_cst + 1;
            if Data_12_order.f_value[dir_idx, freq_idx] <= sig_level_: # Not correct this one...
                count_global_F = count_global_F + 1;
            i_ = i_ + 1;
    percentage_global = numpy.round(1.0*count_global / (1.0*N_dirs*N_freqs) * 100.0, 2);
    percentage_global_1st = numpy.round(1.0*count_global_1st / (1.0*N_dirs*N_freqs) * 100.0, 2);
    percentage_global_cst = numpy.round(1.0*count_global_cst / (1.0*N_dirs*N_freqs) * 100.0, 2);
    percentage_global_F = numpy.round(1.0*count_global_F / (1.0*N_dirs*N_freqs) * 100.0, 2); 
    print "Global percentage (dx^2):"
    print str(percentage_global) + "[%]";
    print "==="
    print "Global percentage (dx):"
    print str(percentage_global_1st) + "[%]";
    print "==="
    print "Global percentage (Constant):"
    print str(percentage_global_cst) + "[%]";
    print "==="
    print "Global percentage (F-value) - WRONG:"
    print str(percentage_global_F) + "[%]";
    print "--------------"

def plot_R2_1_AND_2_Order(Data_1st_order, Data_2nd_order, Data_1st_order_w, Data_2nd_order_w, N_dirs, N_freqs):
    '''
    Function studies the coefficient of determination R^2 (no need for R2_adjusted for these models) for the two models:
    
        y = y0 + beta1*dX
        y = y0 + beta2*dX^2
    
    :param Data_1st_order: A convergence_IO.RegressionModelData class with data for the 1st order model.
    :param Data_2nd_order: A convergence_IO.RegressionModelData class with data for the 2nd order model.
    :param Data_1st_order_w: A convergence_IO.RegressionModelData class with data for the 1st order model [WEIGHTED].
    :param Data_2nd_order_w: A convergence_IO.RegressionModelData class with data for the 2nd order model [WEIGHTED].
    :param N_dirs: The total number of directions.
    :param N_freqs: The total number of frequencies.
    '''
    count_R2_higher_1st_global = 0;
    count_R2_higher_1st_frequency = numpy.zeros(N_freqs, dtype = numpy.int32);
    count_R2_higher_1st_global_w = 0;
    count_R2_higher_1st_frequency_w = numpy.zeros(N_freqs, dtype = numpy.int32);
    relative_error_R2_vs_R1 = [];
    relative_error_R2_vs_R1_w = [];
    i_ = 0; 
    for dir_idx in range(N_dirs):
        print "DIR " + str(dir_idx);
        for freq_idx in range(N_freqs):
            if Data_1st_order.R2[dir_idx, freq_idx] >= Data_2nd_order.R2[dir_idx, freq_idx]:
                count_R2_higher_1st_frequency[freq_idx] = count_R2_higher_1st_frequency[freq_idx] + 1;
                count_R2_higher_1st_global = count_R2_higher_1st_global + 1;
            if Data_1st_order_w.R2[dir_idx, freq_idx] >= Data_2nd_order_w.R2[dir_idx, freq_idx]:
                count_R2_higher_1st_frequency_w[freq_idx] = count_R2_higher_1st_frequency_w[freq_idx] + 1;
                count_R2_higher_1st_global_w = count_R2_higher_1st_global_w + 1;
            # Relative error:
            rel_ = (Data_2nd_order.R2[dir_idx, freq_idx] - Data_1st_order.R2[dir_idx, freq_idx])/Data_1st_order.R2[dir_idx, freq_idx]*100.0;
            relative_error_R2_vs_R1.append( rel_ );
            rel_w = (Data_2nd_order_w.R2[dir_idx, freq_idx] - Data_1st_order_w.R2[dir_idx, freq_idx])/Data_1st_order_w.R2[dir_idx, freq_idx]*100.0;
            relative_error_R2_vs_R1_w.append( rel_w );
            i_ = i_ + 1;
    percentage_global = numpy.round(1.0*count_R2_higher_1st_global / (1.0*N_dirs*N_freqs) * 100.0, 2);
    percentage_frequency = numpy.round(1.0*count_R2_higher_1st_frequency / (1.0*N_dirs) * 100.0, 2);
    percentage_global_w = numpy.round(1.0*count_R2_higher_1st_global_w / (1.0*N_dirs*N_freqs) * 100.0, 2);
    percentage_frequency_w = numpy.round(1.0*count_R2_higher_1st_frequency_w / (1.0*N_dirs) * 100.0, 2);
    print "Global percentage (R2 1st order >= R2 2nd order):"
    print str(percentage_global) + "[%]";
    print "=========================="
    print "Global percentage (R2 1st order >= R2 2nd order) - WEIGHTED:"
    print str(percentage_global_w) + "[%]";
    print "==="
    plt.figure(1);
    plt.plot(Data_1st_order.frequency_vector, percentage_frequency, color = 'b', label = "1 vs 2");
    plt.plot(Data_1st_order_w.frequency_vector, percentage_frequency_w, color = 'g', label = "1 vs 2 [WEIGHTED]");
    plt.plot([Data_1st_order.frequency_vector[0], Data_1st_order.frequency_vector[-1]], [50.0,50.0], color = 'r', linewidth = 1.0);
    plt.legend(loc='best', borderaxespad=1., fancybox=True, shadow=False);
    plt.xlabel('f [Hz]');
    plt.ylabel(r'$R^2[1^{st}] \geq R^2[2^{nd}]$ [%]');
    plt.xlim(0.0, Data_1st_order.frequency_vector[-1] );
    plt.ylim(0.0, 100.0 );
    plt.grid(True, which = "both");
    plt.figure(2);
    plt.title("Relative difference between 2nd order and 1st order");
    n, bins, patches = plt.hist(relative_error_R2_vs_R1, bins = 50, range = (-100.0, 100.0), normed=1, facecolor='b', alpha=0.4, label = "2nd vs 1st");
    n, bins, patches = plt.hist(relative_error_R2_vs_R1_w, bins = 50, range = (-100.0, 100.0), normed=1, facecolor='g', alpha=0.4, label = "2nd vs 1st [WEIGHTED]");
    #n, bins, patches = plt.hist(relative_error_R2_vs_R1, bins = 50, range = (100.0, max(relative_error_R2_vs_R1)), normed=1, facecolor='green', alpha=0.75); # very small frequency
    plt.legend(loc='best', borderaxespad=1., fancybox=True, shadow=False);
    plt.grid(True, which = "both");
    plt.show();

def plot_Weighted_vs_Non_Weighted_Asymptotic_Diff(Data_1st_order, Data_1st_order_w, Data_2nd_order, Data_2nd_order_w, Data_12_order, Data_12_order_w, N_dirs, N_freqs):
    '''
    Function plots the maximal absolute differences in the predicted asymptotic solution for the weighted and non-weighted case. 
    '''
    diff_dB_1st = [];
    diff_dB_2nd = [];
    diff_dB_12 = [];
    plt.figure(1);
    for dir_idx in range(N_dirs):
        #Data_1st_order.asymptotic_solution_average[dir_idx, Data_1st_order.asymptotic_solution_average[dir_idx, :]<=0.0] = 1E-7;
        #Data_1st_order_w.asymptotic_solution_average[dir_idx, Data_1st_order_w.asymptotic_solution_average[dir_idx, :]<=0.0] = 1E-7;
        if dir_idx == 0:
            plt.plot(Data_12_order.frequency_vector, numpy.abs(20.0*numpy.log10(Data_12_order.asymptotic_solution_average[dir_idx, :]) - 20.0*numpy.log10(Data_12_order_w.asymptotic_solution_average[dir_idx, :])), color = 'r', label = "1-2 order, Dir" + str(dir_idx), lw = 1.0, alpha = 0.2);
            plt.plot(Data_2nd_order.frequency_vector, numpy.abs(20.0*numpy.log10(Data_2nd_order.asymptotic_solution_average[dir_idx, :]) - 20.0*numpy.log10(Data_2nd_order_w.asymptotic_solution_average[dir_idx, :])), color = 'g', label = "2nd order, Dir" + str(dir_idx), lw = 1.0, alpha = 0.7);
            plt.plot(Data_1st_order.frequency_vector, numpy.abs(20.0*numpy.log10(Data_1st_order.asymptotic_solution_average[dir_idx, :]) - 20.0*numpy.log10(Data_1st_order_w.asymptotic_solution_average[dir_idx, :])), color = 'b', label = "1st order, Dir" + str(dir_idx), lw = 1.0, alpha = 0.7);
        else:
            plt.plot(Data_12_order.frequency_vector, numpy.abs(20.0*numpy.log10(Data_12_order.asymptotic_solution_average[dir_idx, :]) - 20.0*numpy.log10(Data_12_order_w.asymptotic_solution_average[dir_idx, :])), color = 'r', lw = 1.0, alpha = 0.2);
            plt.plot(Data_2nd_order.frequency_vector, numpy.abs(20.0*numpy.log10(Data_2nd_order.asymptotic_solution_average[dir_idx, :]) - 20.0*numpy.log10(Data_2nd_order_w.asymptotic_solution_average[dir_idx, :])), color = 'g', lw = 1.0, alpha = 0.7);
            plt.plot(Data_1st_order.frequency_vector, numpy.abs(20.0*numpy.log10(Data_1st_order.asymptotic_solution_average[dir_idx, :]) - 20.0*numpy.log10(Data_1st_order_w.asymptotic_solution_average[dir_idx, :])), color = 'b', lw = 1.0, alpha = 0.7);
        diff_dB_1st.append(20.0*numpy.log10(Data_1st_order.asymptotic_solution_average[dir_idx, :]) - 20.0*numpy.log10(Data_1st_order_w.asymptotic_solution_average[dir_idx, :]));
        diff_dB_2nd.append(20.0*numpy.log10(Data_2nd_order.asymptotic_solution_average[dir_idx, :]) - 20.0*numpy.log10(Data_2nd_order_w.asymptotic_solution_average[dir_idx, :]));
        diff_dB_12.append(20.0*numpy.log10(Data_12_order.asymptotic_solution_average[dir_idx, :]) - 20.0*numpy.log10(Data_12_order_w.asymptotic_solution_average[dir_idx, :]));
    plt.legend(loc='best', borderaxespad=1., fancybox=True, shadow=False);
    plt.xlim(0.0, Data_1st_order.frequency_vector[-1] );
    plt.ylim(0.0, 2.0 );
    plt.grid(True, which = "both");
    plt.xlabel('f [Hz]');
    plt.ylabel(r'$\Delta$ [dB]');
    
    plt.figure(2);
    plt.hist(diff_dB_12, bins = 50, range = (0.0, 2.0), normed=1, facecolor='r', alpha=0.2, label = "1-2 order");
    plt.hist(diff_dB_1st, bins = 50, range = (0.0, 2.0), normed=1, facecolor='b', alpha=0.4, label = "1st order");
    plt.hist(diff_dB_2nd, bins = 50, range = (0.0, 2.0), normed=1, facecolor='g', alpha=0.4, label = "2nd order");
    plt.legend(loc='best', borderaxespad=1., fancybox=True, shadow=False);
    plt.grid(True, which = "both");
    plt.show();

def check_consistency_grids(c_sound_G1, c_sound_G2, c_sound_G3, \
                            lambda_sim_G1, lambda_sim_G2, lambda_sim_G3, \
                            source_type_G1, source_type_G2, source_type_G3, \
                            no_of_receivers_per_direction_G1, no_of_receivers_per_direction_G2, no_of_receivers_per_direction_G3, \
                            voxelization_type_string_G1, voxelization_type_string_G2, voxelization_type_string_G3, \
                            n_rec_G1, n_rec_G2, n_rec_G3, fs_G1, fs_G2, fs_G3, N_s_G1, N_s_G2, N_s_G3):
    '''
    Function checks some consistencies for the three grids used.
    
    :param c_sound_G1-3: The sound speed for grids 1-3.
    :param lambda_sim_G1-3: The courant number for grids 1-3.
    :param source_type_G1-3: The source type for grids 1-3.
    :param no_of_receivers_per_direction_G1-3: The number of receivers per diection for grids 1-3.
    :param voxelization_type_string_G1-3: The voxelization type for grids 1-3.
    
    :param n_rec_G1-3: The number of receivers for G1-3.
    :param fs_G1-3: The sampling frequency for G1-3.
    :param N_s_G1-3: The number of samples in PRTF for G1-3.
    '''
    if (c_sound_G1 != c_sound_G2) or (c_sound_G1 != c_sound_G3):
        print "  c_sound_G1 = " + str(c_sound_G1);
        print "  c_sound_G2 = " + str(c_sound_G2);
        print "  c_sound_G3 = " + str(c_sound_G3);
        raise NameError("The sound speed between grids are different!");
    if (lambda_sim_G1 != lambda_sim_G2) or (lambda_sim_G1 != lambda_sim_G3):
        print "  lambda_sim_G1 = " + str(lambda_sim_G1);
        print "  lambda_sim_G2 = " + str(lambda_sim_G2);
        print "  lambda_sim_G3 = " + str(lambda_sim_G3);
        raise NameError("The courant numbers between grids are different!");
    if (source_type_G1 != source_type_G2) or (source_type_G1 != source_type_G3):
        print "  source_type_G1 = " + str(source_type_G1);
        print "  source_type_G2 = " + str(source_type_G2);
        print "  source_type_G3 = " + str(source_type_G3);
        raise NameError("The source types between grids are different!");
    if (no_of_receivers_per_direction_G1 != no_of_receivers_per_direction_G2) or (no_of_receivers_per_direction_G1 != no_of_receivers_per_direction_G3):
        print "  no_of_receivers_per_direction_G1 = " + str(no_of_receivers_per_direction_G1);
        print "  no_of_receivers_per_direction_G2 = " + str(no_of_receivers_per_direction_G2);
        print "  no_of_receivers_per_direction_G3 = " + str(no_of_receivers_per_direction_G3);
        warnings.warn("The number of receivers per direction (used for interpolation) between grids is different!");
    if (voxelization_type_string_G1 != voxelization_type_string_G2) or (voxelization_type_string_G1 != voxelization_type_string_G3):
        print "  voxelization_type_string_G1 = " + str(voxelization_type_string_G1);
        print "  voxelization_type_string_G2 = " + str(voxelization_type_string_G2);
        print "  voxelization_type_string_G3 = " + str(voxelization_type_string_G3);
        raise NameError("The voxelization types between grids are different!");
    if (n_rec_G1 != n_rec_G2) or (n_rec_G1 != n_rec_G3):
        print "  n_G1 = " + str(n_rec_G1);
        print "  n_G2 = " + str(n_rec_G2);
        print "  n_G3 = " + str(n_rec_G3);
        raise NameError("The number of receivers between grids are different!");
    if (1.0*fs_G1/N_s_G1 != 1.0*fs_G2/N_s_G2) or (1.0*fs_G1/N_s_G1 != 1.0*fs_G3/N_s_G3):
        print "  df_G1 = " + str(1.0*fs_G1/N_s_G1);
        print "  df_G2 = " + str(1.0*fs_G2/N_s_G2);
        print "  df_G3 = " + str(1.0*fs_G3/N_s_G3);
        #warnings.warn("The frequency resolutions between grids are different!");
        raise NameError("The frequency resolutions between grids are different!");
    print "\t" + print_colors.BG_GREEN + "Checks OK" + print_colors.END + print_colors.FG_GREEN + " - Passed Grid consistency!" + print_colors.END;
    
def check_receiver_directions_and_intended_locations(direction_list, direction_list_, intended_locations_m, intended_locations_m_, file_name):
    '''
    Function checks the consistency of the direction list ( list containing (aimuth_degree, elevation_degree) pairs ) and the consistency
    of the receiver_interpolation_location_m (list containing the Cartesian coordinates in m [x_m, y_m, z_m] for each receiver in the direction list).
    
    Baseline assumed the variables without underscore suffix ("_").
    
    :param direction_list: Direction list 1).
    :param direction_list_: Direction list 2). Will be compared with direction_list.
    :param intended_locations_m: List containing numpy.arrays of len 3.
    :param intended_locations_m_: List containing numpy.arrays of len 3. Will be compared with intended_locations_m.
    
    :param file_name: A string containing the file-name to be chekced. Used only for error reporting
    '''
    if len(direction_list) != len(intended_locations_m) or len(direction_list_) != len(intended_locations_m_) or len(direction_list) != len(direction_list_):
        raise NameError("Lengths inconsistencies while checking for direction list and receiver location list.");
    for direction_idx in range(len(direction_list_)):
        if direction_list[direction_idx][0] != direction_list_[direction_idx][0] or direction_list[direction_idx][1] != direction_list_[direction_idx][1]:
            print str(file_name);
            raise NameError("Direction inconsistency for dir #" + str(direction_idx) + ": baseline = " + str(direction_list[direction_idx]) + ", file = " + str(direction_list_[direction_idx]));
        if intended_locations_m_[direction_idx][0] != intended_locations_m[direction_idx][0] or intended_locations_m_[direction_idx][1] != intended_locations_m[direction_idx][1] or intended_locations_m_[direction_idx][2] != intended_locations_m[direction_idx][2]:
            print str(file_name);
            raise NameError("Receiver interpolation continuous location [m] for dir #" + str(direction_idx) + ": baseline = " + str(intended_locations_m[direction_idx]) + ", file = " + str(intended_locations_m_[direction_idx]));

def check_frequency_vectors_2grids(freq_PRIR_G1, freq_PRIR_G2, dF1, dF2, grid_no):
    '''
    Identical to check_frequency_vectors() but only for two grids.
    
    :param freq_PRIR_G1: The frequency vector for Grid 1.
    :param freq_PRIR_G2: The frequency vector for Grid 2.
    :param dF1: The frequency resolution for Grid 1.
    :param dF2: The frequency resolution for Grid 2.
    :param grid_no: Not important. Used in messaging.
    '''
    if (len(freq_PRIR_G1) != len(freq_PRIR_G2)):
        raise NameError("Inconsistent frequency vectors for G" + str(grid_no) + ": different lenghts... Freq_V1 len = " + str(len(freq_PRIR_G1)) + ", Freq_V2 len" + str(len(freq_PRIR_G2)));
    if (dF1 != dF2):
        raise NameError("Inconsistent frequency resolutions for G" + str(grid_no) + ": \n\tdF1 = " + str(dF1) + " [Hz]; dF2 = " + str(dF2) + " [Hz]");
    for f_idx in range(len(freq_PRIR_G1)):
        if (not numpy.isclose(freq_PRIR_G1[f_idx], freq_PRIR_G2[f_idx])):
            print "Error... frequency inconsistency for G" + str(grid_no) + "..."
            print "G1: " + str(freq_PRIR_G1[f_idx]);
            print "G2: " + str(freq_PRIR_G2[f_idx]);
            raise NameError("Inconsistency in the frequency vectors!");
    print "\t"+ print_colors.BG_GREEN + "OK" + print_colors.END + print_colors.FG_GREEN +\
            " - Passed frequency consistency for grid " + str(grid_no) + " (" + print_colors.FG_WHITE +  print_colors.BG_GREEN + str(dF1) + " [Hz]" + print_colors.END + print_colors.FG_GREEN + ")!" + print_colors.END+"";
    
def get_Gaussian_window(t_samples, gain_at_t_sim_2 = 1E-8, t_sim = None):
    '''
    Function used to construct a Gaussian with the following specifications:
        It should decay after 6.4 ms.
        
    :param t_samples: A vector with time instances where the Gaussian will be evaluated.
    :param gain_at_t_sim_2: The gain at half the simulation.
    :param t_sim: Where the gain gain_at_t_sim_2 will be (the simulation time).
    '''
    if t_sim is None:
        t_sim = numpy.float64(0.006474796); # [s]
    # Get alpha:
    alpha_ = numpy.sqrt( numpy.log(1.0/gain_at_t_sim_2) ) / (t_sim/1.0);
    #print "Gaussian: alpha = " + str(alpha_);
    return numpy.exp(-numpy.square(alpha_)*numpy.square(t_samples - t_sim/2.0) );

def interpolate_Receiver_responses(MPI_fs_, list_src_roots, MPI_ear_ = "LEFT", verbose_flag = True, N_end = None):
    '''
    Function goes through all the directories inside list_src_roots, reads all the MPI data and interpolates the RECEIVERS.
    
    Useful to plug in for Source interpolation!
    
        Function CHECKS for SIM parameters consistencies, initial source data consistency and direction list consistency between the
      simulations in the list_src_roots list.  
    
    :param MPI_fs_: The sampling frequency for the current grid.
    :param list_src_roots: A list containing the FULL path to the directories where simulation data is read.
    :param MPI_ear: Ear string. Can be: "LEFT", "RIGHT", "CENTER". For the pinna study, only "LEFT" for BLOCKED and "CENTER" for the free-field measurement.
    :param verbose_flag: If True, it displays the #files, modified data in each directory and the processing.
    :param N_end: If not None, it mentions the sample where the signals are cut.
    
    Returns:
    :ret src_data_list_G1: A list containing source data (class MPI_IO.SourceData() ) for each doirectory inside list_src_roots.
    :ret direction_list_G1: A list containing a uniqe list (for EACH directory - this is checked) of (azimuth_deg, elevation_deg) directions.
    :ret Receiver_final_locations_m_list_G1: A list containing numpy.arrays of size 3: the continuous Cartesian locations in [m] where each Receiver (for each direction in direction_list_G1) will be interpolated.
    
    :ret interpolated_signal_G1: numpy.array containing the RECEIVER INTERPOLATED data for each directory, then each direction. Of shape: [Num_directories, Num_directions, Num_samples]
    
    :ret Non_interpolated_signal_G1: numpy.array containing the RECEIVER data (at closest voxel of intended) for each directory, then each direction. Of shape: [Num_directories, Num_directions, Num_samples]
    :ret Num_directories: The number of directories in the list.
    :ret N_directions: The number of directions in the files in each directory.
    :ret N_samples: The number of samples each simulation has.
    '''
    #===========================================================================
    # Reading G1, BLOCKED:
    #===========================================================================
    # Read first one for checks (baseline):
    fs_G1_bs, dX_G1_bs, c_sound_G1_bs, lambda_sim_G1_bs, CUDA_steps_G1_bs, reciprocity_G1_bs,\
        source_type_G1_bs, voxelization_type_string_G1_bs, use_double_FDTD_G1_bs, mesh_file_G1_bs,\
         _, receivers_response_list_MPI_Left_G1, receivers_MPI_azimuths_Left_G1, receivers_MPI_elevations_Left_G1,\
         total_rec_intended_locations_MPI_L_G1, total_rec_locations_idx_MPI_L_G1, total_rec_continuous_locations_m_MPI_L_G1, total_rec_interpolation_index_MPI_L_G1, _, \
          src_data_L_G1_bs, total_sources_L_G1_bs, _ = MPI_IO.read_MPI_data(files_reading_dir_MPI = list_src_roots[0], 
                                                                            MPI_fs = MPI_fs_, 
                                                                            MPI_ear = MPI_ear_, verbose= False, verbose_files = False);
    # Build direction list:
    # Get direction list:
    dirs_G1_bs, intended_locations_G1_bs, _, _, _, _ = PRTF_interpolation.group_receiver_data_by_direction(
                                                                        receivers_response_list = receivers_response_list_MPI_Left_G1, 
                                                                        receivers_azimuths = receivers_MPI_azimuths_Left_G1, 
                                                                        receivers_elevations = receivers_MPI_elevations_Left_G1, 
                                                                        receiver_intended_locations = total_rec_intended_locations_MPI_L_G1, 
                                                                        receivers_locations_index_ = total_rec_locations_idx_MPI_L_G1, 
                                                                        receiver_continuous_locations_m = total_rec_continuous_locations_m_MPI_L_G1, 
                                                                        receiver_interpolation_index = total_rec_interpolation_index_MPI_L_G1
                                                                        );
    N_directories = len(list_src_roots);
    N_directions = len(dirs_G1_bs);
    N_samples = len(receivers_response_list_MPI_Left_G1[0]);
    if N_end is None:
        N_end = N_samples;
    elif N_end > N_samples:
        warnings.warn("Given N_end (" + str(N_end) + ") > N_samples (" + str(N_samples) + ")!");
        N_end = N_samples;
    # Free some data....
    del receivers_response_list_MPI_Left_G1; 
    del receivers_MPI_azimuths_Left_G1;
    del receivers_MPI_elevations_Left_G1;
    del total_rec_intended_locations_MPI_L_G1;
    del total_rec_locations_idx_MPI_L_G1;
    del total_rec_continuous_locations_m_MPI_L_G1;
    del total_rec_interpolation_index_MPI_L_G1;
    # We need:
    src_data_list_G1 = [];             # Keeps the source data for each directory. Size = num_directories
    direction_list_G1 = dirs_G1_bs;    # The direction list - the SMAE for all directiories! Size = num_directions
    Receiver_final_locations_m_list_G1 = intended_locations_G1_bs;
    Receiver_interpolated_signal_G1 = numpy.zeros( (N_directories,N_directions,N_end) );
    Receiver_non_interpolated_signal_G1 = numpy.zeros( (N_directories,N_directions,N_end) );
    read_directory_idx = 0;
    for src_path_dir in list_src_roots:
        # Copy processing from get_interpolated_Receiver_PRTFs_grid() function:
        # BLOCKED sim (reading MPI data):
        fs_simulation_left_G1, dX_MPI_L_G1, c_sound_MPI_L_G1, lambda_sim_MPI_L_G1, CUDA_steps_MPI_L_G1, reciprocity_MPI_L_G1,\
          source_type_MPI_L_G1, voxelization_type_string_MPI_L_G1, use_double_FDTD_L_G1, mesh_file_L_G1,\
           read_file_idx_MPI_L_G1, receivers_response_list_MPI_Left_G1, receivers_MPI_azimuths_Left_G1, receivers_MPI_elevations_Left_G1, \
            total_rec_intended_locations_MPI_L_G1, total_rec_locations_idx_MPI_L_G1, total_rec_continuous_locations_m_MPI_L_G1, total_rec_interpolation_index_MPI_L_G1, \
                ranks_L_G1, src_data_L_G1, total_sources_L_G1, max_time_stamp_G1 = MPI_IO.read_MPI_data(files_reading_dir_MPI = src_path_dir, 
                                                                                 MPI_fs = MPI_fs_, 
                                                                                 MPI_ear = MPI_ear_, verbose= False, verbose_files = False);
        if fs_simulation_left_G1 != MPI_fs_:
            raise NameError("This is not right! You ask for a sampling frequency and read another one!");
        if total_sources_L_G1_bs != total_sources_L_G1:
            raise NameError("Inconsistencies in the number of sources (" + str(total_sources_L_G1) + ") from baseline (" + str(total_sources_L_G1_bs) + ")");
        # Groupd by direction Blocked:
        direction_list_L_G1, intended_locations_L_G1, receivers_response_list_group_by_direction_L_G1,\
           rec_locations_index_group_by_direction_L_G1, rec_continuous_locations_m_group_by_direction_L_G1,\
                rec_interpolation_index_group_by_direction_L_G1 = PRTF_interpolation.group_receiver_data_by_direction(
                                                                        receivers_response_list = receivers_response_list_MPI_Left_G1, 
                                                                        receivers_azimuths = receivers_MPI_azimuths_Left_G1, 
                                                                        receivers_elevations = receivers_MPI_elevations_Left_G1, 
                                                                        receiver_intended_locations = total_rec_intended_locations_MPI_L_G1, 
                                                                        receivers_locations_index_ = total_rec_locations_idx_MPI_L_G1, 
                                                                        receiver_continuous_locations_m = total_rec_continuous_locations_m_MPI_L_G1, 
                                                                        receiver_interpolation_index = total_rec_interpolation_index_MPI_L_G1
                                                                        );
        path_split = src_path_dir.split(os.sep);
        # 1) Check general sim consistency - re-use MPI_consistency functions:
        MPI_IO.check_MPI_file_consistency(file_name_ = path_split[-1], fs = fs_G1_bs, fs_ = fs_simulation_left_G1, 
                                          dX = dX_G1_bs, dX_ = dX_MPI_L_G1, c_sound = c_sound_G1_bs, c_sound_ = c_sound_MPI_L_G1, 
                                          lambda_sim = lambda_sim_G1_bs, lambda_sim_ = lambda_sim_MPI_L_G1, mesh_file = mesh_file_G1_bs, mesh_file_ = mesh_file_L_G1,
                                          CUDA_steps = CUDA_steps_G1_bs, CUDA_steps_ = CUDA_steps_MPI_L_G1, use_double_FDTD = use_double_FDTD_G1_bs, use_double_FDTD_= use_double_FDTD_L_G1, 
                                          reciprocity = reciprocity_G1_bs, reciprocity_ = reciprocity_MPI_L_G1, ear = MPI_ear_, ear_ = MPI_ear_,
                                          source_type = source_type_G1_bs, source_type_ = source_type_MPI_L_G1, voxelization_type_string = voxelization_type_string_G1_bs, voxelization_type_string_ = voxelization_type_string_MPI_L_G1,
                                          src_data = None, src_data_ = None);    
        # 2) Check source data consistency:
        check_Simulation_Consistency_Src_displacement(src_data = src_data_L_G1_bs, src_data_ = src_data_L_G1);
        # 3) Check directions consistency &&
        # 4) Check that the interpolation locations are consistent:
        check_receiver_directions_and_intended_locations(direction_list = dirs_G1_bs, direction_list_ = direction_list_L_G1, 
                                                         intended_locations_m = intended_locations_G1_bs, intended_locations_m_ = intended_locations_L_G1, 
                                                         file_name = path_split[-1]);
        #####################################
        if verbose_flag:
            print "\t" + str(read_directory_idx) + "] Read " + str(read_file_idx_MPI_L_G1) + " files [" + str(max_time_stamp_G1) + "].   N_dir, N_rec, N_samples = (#" + str(len(direction_list_L_G1)) + " x #" + str(len(receivers_response_list_MPI_Left_G1)) + " x #" + str( len(receivers_response_list_MPI_Left_G1[0])) +") [OK]    -->    " + "."+os.sep + path_split[-3] + os.sep +  path_split[-2] + os.sep + path_split[-1];
        src_data_list_G1.append(src_data_L_G1);
        # Interpolated BLOCKED data now (TRILINEAR) - go through all directions:
        for direction_idx in range( len(direction_list_L_G1) ):
            # TRILINEAR interplations
            # rec_interpolation_index_group_by_direction_L_G1 will be of shape [5x5x5,3] = [125,3]. Will have receiver interpolation index [-2,-2,-2], [-2,-2,0], ..., [0,0,0], ..., [2,2,2]:
            # Here, the function trilinear_interpolate_receivers_time will identify the [0,0,0] receiver and interpolate around it in trilinear interpolation...
            interpolated_signal_L_G1, Non_interpolated_signal_L_G1 = PRTF_interpolation.trilinear_interpolate_receivers_time(
                                                                        receivers_response_list = receivers_response_list_group_by_direction_L_G1[direction_idx], 
                                                                        receivers_continuous_locations_m = rec_continuous_locations_m_group_by_direction_L_G1[direction_idx], 
                                                                        receivers_interpolation_index = rec_interpolation_index_group_by_direction_L_G1[direction_idx], 
                                                                        interpolation_intended_location_m = intended_locations_L_G1[direction_idx], 
                                                                        dX = dX_MPI_L_G1, 
                                                                        verbose_flag = False, 
                                                                        plot_interpolation_flag = False,
                                                                        fs = fs_simulation_left_G1);
            # Add to the numpy.arrays:
            Receiver_interpolated_signal_G1[read_directory_idx, direction_idx, :] = interpolated_signal_L_G1[0:N_end];
            Receiver_non_interpolated_signal_G1[read_directory_idx, direction_idx, :] = Non_interpolated_signal_L_G1[0:N_end];
        if verbose_flag:
            1==1; # hide this...
            #print "\t\tInterpolated #" + str( len(direction_list_L_G1) ) + " (RECEIVER) directions."
        read_directory_idx = read_directory_idx + 1;
    # Populate simulation data:
    sim_data = MPI_IO.SimData();
    sim_data.c_sound = numpy.float64(c_sound_MPI_L_G1);
    sim_data.fs = numpy.int64(fs_simulation_left_G1);
    sim_data.dX = numpy.float64(dX_MPI_L_G1);
    sim_data.lambda_sim = numpy.float64(lambda_sim_MPI_L_G1);
    sim_data.source_type = source_type_MPI_L_G1;
    sim_data.no_of_receivers_per_direction = len(receivers_response_list_group_by_direction_L_G1[0]);
    sim_data.voxelization_type_string = voxelization_type_string_MPI_L_G1;
    sim_data.n_receiver_directions = N_directions;
    sim_data.N_samples = N_end;
    sim_data.use_double = use_double_FDTD_G1_bs;
    sim_data.reciprocity = reciprocity_MPI_L_G1;
    return src_data_list_G1, direction_list_G1, Receiver_final_locations_m_list_G1, Receiver_interpolated_signal_G1, Receiver_non_interpolated_signal_G1, N_directories, sim_data;

def get_total_simulation_time_ELEC_Pinna():
    '''
    Function retrieves the total simulation time, as chosen based on measurements and used to truncate the time signals to obtain a desired 
    frequency resolution.
    '''
    total_time_sim_s = 6.25*1E-3;# This is for eardrum measurement too... N_meas = 170. Should give a frequency resolution of 160 Hz.
    return total_time_sim_s;

def check_Simulation_Consistency_Src_displacement(src_data, src_data_):
    '''
    Function checks for consistency between different simulations. It is assumed that the data comes from an MPI reading.
    
        Most relevant data is checked, except (which should be different):
            src_data.final_location_m
            src_data.non_boundary_matrix
            src_data.N_boundary_neightbors
            src_data.interpolation_index
            src_data.displace_source_by_vox
            src_data.final_location_vox
    
    Here are the main parameters (the ones with "_" sufix are the same):
    :param src_data: SourceData() class. Contains data about source. If either of the two is None, the checks are skipped!
    '''
    if not numpy.array_equal(src_data.intended_location_m, src_data_.intended_location_m):
        raise NameError("Source inconsistency: intended_location_m (" + str(src_data.intended_location_m) + " vs " + str(src_data_.intended_location_m) + ")");
    if not numpy.array_equal(src_data.original_location_before_vox, src_data_.original_location_before_vox):
        raise NameError("Source inconsistency: original_location_before_vox (" + str(src_data.original_location_before_vox) + " vs " + str(src_data_.original_location_before_vox) + ")");
    if not numpy.array_equal(src_data.original_location_vox, src_data_.original_location_vox):
        raise NameError("Source inconsistency: original_location_vox (" + str(src_data.original_location_vox) + " vs " + str(src_data_.original_location_vox) + ")");
    if not numpy.array_equal(src_data.original_location_m, src_data_.original_location_m):
        raise NameError("Source inconsistency: original_location_m (" + str(src_data.original_location_m) + " vs " + str(src_data_.original_location_m) + ")");
    if src_data.dX != src_data_.dX:
        raise NameError("Source inconsistency: dX (" + str(src_data.dX) + " vs " + str(src_data_.dX) + ")");
    if not numpy.array_equal(src_data.full_voxelization_dim, src_data_.full_voxelization_dim):
        raise NameError("Source inconsistency: full_voxelization_dim (" + str(src_data.full_voxelization_dim) + " vs " + str(src_data_.full_voxelization_dim) + ")");