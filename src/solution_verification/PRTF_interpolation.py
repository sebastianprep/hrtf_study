'''
This file is released under the MIT License. See
http://www.opensource.org/licenses/mit-license.php

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


Created on Sep 7, 2016

    Module used for interpolation of the pinna PRTF responses. Also tests if 
    results are as they should be.

@author: Sebastian P.
'''
import MPI_IO
import os
import numpy
import warnings
import sys
import scipy.fftpack
import matplotlib.pyplot as plt

class AnsiColors:
    FG_BLACK        = '\033[30m'
    FG_RED          = '\033[31m'
    FG_GREEN        = '\033[32m'
    FG_YELLOW       = '\033[33m'
    FG_BLUE         = '\033[34m'
    FG_MAGENTA      = '\033[35m'
    FG_CYAN         = '\033[36m'
    FG_WHITE        = '\033[37m'

    BG_BLACK        = '\033[40m'
    BG_RED          = '\033[41m'
    BG_GREEN        = '\033[42m'
    BG_YELLOW       = '\033[43m'
    BG_BLUE         = '\033[44m'
    BG_MAGENTA      = '\033[45m'
    BG_CYAN         = '\033[46m'
    BG_WHITE        = '\033[47m'

    END             = '\033[0m'
print_colors = AnsiColors();

def check_localtion_in_voxel(desired_location_m, voxel_center_m, dX):
    '''
    Helper function used to check that the desire location lies within the intended voxel.
    
    :param desired_location_m: list/array/array with the desired (x,y,z) continuous location in [m].
    :param voxel_center_m: list/array/array with the center (x,y,z) of the voxel in [m].
    :param dX: The grid spacing in [m].
    '''
    # X:
    if (desired_location_m[0] < (voxel_center_m[0]-0.5*dX)) or (desired_location_m[0] > (voxel_center_m[0]+0.5*dX)):
        raise NameError("Intended location in X direction (" + str(desired_location_m[0]) + ") is not inside the desired voxel with center " + str(voxel_center_m) + " !")
    # Y:
    if (desired_location_m[1] < (voxel_center_m[1]-0.5*dX)) or (desired_location_m[1] > (voxel_center_m[1]+0.5*dX)):
        raise NameError("Intended location in Y direction (" + str(desired_location_m[1]) + ") is not inside the desired voxel with center " + str(voxel_center_m) + " !")
    # Z:
    if (desired_location_m[2] < (voxel_center_m[2]-0.5*dX)) or (desired_location_m[2] > (voxel_center_m[2]+0.5*dX)):
        raise NameError("Intended location in Y direction (" + str(desired_location_m[2]) + ") is not inside the desired voxel with center " + str(voxel_center_m) + " !")

def group_receiver_data_by_direction(receivers_response_list, receivers_azimuths, 
                         receivers_elevations, receiver_intended_locations, 
                         receivers_locations_index_, receiver_continuous_locations_m, 
                         receiver_interpolation_index):
    '''
    Function groups receiver data (coming for instance from MPI_IO.read_MPI_data()) 
    based on the direction.
    
    This is useful for interpolating data: a list with directions is built and for
    each index in the list, the correspondign receiver data is stored (like all the 
    receiver responses for interpolation).
    
    Input parameters as lists:
    :param receivers_response_list: A list with all the response for each receiver.
    :param receivers_azimuths: A list with all the azimuths angles for each receiver.
    :param receivers_elevations: A list with all the elevation angles for each receiver.
    :param receiver_intended_locations: A list with the intended location for each receiver.
    :param receivers_locations_index_: A list with all the voxel indices for all receivers.
    :param receiver_continuous_locations_m: A list with the center of each receiver location.
    :param receiver_interpolation_index: A list with an interpolatio nindex for each receiver.
    '''
    total_number_of_receivers = len(receivers_response_list);
    # 1) Build available direction list:
    rec_direction_pairs = [];
    rec_intended_locations = [];
    for rec_idx in range(total_number_of_receivers):
        direction_tuple = (receivers_azimuths[rec_idx],receivers_elevations[rec_idx]);
        # Flag that finds out if total_rec_interpolation_index = [0,0,0], i.e. the intended location and not a neighbor receiver used for interpolation
        desired_position_flag = (int(receiver_interpolation_index[rec_idx][0]) == 0) and (int(receiver_interpolation_index[rec_idx][1]) == 0) and (int(receiver_interpolation_index[rec_idx][2]) == 0);
        if desired_position_flag:
            rec_direction_pairs.append( direction_tuple );
            rec_intended_locations.append(receiver_intended_locations[rec_idx]);
    # 2) Group data (note that the reading should check for duplicate direction data otherwise this will fail):
    receivers_response_list_group_by_direction = [];
    rec_locations_index_group_by_direction = [];
    rec_continuous_locations_m_group_by_direction = [];
    rec_interpolation_index_group_by_direction = [];
    total_number_of_directions = len(rec_direction_pairs);
    # Build list for each direction:
    for dir_idx in range(total_number_of_directions):
        receivers_response_list_group_by_direction.append([]);
        rec_locations_index_group_by_direction.append([]);
        rec_continuous_locations_m_group_by_direction.append([]);
        rec_interpolation_index_group_by_direction.append([]);
    # Now loop through all receivers and assign data to each direction:
    for rec_idx in range(total_number_of_receivers):
        direction_tuple = (receivers_azimuths[rec_idx],receivers_elevations[rec_idx]);
        dir_index = rec_direction_pairs.index(direction_tuple);
        receivers_response_list_group_by_direction[dir_index].append(receivers_response_list[rec_idx]);
        rec_locations_index_group_by_direction[dir_index].append(receivers_locations_index_[rec_idx]);
        rec_continuous_locations_m_group_by_direction[dir_index].append(receiver_continuous_locations_m[rec_idx]);
        rec_interpolation_index_group_by_direction[dir_index].append(receiver_interpolation_index[rec_idx]);
        # Extra check:
        if (receiver_intended_locations[rec_idx][0] != rec_intended_locations[dir_index][0]) or \
            (receiver_intended_locations[rec_idx][1] != rec_intended_locations[dir_index][1]) or \
             (receiver_intended_locations[rec_idx][2] != rec_intended_locations[dir_index][2]):
            print "--------------------------------"
            print "Rec index = " + str(rec_idx)
            print "Dir index = " + str(dir_index)
            print "Dir tuple from built set = " + str(rec_direction_pairs[dir_index])
            print "------"
            print "Rec (Az,El) = " + str( (receivers_azimuths[rec_idx],receivers_elevations[rec_idx]) )
            print "Rec intended = " + str(receiver_intended_locations[rec_idx])
            print "Rec index = " + str(receivers_locations_index_[rec_idx]) # THIS SHOULD BE INTENDED....wtf???
            print "Rec interpolation index = " + str(receiver_interpolation_index[rec_idx])
            raise NameError("For the same direction (" + str(direction_tuple) + "), the intended location is different: intended for receiver " + str(receiver_intended_locations[rec_idx]) + " != " + str(rec_intended_locations[dir_index]) + " intended for group." );
            
    # Basic checks:
    no_of_rec_per_direction = len(receivers_response_list_group_by_direction[0]);
    for dir_idx in range(total_number_of_directions):
        if no_of_rec_per_direction != len(receivers_response_list_group_by_direction[dir_idx]):
            warnings.warn("Each direction does not have the same number of receivers (" + str(no_of_rec_per_direction) + ")! [Responses]");
        if no_of_rec_per_direction != len(rec_locations_index_group_by_direction[dir_idx]):
            warnings.warn("Each direction does not have the same number of receivers (" + str(no_of_rec_per_direction) + ")! [Location index]");
        if no_of_rec_per_direction != len(rec_continuous_locations_m_group_by_direction[dir_idx]):
            warnings.warn("Each direction does not have the same number of receivers (" + str(no_of_rec_per_direction) + ")! [Continuous location]");
        if no_of_rec_per_direction != len(rec_interpolation_index_group_by_direction[dir_idx]):
            warnings.warn("Each direction does not have the same number of receivers (" + str(no_of_rec_per_direction) + ")! [Interpolation index]");
    rec_per_dir = len(receivers_response_list_group_by_direction[0]);
    for dir_index in range(1,total_number_of_directions):
        if len(receivers_response_list_group_by_direction[dir_index]) != rec_per_dir:
            warnings.warn("The number of receivers per direction is inconsistent!");
            
    return rec_direction_pairs, rec_intended_locations, receivers_response_list_group_by_direction, rec_locations_index_group_by_direction, rec_continuous_locations_m_group_by_direction, rec_interpolation_index_group_by_direction;

def trilinear_interpolate_receivers_time(receivers_response_list, receivers_continuous_locations_m, receivers_interpolation_index, interpolation_intended_location_m, dX, verbose_flag = False, plot_interpolation_flag = False, fs = 0.0):
    '''
    Function trilinearly interpolates in space at each time step a set of FDTD responses.
    
    Function returns the interpolated signal and the non-interpolated signal (i.e., the one in the voxel where the location resides).
    
    :param receivers_response_list: A list with all the responses around the intended location. Usually, these are all responses for one direction and represent a cube of point receivers.
    :param receivers_continuous_locations_m: The (x,y,z) location in m of each receiver in receivers_response_list (in the current framework, i.e. FV paradigm, these are the centers of the receiver voxels).
    :param receivers_interpolation_index: For each receiver in receivers_response_list, an [x_vox,y_vox,z_vox] containing the distance in voxels from the voxel containing the intended location - [0,0,0].
    :param interpolation_intended_location_m: The continuous (x,y,z) location where the interpolation is done at each time step.
    :param dX: The grid spacing in m.
    
    :param verbose_flag: Boolean. If True, various debugging information is printed.
    :param plot_interpolation_flag: Boolean. If True, the responses used in interpolation with the interpolated signal are plotted (and FFT). 
    :param fs: The sampling frequency. Used in plotting. 
    '''
    sim_length = len(receivers_response_list[0]);
    receivers_per_direction_ = len(receivers_response_list);
    # Find the [0,0,0] voxel:
    initial_voxel_rec_index_ = -1;
    for rec_index in range(receivers_per_direction_):
        if (int(receivers_interpolation_index[rec_index][0]) == 0) and (int(receivers_interpolation_index[rec_index][1]) == 0) and (int(receivers_interpolation_index[rec_index][2]) == 0):
            if initial_voxel_rec_index_>0:
                raise NameError("Two initial voxels [0,0,0]! Something is wrong here!");
            initial_voxel_rec_index_ = rec_index;
    # Check that I actually found a location:
    if initial_voxel_rec_index_ == -1:
        raise NameError("Could not find the [0,0,0] receiver index!");
    # Get the non-interpolated signal:
    Non_interpolated_signal = receivers_response_list[initial_voxel_rec_index_];
    # Checks that the intended location is in the desired voxel:
    check_localtion_in_voxel(desired_location_m = interpolation_intended_location_m, voxel_center_m = receivers_continuous_locations_m[initial_voxel_rec_index_], dX = dX);
    if verbose_flag:
        print "The initial voxel index is : " + str(initial_voxel_rec_index_);
        print "\t" + str(receivers_interpolation_index[initial_voxel_rec_index_]) + " - location : " + str(receivers_interpolation_index[initial_voxel_rec_index_]);
    ############################
    # Trilinear interpolation:
    desired_location_m = interpolation_intended_location_m;
    voxel_center_m = receivers_continuous_locations_m[initial_voxel_rec_index_];
    # First get the second cell in each direction:
    required_neighbors = numpy.int32( numpy.zeros(3) );
    for dim_index in range(3): # X,Y,Z
        if desired_location_m[dim_index]>=voxel_center_m[dim_index]:
            required_neighbors[dim_index] = 1;
        else:
            required_neighbors[dim_index] = -1;
    if verbose_flag:
        print "We need the following neighbors for trilinear interpolation:"
        print "\t\t " + str(required_neighbors);
    # Get voxel indices limits needed for interpolation:
    min_index_xyz = numpy.int32( [99,99,99] );
    max_index_xyz = numpy.int32( [-99,-99,-99] );
    for dim_index in range(3): # X,Y,Z
        min_index_xyz[dim_index] = min(0,required_neighbors[dim_index]);
        max_index_xyz[dim_index] = max(0,required_neighbors[dim_index]);
    if max(min_index_xyz) == 99:
        raise NameError("Something is wrong here! The min index was not found correctly!");
    if min(max_index_xyz) == -99:
        raise NameError("Something is wrong here! The max index was not found correctly!");
    # Build desired location index:
    values_time = numpy.zeros((2,2,2,sim_length), dtype = numpy.float64);
    values_location_m = numpy.zeros((2,2,2,3));
    values_interpolation= numpy.zeros((2,2,2,3));
    desired_tuples = numpy.zeros((2,2,2,3));            # Used for plotting
    index_3D_tuples = numpy.zeros((2,2,2,3));           # Used for plotting
    p0 = numpy.zeros(3);
    for x_index in range(min_index_xyz[0],max_index_xyz[0]+1):
        for y_index in range(min_index_xyz[1],max_index_xyz[1]+1):
            for z_index in range(min_index_xyz[2],max_index_xyz[2]+1):
                desired_tuple = (x_index,y_index, z_index);
                index_3D_tuple = (x_index - min_index_xyz[0], y_index - min_index_xyz[1], z_index - min_index_xyz[2]); # The index in a 3x3 matrix populated according to utils10.lerp_3D_grid()
                desired_tuples[x_index,y_index,z_index] = numpy.float32(desired_tuple);
                index_3D_tuples[x_index,y_index,z_index] = numpy.float32(index_3D_tuple);
                # Find the index:
                desired_loc_index = -1;
                for rec_index in range(receivers_per_direction_):
                    if (int(receivers_interpolation_index[rec_index][0]) == desired_tuple[0]) and (int(receivers_interpolation_index[rec_index][1]) == desired_tuple[1]) and (int(receivers_interpolation_index[rec_index][2]) == desired_tuple[2]):
                        if desired_loc_index>0:
                            raise NameError("Two same voxels for index " + str(desired_tuple) + "! Something is wrong here!");
                        desired_loc_index = rec_index;
                if desired_loc_index <0:
                    raise NameError("Desired receiver index for " + str(desired_tuple) + " not found!!");
                # Populate the time vectors:
                values_time[index_3D_tuple] = numpy.float64(receivers_response_list[desired_loc_index]);
                # Identical results to:
                #values_time[index_3D_tuple[0],index_3D_tuple[1],index_3D_tuple[2]] = receivers_response_list[desired_loc_index];
                # Populate the locations:
                values_location_m[index_3D_tuple] = receivers_continuous_locations_m[desired_loc_index];
                # Identical results to:
                #values_location_m[index_3D_tuple[0],index_3D_tuple[1],index_3D_tuple[2]] = receivers_continuous_locations_m[desired_loc_index];
                values_interpolation[index_3D_tuple] = receivers_interpolation_index[desired_loc_index];
                # Identical results to:
                #values_interpolation[index_3D_tuple[0],index_3D_tuple[1],index_3D_tuple[2]] = receivers_interpolation_index[desired_loc_index];
                # Get the minimum point coordinate!
                if x_index == min_index_xyz[0] and y_index == min_index_xyz[1] and z_index == min_index_xyz[2]:
                    p0 = numpy.copy(receivers_continuous_locations_m[desired_loc_index]);
                if verbose_flag:
                    print "  NEED interp index " + str(desired_tuple) + " <-> " + str(index_3D_tuple) + "\t - location " + str(receivers_continuous_locations_m[desired_loc_index]) + " VALUE at #2245 = " + str(receivers_response_list[desired_loc_index][2245]);
    if verbose_flag:
        print "Minimum world corner is: " + str(p0);
    ###########
    # Interpolation:
    ################    
    if verbose_flag:
        print "\nRECEIVER LOCATION: continuous location cube:"
        for x_idx in range(2):
            for y_idx in range(2):
                for z_idx in range(2):
                    print "loc_mm " + str([x_idx,y_idx,z_idx]) + " = " + str(values_location_m[x_idx,y_idx,z_idx,:]*1E3);
        print "RECEIVER LOCATION end__\n\n"
        print "\nRECEIVER interpolation index:"
        for x_idx in range(2):
            for y_idx in range(2):
                for z_idx in range(2):
                    print "\tInterp_IDX " + str([x_idx,y_idx,z_idx]) + " = " + str(values_interpolation[x_idx,y_idx,z_idx,:]);
        print "RECEIVER interpolation index end__\n\n"           
    
    # First check that the locations make sense:
    check_grid_location_lerp_3D_is_OK(continuous_locations = values_location_m, dX = dX, p0 = p0, tol = 1E-15);
    interpolated_signal = numpy.zeros(sim_length, dtype = numpy.float64);
    for time_index in range(sim_length):
        interpolated_signal[time_index] = lerp_3D_grid(p = interpolation_intended_location_m, 
                                                                 values = values_time[:,:,:,time_index], 
                                                                 dX = dX, 
                                                                 p0 = p0);
    if plot_interpolation_flag:
        # Check also the other interpolation:
        interpolated_signal_ = numpy.zeros(sim_length, dtype = numpy.float64);
        for time_index in range(sim_length):
            interpolated_signal_[time_index] = lagrange_interpolation_3D_grid(p = interpolation_intended_location_m, 
                                                                     values = values_time[:,:,:,time_index], 
                                                                     dX = dX, 
                                                                     p0 = p0);
        ###########
        # The plotting:
        ################
        plt.clf();
        plt.subplot(2,1,1);
        for x_idx in range(2):
            for y_idx in range(2):
                for z_idx in range(2):
                    plt.plot(values_time[x_idx,y_idx,z_idx], label = "Rec - " + str(index_3D_tuples[x_idx,y_idx,z_idx]) + " true idx = " + str(desired_tuples[x_idx,y_idx,z_idx]) );
        plt.plot(interpolated_signal, linewidth = 3.0, linestyle = '--', color = 'k', label = "Interpolated signal" );
        plt.plot(interpolated_signal_, linewidth = 1.5, linestyle = ':', color = 'r', label = "Interpolated signal (Lagrange 3D)" );
        plt.title("Trilinear interpolation, DIR - " + str());
        plt.grid(True, which = 'both');
        plt.legend(loc='best', borderaxespad=1., fancybox=True, shadow=True, fontsize= 9.5, ncol = 1);
        
        # FFT plot:
        plt.subplot(2,1,2);
        for x_idx in range(2):
            for y_idx in range(2):
                for z_idx in range(2):
                    time_vector = values_time[x_idx,y_idx,z_idx];
                    spectrum =  scipy.fftpack.fft( time_vector );
                    freq = scipy.fftpack.fftfreq(len(spectrum)) * fs;
                    # take only the positive part:
                    spectrum = spectrum[freq>=0];
                    freq = freq[freq>=0];
                    angle = numpy.rad2deg( numpy.unwrap( numpy.angle(spectrum) ) ); 
                    spectrum = scipy.absolute(spectrum);
                    spectrum = 20*scipy.log10(spectrum);
                    plt.plot(freq, spectrum, label = "Rec - " + str(index_3D_tuples[x_idx,y_idx,z_idx]) + " true idx = " + str(desired_tuples[x_idx,y_idx,z_idx]) )
        # Interpolated data:
        spectrum =  scipy.fftpack.fft( interpolated_signal );
        freq = scipy.fftpack.fftfreq(len(spectrum)) * fs;
        # take only the positive part:
        spectrum = spectrum[freq>=0];
        freq = freq[freq>=0];
        angle = numpy.rad2deg( numpy.unwrap( numpy.angle(spectrum) ) ); 
        spectrum = scipy.absolute(spectrum);
        spectrum = 20*scipy.log10(spectrum);
        plt.plot(freq, spectrum, linewidth = 3.0, linestyle = '--', color = 'k', label = "Interpolated signal" );
        plt.plot(freq, spectrum, linewidth = 1.5, linestyle = ':', color = 'r' );
        plt.xlim(0,20000);
        plt.ylim(-90,-50);
        plt.grid(True, which = 'both');
        plt.legend(loc='best', borderaxespad=1., fancybox=True, shadow=True, fontsize= 9.5, ncol = 1);
        plt.show();
    return interpolated_signal, Non_interpolated_signal
        
def trilinear_interpolate_Source_time(sim_data, src_data_list_in, sim_data_Int_Rec, interpolation_intended_location_m, verbose_flag_ = False, plot_interpolation_flag = False):
    '''
    Function trilinearly interpolates in space at each time step a set of FDTD responses. This is for the sources.
    
    Use this function AFTER trilinear_interpolate_receivers_time() for each source location!
    
    Function returns the interpolated signals for each direction.
    
    ! Note:
        - function does not check for any consistencies (e.g., that sim_data is indeed the same for all simulations). It is assumed these were done beforehand.
    
    :param sim_data: A converge_PRTF.SimData() object containing the information about the simulations. Should come from MPI_IO.read_MPI_data() and should be the same for all simulations.
    
    :param src_data_list_in: A list containing objects of type MPI_IO.SourceData() objects. Each object contains the source information for each (source) simulations in sim_data_Int_Rec. 
                                Contains continuous locations for the source and interpolation data.
    
    :param sim_data_Int_Rec: A numpy.array containing the FDTD responses. Should contain RECEIVER INTERPOLATED data for each directory, then each direction. Of shape: [Num_directories, Num_directions, Num_samples].
                            Should come from convergence_PRTF.interpolate_Receiver_responses().
    
    :param receivers_continuous_locations_m: The (x,y,z) location in m of each receiver in receivers_response_list (in the current framework, i.e. FV paradigm, these are the centers of the receiver voxels).
    
    :param interpolation_intended_location_m: The continuous (x,y,z) location where the interpolation is done at each time step.
    
    :param verbose_flag_: Boolean. If True, various debugging information is printed.
    :param plot_interpolation_flag: Boolean. If True, the responses used in interpolation with the interpolated signal are plotted (and FFT).  
    '''
    (N_directories, N_directions, N_s) = numpy.shape(sim_data_Int_Rec);
    # Extract data:
    N_directions = sim_data.n_receiver_directions;
    N_source_locations = N_directories;                 # == number of directories
    sim_length = sim_data.N_samples;
    src_data_list = src_data_list_in;
    dX = sim_data.dX;
    fs = sim_data.fs;                                   # Used for plotting.
    
    interpolated_SRC_signals = numpy.zeros( (N_directions,sim_length), dtype = numpy.float64)
    # Loop directions:
    for direction_index in range(N_directions):
        # Start re-using code from PRTF_interpolation.
        # Build desired location index:
        src_INTERP_values_time = numpy.zeros((2,2,2,sim_length), dtype = numpy.float64);
        src_INTERP_values_location_m = numpy.zeros((2,2,2,3));
        src_INTERP_values_interpolation= numpy.zeros((2,2,2,3));
        src_INTERP_desired_tuples = numpy.zeros((2,2,2,3));     # Used for plotting
        p0 = numpy.zeros(3);
        # Populate the time-vectors:
        for x_index in range(2):
            for y_index in range(2):
                for z_index in range(2):
                    desired_array = numpy.int32( (x_index,y_index, z_index) );
                    src_INTERP_desired_tuples[x_index,y_index,z_index] = numpy.float32( (x_index,y_index,z_index) );
                    # Find the index between all src_locations (i.e., all directories):
                    desired_loc_index = -1;
                    for source_index in range(N_source_locations):
                        if numpy.array_equal(src_data_list[source_index].interpolation_index, desired_array):
                            if desired_loc_index>0:
                                print desired_loc_index
                                raise NameError("Two same voxels for interpolation index " + str(desired_array) + "! Something is wrong here!");
                            desired_loc_index = source_index;
                    if verbose_flag_ and desired_loc_index>=0:
                        print "" + print_colors.FG_GREEN + "Found index for " + str(desired_array) + " -> IDX = " + str(desired_loc_index) + print_colors.END;
                        print "\t" + print_colors.FG_GREEN + "final_location_m = " + str(src_data_list[desired_loc_index].final_location_m) + print_colors.END;
                    if desired_loc_index<0:
                        print "" + print_colors.FG_RED + "Data for interpolation index " + str(desired_array) + " NOT FOUND!" + print_colors.END;
                        raise NameError("Desired src for interpolation index " + str(desired_array) + " not found!!");
                    # Populate the time vectors:
                    src_INTERP_values_time[desired_array[0],desired_array[1],desired_array[2]] = numpy.float64(sim_data_Int_Rec[desired_loc_index,direction_index,:]);
                    # Populate the locations:
                    src_INTERP_values_location_m[desired_array[0],desired_array[1],desired_array[2]] = src_data_list[desired_loc_index].final_location_m;
                    # Populate interpolation:
                    src_INTERP_values_interpolation[desired_array[0],desired_array[1],desired_array[2]] = src_data_list[desired_loc_index].interpolation_index;
                    # Get the minimum point coordinate!
                    if x_index == 0 and y_index == 0 and z_index == 0:
                        p0 = numpy.copy(src_data_list[desired_loc_index].final_location_m);
        # Done with data extraction -> interpolate:
        # First check that the locations make sense:
        check_grid_location_lerp_3D_is_OK(continuous_locations = src_INTERP_values_location_m, dX = dX, p0 = p0, tol = 1E-15);
        if verbose_flag_:
            print "  " + print_colors.FG_GREEN + "Src-continuous locations [m] " + print_colors.END + print_colors.BG_GREEN + "OK" + print_colors.END + print_colors.FG_GREEN + " for LERP!" + print_colors.END;
        interpolated_signal = numpy.zeros(sim_length);
        for time_index in range(sim_length):
            interpolated_signal[time_index] = lagrange_interpolation_3D_grid(p = interpolation_intended_location_m, 
                                                                     values = src_INTERP_values_time[:,:,:,time_index], 
                                                                     dX = dX, 
                                                                     p0 = p0);
        interpolated_SRC_signals[direction_index,:] = interpolated_signal;
        if plot_interpolation_flag:
            # Check also the other interpolation:
            interpolated_signal_ = numpy.zeros(sim_length);
            for time_index in range(sim_length):
                interpolated_signal_[time_index] = lerp_3D_grid(p = interpolation_intended_location_m, 
                                                                         values = src_INTERP_values_time[:,:,:,time_index], 
                                                                         dX = dX, 
                                                                         p0 = p0);
            ###########
            # The plotting:
            ################
            plt.clf();
            plt.subplot(2,1,1);
            for x_idx in range(2):
                for y_idx in range(2):
                    for z_idx in range(2):
                        plt.plot(src_INTERP_values_time[x_idx,y_idx,z_idx], label = "Interp. index - " + str(src_INTERP_desired_tuples[x_idx,y_idx,z_idx]) );
            plt.plot(interpolated_signal, linewidth = 3.0, linestyle = '--', color = 'k', label = "Interpolated signal (Lagrange 3D)" );
            plt.plot(interpolated_signal_, linewidth = 1.5, linestyle = ':', color = 'r', label = "Interpolated signal" );
            plt.title("Trilinear interpolation, DIR - " + str());
            plt.grid(True, which = 'both');
            plt.legend(loc='best', borderaxespad=1., fancybox=True, shadow=True, fontsize= 9.5, ncol = 1);
            
            # FFT plot:
            plt.subplot(2,1,2);
            for x_idx in range(2):
                for y_idx in range(2):
                    for z_idx in range(2):
                        time_vector = src_INTERP_values_time[x_idx,y_idx,z_idx];
                        spectrum =  scipy.fftpack.fft( time_vector );
                        freq = scipy.fftpack.fftfreq(len(spectrum)) * fs;
                        # take only the positive part:
                        spectrum = spectrum[freq>=0];
                        freq = freq[freq>=0];
                        angle = numpy.rad2deg( numpy.unwrap( numpy.angle(spectrum) ) ); 
                        spectrum = scipy.absolute(spectrum);
                        spectrum = 20*scipy.log10(spectrum);
                        plt.plot(freq, spectrum, label = "Interp. index - " + str(src_INTERP_desired_tuples[x_idx,y_idx,z_idx])  )
            # Interpolated data:
            spectrum =  scipy.fftpack.fft( interpolated_signal );
            freq = scipy.fftpack.fftfreq(len(spectrum)) * fs;
            # take only the positive part:
            spectrum = spectrum[freq>=0];
            freq = freq[freq>=0];
            angle = numpy.rad2deg( numpy.unwrap( numpy.angle(spectrum) ) ); 
            spectrum = scipy.absolute(spectrum);
            spectrum = 20*scipy.log10(spectrum);
            plt.plot(freq, spectrum, linewidth = 3.0, linestyle = '--', color = 'k', label = "Interpolated signal" );
            plt.plot(freq, spectrum, linewidth = 1.5, linestyle = ':', color = 'r' );
            plt.xlim(0,20000);
            plt.ylim(-90,-50);
            plt.grid(True, which = 'both');
            plt.legend(loc='best', borderaxespad=1., fancybox=True, shadow=True, fontsize= 9.5, ncol = 1);
            plt.show();
        
        if verbose_flag_:
            for x_index in range(2):
                for y_index in range(2):
                    for z_index in range(2):
                        print str((x_index,y_index,z_index)) + " - " + str(src_INTERP_values_location_m[x_index,y_index,z_index]);
                        print "\t I_idx = " + str(src_INTERP_values_interpolation[x_index,y_index,z_index]);
    return interpolated_SRC_signals;

def check_grid_location_lerp_3D_is_OK(continuous_locations, dX, p0, tol = 1E-15):
    '''
    Function checks that the continuous locations follow the rules of lerp_3D_grid() and that the minimum point p0 (also to be
    used in lerp_3D_grid() is correct.
    
    The 8 points needed are:
        {(x0,y0,z0),(x0,y0,z1),(x0,y1,z0), (x0,y1,z1),(x1,y0,z0),(x1,y0,z1),(x1,y1,z0),(x1,y1,z1)}
        
        Where it is assumed x0 > 0, y0 > 0, z0 > 0 and x0<x1<x2, y0<y1<y2, z0<z1<z2
    
    When you extract values for lerp_3D_grid(), also extract the actual grid location and the use this function to check you are doing the right thing.
    
    !NOTE: checks based on tolerance and absolute value.
    
    :param continuous_locations: Vector of shape (2,2,2,3) containing the continuous Cartesian (x,y,z) locations (maybe in meters) for the cube used in interpolation in lerp_3D_grid().
    :param dX: The grid spacing in meters.
    :param p0: The minimum point in 3D. Pass this to the lerp_3D_grid() function!
    :param tol: The tolerance with which the locations are compared. 
            E.g., |val1.x - val2.x| > tol, then the numbers are not considered equal.
            E.g., |val1.y - val2.y| < tol, then the numbers are considered equal.
    '''
    # Check dimensionality:
    if numpy.prod(continuous_locations.shape) != 24 or continuous_locations.shape != (2L, 2L, 2L, 3L): # First check became redundant...
        raise NameError("The input must have dimension 24 = 2x2x2x3! It has " + str(continuous_locations.shape));
    if numpy.prod(p0.shape) != 3 or p0.shape[0] != 3L: # First check became redundant...
        raise NameError("The point p0 must have dimension 3! It has " + str(p0.shape));
    first_ = continuous_locations[0,0,0,:];
    second_ = continuous_locations[0,0,1,:]; 
    if numpy.abs(first_[0] - second_[0]) > tol or numpy.abs(first_[1] - second_[1]) > tol or numpy.abs(first_[2] + dX - second_[2]) > tol:
        raise NameError("Location mismatch for lerp between 1st (" + str(first_*1E3) + " mm) and 2nd (" + str(second_*1E3) + " mm) Cartesian loation!");
    second_ = continuous_locations[0,1,0,:]; 
    if numpy.abs(first_[0] - second_[0]) > tol or numpy.abs(first_[1] + dX - second_[1]) > tol or numpy.abs(first_[2] - second_[2]) > tol:
        raise NameError("Location mismatch for lerp between 1st (" + str(first_*1E3) + " mm) and 3rd (" + str(second_*1E3) + " mm) Cartesian loation!");
    second_ = continuous_locations[0,1,1,:];
    if numpy.abs(first_[0] - second_[0]) > tol or numpy.abs(first_[1] + dX - second_[1]) > tol or numpy.abs(first_[2] + dX - second_[2]) > tol:
        raise NameError("Location mismatch for lerp between 1st (" + str(first_*1E3) + " mm) and 4th (" + str(second_*1E3) + " mm) Cartesian loation!");    
    second_ = continuous_locations[1,0,0,:];
    if numpy.abs(first_[0] + dX - second_[0]) > tol or numpy.abs(first_[1] - second_[1]) > tol or numpy.abs(first_[2] - second_[2]) > tol:
        raise NameError("Location mismatch for lerp between 1st (" + str(first_*1E3) + " mm) and 5th (" + str(second_*1E3) + " mm) Cartesian loation!");
    second_ = continuous_locations[1,0,1,:];
    if numpy.abs(first_[0] + dX - second_[0]) > tol or numpy.abs(first_[1] - second_[1]) > tol or numpy.abs(first_[2] + dX - second_[2]) > tol:
        raise NameError("Location mismatch for lerp between 1st (" + str(first_*1E3) + " mm) and 6th (" + str(second_*1E3) + " mm) Cartesian loation!");
    second_ = continuous_locations[1,1,0,:];
    if numpy.abs(first_[0] + dX - second_[0]) > tol or numpy.abs(first_[1] + dX - second_[1]) > tol or numpy.abs(first_[2] - second_[2]) > tol:
        raise NameError("Location mismatch for lerp between 1st (" + str(first_*1E3) + " mm) and 7th (" + str(second_*1E3) + " mm) Cartesian loation!");
    second_ = continuous_locations[1,1,1,:];
    if numpy.abs(first_[0] + dX - second_[0]) > tol or numpy.abs(first_[1] + dX - second_[1]) > tol or numpy.abs(first_[2] + dX - second_[2]) > tol:
        raise NameError("Location mismatch for lerp between 1st (" + str(first_*1E3) + " mm) and 8th (" + str(second_*1E3) + " mm) Cartesian loation!");
    # Also check the first and last are the minimums and maximums:
    x_min = numpy.min(continuous_locations[:,:,:,0].flatten());
    x_max = numpy.max(continuous_locations[:,:,:,0].flatten());
    y_min = numpy.min(continuous_locations[:,:,:,1].flatten());
    y_max = numpy.max(continuous_locations[:,:,:,1].flatten());
    z_min = numpy.min(continuous_locations[:,:,:,2].flatten());
    z_max = numpy.max(continuous_locations[:,:,:,2].flatten());
    # Now check:
    if continuous_locations[0,0,0,0] != x_min or continuous_locations[0,0,0,1] != y_min or continuous_locations[0,0,0,2] != z_min:
        raise NameError("The first location is not the minimal location!");
    if continuous_locations[1,1,1,0] != x_max or continuous_locations[1,1,1,1] != y_max or continuous_locations[1,1,1,2] != z_max:
        raise NameError("The last location is not the maximal location!");
    # Check p0:
    if p0[0] != x_min or p0[1] != y_min or p0[2] != z_min:
        raise NameError("Point p0 is not the minimal location!");
    
def lerp_3D_grid(p, values, dX, p0 = [0.0,0.0,0.0]):
    '''
    Function trilinearly interpolates on a regular 3D Cartesian grid of grid spacing 
    h=dX=dY=dZ, a function between 8 points {(x0,y0,z0),(x0,y0,z1),(x0,y1,z0),
    (x0,y1,z1),(x1,y0,z0),(x1,y0,z1),(x1,y1,z0),(x1,y1,z1)}.
    
     Note that function assumes x0,y0,z0>=0. Some checks are done: that the point is insde the interpolation interval and values has (2x2x2) shape.
     
    The derivation is based on tensor product of Lagrange 1st order interpolators - 
    see [1], pages 11-12, 47-48.
    
    Tested OK on the inside when compared to scipy.interpolate.RegularGridInterpolator(..., 'linear', ...):
        L_inf norm of error between this and scipy.interpolate.RegularGridInterpolator was 6.65E-7.
        Tested some cube and 50**3 values.
    
    Example of values:
        values = [ [ [1.2, 2.5],[4.9,-1.2] ], [ [-8.3,16.24],[-1.05,2.33] ] ];
        #             000  001   010  011        100   101    110   111 
    
        Reference: [1] Cheney E.W., Light W.A., "A Course in Approximation Theory", 
        American Mathematical Soc., (2009) 
    
    :param p: 1D vector of size 3. The (x,y,z) point at which the interpolation 
        takes place. Needs a size of 3.
    
    :param p0: 1D vector of size 3. The (x0,y0,z0) point. Needs a size of 3.
    :param values: 3D vector of size (2,2,2). The values of the interpolated function. 
        It needs a size of 2 per dimension and 3 dimensions. It should be like
        [ [ [val_000, val_001],[val_010,val_011] ], [ [val_100,val_101],[val_110,val_111] ] ];
        WHERE {000}-{111} encoding is for:
            {min-max X, min-max Y, min-max Z}
            
            e.g.: val_101 is for: {max_x,min_y,max_z}
                  val_011 is for: {min_x,max_y,max_z}
        In general:
            values[i][j][k] = value at (xi,yj,zk), where i,j,k<=1.
    :param dX: The grid spacing. The second point is x1=x0+(dX,dX,dX)
    '''
    # Do a check that the desired point is inside the domain!
    if (p[0] < p0[0]) or ( p[0]> (p0[0]+dX)):
        raise NameError("Desired point (" + str(p[0]) + ") is outside the interpolation interval (" + str(p0[0]) + "-" + str(p0[0]+dX) + ") in X direction!");
    if (p[1] < p0[1]) or ( p[1]> (p0[1]+dX)):
        raise NameError("Desired point (" + str(p[1]) + ") is outside the interpolation interval (" + str(p0[1]) + "-" + str(p0[1]+dX) + ") in Y direction!");
    if (p[2] < p0[2]) or ( p[2]> (p0[2]+dX)):
        raise NameError("Desired point (" + str(p[2]) + ") is outside the interpolation interval (" + str(p0[2]) + "-" + str(p0[2]+dX) + ") in Z direction!");
    if numpy.prod(values.shape) != 8 or values.shape != (2L, 2L, 2L):   # First check became redundant...
        raise NameError("The input must have dimension 8 = 2x2x2! It has " + str(values.shape));
    # First, build the Lagrange polynomials (without the 1/dX**3):
    u_ = numpy.float64( (p0[0]+dX-p[0] , p[0]-p0[0]) );
    v_ = numpy.float64( (p0[1]+dX-p[1] , p[1]-p0[1]) );
    w_ = numpy.float64( (p0[2]+dX-p[2] , p[2]-p0[2]) );
    S = numpy.float64(0.0);
    # Return interpolant:
    for i_ in range(2):
        for j_ in range(2):
            for k_ in range(2):
                S = S + values[i_][j_][k_]*u_[i_]*v_[j_]*w_[k_];
    return S/(dX*dX*dX);

def lagrange_interpolation_3D_grid(p, values, dX, p0 = [0.0,0.0,0.0]):
    '''
    Function interpolates a function on a regular 3D Cartesian of spacing 
    h=dX=dY=dZ, a continuous function.
        The intporlation is done as a tensor product in each dimension based  
    on Lagrange interpolation in each dimension. 
        The interpolation function assumes a cubic grid of points (so, the number
    of points in each direction Nx, Ny, Nz is the same): N = Nx x Ny x Nz. 
    
        Lagrange basis:
            u_j(x) = PRODUCT(x-x_j)/(x_j-x_i), i = 0 ... Nx-1, j!=i
    
         Note that function assumes x0,y0,z0>=0 and NO CHECKS are done!
     
    The derivation is based on tensor product of Lagrange interpolators - 
    see [1], pages 11-12, 46-48.
    
        The formal accuracy in h is (Nx+1) - see [2, p.170]. E.g., using 3 points
    per dimension, i.e. 27 points, the formal accuracy is h^4.
    
        Note that it is better to do the interpolation around the center of the
    input cube of sampled valued - the interpolation diverges as N->infinity (Runge's
    phenomenon). However, it converges in the middle[3].  
    
    Example of values Nx=Ny=Nz=3:
        index as Nx Ny Nz (e.g. 101 => Nx =1 = Nz, Ny = 0)
        values = [ 
                     [ 
                         [1.2, 2.5, -1.2], [4.9,-1.2, 0.2], [9.6,0.02,8.7] 
                          000  001  002     010  011  012    020  021 022
                      ], 
                     [ 
                         [-8.3,16.24, 7.3], [-1.05,2.33,-0.3], [2.6,3.02,1.4]
                          100  101  102      110   111   112    120  121 122 
                     ], 
                     [ 
                         [2.6, 4.4, -7.0], [-6.1, 3.22, 0.0], [2.6, 4.4, -7.0]
                          200  201   202     210  211   212    220  221   222  
                     ] 
                 ]; 
    
    Tested OK on the inside when compared to 
        scipy.interpolate.RegularGridInterpolator(..., 'linear', ...) in 3D 
            L_infinity norm was ~5.3E-15.
    
    References: 
        [1] Cheney E.W., Light W.A., "A Course in Approximation Theory", American  
        Mathematical Soc., (2009) 
        [2] Berezin I.S., Zhidkov N.P., "Computing Methods", Vol 1, Pergamon Press, 
        (1965)
        [3] Davis P.J., "Interpolation and approximation", American Mathematical Soc., (2009
    
    :param p: 1D vector of size 3. The (x,y,z) point at which the interpolation 
        takes place. Needs a size of 3.
    
    :param p0: 1D vector of size 3. The (x0,y0,z0) point. Needs a size of 3.
    :param values: 3D vector of size (Nx,Ny,Nz). The values of the interpolated function. 
        It needs a size of Nx per dimension and 3 dimensions. It should be like
        [ 
            [ [val_000, val_001,.. val_00Nz],[val_010,val_011,...val_01Nz], ... , [val_0Ny0,val_0Ny1,...val_0NyNz] ], 
            [ [val_100, val_101,.. val_10Nz],[val_110,val_111,...val_11Nz], ... , [val_1Ny0,val_1Ny1,...val_1NyNz] ],
            ...
            [ [val_Nx00, val_Nx01,.. val_Nx0Nz],[val_Nx10,val_Nx11,...val_Nx1Nz], ... , [val_NxNy0,val_NxNy1,...val_NxNyNz] ],
        ];
        WHERE {000}-{NxNyNz} encoding is for:
            {min-max X, min-max Y, min-max Z}
            
            e.g.: val_Nx0Nz is for: {max_x,min_y,max_z}
                  val_0NyNz is for: {min_x,max_y,max_z}
        In general:
            values[i][j][k] = value at (xi,yj,zk), where i,j,k<=1.
            
    :param dX: The grid spacing. For example, the (1,1,1) point is x1=x0+(dX,dX,dX)
    '''
    # Basic checks:
    (Nx,Ny,Nz) = numpy.shape(values)
    if Nx!=Ny or Nx!= Nz:
        raise NameError("Function expects a cube of values!"); # This can be easily extended to non-cube by doing separate for loops.
    if len(p) != 3:
        raise NameError("The required interpolation point must be in 3D!");
    if len(p0) != 3:
        raise NameError("The required minimum point must be in 3D!");
    if (p[0] < p0[0]) or (p[0] > (p0[0] + dX*(Nx-1))):
        warnings.warn("X coordinate of interpolation point is outside the given points!");
    if (p[1] < p0[1]) or (p[1] > (p0[1] + dX*(Nx-1))):
        warnings.warn("Y coordinate of interpolation point is outside the given points!");
    if (p[2] < p0[2]) or (p[2] > (p0[2] + dX*(Nx-1))):
        warnings.warn("Z coordinate of interpolation point is outside the given points!"); 
    # First, build the Lagrange polynomials (without the 1/dX**3):
    u_ = numpy.ones(Nx);
    v_ = numpy.ones(Ny);
    w_ = numpy.ones(Nz);
    for j_basis_index in range(Nx): # These loops would need changing if a non-cube is used for interpolation...
        for i_point_index in range(Nx):
            if i_point_index != j_basis_index:
                u_[j_basis_index] = u_[j_basis_index]*( ( p[0] - (p0[0]+dX*i_point_index) ) / ( dX *(j_basis_index-i_point_index) ) );
                v_[j_basis_index] = v_[j_basis_index]*( ( p[1] - (p0[1]+dX*i_point_index) ) / ( dX *(j_basis_index-i_point_index) ) );
                w_[j_basis_index] = w_[j_basis_index]*( ( p[2] - (p0[2]+dX*i_point_index) ) / ( dX *(j_basis_index-i_point_index) ) );
    S = 0.0;
    # Return interpolant:
    for i_ in range(Nx):
        for j_ in range(Ny):
            for k_ in range(Nz):
                S = S + values[i_][j_][k_]*u_[i_]*v_[j_]*w_[k_];
    return S;