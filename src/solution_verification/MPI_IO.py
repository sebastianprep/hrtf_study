'''
This file is released under the MIT License. See
http://www.opensource.org/licenses/mit-license.php

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


Created on Nov 19, 2015

    New module that will do IO on MPI packed simulated data. They should be spread across many hdf5 files (one for each MPI node). 

    Code initially taken from MPI/test_MPI_receivers.py (module not given in this repo).

@author: prepels1
'''
import h5py
import os
import re
import numpy
import time
import warnings
import weave

class SimData:
    '''
    Class with general data about the simulation. Used to pass data more easily between functions.
    
    Unfortunately, I did not do this from the beginning...
    
    Uses numpy arrays - it is easier to write in hdf5 format!
    '''
    def __init__(self):
        self.c_sound = numpy.float64( 0.0 );                               # The speed of sound [m/s].
        self.fs     = numpy.int64( 0 );                                    # The sampling frequency [Hz]. You need 64 due to the code that overflows for dT^2 = 1.0/(fs^2).
        self.dX = numpy.float64( 0.0 );                                    # The grid spacing [m].
        self.dT = numpy.float64( 0.0 );                                    # The sampling time - redundant with fs [s].
        self.lambda_sim = numpy.float64( 0.0 );                            # The Courant number [-].
        self.source_type = "";                                             # The source type. Usually "SS" (soft) or "HS" (hard).
        self.no_of_receivers_per_direction = numpy.int32( 0 );             # The number of receivers per direction (usually more due to interpolation).
        self.voxelization_type_string = "";                                # String describing the type of voxelization. Usually "surface_conservative" or "surface_6_separating" or "solid".
        self.n_receiver_directions = numpy.int32( 0 );                     # The number of receivers (excluding the receivers used for interpolation - no_of_receivers_per_direction).
        self.N_samples = numpy.int32( 0 );                                 # The number of samples per simulation.
        self.use_double = False;                                           # Boolean. If the simulation is done in single or double precision.
        self.reciprocity = False;                                          # Boolean. If the simulation is done using the principle of reciprocity.
        self.ear = "";                                                     # String. Encapsulates the ear string - "LEFT", "RIGHT" or "CENTER"
        self.mesh_file = "";                                               # String containing the intial .obj used to determine the source location. This can be traced back to the original python data in Simulations Python package.

class SourceData:
    '''
    Class with data about the source - used in source interpolation.
    
    Uses numpy arrays - it is easier to write in hdf5 format!
    '''
    def __init__(self):
        self.SIG_source_type = "None";                                         # The type of source string. E.g., hard source, soft source etc.
        self.SIG_signal = "None";                                              # The signal type used as a source.
        self.SIG_amplitude = 1.0;                                              # The amplitude of the signal.
        self.SIG_total_steps = 0;                                              # Total number of steps in the source signal.
        self.SIG_signal_param_1 = 0.0;                                         # The first parameter for the signal (e.g., pulse width, sine frequency etc.).
        self.intended_location_m = numpy.float64( (0.0, 0.0, 0.0) );        # The location as intended (usually found in get_Left_Right_Mic_Locations_m() )
        # Original:
        self.original_location_before_vox = numpy.int32( (0, 0, 0) );       # The location in voxels as intended from intended_location_m (BEFORE searching for a source location, e.g., set_up_Left_Ear_source_Kemar_3m_box() ). Only for LEFT/RIGHT simulations.
        self.original_location_vox = numpy.int32( (0, 0, 0) );              # The location in voxels as intended from intended_location_m (after searching for a source location, e.g., set_up_Left_Ear_source_Kemar_3m_box() ).
        self.original_location_m = numpy.float64( (0.0, 0.0, 0.0) );        # The corresponding location in meters after correction - from self.original_location_vox .
        # Final:
        self.final_location_vox = numpy.int32( (0, 0, 0) );                 # The final location in voxels, after forcing the location.
        self.final_location_m = numpy.float64( (0.0, 0.0, 0.0) );           # The corresponding location in meters - from self.final_location_vox .
        # Extra:
        self.non_boundary_matrix = numpy.asfortranarray( # uint8
                                    numpy.ubyte( numpy.zeros((3,3,3))));    # Extracts a 3x3x3 voxel cube around the source from the voxelization matrix to have an idea what is there in the voxel world.
        self.N_boundary_neightbors = numpy.int32(0);                        # The number of boundary neighbors
        self.interpolation_index = numpy.int32( (0, 0, 0) );                # Might be used in interpolation functions
        self.displace_source_by_vox = numpy.int32( (0, 0, 0) );             # Redundant: should be final_location_vox - original_location_vox
        self.dX = numpy.float64(0.0);                                       # Contains the voxel size in m
        self.full_voxelization_dim = numpy.int32( (0, 0, 0) );              # The full voxelization dimensions

    def __str__(self):
        str_ = "::: Source :" + "\n\tIntended location [mm] = \t" +  str(self.intended_location_m*1E3) + "\n\tOriginal location BEFORE [vox]      \t= \t" +\
                                str(self.original_location_before_vox) + "\n\tOriginal location AFTER correction [vox]     = \t" + str(self.original_location_vox) +\
                               "\n\tOriginal location AFTER correction [mm] = \t" +  str(self.original_location_m*1E3) + "\n\n\tFinal location [vox] = \t\t" +  str(self.final_location_vox)+\
                               "\n\tFinal location [mm] = \t\t" +  str(self.final_location_m*1E3) + "\n\tTotal boundary neighbours [vox] = \t" + str(self.N_boundary_neightbors) +\
                                "\n\tInterpolation index = \t\t" +  str(self.interpolation_index) +\
                               "\n\tDisplace source by [vox] = \t" +  str(self.displace_source_by_vox) +\
                               "\n\n\tdX \t\t\t\t= " + str(self.dX)+\
                               "\n\tFull voxelization dimension [vox] = \t" + str(self.full_voxelization_dim)+\
                               "\n  SIGnal data::\n\tSIG_source_type = \t'" + str(self.SIG_source_type)+"'"+\
                               "\n\tSIG_signal = \t\t'" + str(self.SIG_signal)+"'"+\
                               "\n\tSIG_amplitude = \t" + str(self.SIG_amplitude)+\
                               "\n\tSIG_total_steps = \t" + str(self.SIG_total_steps)+\
                               "\n\tSIG_signal_param_1 = \t" + str(self.SIG_signal_param_1);
        return str_;
    
    def print_non_boundary_matrix(self):
        '''
        Function just prints the extracted voxelization cube.
        '''
        for z_idx in range(3):
            print "Z = " + str(z_idx);
            for y_idx in range(3):
                if z_idx == 1 and y_idx == 1:
                    print "  y" + str(y_idx) + " -> " + str(self.non_boundary_matrix[0,y_idx,z_idx]) + ", \\" + str(self.non_boundary_matrix[1,y_idx,z_idx]) + "/, " + str(self.non_boundary_matrix[2,y_idx,z_idx]) + " -> source here";  
                else:
                    print "  y" + str(y_idx) + " -> " + str(self.non_boundary_matrix[:,y_idx,z_idx]);
    
    def internal_checks(self):
        '''
        Function does some consistency checks.
        '''
        # First, check that source is not a solid:
        if self.non_boundary_matrix[1,1,1] == 0:
            raise NameError("Source location is a solid voxel!");
        # Check that final location is within one voxel in each direction (these make no sense...):
#         if numpy.abs(self.intended_location_m[0] - self.original_location_m[0]) > self.dX:
#             raise NameError("The intended location is more than a voxel away in X direction than the final location");
#         if numpy.abs(self.intended_location_m[1] - self.original_location_m[1]) > self.dX:
#             raise NameError("The intended location is more than a voxel away in Y direction than the final location");
#         if numpy.abs(self.intended_location_m[2] - self.original_location_m[2]) > self.dX:
#             raise NameError("The intended location is more than a voxel away in Z direction than the final location");

def read_MPI_single_file(file_name, reading_directory = os.path.join(os.getcwd(), 'Sim_IR'), verbose = False):
    '''
    Function reads a single hdf5 file coming from the C++ MPI code.
    
    :param file_name: The name of hdf5 file to be read.
    :param reading_directory: The reading directory.
    :param verbose: If True, some reading information is print.
    '''
    f = h5py.File (reading_directory + os.sep + file_name, 'r');
    # Reading data:
    fs = f['fs'][0];
    dX = f['dX'][0];
    c_sound = f['c_sound'][0];
    lambda_sim = f['lambda_sim'][0];
    mesh_file = str(f['mesh_file'][...]);
    CUDA_steps = f['CUDA_steps'][0];
    no_of_receivers = f['no_of_receivers'][0];
    rank = f['rank'][0];
    packed_returned_data = f['packed_returned_data'][...];
    use_double_FDTD = bool(f['use_double_FDTD'][0]);
    reciprocity = bool(f['reciprocity'][0]);
    ear = str(f['ear'][...]);
    source_type = str(f['source_type'][...]);
    voxelization_type_string = str(f['voxelization_type_string'][...]);
    # Get group keys:
    group_keys = [];
    dataset_info = f.items();
    for i_ in range ( len(dataset_info)):
        if isinstance(dataset_info[i_][1], h5py.Group): # .Dataset for datasets...
            group_keys.append(dataset_info[i_][0]);
    src_data = None;
    N_sources = 0;
    # Source data:
    if "source_data" in group_keys:
        group_datasets_info = f['source_data'].items();
        set_keys = [];
        for i_ in range ( len(group_datasets_info)):
            if isinstance(group_datasets_info[i_][1], h5py.Dataset): # .Dataset for datasets...
                set_keys.append(group_datasets_info[i_][0]);
        src_data = SourceData();
        src_data.N_boundary_neightbors = f['source_data/N_boundary_neightbors'][0];
        src_data.dX = f['source_data/dX'][0];
        src_data.displace_source_by_vox = f['source_data/displace_source_by_vox'][...];
        src_data.final_location_m = f['source_data/final_location_m'][...];
        src_data.final_location_vox = f['source_data/final_location_vox'][...];
        src_data.full_voxelization_dim = f['source_data/full_voxelization_dim'][...];
        src_data.intended_location_m = f['source_data/intended_location_m'][...];
        src_data.interpolation_index = f['source_data/interpolation_index'][...];
        src_data.original_location_before_vox = f['source_data/original_location_before_vox'][...];
        src_data.original_location_m = f['source_data/original_location_m'][...];
        src_data.original_location_vox = f['source_data/original_location_vox'][...];
        src_data.non_boundary_matrix = numpy.reshape(f['source_data/src_non_boundary_matrix'][...], (3,3,3), order = 'F');
        N_sources = f['source_data/_no_of_sources'][0];
        #print set_keys
        if "SIG_signal" in set_keys:
            src_data.SIG_source_type = f['source_data/SIG_source_type'][...];
            src_data.SIG_signal = f['source_data/SIG_signal'][...];
            src_data.SIG_amplitude = f['source_data/SIG_amplitude'][0];
            src_data.SIG_total_steps = f['source_data/SIG_total_steps'][0];
            src_data.SIG_signal_param_1 = f['source_data/SIG_signal_param_1'][0];
        if verbose:
            print src_data
            src_data.print_non_boundary_matrix();
    else:
        if verbose:
            print "No src_data found. Returning a default Object with zeros inside and N_sources = 0."
    f.close();
    
    size_for_one_rec = 14 + CUDA_steps;
    if verbose and no_of_receivers>0:
        print "-------===  Read file: " + file_name + " ===-------"
        print "\tRead fs = " + str(fs) + " of type " + str(type(fs));
        print "\tRead dX = " + str(dX) + " of type " + str(type(dX));
        print "\tRead c_sound = " + str(c_sound) + " of type " + str(type(c_sound));
        print "\tRead lambda_sim = " + str(lambda_sim) + " of type " + str(type(lambda_sim));
        print "\tRead mesh_file = '" + str(mesh_file) + "' of type " + str(type(mesh_file));
        print "\tRead CUDA_steps = '" + str(CUDA_steps) + "' of type " + str(type(CUDA_steps));
        print "\tRead no_of_receivers = '" + str(no_of_receivers) + "' of type " + str(type(no_of_receivers));
        print "\tRead rank = '" + str(rank) + "' of type " + str(type(rank));
        print "\tRead use_double_FDTD = '" + str(use_double_FDTD) + "' of type " + str(type(use_double_FDTD));
        print "\tRead reciprocity = '" + str(reciprocity) + "' of type " + str(type(reciprocity));
        print "\tRead ear = '" + str(ear) + "' of type " + str(type(ear));
        print "\tRead source_type = '" + str(source_type) + "' of type " + str(type(source_type));
        print "\tRead voxelization_type_string = '" + str(voxelization_type_string) + "' of type " + str(type(voxelization_type_string));
        
        print "\n\tRead packed_returned_data = " + str(packed_returned_data)
        print "\t Its type is = " + str(type(packed_returned_data));
        print "\t  Type of first element = " + str(type(packed_returned_data[0]));
        print "\t packed_returned_data[0:10] = " + str(packed_returned_data[0:20]);
        
        for rec_index_ in range(2):
            print "    REC " + str(rec_index_) + ":    "
            print "\t azimuth = " + str(packed_returned_data[ rec_index_ * size_for_one_rec + 0])
            print "\t elevation = " + str(packed_returned_data[ rec_index_ * size_for_one_rec + 1])
            print "\t location_index = " + str( packed_returned_data[ (rec_index_ * size_for_one_rec + 2):(rec_index_ * size_for_one_rec + 5) ] )
            print "\t continuous_location_m = " + str( packed_returned_data[ (rec_index_ * size_for_one_rec + 5):(rec_index_ * size_for_one_rec + 8) ] )
            print "\t rec_interpolation_index = " + str( packed_returned_data[ (rec_index_ * size_for_one_rec + 8):(rec_index_ * size_for_one_rec + 11) ] )
            print "\t rec_intended_location = " + str( packed_returned_data[ (rec_index_ * size_for_one_rec + 11):(rec_index_ * size_for_one_rec + 14) ] )
            print "\t first_10_packed_data = " + str( packed_returned_data[ (rec_index_ * size_for_one_rec + 14):( rec_index_ * size_for_one_rec + 24) ] )
        os.system("pause")
        
    # Unpack the data:
    # Let's use lists since it is easier to check if an element is inside:
    rec_azimuths = [];
    rec_elevations = [];
    rec_intended_location = [];
    rec_locations_idx = [];
    rec_continuous_locations_m = [];
    rec_interpolation_index = [];
    rec_data = [];
    for rec_index in range(no_of_receivers):
        rec_azimuths.append( packed_returned_data[ rec_index * size_for_one_rec + 0] );
        rec_elevations.append( packed_returned_data[ rec_index * size_for_one_rec + 1] );
        rec_locations_idx.append( packed_returned_data[ (rec_index * size_for_one_rec + 2):(rec_index * size_for_one_rec + 5) ] );
        rec_continuous_locations_m.append( packed_returned_data[ (rec_index * size_for_one_rec + 5):(rec_index * size_for_one_rec + 8) ] );
        rec_interpolation_index.append( packed_returned_data[ (rec_index * size_for_one_rec + 8):(rec_index * size_for_one_rec + 11) ] );
        rec_intended_location.append( packed_returned_data[ (rec_index * size_for_one_rec + 11):(rec_index * size_for_one_rec + 14) ] );
        rec_data.append( packed_returned_data[ (rec_index * size_for_one_rec + 14):( (rec_index +1) * size_for_one_rec) ] );
    if verbose and no_of_receivers>0:
        N_ = 10;
        print "\t UNPACKED DATA [0:" + str(N_) + "]:"
        print "\n\t\t rec_azimuths = " + str(rec_azimuths[0:N_]);
        print "\t\t rec_elevations = " + str(rec_elevations[0:N_]);
        print "\t\t rec_intended_location = " + str(rec_intended_location[0:N_]);
        print "\t\t rec_locations_idx = " + str(rec_locations_idx[0:N_]);
        print "\t\t rec_continuous_locations_m = " + str(rec_continuous_locations_m[0:N_]);
        print "\t\t rec_interpolation_index = " + str(rec_interpolation_index[0:N_]);
        print "\t\t rec_data[0] = " + str(rec_data[0][0:N_]);
        print "\t\t len(rec_data[0]) = " + str(len(rec_data[0]));
        os.system("pause")
    return fs, dX, c_sound, lambda_sim, mesh_file, CUDA_steps, no_of_receivers,\
         rank, rec_azimuths, rec_elevations, rec_intended_location, rec_locations_idx,\
          rec_continuous_locations_m, rec_interpolation_index, rec_data, use_double_FDTD,\
           reciprocity, ear, source_type, voxelization_type_string, src_data, N_sources;

def check_MPI_file_consistency(file_name_, fs, fs_, dX, dX_, c_sound, c_sound_, lambda_sim, lambda_sim_, mesh_file, mesh_file_, \
                               CUDA_steps, CUDA_steps_, use_double_FDTD, use_double_FDTD_, reciprocity, reciprocity_, \
                               ear, ear_, source_type, source_type_, voxelization_type_string, voxelization_type_string_,
                               src_data = None, src_data_ = None):
    '''
    Function checks consistency between each two variables: the regular one and the one with "_" at the end.
    
    Used when reading multiple files to check consistency between them:
    :param file_name_: The current file that is checked for consistency with the rest of files.
    
    Here are the main parameters (the ones with "_" sufix are the same):
    :param fs: The sampling frequency [Hz].
    :param dX: The grid spacing [m]. 
    :param c_sound: The sound speed [m/s].
    :param lambda_sim: Courant number/factor of the scheme.
    :param mesh_file: The mesh file (probably an .obj) file based on which simulation was ran.
    :param CUDA_steps: The number of steps in the simulation.
    :param reciprocity: Boolean. If reciprocity was used in the simulation.
    :param ear: String. The ear that was simulated. Usually "LEFT", "RIGHT", "CENTER".
    :param source_type: String. Type of source. Usually "SS" or "HS".
    :param voxelization_type_string: String. The type of voxelization.
    :param src_data: SourceData() class. Contains data about source. If either of the two is None, the checks are skipped!
    '''
    if fs_ != fs:
        raise NameError("fs of file " + file_name_ + " is different (" + str(fs_) + ") than in the first file (" + str(fs) + ")");
    if dX_ != dX:
        raise NameError("dX of file " + file_name_ + " is different (" + str(dX_) + ") than in the first file (" + str(dX) + ")");
    if c_sound_ != c_sound:
        raise NameError("c_sound of file " + file_name_ + " is different (" + str(c_sound_) + ") than in the first file (" + str(c_sound) + ")");
    if lambda_sim_ != lambda_sim:
        raise NameError("lambda_sim of file " + file_name_ + " is different (" + str(lambda_sim_) + ") than in the first file (" + str(lambda_sim) + ")");
    if mesh_file_ != mesh_file:
        raise NameError("mesh_file of file " + file_name_ + " is different (" + str(mesh_file_) + ") than in the first file (" + str(mesh_file) + ")");
    if CUDA_steps_ != CUDA_steps:
        #raise NameError("CUDA_steps of file " + file_name_ + " is different (" + str(CUDA_steps_) + ") than in the first file (" + str(CUDA_steps) + ")");
        warnings.warn("CUDA_steps of file " + file_name_ + " is different (" + str(CUDA_steps_) + ") than in the first file (" + str(CUDA_steps) + ")");
    if use_double_FDTD_ != use_double_FDTD:
        raise NameError("use_double_FDTD of file " + file_name_ + " is different (" + str(use_double_FDTD_) + ") than in the first file (" + str(use_double_FDTD) + ")");
    if reciprocity_ != reciprocity:
        raise NameError("reciprocity of file " + file_name_ + " is different (" + str(reciprocity_) + ") than in the first file (" + str(reciprocity) + ")");
    if ear_ != ear:
        raise NameError("ear of file " + file_name_ + " is different (" + str(ear_) + ") than in the first file (" + str(ear) + ")");
    if source_type_ != source_type:
        raise NameError("source_type of file " + file_name_ + " is different (" + str(source_type_) + ") than in the first file (" + str(source_type) + ")");
    if voxelization_type_string_ != voxelization_type_string:
        raise NameError("voxelization_type_string of file " + file_name_ + " is different (" + str(voxelization_type_string_) + ") than in the first file (" + str(voxelization_type_string) + ")");
    if src_data is not None and src_data_ is not None:
        if not numpy.array_equal(src_data.intended_location_m, src_data_.intended_location_m):
            raise NameError("Source inconsistency: intended_location_m (" + str(src_data.intended_location_m) + " vs " + str(src_data_.intended_location_m) + ")");
        if not numpy.array_equal(src_data.original_location_before_vox, src_data_.original_location_before_vox):
            raise NameError("Source inconsistency: original_location_before_vox (" + str(src_data.original_location_before_vox) + " vs " + str(src_data_.original_location_before_vox) + ")");
        if not numpy.array_equal(src_data.original_location_vox, src_data_.original_location_vox):
            raise NameError("Source inconsistency: original_location_vox (" + str(src_data.original_location_vox) + " vs " + str(src_data_.original_location_vox) + ")");
        if not numpy.array_equal(src_data.original_location_m, src_data_.original_location_m):
            raise NameError("Source inconsistency: original_location_m (" + str(src_data.original_location_m) + " vs " + str(src_data_.original_location_m) + ")");
        if not numpy.array_equal(src_data.final_location_vox, src_data_.final_location_vox):
            raise NameError("Source inconsistency: final_location_vox (" + str(src_data.final_location_vox) + " vs " + str(src_data_.final_location_vox) + ")");
        if not numpy.array_equal(src_data.final_location_m, src_data_.final_location_m):
            raise NameError("Source inconsistency: final_location_m (" + str(src_data.final_location_m) + " vs " + str(src_data_.final_location_m) + ")");
        if not numpy.array_equal(src_data.non_boundary_matrix, src_data_.non_boundary_matrix):
            raise NameError("Source inconsistency: non_boundary_matrix (" + str(src_data.non_boundary_matrix) + " vs " + str(src_data_.non_boundary_matrix) + ")");
        if not numpy.array_equal(src_data.N_boundary_neightbors, src_data_.N_boundary_neightbors):
            raise NameError("Source inconsistency: N_boundary_neightbors (" + str(src_data.N_boundary_neightbors) + " vs " + str(src_data_.N_boundary_neightbors) + ")");
        if not numpy.array_equal(src_data.interpolation_index, src_data_.interpolation_index):
            raise NameError("Source inconsistency: interpolation_index (" + str(src_data.interpolation_index) + " vs " + str(src_data_.interpolation_index) + ")");
        if not numpy.array_equal(src_data.displace_source_by_vox, src_data_.displace_source_by_vox):
            raise NameError("Source inconsistency: displace_source_by_vox (" + str(src_data.displace_source_by_vox) + " vs " + str(src_data_.displace_source_by_vox) + ")");
        if src_data.dX != src_data_.dX:
            raise NameError("Source inconsistency: dX (" + str(src_data.dX) + " vs " + str(src_data_.dX) + ")");
        if src_data.SIG_source_type != src_data_.SIG_source_type:
            raise NameError("Source inconsistency: SIG_source_type (" + str(src_data.SIG_source_type) + " vs " + str(src_data_.SIG_source_type) + ")");
        if src_data.SIG_signal != src_data_.SIG_signal:
            raise NameError("Source inconsistency: SIG_signal (" + str(src_data.SIG_signal) + " vs " + str(src_data_.SIG_signal) + ")");
        if src_data.SIG_amplitude != src_data_.SIG_amplitude:
            raise NameError("Source inconsistency: SIG_amplitude (" + str(src_data.SIG_amplitude) + " vs " + str(src_data_.SIG_amplitude) + ")");
        if src_data.SIG_total_steps != src_data_.SIG_total_steps:
            raise NameError("Source inconsistency: SIG_total_steps (" + str(src_data.SIG_total_steps) + " vs " + str(src_data_.SIG_total_steps) + ")");
        if src_data.SIG_signal_param_1 != src_data_.SIG_signal_param_1:
            raise NameError("Source inconsistency: SIG_signal_param_1 (" + str(src_data.SIG_signal_param_1) + " vs " + str(src_data_.SIG_signal_param_1) + ")");
        if not numpy.array_equal(src_data.full_voxelization_dim, src_data_.full_voxelization_dim):
            raise NameError("Source inconsistency: full_voxelization_dim (" + str(src_data.full_voxelization_dim) + " vs " + str(src_data_.full_voxelization_dim) + ")");
        if src_data.dX != dX:
            raise NameError("Inconsistency between the grid spacing of simulation (" + str(src_data.dX) + ") and of source(" + str(dX) + ")");
               
def read_MPI_data(files_reading_dir_MPI, MPI_fs, MPI_ear, verbose = False, verbose_files = True, check_direction_duplicates_flag = True):
    '''
    Function reads all the available MPI output files coming from an MPI simulation. 
    
    Function matches the sampling frequency (MPI_fs) to all the found file names inside files_reading_dir. See MPI C++ code for file name conventions.
    
    Function performs the following checks:
        -) checks for consistency between files for the parameters in the hdf5 datasets.
        -) checks for duplicates of [azimuth,elevation] pairs.
    
    :param files_reading_dir: The directory where all the files are scanned.
    :param MPI_fs: The sampling frequency of the MPI simulations.
    :param MPI_ear: Ear string. Can be: "LEFT", "RIGHT", "CENTER".
    :param verbose: If True, some reading information is print for EACH file.
    :param verbose_files: Another level of verbose flag: here, each MPI file reading is printed, but not the details within.
    :param check_direction_duplicates_flag: Boolean. If True, the function checks that no duplicate directions are found. Note this does not work for L/R receivers.
    
    Returns:
    :ret fs: The read fs
    :ret dX: The read dX
    :ret c_sound: The read sound speed.
    :ret lambda_sim: The read courant number.
    :ret CUDA_steps: The read number of CUDA steps.
    :ret reciprocity: Boolean. If reciprocity was used in the MPI files.
    :ret source_type: String. The source type.
    :ret voxelization_type_string: String. The voxelization type.
    :ret read_file_idx: Total number of files read.
    :ret total_rec_data: List containing all the receiver data from all MPI files.
    :ret total_rec_azimuths: List containing the corresponding receiver azimuths from all MPI files. 
    :ret total_rec_elevations: List containing the corresponding receiver elevations from all MPI files.
    :ret total_rec_locations: List containing the corresponding receiver locations from all MPI files.
    :ret max_time_stamp: The latest modified date for the .hdf5 files read. This is when you set verbose_files = False, but still want to know the modified date.
    '''
    files_reading_dir = os.listdir(files_reading_dir_MPI);
    files_reading_dir_filtered = [f for f in files_reading_dir if re.match(r'.*' + str(MPI_fs) +'.*' + str(MPI_ear) + '.*\.hdf5', f)];
    read_file_idx = 0;
    total_number_of_receivers = 0;
    ranks = [];
    total_rec_azimuths = [];
    total_rec_elevations = [];
    total_rec_locations_idx = [];
    total_rec_continuous_locations_m = [];
    total_rec_interpolation_index = [];
    total_rec_intended_location = [];
    total_rec_data = [];
    if verbose_files:
        print "\n\tReading from dir: " + str(files_reading_dir_MPI) + ":\n\t===========================\n"
    total_sources = 0;
    max_time_stamp = 0.0;
    for file_name_ in files_reading_dir_filtered:
        time_stamp_ = time.ctime(os.path.getmtime( os.path.join(files_reading_dir_MPI, file_name_)));
        if time_stamp_ > max_time_stamp:
            max_time_stamp = time_stamp_;
        if verbose_files:
            print "\t[" + str(read_file_idx) + "] Reading file '" + file_name_ + "'";
            print "\t\t Last Modified: " + str(time_stamp_) ;
        if read_file_idx == 0:
            fs, dX, c_sound, lambda_sim, mesh_file, \
            CUDA_steps, no_of_receivers, rank, \
            rec_azimuths, rec_elevations, rec_intended_location, rec_locations_idx, \
            rec_continuous_locations_m, rec_interpolation_index, \
            rec_data, use_double_FDTD, reciprocity, \
            ear, source_type, voxelization_type_string, src_data, N_sources = read_MPI_single_file(file_name = file_name_, reading_directory = files_reading_dir_MPI, verbose = verbose);
            total_sources = total_sources + N_sources;
        else:
            fs_, dX_, c_sound_, lambda_sim_, mesh_file_, \
            CUDA_steps_, no_of_receivers, rank, \
            rec_azimuths, rec_elevations, rec_intended_location, rec_locations_idx, \
            rec_continuous_locations_m, rec_interpolation_index, \
            rec_data, use_double_FDTD_, reciprocity_, \
            ear_, source_type_, voxelization_type_string_, src_data_, N_sources_ = read_MPI_single_file(file_name = file_name_, reading_directory = files_reading_dir_MPI, verbose = verbose);
            total_sources = total_sources + N_sources_;
            # Checks:
            ###########
            check_MPI_file_consistency(file_name_, fs, fs_, dX, dX_, c_sound, c_sound_, lambda_sim, lambda_sim_, mesh_file, mesh_file_, \
                               CUDA_steps, CUDA_steps_, use_double_FDTD, use_double_FDTD_, reciprocity, reciprocity_, \
                               ear, ear_, source_type, source_type_, voxelization_type_string, voxelization_type_string_, src_data, src_data_);
        if verbose_files:
            print "\t\t Receivers inside: " + str(no_of_receivers);
        total_number_of_receivers = total_number_of_receivers + no_of_receivers;
        for rec_idx in range(len(rec_data)):
            ranks.append(rank);
        total_rec_azimuths = total_rec_azimuths + rec_azimuths;
        total_rec_elevations = total_rec_elevations + rec_elevations;
        total_rec_locations_idx = total_rec_locations_idx + rec_locations_idx;
        total_rec_continuous_locations_m = total_rec_continuous_locations_m + rec_continuous_locations_m;
        total_rec_interpolation_index = total_rec_interpolation_index + rec_interpolation_index;
        total_rec_intended_location = total_rec_intended_location + rec_intended_location;
        total_rec_data = total_rec_data + rec_data;
        read_file_idx = read_file_idx + 1;
    if verbose_files:
        print "\t ---------------------";
        print "\t Total #receivers: " + str(total_number_of_receivers) + "   #sources: " + str(total_sources);
        print "\t -------------------------------------------";
    if read_file_idx == 0:
        raise NameError("No matched files for the requested ear and sampling frequency (" + str(MPI_fs) + ") and ear = " + str(MPI_ear) + "!\nRead inside " + str(files_reading_dir_MPI) + " directory.");
    if MPI_fs != fs:
        raise NameError("The requested MPI_fs differs from the read sampling frequency! Read fs: " + str(fs));
    # Test for duplicate (rec_azim, rec_elev) pairs:
    rec_MPI_direction_pairs = [];
    for rec_idx in range(total_number_of_receivers):
        direction_tuple = (total_rec_azimuths[rec_idx],total_rec_elevations[rec_idx]);
        # Flag that finds out if total_rec_interpolation_index = [0,0,0], i.e. the intended location and not a neighbor receiver used for interpolation
        desired_position_flag = (int(total_rec_interpolation_index[rec_idx][0]) == 0) and (int(total_rec_interpolation_index[rec_idx][1]) == 0) and (int(total_rec_interpolation_index[rec_idx][2]) == 0);
        if desired_position_flag:
            if check_direction_duplicates_flag and direction_tuple in rec_MPI_direction_pairs:
                raise NameError("Direction duplicate in MPI data!\n\tThe direction tuple " + str(direction_tuple) + " already exists in the direction list!\n\t Indices: " + str(rec_MPI_direction_pairs.index(direction_tuple)) );
            rec_MPI_direction_pairs.append( direction_tuple );
    return fs, dX, c_sound, lambda_sim, CUDA_steps, reciprocity, source_type, voxelization_type_string, use_double_FDTD, mesh_file,  \
         read_file_idx, total_rec_data, total_rec_azimuths, total_rec_elevations, total_rec_intended_location,\
          total_rec_locations_idx, total_rec_continuous_locations_m, total_rec_interpolation_index, ranks, src_data, total_sources, max_time_stamp;