'''
This file is released under the MIT License. See
http://www.opensource.org/licenses/mit-license.php

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Created on Apr 8, 2014

    Module has various utils for HRTFs - started with functions from valdemar module in the same package.

@author: Sebastian Prepelita
'''
import numpy
import scipy.signal
import scipy.fftpack
import warnings

def process_HRTF_simulations(ear_simulation, middle_of_head_simulation, 
                             sampling_frequency, 
                             downsampling_factor = 1,
                             cap_to_samples_start = 0, 
                             cap_to_samples_end = None, 
                             low_pass_filter_flag = True, 
                             low_pass_filter_length = 150, 
                             cut_off_freq_Hz = 15000, 
                             circularly_shift_by_samples = 0,
                             use_polyphase_resampling_flag = False, # Not implemented yet...
                             upsample_factor = 1,
                             downsample_factor = 1,
                             time_window = 'rectangular',
                             time_window_parameters = None):
    '''
    Function used to process two simulations to obtain HRTFs - can be used with measurements too.
    
    The procedure is:
        * Cap time signals to cap_to_samples
        * Downsample both time signals by downsampling_factor
        * FFT and complex division
        * If low_pass_filter_flag, create a linear low-pass filter and apply to result
        * Circularly shift the result by circularly_shift_by_samples to remove time aliasing in circular de-convolution
    
    :param ear_simulation: the time domain ear simulation
    :param middle_of_head_simulation: the time domain 'middle of the head' simulation
    :param sampling_frequency: the sampling frequency of the simulation
    :param downsampling_factor: how much the time signals are downsampled - for example, a value of 2 will downsample the time domain signals by two.
    
    :param cap_to_samples_start: If >0, then the time signals are read from this value - useful to match the same frequency resolution.
    :param cap_to_samples_end: If not None, then the time signals are read until this value - some CUDA simulations simulate more than neccesary.
    
    :param low_pass_filter_flag: Boolean. If True, the FDTD signals are low-pass filtered after downsampling with a linear phase low-pass.
    :param low_pass_filter_length: The length of the line-phase low pass filter.
    :param cut_off_freq_Hz: The cut-off frequency in Hz for the low-pass fitler (careful not to go above the Nyquist of CIPIC database simulations).
    
    :param circularly_shift_by_samples: int value, mentioning the number of samples that the resulting HRIR from FDTD is circularly shifted in time. Positive 
                                        value means shift to left and negative value means shift to right.
                                        This is for time alignment with CIPIC measurement, causality in the resulting HRIR and phase alignment with CIPIC HRIRs.
    
    :param use_polyphase_resampling_flag: Boolean. If true, the signal is upsamnpled (zero-insertion), then filtered with the linear-phase cut_off_freq_Hz, then downsampled (decimated).
                                The upfirdn library will be used. Note that this can easily be done manually.
                                Requires use_polyphase_resampling_flag = True.
                                
    :param time_window: The window applied to the time signal. Rectangular is by default.
        Other supported windows: 'tukey' window.
    :param time_window_parameters: Any parameters to be passed to the time window (e.g., alpha for the tukey window). 
    '''
    #     hann_window = numpy.hanning(N);
#     signal_windowed = signal * hann_window;
    #------------------------------------------------------------------------------ Cap time signals
    if cap_to_samples_end is not None:
        ear_simulation = ear_simulation[0:cap_to_samples_end];
        middle_of_head_simulation = middle_of_head_simulation[0:cap_to_samples_end];
    if cap_to_samples_start >0:
        ear_simulation = ear_simulation[cap_to_samples_start::];
        middle_of_head_simulation = middle_of_head_simulation[cap_to_samples_start::];
        
    # Resample and filter:
    if use_polyphase_resampling_flag:
        if not low_pass_filter_flag:
            raise NameError('The function was designed to resample only when the low-pass filtering is enabled - the low-pass filtering will be included in the decimation/zero-insertion process.');
        # TODO: !!
        #ear_simulation = upfirdn.upfirdn(x = ear_simulation, uprate=2, downrate=3, all_samples=True);
    #------------------------------------------------------------------------------ Downsample
    if downsampling_factor != 1:
        ########################
        ## This downsampling method is not good - causes aliasing or ringing....
        ##   It works fine for a periodic signal, like a sin(__)
        ################################################################################################
        if downsampling_factor % 1 > 0.0:
            warnings.warn("\n The downsampling of simulation is not done from an integer multiplier [ds="+str(downsampling_factor)+"]! Downsampling might cause issues.\n")
        ear_simulation = scipy.signal.resample( ear_simulation, len(ear_simulation) / downsampling_factor);
        middle_of_head_simulation = scipy.signal.resample( middle_of_head_simulation, len(middle_of_head_simulation) / downsampling_factor);
        new_sampling_frequency = sampling_frequency / downsampling_factor;
    else:
        new_sampling_frequency = sampling_frequency;
        
    #------------------------------------------------------------------------------ Apply window:
    if time_window == 'rectangular':
        window = numpy.ones( len(ear_simulation) );
    if time_window == 'hanning':
        window = numpy.hanning( len(ear_simulation) );
    if time_window == 'tukey':
        window = tukey(len(ear_simulation), alpha = time_window_parameters);
    ear_simulation = window * ear_simulation;
    middle_of_head_simulation = window * middle_of_head_simulation;
    #------------------------------------------------------------------------------ Complex division
    ear_simulation = scipy.fftpack.fft(ear_simulation);
    middle_of_head_simulation = scipy.fftpack.fft(middle_of_head_simulation);
    HRTF = ear_simulation / middle_of_head_simulation;
    
    #------------------------------------------------------------------------------ Low-pass filter resulted filter:
    if low_pass_filter_flag:
        if low_pass_filter_length > len(HRTF):
            raise NameError('The desired low pass filter length is higher than the FDTD time signals length. The script was not designed for this!');
        # The low pass filtering is done directly in the frequency domain by multiplication - in time domain the results are mega crappy due to time aliasing of HRIRs
        h_low_pass_linear_phase = scipy.signal.firwin( numtaps = low_pass_filter_length, cutoff= cut_off_freq_Hz, nyq = new_sampling_frequency/2, pass_zero = True);
        # Zero padd:
        h_low_pass_linear_phase = numpy.hstack( (h_low_pass_linear_phase, numpy.zeros( len(HRTF) - len(h_low_pass_linear_phase) ) ) );
        # Zero padd and plot if one needs this:
        h_low_pass_linear_phase = scipy.fftpack.fft(h_low_pass_linear_phase);
        HRTF = HRTF * h_low_pass_linear_phase;
    
    HRIR = numpy.real( scipy.fftpack.ifft(HRTF) );
    #------------------------------------------------------------------------------ Circularly shift
    HRIR = shiftSignal(signal = HRIR, ShiftBy = circularly_shift_by_samples);
    return HRIR, new_sampling_frequency;

def tukey(M, alpha=0.5, sym=True):
    r"""Return a Tukey window, also known as a tapered cosine window.
    
    Taken from scipy.signal.tukey (scipy version 0.18.0dev -> until it reaches the official one)
    
    Parameters
    ----------
    M : int
        Number of points in the output window. If zero or less, an empty
        array is returned.
    alpha : float, optional
        Shape parameter of the Tukey window, representing the faction of the
        window inside the cosine tapered region.
        If zero, the Tukey window is equivalent to a rectangular window.
        If one, the Tukey window is equivalent to a Hann window.
    sym : bool, optional
        When True (default), generates a symmetric window, for use in filter
        design.
        When False, generates a periodic window, for use in spectral analysis.
    Returns
    -------
    w : ndarray
        The window, with the maximum value normalized to 1 (though the value 1
        does not appear if `M` is even and `sym` is True).
    References
    ----------
    .. [1] Harris, Fredric J. (Jan 1978). "On the use of Windows for Harmonic
           Analysis with the Discrete Fourier Transform". Proceedings of the
           IEEE 66 (1): 51-83. doi:10.1109/PROC.1978.10837
    .. [2] Wikipedia, "Window function",
           http://en.wikipedia.org/wiki/Window_function#Tukey_window
    Examples
    --------
    Plot the window and its frequency response:
    >>> from scipy import signal
    >>> from scipy.fftpack import fft, fftshift
    >>> import matplotlib.pyplot as plt
    >>> window = signal.tukey(51)
    >>> plt.plot(window)
    >>> plt.title("Tukey window")
    >>> plt.ylabel("Amplitude")
    >>> plt.xlabel("Sample")
    >>> plt.ylim([0, 1.1])
    >>> plt.figure()
    >>> A = fft(window, 2048) / (len(window)/2.0)
    >>> freq = np.linspace(-0.5, 0.5, len(A))
    >>> response = 20 * np.log10(np.abs(fftshift(A / abs(A).max())))
    >>> plt.plot(freq, response)
    >>> plt.axis([-0.5, 0.5, -120, 0])
    >>> plt.title("Frequency response of the Tukey window")
    >>> plt.ylabel("Normalized magnitude [dB]")
    >>> plt.xlabel("Normalized frequency [cycles per sample]")
    """
    if M < 1:
        return numpy.array([])
    if M == 1:
        return numpy.ones(1, 'd')

    if alpha <= 0:
        return numpy.ones(M, 'd')
    elif alpha >= 1.0:
        return scipy.signal.hann(M, sym=sym)

    odd = M % 2
    if not sym and not odd:
        M = M + 1

    n = numpy.arange(0, M)
    width = int(numpy.floor(alpha*(M-1)/2.0))
    n1 = n[0:width+1]
    n2 = n[width+1:M-width-1]
    n3 = n[M-width-1:]

    w1 = 0.5 * (1 + numpy.cos(numpy.pi * (-1 + 2.0*n1/alpha/(M-1))))
    w2 = numpy.ones(n2.shape)
    w3 = 0.5 * (1 + numpy.cos(numpy.pi * (-2.0/alpha + 1 + 2.0*n3/alpha/(M-1))))

    w = numpy.concatenate((w1, w2, w3))

    if not sym and not odd:
        w = w[:-1]
    return w

def shiftSignal( signal, ShiftBy):
    """
    A function to circularly shift a numpy array by a number of samples.
    
    Example::
    
        shiftSignal(signal, -3)
        
    :param signal: The signal to be shifted
    :type signal: numpy.array
    :param ShiftBy: The number of samples that the signal wil be shifted. If positive, signal will be shited to the right, otherwise to the left.
    :type ShiftBy: int
    
    :return: A tuple containing: (Vector frequency, Fourier magnitude)
    :rtype: tuple
    """
    return numpy.roll(signal, ShiftBy )