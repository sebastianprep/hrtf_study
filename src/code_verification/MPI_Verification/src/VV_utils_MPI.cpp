///////////////////////////////////////////////////////////////////////////////
//
// This file is released under the MIT License. See
// http://www.opensource.org/licenses/mit-license.php
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//  Author: Sebastian Prepelita, Aalto University
//  Created on: Jun 8, 2016
//      - updates to mesh_size_t & full HOST voxelization @2017
//
///////////////////////////////////////////////////////////////////////////////
/*
 * VV_utils_MPI.cpp
 *
 *    File with helper function to set-up a simulation for a code verification
 *    study.
 *
 *    Different header than VV_utils.cpp such that the MPI libraries would not be
 *    needed for the VV_utils.h functions.
 *
 */
#include "VV_utils_MPI.h"
#include "../MPI/MPI_Utils.h"
#include "VV_utils.h"

void set_up_Parameters_MES_single_MPI(
            SimulationParameters* parameter_ptr__,
            uint3 MPI_voxelization_dim,
            uint3 FULL_voxelization_dim,
            unsigned int fs,
            double lambda_,
            double c_sound,
            double dX,
            double dT,
            unsigned int total_steps,
            unsigned int CUDA_steps, // should be total_steps - 2
            double L_xyz,
            unsigned int start_voxel_idx,
            unsigned int size_x,
            unsigned int size_y,
            unsigned int size_z,
            int MPI_rank,
        const std::vector< std::vector< mesh_size_t >> &MPI_partition_indexing_
            )
{
    parameter_ptr__->setSpatialFs(fs);
    parameter_ptr__->setLambda(lambda_);
    parameter_ptr__->setC(c_sound);
    parameter_ptr__->setNumSteps(CUDA_steps);
    parameter_ptr__->setOctave(0);

    parameter_ptr__->setAddPaddingToElementIdx(false);

    unsigned int Nx = (unsigned int)(L_xyz/dX), Ny = (unsigned int)(L_xyz/dX),
            Nz = (unsigned int)(L_xyz/dX);
    // Call to check_voxelization_consistency_MMS should be done in MAIN, i.e.,
    //      code_Validation_BOX_MPI.cpp.
    // Some additional checks:
    if (Nx != size_x){
        log_msg<LOG_ERROR>(L"Nx (%u) is not the same as size_x (%u)."
            "[VV_uils_MPI.cpp - set_up_Parameters_MMS_single()] @l52") %Nx %size_x;
        exit(EXIT_FAILURE);
    }
    if (Ny != size_y){
        log_msg<LOG_ERROR>(L"Ny (%u) is not the same as size_y (%u)."
          "[VV_uils_MPI.cpp - set_up_Parameters_MMS_single() @l58]") %Ny %size_y;
        exit(EXIT_FAILURE);
    }
    if (Nz != size_z){
        log_msg<LOG_ERROR>(L"Nz (%u) is not the same as size_z (%u)."
          "[VV_uils_MPI.cpp - set_up_Parameters_MMS_single() @l60]") %Nz %size_z;
        exit(EXIT_FAILURE);
    }
    ///////////////////////////////////////////////////////////////////////////
    // SOURCES - No sources
    ///////////////////////////////////////////////////////////////////////////
    unsigned int total_HS = 0, total_SS = 0, total_sources = 0;
    ///////////////////////////////////////////////////////////////////////////
    // Receivers - no receivers:
    ///////////////////////////////////////////////////////////////////////////
    unsigned int total_receivers = 0;
    log_msg<LOG_INFO>(L"[R%02d] V&V_MPI MES - added a total of %u sources (%u "
      "HARD and %u  SOFT) #receivers = %u \n======================"
        "=============") %MPI_rank %parameter_ptr__->getNumSources() %total_HS
            %total_SS %parameter_ptr__->getNumReceivers();
}

std::vector<unsigned int> set_up_Initial_conditions_MES_single_MPI(
        CudaMesh* the_mesh,
        unsigned char* MPI_h_position_idx,
        unsigned char* MPI_h_material_idx,
        uint3 MPI_voxelization_dim,
        uint3 FULL_voxelization_dim,
        unsigned int GPU_partitions,
        unsigned int start_voxel_idx,
        unsigned int size_x,
        unsigned int size_y,
        unsigned int size_z,
        double c_sound,
        double dX,
        double dT,
        double P0,
        double L_xyz,
        int n_xyz,
        int MPI_rank,
        const std::vector< std::vector< mesh_size_t >> &MPI_partition_indexing_,
        unsigned int dev_start_id)
{
    // First, set-up the partitions (i.e., push Vox data):
    std::vector<unsigned int> dev_list = set_up_mesh_Partitions(
                                                    the_mesh,
                                                    MPI_h_position_idx,
                                                    MPI_h_material_idx,
                                                    GPU_partitions,
                                                    dev_start_id);
    log_msg<LOG_DEBUG>(L"[R%02d] VV_utils_MPI.set_up_Initial_conditions_MES_single_MPI()"
       " - Done setting up mesh partitions (Exact solution).") %MPI_rank;
    //////////////////////////////////// Init pressure meshes...
    // This is the MPI size (not full size):
    mesh_size_t mem_size = (mesh_size_t)MPI_voxelization_dim.x *
            (mesh_size_t)MPI_voxelization_dim.y*(mesh_size_t)MPI_voxelization_dim.z;
    // Allocate temporary pressure pointer:
    float* p_current = new float[mem_size];
    memset(p_current, 0, mem_size*sizeof(float));
    float* p_old = new float[mem_size];
    memset(p_old, 0, mem_size*sizeof(float));
    ///////////////////////////////////// Calculating MS:
    mesh_size_t size_XY = (mesh_size_t)MPI_voxelization_dim.x *
            (mesh_size_t)MPI_voxelization_dim.y;
    log_msg<LOG_DEBUG>(L"[R%02d] VV_utils_MPI: Setting initial conditions inside "
    "MPI_rank %d, between z slices [%llu,%llu] - total z dimension of %u.")
        %MPI_rank %MPI_rank %MPI_partition_indexing_.at(MPI_rank).at(0)
            %MPI_partition_indexing_.at(MPI_rank).back() %MPI_voxelization_dim.z;
    for(unsigned int idx_z =start_voxel_idx;
            idx_z < start_voxel_idx+ size_z; idx_z++)
        for(unsigned int idx_y =start_voxel_idx;
                idx_y < start_voxel_idx+ size_y; idx_y++)
            for(unsigned int idx_x =start_voxel_idx;
                    idx_x < start_voxel_idx + size_x; idx_x++)
            {
                // Check if point is inside MPI (include halos):
                if ( (idx_z >= MPI_partition_indexing_.at(MPI_rank).at(0)) &&
                        (idx_z <= MPI_partition_indexing_.at(MPI_rank).back()) )
                {
                    // The z index is moved down for the MPI domain.
                    *(p_old + ((mesh_size_t)idx_z-MPI_partition_indexing_.at(MPI_rank).at(0))
                            *size_XY + (mesh_size_t)idx_y*MPI_voxelization_dim.x + idx_x) =
                            (float)get_Exact_solution_BOX_Rigid_Point(
                                    idx_x-start_voxel_idx,        // N_x
                                    idx_y-start_voxel_idx,        // N_y
                                    idx_z-start_voxel_idx,        // N_z
                                    dX,
                                    0.0*dT,        // t
                                    P0,
                                    n_xyz,        // n_x
                                    n_xyz,        // n_y
                                    n_xyz,        // n_z
                                    L_xyz,        // L_x
                                    L_xyz,        // L_y
                                    L_xyz,        // L_z
                                    c_sound);
                    *(p_current+ ((mesh_size_t)idx_z-MPI_partition_indexing_.at(MPI_rank).at(0))
                            *size_XY + (mesh_size_t)idx_y*MPI_voxelization_dim.x + idx_x) =
                            (float)get_Exact_solution_BOX_Rigid_Point(
                                    idx_x-start_voxel_idx,        // N_x
                                    idx_y-start_voxel_idx,        // N_y
                                    idx_z-start_voxel_idx,        // N_z
                                    dX,
                                    1.0*dT,        // t
                                    P0,
                                    n_xyz,        // n_x
                                    n_xyz,        // n_y
                                    n_xyz,        // n_z
                                    L_xyz,        // L_x
                                    L_xyz,        // L_y
                                    L_xyz,        // L_z
                                    c_sound);
                }
            }
    log_msg<LOG_DEBUG>(L"[R%02d] VV_utils_MPI.set_up_Initial_conditions"
     "_MES_single_MPI() - Pushing initial conditions... #N_partitions = %u")
               %MPI_rank %the_mesh->getNumberOfPartitions();
    push_initial_pressure_fields(the_mesh, p_current, p_old);
    // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
    //log_msg<LOG_TRACE>(L"Set initial conditions: max(p_old) = %f \n "
    //    "max(p_current) = %f, mem_size = %llu") %get_abs_max_value(p_old, mem_size)
    //            %get_abs_max_value(p_current, mem_size) %mem_size;
    // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
    delete[] p_current;
    delete[] p_old;
    log_msg<LOG_DEBUG>(L"[R%02d] set_up_Initial_conditions_MES_single_MPI(): "
                "Done pushing the initial conditions from host to device. "
                "Initial conditions MMS done!") %MPI_rank;
    return dev_list;
}

void set_up_Parameters_MMS_single_MPI(
    unsigned char* MPI_h_postition_idx_,
    mesh_size_t mem_size,
    SimulationParameters* parameter_ptr__,
    uint3 MPI_voxelization_dim,
    uint3 FULL_voxelization_dim,
    unsigned int fs,
    double lambda_,
    double c_sound,
    double dX,
    double dT,
    unsigned int total_steps,
    unsigned int CUDA_steps, // should be total_steps - 2
    double P0,
    double Y_xyz,
    double L_xyz,
    int n_xyz,
    double phi_xyz,
    bool test_only_interior_flag,
    unsigned int start_voxel_idx,
    unsigned int size_x,
    unsigned int size_y,
    unsigned int size_z,
    int MPI_rank,
    const std::vector< std::vector< mesh_size_t >> &MPI_partition_indexing_)
{
    parameter_ptr__->setSpatialFs(fs);
    parameter_ptr__->setLambda(lambda_);
    parameter_ptr__->setC(c_sound);
    parameter_ptr__->setNumSteps(CUDA_steps);
    parameter_ptr__->setOctave(0);

    parameter_ptr__->setAddPaddingToElementIdx(false);

    unsigned int Nx = (unsigned int)(L_xyz/dX), Ny = (unsigned int)(L_xyz/dX),
            Nz = (unsigned int)(L_xyz/dX);
    // call to check_voxelization_consistency_MMS should be done in MAIN, i.e.,
    //      code_Validation_BOX_MPI.cpp.
    // Some additional checks:
    if (Nx != size_x){
        log_msg<LOG_ERROR>(L"Nx (%u) is not the same as size_x (%u)."
          "[VV_tuils.cpp - set_up_Parameters_MMS_single()] @l308") %Nx %size_x;
        exit(EXIT_FAILURE);
    }
    if (Ny != size_y){
        log_msg<LOG_ERROR>(L"Ny (%u) is not the same as size_y (%u)."
          "[VV_tuils.cpp - set_up_Parameters_MMS_single()] @l313") %Ny %size_y;
        exit(EXIT_FAILURE);
    }
    if (Nz != size_z){
        log_msg<LOG_ERROR>(L"Nz (%u) is not the same as size_z (%u)."
          "[VV_tuils.cpp - set_up_Parameters_MMS_single()] @l318") %Nz %size_z;
        exit(EXIT_FAILURE);
    }
    ///////////////////////////////////////////////////////////////////////////
    // SOURCES:
    ///////////////////////////////////////////////////////////////////////////
    // In the CUDA mesh, the source is added from sample #1 (to p_current).Thus:
    //    -) the first computation is done on initial conditions (p_0 and p_1)
    // --> get p_2 (so first source term is 0)
    //    -) p_3 is not calculated based on p_2 and p_1. The forcing needs to be
    // applied to p_2 --> for this calculation forcing is != --> second force
    // term is != and equals the forcing at p_2 ( t=2*dT )
    //    -) from p_4 on, just apply forcing...
    //     Thus, source_term = [0.0, forcing(2*dT), forcing(3*dT), ... ]
    std::vector<float> source_data_vector(total_steps);
    memset(&source_data_vector[0], 0, sizeof(source_data_vector[0]) *
            source_data_vector.size());
    unsigned int total_sources = 0, total_SS = 0, total_HS = 0;
    // Used in checks if the source is inside the domain:
    mesh_size_t temp_z_idx;
    if (test_only_interior_flag){
        log_msg<LOG_DEBUG>(L"V&V : Testing only interior - HARD sources on the "
                "outer domain; SOFT sources inside the domain. [Nx,Ny,Nz] = "
                "[%u,%u,%u]") %Nx %Ny %Nz;
        // Add sources:
        for(unsigned int idx_x = 0; idx_x< Nx; idx_x ++)
            for(unsigned int idx_y = 0; idx_y< Ny; idx_y ++)
                for(unsigned int idx_z = 0; idx_z< Nz; idx_z ++)
                {
                    if (
                            (idx_x*idx_y*idx_z == 0)
                            ||
                            ((idx_x-Nx+1)*(idx_y-Ny+1)*(idx_z-Nz+1) == 0)
                        )
                    {
                        // Check and update source position inside the MPI domain:
                        temp_z_idx = (mesh_size_t)start_voxel_idx + idx_z;
                        if(MPI_check_source(&temp_z_idx, MPI_partition_indexing_,
                                                            MPI_rank) )
                              {
                                // The outer hard sources (index 0 or Nx/Ny/Nz):
                                Source s( dX*( start_voxel_idx + idx_x),// x [m]
                                          dX*( start_voxel_idx + idx_y),// y [m]
                                          dX*( temp_z_idx), // z [m]
                                          SRC_HARD, // source_type
                                          DATA, // input type = DATA
                                          0 // group ?!
                                        );
                                s.setInputDataIdx(total_sources);
                                parameter_ptr__->addSource_no_logging( s );
    /*
    //log_msg<LOG_TRACE>( L"Source added [%d,%d,%d]. "
     * "Getting MMS time vector at [%d,%d,%d], from sample %d to sample %d "
     * "(total_samples = %d). CUDA_steps = %d. total_steps = %d" )
    //        %(start_voxel_idx + idx_x) %(start_voxel_idx + idx_y)
     * %(start_voxel_idx + idx_z) %idx_x %idx_y %idx_z %1 %total_steps
     * %(total_steps-1+1) %CUDA_steps %total_steps;
     *
     */
                                // Get data for the HARD sources -> use MMS
                                get_Manufactured_solution_BOX_ALL_Walls_abs_TimeVector(
                                        &source_data_vector[0],
                                        idx_x, idx_y, idx_z,
                                        1,                     // t_start_sample
                                        total_steps-1,         //t_end_sample
                                        dX, dT, P0, Y_xyz, n_xyz, L_xyz,phi_xyz,
                                        c_sound);
                                //log_msg<LOG_TRACE>( L"Done! Adding input "
                                //        "data..." );
                                parameter_ptr__->addInputData(source_data_vector);
                                //log_msg<LOG_TRACE>( L"Input data added..." );
                                total_HS += 1;
                                total_sources += 1;
                             }
                    }
                    else
                    {
                        // Check and update source position inside the MPI domain:
                        temp_z_idx = (mesh_size_t)start_voxel_idx + idx_z;
                        if(MPI_check_source(&temp_z_idx,MPI_partition_indexing_,
                                                            MPI_rank) )
                              {
                                // The interior soft sources:
                                Source s( dX*( start_voxel_idx + idx_x),// x [m]
                                          dX*( start_voxel_idx + idx_y),// y [m]
                                          dX*( temp_z_idx ), // z [m]
                                          SRC_SOFT, // source_type
                                          DATA, // input type = DATA
                                          0 // group ?!
                                        );
                                s.setInputDataIdx(total_sources);
                                parameter_ptr__->addSource_no_logging( s );
                                // Get data - SOFT sources --> forcing
                                get_Forcing_BOX_ALL_Walls_abs_TimeVector(
                                        &source_data_vector[0],
                                        MPI_h_postition_idx_,
                                        MPI_voxelization_dim.x,
                                        MPI_voxelization_dim.y,
                                        MPI_voxelization_dim.z,
                                        lambda_,
                                        idx_x, idx_y, idx_z, temp_z_idx,
                                        start_voxel_idx,
                                        0,                     // t_start_sample
                                        total_steps - 2,     // t_end_sample
                                        dX, dT, P0, Y_xyz, n_xyz, L_xyz,phi_xyz,
                                        c_sound);
                                //First is zero, then it starts from 1.
                                source_data_vector[0] = 0.0f;
                                parameter_ptr__->addInputData(source_data_vector);
                                total_SS += 1;
                                total_sources += 1;
                              }
                    }

                }

    } else
    {
        // TESTING BOUNDARY IMPLEMENTATION TOO!
        log_msg<LOG_DEBUG>(L"[R%02d] V&V : Testing all solution, including boundaries - "
                "only SOFT sources inside the domain. [Nx,Ny,Nz] = "
                "[%u,%u,%u]") %MPI_rank %Nx %Ny %Nz;
        // Add sources:
        for(unsigned int idx_x = 0; idx_x< Nx; idx_x ++)
            for(unsigned int idx_y = 0; idx_y< Ny; idx_y ++)
                for(unsigned int idx_z = 0; idx_z< Nz; idx_z ++)
                {
                    // Check and update source position inside the MPI domain:
                    temp_z_idx = (mesh_size_t)start_voxel_idx + idx_z;
                    if(MPI_check_source(&temp_z_idx,MPI_partition_indexing_,
                                                        MPI_rank) )
                    {
                        // Soft source:
                        Source s( dX*( start_voxel_idx + idx_x), // x [m]
                                  dX*( start_voxel_idx + idx_y), // y [m]
                                  dX*( temp_z_idx), // z [m]
                                  SRC_SOFT, // source_type
                                  DATA, // input type = DATA
                                  0 // group ?!
                                );
                        s.setInputDataIdx(total_sources);
                        parameter_ptr__->addSource_no_logging( s );
                        // Get data - SOFT sources --> forcing
                        get_Forcing_BOX_ALL_Walls_abs_TimeVector(
                                &source_data_vector[0],
                                MPI_h_postition_idx_,
                                MPI_voxelization_dim.x,
                                MPI_voxelization_dim.y,
                                MPI_voxelization_dim.z,
                                lambda_,
                                idx_x, idx_y, idx_z, temp_z_idx,
                                start_voxel_idx,
                                0,                         // t_start_sample
                                total_steps - 2,         // t_end_sample
                                dX, dT, P0, Y_xyz, n_xyz, L_xyz, phi_xyz,
                                c_sound);
                        //First is zero, then it starts from 1.
                        source_data_vector[0] = 0.0f;
                        parameter_ptr__->addInputData( source_data_vector );
                        total_SS += 1;
                        total_sources += 1;
                    }
                }
    }
    ///////////////////////////////////////////////////////////////////////////
    // Receivers - no receivers:
    ///////////////////////////////////////////////////////////////////////////
    unsigned int total_receivers = 0;
    log_msg<LOG_INFO>(L"[R%02d] V&V_MPI MMS - added a total of %u sources (%u "
      "HARD and %u  SOFT) #receivers = %u \n======================"
        "=============") %MPI_rank %parameter_ptr__->getNumSources() %total_HS
            %total_SS %parameter_ptr__->getNumReceivers();
}

std::vector<unsigned int> set_up_Initial_conditions_MMS_single_MPI(
    CudaMesh* the_mesh,
    unsigned char* MPI_h_position_idx,
    unsigned char* MPI_h_material_idx,
    uint3 MPI_voxelization_dim,
    uint3 FULL_voxelization_dim,
    unsigned int GPU_partitions,
    unsigned int start_voxel_idx,
    unsigned int size_x,
    unsigned int size_y,
    unsigned int size_z,
    double c_sound,
    double dX,
    double dT,
    double P0,
    double Y_xyz,
    double L_xyz,
    int n_xyz,
    double phi_xyz,
    int MPI_rank,
    const std::vector< std::vector< mesh_size_t >> &MPI_partition_indexing_,
    unsigned int dev_start_id)
{
    // First, set-up the partitions (i.e., push Vox data):
    std::vector<unsigned int> dev_list = set_up_mesh_Partitions(
                                                    the_mesh,
                                                    MPI_h_position_idx,
                                                    MPI_h_material_idx,
                                                    GPU_partitions,
                                                    dev_start_id);
    log_msg<LOG_DEBUG>(L"[R%02d] VV_utils_MPI.set_up_Initial_conditions_MES_single_MPI()"
           " - Done setting up mesh partitions (Manufactured solution).") %MPI_rank;
    //////////////////////////////////// Init pressure meshes...
    // This is the MPI size (not full size):
    mesh_size_t mem_size = (mesh_size_t)MPI_voxelization_dim.x *
            (mesh_size_t)MPI_voxelization_dim.y*(mesh_size_t)MPI_voxelization_dim.z;
    // Allocate temporary pressure pointer:
    float* p_current = new float[mem_size];
    memset(p_current, 0, mem_size*sizeof(float));
    float* p_old = new float[mem_size];
    memset(p_old, 0, mem_size*sizeof(float));
    ///////////////////////////////////// Calculating MS:
    mesh_size_t size_XY = (mesh_size_t)MPI_voxelization_dim.x *
                            (mesh_size_t)MPI_voxelization_dim.y;
    log_msg<LOG_DEBUG>(L"[R%02d] VV_utils_MPI: Setting initial conditions inside "
      "MPI_rank %d, between z slices [%llu,%llu] - total z dimension of %u") %MPI_rank
          %MPI_rank %MPI_partition_indexing_.at(MPI_rank).at(0)
            %MPI_partition_indexing_.at(MPI_rank).back() %MPI_voxelization_dim.z;
    for(unsigned int idx_z =start_voxel_idx;
            idx_z < start_voxel_idx+ size_z; idx_z++)
        for(unsigned int idx_y =start_voxel_idx;
                idx_y < start_voxel_idx+ size_y; idx_y++)
            for(unsigned int idx_x =start_voxel_idx;
                    idx_x < start_voxel_idx + size_x; idx_x++)
            {
                // Check if point is inside MPI (include halos):
                if ( (idx_z >= MPI_partition_indexing_.at(MPI_rank).at(0)) &&
                        (idx_z <= MPI_partition_indexing_.at(MPI_rank).back()) )
                {
                    *(p_old + ((mesh_size_t)idx_z-MPI_partition_indexing_.at(MPI_rank).at(0))
                            *size_XY + (mesh_size_t)idx_y*MPI_voxelization_dim.x + idx_x)=
                            get_Manufactured_solution_BOX_ALL_Walls_abs_Point(
                                    idx_x - start_voxel_idx,        // N_x
                                    idx_y - start_voxel_idx,        // N_y
                                    idx_z - start_voxel_idx,        // N_z
                                    dX, P0,
                                    0.0*dT,            // t
                                    Y_xyz, n_xyz, L_xyz, phi_xyz, c_sound);
                    *(p_current+((mesh_size_t)idx_z-MPI_partition_indexing_.at(MPI_rank).at(0))
                            *size_XY + (mesh_size_t)idx_y*MPI_voxelization_dim.x + idx_x)=
                            get_Manufactured_solution_BOX_ALL_Walls_abs_Point(
                                    idx_x - start_voxel_idx,        // N_x
                                    idx_y - start_voxel_idx,        // N_y
                                    idx_z - start_voxel_idx,        // N_z
                                    dX, P0,
                                    1.0*dT,        // t
                                    Y_xyz, n_xyz, L_xyz, phi_xyz, c_sound);
                }
            }
    // Moving them to the cuda mesh:
    push_initial_pressure_fields(the_mesh, p_current, p_old);
    // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
    //log_msg<LOG_TRACE>(L"Set initial conditions: max(p_old) = %f \n "
    //    "max(p_current) = %f, mem_size = %llu") %get_abs_max_value(p_old, mem_size)
    //            %get_abs_max_value(p_current, mem_size) %mem_size;
    // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
    delete[] p_current;
    delete[] p_old;
    log_msg<LOG_DEBUG>(L"[R%02d] set_up_Initial_conditions_MMS_single_MPI(): "
            "Done pushing the initial conditions from host to device. "
            "Initial conditions MMS done!") %MPI_rank;
    return dev_list;
}


double get_L2_square_norm_single_MPI(
        float* MPI_FDTD_matrix,
        uint3 MPI_voxelization_dim,
        unsigned int start_voxel_idx,
        unsigned int size_x,
        unsigned int size_y,
        unsigned int size_z,
        float* analytical_matrix,
        double dX,
        unsigned int MPI_partition_Z_start_idx,
        unsigned int MPI_partition_Z_end_idx,
        int MPI_rank,
        const std::vector< std::vector< mesh_size_t >> &MPI_partition_indexing_
        ){
    if (dX <= 0.0){
        log_msg<LOG_ERROR>(L"[R%02d] The grid spacing is negative. "
                "This makes no sense! [VV_tuils_MPI.cpp - "
                "get_L2_square_norm_single_MPI()] @l665") %MPI_rank;
        exit(EXIT_FAILURE);
    }
    double L2_squared = 0.0, temp = 0.0;
    mesh_size_t size_XY_FDTD = (mesh_size_t)MPI_voxelization_dim.x*(mesh_size_t)MPI_voxelization_dim.y;
    mesh_size_t size_XY_analytical = (mesh_size_t)size_x*(mesh_size_t)size_y;
    double dV = dX*dX*dX;
    mesh_size_t temp_z;
    for(mesh_size_t idx_z =0; idx_z < size_z; idx_z++)
        for(mesh_size_t idx_y =0; idx_y < size_y; idx_y++)
            for(mesh_size_t idx_x = 0;idx_x < size_x; idx_x++)
            {
                temp_z = idx_z+start_voxel_idx;
                // Check if z_index inside the MPI domain:
                if((temp_z >= MPI_partition_Z_start_idx)
                        && (temp_z <= MPI_partition_Z_end_idx)){
                    temp = fabs(
                            MPI_FDTD_matrix[(temp_z-MPI_partition_indexing_.at(MPI_rank).at(0))
                                            *size_XY_FDTD+
                               (idx_y+start_voxel_idx)*MPI_voxelization_dim.x+
                               (idx_x+start_voxel_idx)] -
                           analytical_matrix[idx_z*size_XY_analytical+
                                             idx_y*size_x+
                                             idx_x]
                              );
                    L2_squared = L2_squared + temp*temp;
                }
            }
    //result = sqrt(dV*result); // Return the square
    //double result_normalized = sqrt(result / (1.0*size_XY_analytical*size_z));
    return dV*L2_squared;
}
