///////////////////////////////////////////////////////////////////////////////
//
// This file is released under the MIT License. See
// http://www.opensource.org/licenses/mit-license.php
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//  Author: Sebastian Prepelita, Aalto University
//  Created on: Jun 8 2016, updated Jan 2018
//
///////////////////////////////////////////////////////////////////////////////
/*
 * code_Verification_BOX_MPI.cpp
 *
 *     Same code verification code as in code_Verification_BOX.cpp, but now done
 *     with MPI on top (most code copied from MPI/FDTD_MPI_frame.cpp).
 *
 *    See code_Verification_BOX.cpp for more information on the code verification
 *    methods included.
 *
 *        See logger.h for LogMX (www.logmx.com) parsers.
 *
 *  Dependencies: ParallelFDTD library, Voxelizer library, CUDA, boost, hdf5,
 *          CUDA-aware MPI library (e.g., OpenMPI, MVPICH-GDR).
 *      make commands:
 *          make test       // makes the verification program for single node
 *          make test_mpi   // makes the verification program for MPI simulations
 *          make clean      // cleans the built files
 *          make clean_sim  // cleans any simulated related files (e.g., log files, simulation results)
 *
 *  Main parameters:
 *      argv[1] - {true,false} string mentioning if the code verifies the exact solution
 *                  or the manufactured solution.
 *      argv[2] - {true,false} string mentioning if, for the manufactured solution (i.e.,
 *                argv[1]="false"), the interior update or full update including boundaries
 *                is tested. See [R1].
 *      argv[3] - unsigned int mentioning the number of GPU partitions used per MPI node
 *                          E.g., if 4 CUDA devices are present, one can force using only 2.
 *
 *    Tested OK (@CSC, @AWS):
 *        K40: CUDA 6.5, MVPICH2.0-GDR
 *            MES - 1 and 2 GPU_partitions
 *            MMS - Interior: 1 and 2 GPU_partitions
 *                  Boundaries: 2 GPU_partitions
 *        K80: CUDA 8.0 & 9.0, OpenMPI (2.1.2),
 *              - 2 & 3 MPI-nodes
 *              - 1, 2, 4 and 16 DEVices per node
 *        P100: CUDA 9.0, OpenMPI 2.1.2,
 *              - 2 & 3 MPI-nodes
 *              - 4 devices per node
 */

// ParallelFDTD lib:
#include "App.h"
#include "kernels/voxelizationUtils.h"
#include "kernels/cudaMesh.h"
#include "kernels/kernels3d.h"

// MPI:
#include "../MPI/MPI_Utils.h"
#include "VV_utils_MPI.h"

#include <stdlib.h>
#include <stdio.h>

// The usual V&V utils:
#include "VV_utils.h"
#include <boost/asio/ip/host_name.hpp>

///////////////////////////////////////////////////////////////////////////////
/// \brief Function defines the grid spacings (in meters) that the convergence
/// study will contain. Just push additional grid spacing, but note that the
/// end_time_sample will end up in a different spot if the grid spacing is not
/// doubled/halved. Code not designed for this (interpolation neeeded).
///     Easier to pass the vector as arguments to main (TODO).
///
/// \param[out] dX_vector, A vector with the grid spacings in meters. Note
///             the code is run in this order for each Y_x.
///////////////////////////////////////////////////////////////////////////////
void set_dX_vector(std::vector<double>& dX_vector){
    /// Set grid spacing for convergence: [0.16, 0.08, 0.04, 0.02, 0.01]
    dX_vector.push_back(0.16);
    dX_vector.push_back(0.08);
    dX_vector.push_back(0.04);
    dX_vector.push_back(0.02);
    dX_vector.push_back(0.01);
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Function defines the specific acoustic admittances Y_x (a-dimensional,
/// see [Morse and Ingard, "Theoretical acoustics" (1968) p261]) for which
/// a convergence study will be ran. For each Y_x, a convergence study will be done
/// with grid spacings coming from set_dX_vector().
///     Note the exact solution is done inside a rigid box, so only Y_x = 0 is
/// valid, while for manufactures solution Y_x=0 is not valid.
///     Easier to pass the vector as arguments to main (TODO).
///
/// \param[in] use_ExactSolution_flag, Boolean which defines if an exact solution
///             or a manufactured solution is used.
/// \param[out] Y_x_vector, A vector with the grid spacings in meters. Note
///             the code is run in this order for each Y_x.
///////////////////////////////////////////////////////////////////////////////
void set_Y_x_vector(std::vector<double>& Y_x_vector,
                    bool use_ExactSolution_flag){
    if (use_ExactSolution_flag){
        Y_x_vector.push_back(0.0);
    } else{
        //Y_x_vector.push_back(0.001);
        Y_x_vector.push_back(0.02);
        Y_x_vector.push_back(0.1);
        Y_x_vector.push_back(0.2);
        Y_x_vector.push_back(0.3);
        Y_x_vector.push_back(0.5);
        Y_x_vector.push_back(0.7);
        Y_x_vector.push_back(0.9);
        Y_x_vector.push_back(1.0);
    }
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Function defines the Courant number/mesh ratio (a-dimensional) for
/// which the converge studies are done. For each lambda_sim returned by this
/// function, all specific acoustic impedances are swept ( coming from
/// set_Y_x_vector() ) and then for each (lambda_sim,Y_x) pair a convergence
/// study is done with grid spacings dX from set_dX_vector().
///
///     Here, a value of 0.5 is used (to avoid truncation error cancellation for
/// n_xyz = 1.
///     Easier to pass the vector as arguments to main (TODO).
///
/// \param[in] use_ExactSolution_flag, Boolean which defines if an exact solution
///             or a manufactured solution is used. Feel free to use it.
/// \param[out] lambda_sim_vector, A vector with the Courant numbers . Note
///             the code is run in this order for each Y_x.
///////////////////////////////////////////////////////////////////////////////
void set_lambda_sim_vector(std::vector<double>& lambda_sim_vector,
                                                bool use_ExactSolution_flag){
    lambda_sim_vector.push_back(0.5);
    // lambda_sim_vector.push_back( 1.0/sqrt(3) );
    /*
    if (use_ExactSolution_flag){
        lambda_sim_vector.push_back( 1.0/sqrt(3) );
    } else {
        lambda_sim_vector.push_back(0.5);
    }
    */
}

// The functions needed for launch_ParallelFDTD code:
extern "C"
{
  bool interruptCallback_(){
    return false;
  }
}
extern "C"
{
  void progressCallback_(int step, int max_step, float t_per_step ){
    log_msg<LOG_FINE>(L"          FDTD progress: %d/%d, time per step = %f")
        %step %max_step %t_per_step;
    return;
  }
}

int main(int argc, char **argv)
{
    ///////// Init app class (this is first for the logger):
    FDTD::App app;
    app.initializeDevices();

    log_msg<LOG_INFO>(L"MPI Code Verification (BOX) - start.");

    ///////////////////////////////////////////////////////////////////////////
    // Manufactured (use_ExactSolution_flag=false) or exact solution
    //    (use_ExactSolution_flag = true):
    bool use_ExactSolution_flag;
    bool test_only_interior_flag; // if use_ExactSolution_flag = false
    // Second one is to re-initialize it:
    unsigned int GPU_partitions = UINT_MAX, GPU_partitions_input = UINT_MAX;
    unsigned int MPI_dev_start_id = 0;// The starting ID for GPU devices where compute
                                //  is done

    // Read file name from calling script:
    if (argc < 4)
    {
        log_msg<LOG_ERROR>(L"FDTD_verif - Calling code_Verification_MPI program "
                "without any argument! Please, the first argument should be "
                "use_ExactSolution_flag, second test_only_interior_flag and"
                "third the #GPU partitions!");
        exit(EXIT_FAILURE);
    }
    if (argc > 4)
    {
        log_msg<LOG_ERROR>(L"MPI FDTD_verif - Calling code_Verification_MPI "
    "program with more than one argument! Program designed for two inputs: "
            "use_ExactSolution_flag and second test_only_interior_flag!");
        exit(EXIT_FAILURE);
    }
    ///////////////////
    // MPI variables:
    /////////////////////////////////////////////////////////
    int MPI_size, MPI_rank, MPI_name_length;
    int MPI_error_var;
    char MPI_name[BUFSIZ]; // BUFSIZ should be from <stdio.h>
    // Init the MPI:
    MPI_error_var = MPI_Init(&argc, &argv);
    // Get size and rank:
    MPI_Comm_size(MPI_COMM_WORLD, &MPI_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &MPI_rank);
    MPI_Get_processor_name(MPI_name, &MPI_name_length);
    log_msg<LOG_INFO>(L"[R%02d] >>> MPI FDTD_verif - Init MPI done. This is node %s: "
            "MPI_SIZE = %d MPI_RANK = %d ") %MPI_rank %MPI_name %MPI_size %MPI_rank;
    ////////////////////////////////////////////////////////////////////////////
    std::string use_ExactSolution_flag_string(argv[1]);
    std::string test_only_interior_flag_string(argv[2]);
    std::string GPU_partitions_string(argv[3]);
    log_msg<LOG_DEBUG>(L"[R%02d] Read the following arguments: %s") %MPI_rank
        %(use_ExactSolution_flag_string + " ; " +
        test_only_interior_flag_string+" ; "+GPU_partitions_string).c_str();
    // Use exact solution:
    if(use_ExactSolution_flag_string == "true"){
        use_ExactSolution_flag = true;
    } else if(use_ExactSolution_flag_string == "false"){
        use_ExactSolution_flag = false;
    }else{
        log_msg<LOG_ERROR>(L"[R%02d] MPI FDTD_verif - use_ExactSolution_flag must be "
                "either 'true' or 'false' (case insensitive)!") %MPI_rank;
        exit(EXIT_FAILURE);
    }
    // Use interior:
    if(test_only_interior_flag_string == "true"){
        test_only_interior_flag = true;
    } else if(test_only_interior_flag_string == "false"){
        test_only_interior_flag = false;
    }else{
        log_msg<LOG_ERROR>(L"[R%02d] MPI FDTD_verif - test_only_interior_flag must be "
                "either 'true' or 'false' (case insensitive)!") %MPI_rank;
        exit(EXIT_FAILURE);
    }
    // #GPU partitions:
    GPU_partitions = strtoul(argv[3], NULL, 0);
    GPU_partitions_input = GPU_partitions;
    ///////////////////////////////////////////////////////////////////////////
    log_msg<LOG_INFO>(L"[R%02d] MPI Reading scene data...") %MPI_rank;
    ////// Reading hdf5:
    char python_data_file[] = "BOX_1_28m_RIGID_python.hdf5";
    int num_triangles = 0, num_vertices = 0, num_materials = 0,
            no_of_used_materials = 0, no_of_unique_materials = 0;
    long total_material_coeff_size_ = 0;
    double* material_coefficients_double = 0;
    float* vertices_ = 0;
    unsigned int* triangles_ = 0;
    unsigned char* materials_ = 0;
    char* mesh_file = new char[1024];

    read_HDF5_Scene_Data(python_data_file,
                // Non-array data:
                &num_triangles, &num_vertices, &num_materials,
                &no_of_used_materials, &no_of_unique_materials,
                &total_material_coeff_size_,
                mesh_file,
                // arrays:
                material_coefficients_double,
                vertices_,
                triangles_,
                materials_
                );
    // Make material coefficients as floats:
    float* material_coefficients_single = new float[total_material_coeff_size_];
    for (unsigned int i; i< total_material_coeff_size_; i++){
        material_coefficients_single[i] = material_coefficients_double[i];
    }
    delete[] material_coefficients_double;

    /////////////////////////////////////////////////////////
    double lambda_sim = -0.5; // Courant number
    // Total simulation time [s]; 0.006474796 OR 0.0066
    double t_sim = 0.0066;
    double c_sound = 0.0;
    char voxelization_type = 2; // Surface conservative
    int N_x_CUDA = 32, N_y_CUDA = 4, N_z_CUDA = 1;
    uint3 block_size__ = {N_x_CUDA, N_y_CUDA, N_z_CUDA};
    /////////////// Solution config:
    double L_xyz = 1.28; // [m]
    int n_xyz = 1;
    double P0 = 1.0;

    std::vector<double> dX_vector;
    set_dX_vector(dX_vector);
    std::vector<double> Y_x_vector; // Admittance vector - only for MS:
    set_Y_x_vector(Y_x_vector, use_ExactSolution_flag);
    std::vector<unsigned int> GPU_partitions_vector;

    std::string temp_string = "dX_vector = {";
    char* number_ = new char[32];
    for (std::vector<double>::const_iterator i = dX_vector.begin();
            i != dX_vector.end(); ++i)
    {
        sprintf(number_, "%f,", *i);
        temp_string = temp_string + std::string(number_) + ",";
    }
    temp_string = temp_string + "} \n Y_x_vector = {";
    for (std::vector<double>::const_iterator i = Y_x_vector.begin();
                i != Y_x_vector.end(); ++i){
        sprintf(number_, "%f,", *i);
        temp_string = temp_string + std::string(number_) + ",";
    }

    temp_string = temp_string + "}";

    // Logging:
    log_msg<LOG_CONFIG>(L"[R%02d] %s\n t_sim = %f\n n_xyz = %d\n lambda_sim (temp) = %f\n c_sound "
        "(temp) = %f\n GPU_partitions = %u\n dev_start_id = %u\n N_x_CUDA = %d\n N_y_CUDA = %d\n "
        "N_z_CUDA = %d\n use_ExactSolution_flag = %s\n test_only_interior_flag = %s") %MPI_rank
        %temp_string.c_str() %t_sim %n_xyz %lambda_sim %c_sound %GPU_partitions %MPI_dev_start_id
        %N_x_CUDA %N_y_CUDA %N_z_CUDA %get_bool_string(use_ExactSolution_flag).c_str()
        %get_bool_string(test_only_interior_flag).c_str();

    std::vector<double> c_sound_vector;
    std::vector<double> lambda_sim_vector;
    std::vector<double> L2_norm_vector;
    std::vector<unsigned int> total_steps_vector;

    double dX = 0.0;
    double dT = 0.0;
    unsigned int fs = 0;
    double Y_xyz = Y_x_vector[0];
    // Include the initial conditions in the total steps
    unsigned int total_steps = 0;
    unsigned int N_t = 0;
    unsigned int CUDA_steps = 0;

    set_lambda_sim_vector(lambda_sim_vector, use_ExactSolution_flag);

////////////////////////////////////////////////////////////////////////////////
///     Outer most loop:
for(int lambda_index = 0; lambda_index < lambda_sim_vector.size();
                                                            lambda_index ++ )
{
    lambda_sim = lambda_sim_vector[lambda_index];
    log_msg<LOG_CONFIG>(L"[R%02d] Working at lambda_sim = %f ") %MPI_rank %lambda_sim;
    // Get the correct simulation times:
    set_convergence_parameters(
                                t_sim,
                                c_sound,
                                dX_vector,
                                lambda_sim
                              );
    c_sound_vector.push_back(c_sound);

    if (lambda_sim <= 0.0 || lambda_sim > 1.0/sqrt(3)){
        log_msg<LOG_ERROR>(L"[R%02d] lambda_sim (%f) is incorrect! ") %MPI_rank %lambda_sim;
        exit(EXIT_FAILURE);
    }
////////////////////////////////////////////////////////////////////////////////
///     Y_x loop:
for(int Y_x_index = 0;Y_x_index < Y_x_vector.size(); Y_x_index ++ )
{
    Y_xyz = Y_x_vector[Y_x_index];

    if (use_ExactSolution_flag && Y_xyz != 0.0){
        log_msg<LOG_ERROR>(L"The exact solution only works with rigid wall "
                "impedance! You are using %f") %Y_xyz;
        exit(EXIT_FAILURE);
    }
    log_msg<LOG_INFO>(L"Y_x = %f. Starting dX_vector loop...") %Y_xyz;
////////////////////////////////////////////////////////////////////////////////
/// dX loop...
for(int dX_index = 0; dX_index< dX_vector.size(); dX_index ++)
{
    // Re-init GPU_partitions:
    GPU_partitions = GPU_partitions_input;
    // From Python: set up conditions for the simulation
    dX = dX_vector.at(dX_index);
    dT = dX*lambda_sim/c_sound;
    fs = c_sound / (dX*lambda_sim);
    if ( ((mesh_size_t)( round(t_sim / dT)) + 1) > (mesh_size_t)UINT_MAX ){
        log_msg<LOG_ERROR>(L"[R%02d] total_steps overflow for specified type (UINT)! "
            "Either the simulation time [%E s] is too large or dT [%E s] is too small!")
            %MPI_rank %t_sim %dT;
        exit(EXIT_FAILURE);
    }
    total_steps = (unsigned int)( round(t_sim / dT)) + 1;
    N_t = total_steps - 2;
    CUDA_steps = N_t;
    total_steps_vector.push_back(total_steps);

    // Check overflow:
    if ( (L_xyz/dX) >= (double)INT_MAX){
        log_msg<LOG_ERROR>(L"[R%02d] L_xyz/dX is too large for assumed type (U_INT)! "
            "Either the box size [%f] is too large or dX [%f] is too small!")
            %MPI_rank %L_xyz %dX;
        exit(EXIT_FAILURE);
    }
    int Nx = (int)(L_xyz/dX), Ny = (int)(L_xyz/dX), Nz = (int)(L_xyz/dX);

    log_msg<LOG_CONFIG>(L"[R%02d] V&V MPI START dX = %E, dT = %E\n calculated fs = %f, "
      "final fs = %d Y_xyz = %E, P0 = %E, n_xyz = %d, c_sound = %f, L_xyz = %f,\n"
        "total_steps = %u, N_t = %d, CUDA_steps = %u\n [Nx,Ny,Nz] = [%d,%d,%d]")
      %MPI_rank %dX %dT %(c_sound / (dX*lambda_sim)) %fs %Y_xyz %P0 %n_xyz %c_sound
      %L_xyz %total_steps %N_t %CUDA_steps %Nx %Ny %Nz;

    log_msg<LOG_DEBUG>(L"[R%02d] dX = %f --> Get full voxelization data.") %MPI_rank %dX;
    CudaMesh* the_mesh = new CudaMesh();
    /////////////////////////////////////////////// Get FULL voxelization dims:
    uint3 FULL_voxelization_dim_ = get_Geometry_surface_Voxelization_dims(
                            vertices_, triangles_, num_triangles,
                            num_vertices, dX);
    log_msg<LOG_DEBUG>(L"Full voxelization dimensions = [%u,%u,%u]")
            %FULL_voxelization_dim_.x %FULL_voxelization_dim_.y
                %FULL_voxelization_dim_.z;
    ///////////////////////////////////////////
    //        Split domain for this MPI node:
    ///////////////////////////////////////////
    //Recycle existing code of domain splitting (done in the Z direction):
    std::vector< std::vector< mesh_size_t> > MPI_partition_indexing_;
    // Check overflow:
    if ( FULL_voxelization_dim_.z >= (unsigned int)INT_MAX){
        log_msg<LOG_ERROR>(L"[R%02d] FULL_voxelization_dim_.z[%u] overflow for int!")
            %MPI_rank %FULL_voxelization_dim_.z;
        exit(EXIT_FAILURE);
    }
    log_msg<LOG_DEBUG>(L"[R%02d] Splitting domain for domain (%s: SIZE "
        "= %d RANK = %d ). Starting partial voxelization") %MPI_rank %MPI_name
                %MPI_size %MPI_rank;
    MPI_partition_indexing_ =
        the_mesh->getPartitionIndexing(MPI_size, (int)FULL_voxelization_dim_.z);
    // Basic check first: number of MPI nodes must match the partitions given
    if (MPI_partition_indexing_.size() != MPI_size){
        log_msg<LOG_ERROR>(L"[R%02d] V&V MPI - Number of partitions (%d) is different"
            " than MPI_size (%d)") %MPI_rank %MPI_partition_indexing_.size() %MPI_size;
        exit(EXIT_FAILURE);
    }
    ///////////////////////////////////////////////////// Partial Voxelization:
    unsigned char* MPI_h_postition_idx_ = (unsigned char*)NULL;
    unsigned char* MPI_h_materials_idx_ = (unsigned char*)NULL;
    uint3 MPI_voxelization_dim_ = make_uint3(0,0,0);
    unsigned char voxelization_result;
    switch(voxelization_type){
        case 2:
            voxelization_result = voxelizeGeometrySurfToHost(
                             vertices_, triangles_, materials_, num_triangles,
                             num_vertices, no_of_used_materials, dX,
                             &MPI_h_postition_idx_, &MPI_h_materials_idx_,
                             &MPI_voxelization_dim_,
                             1, //Add a layer of air cells around
                             VOX_CONSERVATIVE,
                             make_uint3(32, 4, 1), // voxelize_block_size
                             MPI_partition_indexing_.at(MPI_rank).at(0),
                             MPI_partition_indexing_.at(MPI_rank).back()
                             );
            log_msg<LOG_DEBUG>(L"[R%02d] V&V - Partial CONSERVATIVE voxelization "
              "done... Dimensions of [%d,%d,%d]. Vox_res = %d") %MPI_rank
                  %(int)MPI_voxelization_dim_.x %(int)MPI_voxelization_dim_.y
                    %(int)MPI_voxelization_dim_.z %(int)voxelization_result;
            break;
        default:
            //Remove support for solid now-partial voxelization is more tedious
            log_msg<LOG_ERROR>(L"[R%02d] V&V FDTD_MPI - only surface conservative  "
                    "voxelization supported a.t.m.!") %MPI_rank;
            exit(EXIT_FAILURE);
    }
    // Voxelization checks:
    if (voxelization_result != 0){
        log_msg<LOG_ERROR>(L"[R%02d] V&V MPI FDTD_verif - Voxelization process with"
        " errors (voxelization_result = %u)! Additional checks should be done "
        "here - see Python voxelization_helper.deal_with_voxelization_errors()")
                %MPI_rank %voxelization_result;
        exit(EXIT_FAILURE);
    }
    // Check that number of GPU partitions:
    check_GPU_partitions(GPU_partitions,
            (mesh_size_t)MPI_voxelization_dim_.z, MPI_rank);
    // Since GPU_partitions depends only on dX, append only the first len(dX_vector):
    if (GPU_partitions_vector.size() < dX_vector.size()){
        GPU_partitions_vector.push_back(GPU_partitions);
    }
    // Set the admittance for the walls:
    // material_specific_admittances =  {1.0, 0.0, Y_x}
    //  For 'air', 'default material', 'walls_of_box'
    // First two are set from the BOX_1_28m_RIGID_python.hdf5 -> set the Y_x:
    material_coefficients_single[2*20] = Y_xyz;
    if (use_ExactSolution_flag)
        log_msg<LOG_INFO>(L"[R%02d] MPI dX = %E ==================== Starting EXACT "
                "solution, Y_xyz = %E ==================== ") %MPI_rank %dX %Y_xyz;
    else
        log_msg<LOG_INFO>(L"[R%02d] MPI dX = %E ==================== Starting "
        "MANUFACTURED solution, Y_xyz = %E ================== ") %MPI_rank %dX %Y_xyz;

    mesh_size_t mem_size = (mesh_size_t)MPI_voxelization_dim_.x *
            (mesh_size_t)MPI_voxelization_dim_.y*(mesh_size_t)MPI_voxelization_dim_.z;
    mesh_size_t dimXY = (mesh_size_t)MPI_voxelization_dim_.x*
                            (mesh_size_t)MPI_voxelization_dim_.y;
    /////////////////////////////////////////////////////////////// MPI again:
    // The MPI Z-slice indexes without guard/ghost slices:
    mesh_size_t MPI_partition_Z_start_idx = -1, MPI_partition_Z_end_idx = -1;
    int MPI_rank_neigbor_down = -2, MPI_rank_neigbor_up = -2;
    MPI_domain_get_useful_data( MPI_partition_indexing_, MPI_rank, MPI_size,
                        &MPI_partition_Z_start_idx, &MPI_partition_Z_end_idx,
                        &MPI_rank_neigbor_down, &MPI_rank_neigbor_up );
    long MPI_halo_size_MB = (long)((long double)dimXY * (sizeof(float))*1e-6);
    log_msg<LOG_CONFIG>(L"[R%02d] V&V FDTD_MPI Partitioning MPI_rank %d (total "
        "MPI_size = %d)- partition_Z_start_idx = %llu, MPI_partition_Z_end_"
        "idx = %llu, MPI_partitioning.at(0) = %llu, MPI_partitioning.back()"
        " = %llu FULL_VOX.z = %u\nMPI Message size [MB] = %ld.") %MPI_rank
        %MPI_rank %MPI_size %MPI_partition_Z_start_idx
        %MPI_partition_Z_end_idx %(MPI_partition_indexing_.at(MPI_rank).at(0))
        %(MPI_partition_indexing_.at(MPI_rank).back())
        %(FULL_voxelization_dim_.z) %MPI_halo_size_MB;
    ///////////////////////////////////////////////////////////////////////////
    // Need a full voxelization! Not optimal - cannot use any size. Otherwise
    // it is hard to get start_voxel_idx for a high MPI_rank unless hard coded
    log_msg<LOG_DEBUG>(L"[R%02d] Doing also a FULL voxelization to get the "
            "start_voxel and size_x/y/z.") %MPI_rank;
    unsigned char* FULL_h_postition_idx_ = (unsigned char*)NULL;
    unsigned char* FULL_h_materials_idx_ = (unsigned char*)NULL;
    uint3 FULL_voxelization_dim_2 = make_uint3(0,0,0);
    voxelization_result = voxelizeGeometrySurfToHost(
                     vertices_, triangles_, materials_, num_triangles,
                     num_vertices, no_of_used_materials, dX,
                     &FULL_h_postition_idx_, &FULL_h_materials_idx_,
                     &FULL_voxelization_dim_2,
                     1, //Add a layer of air cells around
                     VOX_CONSERVATIVE,
                     make_uint3(32, 4, 1), // voxelize_block_size
                     -1, //MPI_partition_indexing_.at(MPI_rank).at(0),
                     -1  //MPI_partition_indexing_.at(MPI_rank).back()
                     );
    log_msg<LOG_DEBUG>(L"[R%02d] V&V - FULL CONSERVATIVE voxelization "
          "done... Dimensions of [%d,%d,%d]. Vox_res = %d") %MPI_rank
              %(int)FULL_voxelization_dim_2.x %(int)FULL_voxelization_dim_2.y
                %(int)FULL_voxelization_dim_2.z %(int)voxelization_result;
    // Voxelization checks:
    if (voxelization_result != 0){
        log_msg<LOG_ERROR>(L"[R%02d] V&V MPI FDTD_verif - Voxelization process with"
          " errors (voxelization_result = %u)! Additional checks should be done "
          "here - see Python voxelization_helper.deal_with_voxelization_errors()")
                %MPI_rank %voxelization_result;
        exit(EXIT_FAILURE);
    }
    // Re-check:
    if ((FULL_voxelization_dim_2.x != FULL_voxelization_dim_.x) ||
            (FULL_voxelization_dim_2.y != FULL_voxelization_dim_.y) ||
             (FULL_voxelization_dim_2.z != FULL_voxelization_dim_.z))
    {
        log_msg<LOG_ERROR>(L"[R%02d] V&V FDTD_MPI - Revoxelizing the entire domain "
            "did not give the same voxelization dimensions as the "
                "get_Geometry_surface_Voxelization_dims() function.") %MPI_rank;
        exit(EXIT_FAILURE);
    }
    mesh_size_t mem_size_FULL = (mesh_size_t)FULL_voxelization_dim_2.x *
      (mesh_size_t)FULL_voxelization_dim_2.y*(mesh_size_t)FULL_voxelization_dim_2.z;
    // Local MPI device_index
    //        needed when the forcing at the last step is applied:
    unsigned int start_voxel_idx, size_x, size_y, size_z;
    log_msg<LOG_INFO>(L"[R%02d] MPI dX = %E  Checking voxelization consistency and "
                "inner air volume relevant data...") %MPI_rank %dX;
    check_voxelization_consistency_MMS(
                        FULL_h_postition_idx_, // On host
                        FULL_voxelization_dim_,
                        dX,
                        L_xyz,
                        start_voxel_idx,
                        size_x,
                        size_y,
                        size_z
                        );
    log_msg<LOG_INFO>(L"[R%02d] MPI dX = %E ->  Checks passed! The inside air box: "
        "start_voxel_idx = %u, [size_x,size_y,size_z] = [%u,%u,%u]\nSetting up "
        "sources, cuda mesh, then initial conditions...") %MPI_rank %dX
            %start_voxel_idx %size_x %size_y %size_z;
    SimulationParameters* parameter_ptr__ = new SimulationParameters();
    double phi_xyz = M_PI / 4.0;
    std::vector<unsigned int> used_dev_list;

    if (use_ExactSolution_flag){
        set_up_Parameters_MES_single_MPI(
                            parameter_ptr__,
                            MPI_voxelization_dim_,
                            FULL_voxelization_dim_,
                            fs, lambda_sim, c_sound, dX, dT, total_steps,
                            CUDA_steps, // should be total_steps - 2
                            L_xyz,
                            start_voxel_idx, size_x, size_y, size_z,
                            MPI_rank,
                            MPI_partition_indexing_
                            );

        log_msg<LOG_INFO>(L"[R%02d] MPI dX = %E ->  MES sources done. Set the "
                "cuda_mesh:") %MPI_rank %dX;
        the_mesh->setDouble(false);
        the_mesh->setupMesh<float>(no_of_unique_materials,
                                   material_coefficients_single,
                                   parameter_ptr__->getParameterPtr(),
                                   MPI_voxelization_dim_,
                                   block_size__,
                                   0);//  0: forward-difference, 1: centered
        log_msg<LOG_INFO>(L"[R%02d] MPI dX = %E ->  Cuda mesh done. Setting up initial "
          "conditions MES:\n=====================================") %MPI_rank %dX;
        used_dev_list = set_up_Initial_conditions_MES_single_MPI(
                the_mesh,
                MPI_h_postition_idx_,
                MPI_h_materials_idx_,
                MPI_voxelization_dim_,
                FULL_voxelization_dim_,
                GPU_partitions,
                start_voxel_idx,
                size_x,
                size_y,
                size_z,
                c_sound,
                dX,
                dT,
                P0,
                L_xyz,
                n_xyz,
                MPI_rank,
                MPI_partition_indexing_,
                MPI_dev_start_id);
    } else
    {
        set_up_Parameters_MMS_single_MPI(
                        MPI_h_postition_idx_,
                        mem_size,
                        parameter_ptr__,
                        MPI_voxelization_dim_,
                        FULL_voxelization_dim_,
                        fs, lambda_sim, c_sound, dX, dT,
                        total_steps,
                        CUDA_steps, // should be total_steps - 2
                        P0, Y_xyz, L_xyz, n_xyz, phi_xyz,
                        test_only_interior_flag,
                        start_voxel_idx, size_x, size_y, size_z,
                        MPI_rank,
                        MPI_partition_indexing_);
        log_msg<LOG_INFO>(L"[R%02d] MPI dX = %E ->  MMS sources done. Set the "
                "cuda_mesh:") %MPI_rank %dX;
        the_mesh->setDouble(false);
        the_mesh->setupMesh<float>(no_of_unique_materials,
                                   material_coefficients_single,
                                   parameter_ptr__->getParameterPtr(),
                                   MPI_voxelization_dim_,
                                   block_size__,
                                   0);//  0: forward-difference, 1: centered
        log_msg<LOG_INFO>(L"[R%02d] MPI dX = %E ->  Cuda mesh done. Setting up initial "
          "conditions MMS:\n=====================================") %MPI_rank %dX;
        used_dev_list = set_up_Initial_conditions_MMS_single_MPI(
                the_mesh,
                MPI_h_postition_idx_,
                MPI_h_materials_idx_,
                MPI_voxelization_dim_,
                FULL_voxelization_dim_,
                GPU_partitions,
                start_voxel_idx,
                size_x,
                size_y,
                size_z,
                c_sound,
                dX,
                dT,
                P0,
                Y_xyz,
                L_xyz,
                n_xyz,
                phi_xyz,
                MPI_rank,
                MPI_partition_indexing_,
                MPI_dev_start_id);
    }
    log_msg<LOG_INFO>(L"[R%02d] MPI dX = %E ->   Sources done."
            "\n=======================================") %MPI_rank %dX;
    // Clear pointers for full matrix:
    free(FULL_h_postition_idx_);
    free(FULL_h_materials_idx_);
    log_msg<LOG_DEBUG>(L"[R%02d] MPI - free() call for full voxelization "
                            "data!") %MPI_rank;
    // MPI SANITY CHECKS:
    MPI_checks(the_mesh, MPI_rank, MPI_partition_indexing_);
    log_msg<LOG_INFO>(L"[R%02d] V&V FDTD_MPI - MPI checks done ok.") %MPI_rank;

    float* h_return_values=new float[(mesh_size_t)parameter_ptr__->getNumSteps()*
                                (mesh_size_t)parameter_ptr__->getNumReceivers()];
////////////////////////////////////////////////////////////////////////////////
////////////////////////      FDTD UPDATE     //////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
    // The MPI FDTD step-by-step update:
    float time;
    log_msg<LOG_DEBUG>(L"[R%02d] V&V FDTD_MPI - Start computing single precision "
        "for %u device partitions... Number of receivers = %u x %u[steps]")
        %MPI_rank %the_mesh->getNumberOfPartitions()
        %parameter_ptr__->getNumReceivers() %parameter_ptr__->getNumSteps();
    log_msg<LOG_DEBUG>(L"[R%02d] MPI dX = %E -> FDTD_MPI - Prepare receiver data "
            "on device...") %MPI_rank %dX;
    std::vector<std::pair<float*, std::pair<mesh_size_t, int>>> d_receiver_data=
        prepare_receiver_data_on_device<float>(the_mesh, parameter_ptr__);
    log_msg<LOG_INFO>(L"[R%02d] MPI dX = %E -> Launching FDTD3d..."
        "\n=======================================") %MPI_rank %dX;
    for(unsigned int step = 0; step < parameter_ptr__->getNumSteps(); step++)
    {
        time = launchFDTD3dStep_single(the_mesh, parameter_ptr__, step,
                                    &d_receiver_data,
                                    interruptCallback_, progressCallback_);
        time += MPI_switch_Halos_single(the_mesh, step, MPI_rank,
                            MPI_rank_neigbor_down, MPI_rank_neigbor_up);
        log_msg<LOG_TRACE>(L"[R%02d] MPI dX = %E -> FDTD_MPI - [STEP %04u] Step"
                " done. Time per step = %f") %MPI_rank %dX %step %time;
    }
    log_msg<LOG_DEBUG>(L"MPI dX = %E -> FDTD_MPI - Copy receiver data from "
            "device to host...") %dX;
    get_receiver_data_from_device<float>(the_mesh, parameter_ptr__,
                                    h_return_values, &d_receiver_data);
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//////////////////////      FDTD UPDATE  end    ////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
    log_msg<LOG_INFO>(L"[R%02d] MPI dX = %E -> FDTD3d DONE. Retrieving the final"
        " calculated mesh....\n---------------------------------------------")
                        %MPI_rank %dX;
    float* MPI_final_pressure_field_h = new float[mem_size];
    get_full_final_pressureToHost(the_mesh, MPI_final_pressure_field_h);
    log_msg<LOG_INFO>(L"[R%02d] Last pressure value retrieved.") %MPI_rank;
    // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
    //log_msg<LOG_TRACE>(L"[R%02d] BEFORE FORCING: max( |final_pressure| ) = %f")
    //        %MPI_rank %get_abs_max_value(MPI_final_pressure_field_h, mem_size);
    // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
    log_msg<LOG_TRACE>(L"[R%02d] CUDA_STEPS = %u, total_steps = %u\nmax"
        "(final_p_field) = %f") %MPI_rank %CUDA_steps %total_steps
            %get_abs_max_value(MPI_final_pressure_field_h, mem_size);
    if(not use_ExactSolution_flag)
    {
        log_msg<LOG_INFO>(L"[R%02d] MPI dX = %E -> Applying last forcing "
                "term to MMS:") %MPI_rank %dX;
        // Calculate the latest step contained in p_new. Here is an example to
        //get the formula for CUDA_steps =3:
        // t_idx:                 /------ 0 ------\  /------- 1 -----\
        // Cuda steps:             0  ->  1 -> 2  -> 0  -> 1  -> 2  -> 0
        // Time index:   0*dT  1*dT  3*dT  4*dT  5*dT  6*dT  7*dT  8*dT
        unsigned int step_p_new = (0+1)*CUDA_steps + 1;
        double t_p_new = 1.0*( step_p_new )*dT;
        double t_forcing = 1.0*( step_p_new - 1 )*dT;
        // Apply forcing term to the last pressure value:
        mesh_size_t size_XY = (mesh_size_t)MPI_voxelization_dim_.x*
                (mesh_size_t)MPI_voxelization_dim_.y;
        for(unsigned int idx_z =start_voxel_idx;
                idx_z < start_voxel_idx+ size_z; idx_z++)
            for(unsigned int idx_y =start_voxel_idx;
                    idx_y < start_voxel_idx+ size_y; idx_y++)
                for(unsigned int idx_x =start_voxel_idx;
                        idx_x < start_voxel_idx + size_x; idx_x++)
                {
                    // Check if point is inside MPI (include halos):
                    if ( (idx_z >= MPI_partition_indexing_.at(MPI_rank).at(0)) &&
                            (idx_z <= MPI_partition_indexing_.at(MPI_rank).back()) )
                    {
                        // Apply forcing to the inner field:
                        MPI_final_pressure_field_h[
                             ((mesh_size_t)idx_z-MPI_partition_indexing_.at(MPI_rank).at(0))
                             *size_XY + (mesh_size_t)idx_y*MPI_voxelization_dim_.x +
                             (mesh_size_t)idx_x]
                            += get_Forcing_BOX_ALL_Walls_abs_Point(
                                MPI_h_postition_idx_,
                                MPI_voxelization_dim_.x,
                                MPI_voxelization_dim_.y,
                                MPI_voxelization_dim_.z,
                                lambda_sim,
                                idx_x - start_voxel_idx,        // N_x
                                idx_y - start_voxel_idx,        // N_y
                                idx_z - start_voxel_idx,        // N_z
                                idx_z-MPI_partition_indexing_.at(MPI_rank).at(0),
                                start_voxel_idx,            //start_voxel_idx
                                dX,
                                dT,
                                t_forcing,
                                P0,
                                Y_xyz,
                                n_xyz,
                                L_xyz,
                                phi_xyz,
                                c_sound);
                    }

                }
        // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
        //log_msg<LOG_TRACE>(L"[R%02d] AFTER FORCING: max( |final_pressure| ) = %f")
        //        %MPI_rank %get_abs_max_value(MPI_final_pressure_field_h, mem_size);
        // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
        if (test_only_interior_flag)
        {
            log_msg<LOG_INFO>(L"[R%02d] MPI dX = %E -> Setting the outer "
                    "values from manufactured solution:") %MPI_rank %dX;
            mesh_size_t temp_z_idx;
            // Setting the outer field:
            for(unsigned int idx_x = 0; idx_x< Nx; idx_x ++)
                for(unsigned int idx_y = 0; idx_y< Ny; idx_y ++)
                    for(unsigned int idx_z = 0; idx_z< Nz; idx_z ++)
                    {
                        // The outer hard sources (index 0 or Nx/Ny/Nz)
                        if (
                            (idx_x*idx_y*idx_z == 0)
                            ||
                            ((idx_x-Nx+1)*(idx_y-Ny+1)*(idx_z-Nz+1) == 0)
                            )
                        {
                            temp_z_idx = (mesh_size_t)start_voxel_idx + idx_z;
                            // Check if point is inside MPI (include halos):
                            if ( MPI_check_source(&temp_z_idx, MPI_partition_indexing_,
                                    MPI_rank) )
                            {
                                MPI_final_pressure_field_h[
                                  temp_z_idx*size_XY +
                                  ((mesh_size_t)idx_y+start_voxel_idx)*MPI_voxelization_dim_.x +
                                  ((mesh_size_t)idx_x+start_voxel_idx)] =
                          get_Manufactured_solution_BOX_ALL_Walls_abs_Point(
                                      idx_x,        // N_x
                                      idx_y,        // N_y
                                      idx_z,        // N_z
                                      dX,
                                      P0,
                                      t_sim,        // t
                                      Y_xyz, n_xyz, L_xyz, phi_xyz, c_sound);
                            }

                        }
                    }
            // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
            //log_msg<LOG_TRACE>(L"[R%02d] AFTER FORCING and HS: max( |final_pressure| ) = %f")
            //        %MPI_rank %get_abs_max_value(MPI_final_pressure_field_h, mem_size);
            // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
        }
    }
    log_msg<LOG_INFO>(L"[R%02d] Last sample forcing done. Calculating "
                        "analytical matrix:") %MPI_rank;
    float* final_continuous_solution = new float[(mesh_size_t)size_x*
                                                 (mesh_size_t)size_y*
                                                 (mesh_size_t)size_z];
    if (use_ExactSolution_flag){
        get_Exact_solution_BOX_Rigid(
                final_continuous_solution,
                size_x,        // N_x
                size_y,        // N_y
                size_z,        // N_z
                dX,
                t_sim,        // t
                P0,
                n_xyz,    // n_x
                n_xyz,    // n_y
                n_xyz,    // n_z
                L_xyz,    // L_x
                L_xyz,    // L_y
                L_xyz,    // L_z
                c_sound);
        log_msg<LOG_TRACE>(L"[R%02d] max(P_ES) = %E at t = %f [us]") %MPI_rank
        %get_abs_max_value(final_continuous_solution,
                            (mesh_size_t)size_x*size_y*size_z)
        %(t_sim*1E6);
    }else{
        get_Manufactured_solution_BOX_ALL_Walls_abs(
                final_continuous_solution,
                size_x,    // N_x
                dX,
                t_sim,    // t
                P0,
                Y_xyz,
                n_xyz,
                L_xyz,
                phi_xyz,
                c_sound
                );
        log_msg<LOG_TRACE>(L"[R%02d] max(P_MS) = %E at t = %f [us]") %MPI_rank
            %get_abs_max_value(final_continuous_solution,
                                (mesh_size_t)size_x*size_y*size_z)
            %(t_sim*1E6);
    }
    // Now get the L2 norm:
    log_msg<LOG_INFO>(L"[R%02d] Last sample forcing done. Calculating L2 norm:")
            %MPI_rank;
    double L2_norm_squared = get_L2_square_norm_single_MPI(
            MPI_final_pressure_field_h,
            MPI_voxelization_dim_,
            start_voxel_idx,
            size_x,
            size_y,
            size_z,
            final_continuous_solution,
            dX,
            MPI_partition_Z_start_idx,
            MPI_partition_Z_end_idx,
            MPI_rank,
            MPI_partition_indexing_);
    log_msg<LOG_CONFIG>(L"[R%02d] Got L2-squared error for dX = %E --> %E")
                %MPI_rank %dX %L2_norm_squared;
    L2_norm_vector.push_back(L2_norm_squared);
    // Local (wrt dX) clean-up:
    free(MPI_h_postition_idx_);
    free(MPI_h_materials_idx_);
    delete[] MPI_final_pressure_field_h;
    delete[] h_return_values;
    delete parameter_ptr__;
    delete the_mesh;
} // END dX_vector loop
} // END Y_x_vector loop
} // END lambda_vector loop
    //char* number_ = new char[32];
    temp_string = "L2_norm_vector = {";
    for (std::vector<double>::const_iterator i = L2_norm_vector.begin();
            i != L2_norm_vector.end(); ++i)
    {
        sprintf(number_, "%E", *i);
        temp_string = temp_string + number_ + ",";
    }
    temp_string = temp_string + "}\ndX_vector = {";
    for (std::vector<double>::const_iterator i = dX_vector.begin();
                i != dX_vector.end(); ++i){
        sprintf(number_, "%f", *i);
        temp_string = temp_string + std::string(number_) + ",";
    }
    temp_string = temp_string + "} \n";

    log_msg<LOG_CONFIG>(L"[R%02d] %s\n ******************************************"
            "***************") %MPI_rank %temp_string.c_str();
    ///////////////////////////////////////////////////////////////////////////
    // Writing L2 norm:
    ///////////////////////////////////////////////////////////////////////////
    sprintf(number_, "R%02d", MPI_rank);
    write_L2_norm(  dX_vector,
                    lambda_sim_vector,
                    c_sound_vector,
                    Y_x_vector,
                    L2_norm_vector,
                    total_steps_vector,
                    GPU_partitions_vector,
                    t_sim,
                    n_xyz,
                    GPU_partitions_input,
                    mesh_file,
                    (std::string(number_) + "_" + boost::asio::ip::host_name() +
                       "_MPI_code_V_" + std::string(use_ExactSolution_flag ?
                           "MES" : "MMS")).c_str(),
                    voxelization_type,
                    (int)use_ExactSolution_flag,
                    (int)test_only_interior_flag,
                    MPI_size,
                    MPI_rank
                );
    log_msg<LOG_DEBUG>(L"[R%02d] V&V MPI - Dumping hdf5 data complete") %MPI_rank;

    // Clear MPI stuff:
    MPI_Finalize();
    // Clean-up:
    //delete[] python_data_file; // Seg. fault with the new constant char...
    delete[] number_;
    delete[] material_coefficients_single;
    delete[] vertices_;
    delete[] triangles_;
    delete[] materials_;
    delete[] mesh_file;
    log_msg<LOG_INFO>(L"[R%02d] ------======= MPI Code Verification (BOX) "
                "- end OK. ========------") %MPI_rank;
    app.close();
    return(0);
}

