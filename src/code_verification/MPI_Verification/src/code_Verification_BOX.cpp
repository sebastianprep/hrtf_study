///////////////////////////////////////////////////////////////////////////////
//
// This file is released under the MIT License. See
// http://www.opensource.org/licenses/mit-license.php
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//  Author: Sebastian Prepelita, Aalto University
//  Created on: May 31, 2016
//      - updates to mesh_size_t @2017
//      - updates to voxelization on Host (supports more GPU memory per node)
//      - updates to >2 GPU devices/node @2017
//          [Tested OK on {1,2,3,4} devices - dev_start_id = 0]
//      - created Makefile @ 2017
//
///////////////////////////////////////////////////////////////////////////////
/*
 * code_Verification_BOX.cpp
 *
 *    This program does code Verification convergence studies in the following context:
 *        - code verification based on MMS (Method of Manufactured Solutions)
 *      or MES (Method of Manufactured Solution) - see use_ExactSolution_flag variable
 *        - code verification done with ParallelFDTD library
 *              - NOTE: for each step, the receiver loop is assumed after the kernel
 *                      execution - see kernels3d.cu.
 *        - code verification done in C++, SINGLE precision.
*                       Can easily be extended to double precision
 *        - code verification is designed for a computational node/machine with
 *        at least one GPU device and CUDA 5.0
 *
 *    Code was initially ported from Python (author's repository).
 *
 *    The Manufactured Solution (MS) is inside a cube of side L_x with uniform
 * specific acoustic admittance Y_x ( see [R1] ).
 *
 *       The Exact Solution (ES) is inside a rigid cube of side L_x. Solution is
 * well-known for such a test case - see [R2 p.571]. The code could handle a
 * rectangular room but it was designed for a cubic room - some functions in
 * VV_utils need updating in case a rectangular room is used.
 *
 *        See logger.h for LogMX (www.logmx.com) parsers.
 *
 *  Dependencies: ParallelFDTD library, Voxelizer library, CUDA, boost, hdf5
 *      make commands:
 *          make test       // makes the verification program for single node
 *          make test_mpi   // makes the verification program for MPI simulations
 *          make clean      // cleans the built files
 *          make clean_sim  // cleans any simulated related files (e.g., log files, simulation results)
 *
 *  Main parameters:
 *      argv[1] - {true,false} string mentioning if the code verifies the exact solution
 *                  or the manufactured solution.
 *      argv[2] - {true,false} string mentioning if, for the manufactured solution (i.e.,
 *                argv[1]="false"), the interior update or full update including boundaries
 *                is tested. See [R1].
 *      argv[3] - unsigned int mentioning the number of GPU partitions used. E.g., if 4 CUDA
 *              devices are present, one can force using only 2.
 *
 * -----------------------------
 *
 *  References:
 *      [R1] ...
 *      [R2] Morse P. M. , Theoretical Acoustics (Princeton University Press) (1986)
 *
 */

// ParallelFDTD lib:
#include "App.h"
#include "kernels/voxelizationUtils.h"
#include "kernels/cudaMesh.h"
#include "kernels/kernels3d.h"

#include <stdlib.h>
#include <stdio.h>

#include "VV_utils.h"
#include <boost/asio/ip/host_name.hpp>


///////////////////////////////////////////////////////////////////////////////
/// \brief Function defines the grid spacings (in meters) that the convergence
/// study will contain. Just push additional grid spacing, but note that the
/// end_time_sample will end up in a different spot if the grid spacing is not
/// doubled/halved. Code not designed for this (interpolation neeeded).
///     Easier to pass the vector as arguments to main (TODO).
///
/// \param[out] dX_vector, A vector with the grid spacings in meters. Note
///             the code is run in this order for each Y_x.
///////////////////////////////////////////////////////////////////////////////
void set_dX_vector(std::vector<double>& dX_vector)
{
    /// Set grid spacing for convergence: [0.16, 0.08, 0.04, 0.02, 0.01]
    dX_vector.push_back(0.16);
    dX_vector.push_back(0.08);
    dX_vector.push_back(0.04);
    dX_vector.push_back(0.02); // Takes more than 15 mins...
    dX_vector.push_back(0.01);
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Function defines the specific acoustic admittances Y_x (a-dimensional,
/// see [Morse and Ingard, "Theoretical acoustics" (1968) p261]) for which
/// a convergence study will be ran. For each Y_x, a convergence study will be done
/// with grid spacings coming from set_dX_vector().
///     Note the exact solution is done inside a rigid box, so only Y_x = 0 is
/// valid, while for manufactures solution Y_x=0 is not valid.
///     Easier to pass the vector as arguments to main (TODO).
///
/// \param[in] use_ExactSolution_flag, Boolean which defines if an exact solution
///             or a manufactured solution is used.
/// \param[out] Y_x_vector, A vector with the grid spacings in meters. Note
///             the code is run in this order for each Y_x.
///////////////////////////////////////////////////////////////////////////////
void set_Y_x_vector(std::vector<double>& Y_x_vector,
                    bool use_ExactSolution_flag)
{
    if (use_ExactSolution_flag){
        Y_x_vector.push_back(0.0);
    } else{
        Y_x_vector.push_back(0.02);
        Y_x_vector.push_back(0.1);
        Y_x_vector.push_back(0.2);
        Y_x_vector.push_back(0.3);
        Y_x_vector.push_back(0.5);
        Y_x_vector.push_back(0.7);
        Y_x_vector.push_back(0.9);
        Y_x_vector.push_back(1.0);
    }
}

///////////////////////////////////////////////////////////////////////////////
/// \brief Function defines the Courant number/mesh ratio (a-dimensional) for
/// which the converge studies are done. For each lambda_sim returned by this
/// function, all specific acoustic impedances are swept ( coming from
/// set_Y_x_vector() ) and then for each (lambda_sim,Y_x) pair a convergence
/// study is done with grid spacings dX from set_dX_vector().
///
///     Here, a value of 0.5 is used (to avoid truncation error cancellation for
/// n_xyz = 1.
///     Easier to pass the vector as arguments to main (TODO).
///
/// \param[in] use_ExactSolution_flag, Boolean which defines if an exact solution
///             or a manufactured solution is used. Feel free to use it.
/// \param[out] lambda_sim_vector, A vector with the Courant numbers . Note
///             the code is run in this order for each Y_x.
///////////////////////////////////////////////////////////////////////////////
void set_lambda_sim_vector(std::vector<double>& lambda_sim_vector,
                                    bool use_ExactSolution_flag)
{
    lambda_sim_vector.push_back(0.5);
    // lambda_sim_vector.push_back( 1.0/sqrt(3) );
    /*
    if (use_ExactSolution_flag){
        lambda_sim_vector.push_back( 1.0/sqrt(3) );
    } else {
        lambda_sim_vector.push_back(0.5);
    }
    */
}

// The functions needed for launch_ParallelFDTD code:
extern "C"
{
  bool interruptCallback_(){
    return false;
  }
}
extern "C"
{
  void progressCallback_(int step, int max_step, float t_per_step ){
    log_msg<LOG_FINE>(L"          FDTD progress: %d/%d, time per step = %f")
        %step %max_step %t_per_step;
    return;
  }
}

int main(int argc, char **argv)
{
    ///////// Init app class (this is first for the logger):
    FDTD::App app;
    app.initializeDevices();

    log_msg<LOG_INFO>(L"Code Verification (BOX) - start.");

    ///////////////////////////////////////////////////////////////////////////
    // Manufactured (use_ExactSolution_flag=false) or exact solution
    //    (use_ExactSolution_flag = true):
    bool use_ExactSolution_flag;
    bool test_only_interior_flag; // if use_ExactSolution_flag = false
    // Second one is to re-initialize it:
    unsigned int GPU_partitions = UINT_MAX, GPU_partitions_input = UINT_MAX;

    // Read file name from calling script:
    if (argc < 4)
    {
        log_msg<LOG_ERROR>(L"FDTD_verif - Calling code_Verification program "
                "without any argument! Please, the first argument should be "
                "use_ExactSolution_flag, second test_only_interior_flag and"
                "third the #GPU partitions!");
        exit(EXIT_FAILURE);
    }
    if (argc > 4)
    {
        log_msg<LOG_ERROR>(L"FDTD_verif - Calling code_Verification program "
                "with more than one argument! Program designed for two inputs: "
            "use_ExactSolution_flag and second test_only_interior_flag!");
        exit(EXIT_FAILURE);
    }
    std::string use_ExactSolution_flag_string(argv[1]);
    std::string test_only_interior_flag_string(argv[2]);
    std::string GPU_partitions_string(argv[3]);
    log_msg<LOG_DEBUG>(L"Read the following arguments: %s")
        %(use_ExactSolution_flag_string + " ; " +
        test_only_interior_flag_string+" ; "+GPU_partitions_string).c_str();
    // Use exact solution:
    if(use_ExactSolution_flag_string == "true"){
        use_ExactSolution_flag = true;
    } else if(use_ExactSolution_flag_string == "false"){
        use_ExactSolution_flag = false;
    }else{
        log_msg<LOG_ERROR>(L"FDTD_verif - use_ExactSolution_flag must be either "
                "'true' or 'false' (case insensitive)!");
        exit(EXIT_FAILURE);
    }
    // Use interior:
    if(test_only_interior_flag_string == "true"){
        test_only_interior_flag = true;
    } else if(test_only_interior_flag_string == "false"){
        test_only_interior_flag = false;
    }else{
        log_msg<LOG_ERROR>(L"FDTD_verif - test_only_interior_flag must be either "
                "'true' or 'false' (case insensitive)!");
        exit(EXIT_FAILURE);
    }
    // #GPU partitions:
    GPU_partitions = strtoul(argv[3], NULL, 0);
    GPU_partitions_input = GPU_partitions;
    ///////////////////////////////////////////////////////////////////////////
    log_msg<LOG_INFO>(L"Reading scene data...");
    ////// Reading hdf5:
    char python_data_file[] = "BOX_1_28m_RIGID_python.hdf5";
    int num_triangles = 0, num_vertices = 0, num_materials = 0,
            no_of_used_materials = 0, no_of_unique_materials = 0;
    long total_material_coeff_size_ = 0;
    double* material_coefficients_double = 0;
    float* vertices_ = 0;
    unsigned int* triangles_ = 0;
    unsigned char* materials_ = 0;
    char* mesh_file = new char[1024];

    read_HDF5_Scene_Data(python_data_file,
                // Non-array data:
                &num_triangles, &num_vertices, &num_materials,
                &no_of_used_materials, &no_of_unique_materials,
                &total_material_coeff_size_,
                mesh_file,
                // arrays:
                material_coefficients_double,
                vertices_,
                triangles_,
                materials_
                );
    // Make material coefficients as floats:
    float* material_coefficients_single = new float[total_material_coeff_size_];
    for (unsigned int i; i< total_material_coeff_size_; i++){
        material_coefficients_single[i] = material_coefficients_double[i];
    }
    delete[] material_coefficients_double;

    /////////////////////////////////////////////////////////
    double lambda_sim = -0.5; // Courant number
    // Total simulation time [s]; 0.006474796 OR 0.0066
    double t_sim = 0.0066;
    double c_sound = 0.0;
    char voxelization_type = 2; // Surface conservative
    int N_x_CUDA = 32, N_y_CUDA = 4, N_z_CUDA = 1;
    uint3 block_size__ = {N_x_CUDA, N_y_CUDA, N_z_CUDA};
    /////////////// Solution config:
    double L_xyz = 1.28; // [m]
    int n_xyz = 1;
    double P0 = 1.0;

    std::vector<double> dX_vector;
    set_dX_vector(dX_vector);
    std::vector<double> Y_x_vector; // Admittance vector - only for MS:
    set_Y_x_vector(Y_x_vector, use_ExactSolution_flag);
    std::vector<unsigned int> GPU_partitions_vector;

    std::string temp_string = "dX_vector = {";
    char number_[32];
    for (std::vector<double>::const_iterator i = dX_vector.begin();
            i != dX_vector.end(); ++i)
    {
        sprintf(number_, "%f,", *i);
        temp_string = temp_string + std::string(number_) + ",";
    }
    temp_string = temp_string + "} \n Y_x_vector = {";
    for (std::vector<double>::const_iterator i = Y_x_vector.begin();
                i != Y_x_vector.end(); ++i){
        sprintf(number_, "%f,", *i);
        temp_string = temp_string + std::string(number_) + ",";
    }
    temp_string = temp_string + "}";

    // Logging:
    log_msg<LOG_CONFIG>(L" %s\n t_sim = %f\n n_xyz = %d\n c_sound = %f\n"
        " GPU_partitions = %u\n N_x_CUDA = %d\n N_y_CUDA = %d\n N_z_CUDA = %d\n"
        " use_ExactSolution_flag = %s\n test_only_interior_flag = %s")
        %temp_string.c_str() %t_sim %n_xyz %c_sound %GPU_partitions
        %N_x_CUDA %N_y_CUDA %N_z_CUDA %get_bool_string(use_ExactSolution_flag).c_str()
        %get_bool_string(test_only_interior_flag).c_str();

    std::vector<double> c_sound_vector;
    std::vector<double> lambda_sim_vector;
    std::vector<double> L2_norm_vector;
    std::vector<unsigned int> total_steps_vector;

    double dX = 0.0;
    double dT = 0.0;
    unsigned int fs = 0;
    double Y_xyz = Y_x_vector[0];
    // Include the initial conditions in the total steps
    unsigned int total_steps = 0;
    unsigned int N_t = 0;
    unsigned int CUDA_steps = 0;

    set_lambda_sim_vector(lambda_sim_vector, use_ExactSolution_flag);

////////////////////////////////////////////////////////////////////////////////
///     Outer most loop:
for(int lambda_index = 0; lambda_index < lambda_sim_vector.size();
                                                            lambda_index ++ )
{
    lambda_sim = lambda_sim_vector[lambda_index];
    log_msg<LOG_CONFIG>(L" Working at lambda_sim = %f ") %lambda_sim;
    // Get the correct simulation times:
    set_convergence_parameters(
                                t_sim,
                                c_sound,
                                dX_vector,
                                lambda_sim
                              );
    c_sound_vector.push_back(c_sound);

    if (lambda_sim <= 0.0 || lambda_sim > 1.0/sqrt(3)){
        log_msg<LOG_ERROR>(L"lambda_sim (%f) is incorrect! ") %lambda_sim;
        exit(EXIT_FAILURE);
    }
////////////////////////////////////////////////////////////////////////////////
///     Y_x loop:
for(int Y_x_index = 0;Y_x_index < Y_x_vector.size(); Y_x_index ++ )
{
    Y_xyz = Y_x_vector[Y_x_index];

    if (use_ExactSolution_flag && Y_xyz != 0.0){
        log_msg<LOG_ERROR>(L"The exact solution only works with rigid wall "
                "impedance! You are using %f") %Y_xyz;
        exit(EXIT_FAILURE);
    }
    log_msg<LOG_INFO>(L"Y_x = %f. Starting dX_vector loop...") %Y_xyz;
////////////////////////////////////////////////////////////////////////////////
/// dX loop...
for(int dX_index = 0; dX_index< dX_vector.size(); dX_index ++)
{
    // Re-init GPU_partitions:
    GPU_partitions = GPU_partitions_input;
    // From Python: set up conditions for the simulation
    dX = dX_vector.at(dX_index);
    dT = dX*lambda_sim/c_sound;
    fs = c_sound / (dX*lambda_sim);
    if ( ((mesh_size_t)( round(t_sim / dT)) + 1) > (mesh_size_t)UINT_MAX ){
        log_msg<LOG_ERROR>(L"total_steps overflow for specified type (UINT)! "
            "Either the simulation time [%E s] is too large or dT [%E s] is too small!")
            %t_sim %dT;
        exit(EXIT_FAILURE);
    }
    total_steps = (unsigned int)( round(t_sim / dT)) + 1;
    N_t = total_steps - 2;
    CUDA_steps = N_t;
    total_steps_vector.push_back(total_steps);

    // Check overflow:
    if ( (L_xyz/dX) >= (double)INT_MAX){
        log_msg<LOG_ERROR>(L"L_xyz/dX is too large for assumed type (U_INT)! "
            "Either the box size [%f] is too large or dX [%f] is too small!")
            %L_xyz %dX;
        exit(EXIT_FAILURE);
    }
    int Nx = (int)(L_xyz/dX), Ny = (int)(L_xyz/dX), Nz = (int)(L_xyz/dX);

    log_msg<LOG_CONFIG>(L"V&V MPI START dX = %E, dT = %E\n calculated fs = %f, "
      "final fs = %d Y_xyz = %E, P0 = %E, n_xyz = %d, c_sound = %f, L_xyz = %f,\n"
        "total_steps = %u, N_t = %d, CUDA_steps = %u\n [Nx,Ny,Nz] = [%d,%d,%d]")
      %dX %dT %(c_sound / (dX*lambda_sim)) %fs %Y_xyz %P0 %n_xyz %c_sound %L_xyz
      %total_steps %N_t %CUDA_steps %Nx %Ny %Nz;

    log_msg<LOG_DEBUG>(L"dX = %f --> Voxelizing mesh") %dX;
    CudaMesh* the_mesh = new CudaMesh();
    /////////////////////////////////////////////// Get FULL voxelization dims:
    uint3 mesh_voxelization_dim_ = get_Geometry_surface_Voxelization_dims(
                            vertices_, triangles_, num_triangles,
                            num_vertices, dX);
    log_msg<LOG_DEBUG>(L"Full voxelization dimensions = [%u,%u,%u]")
        %mesh_voxelization_dim_.x %mesh_voxelization_dim_.y
            %mesh_voxelization_dim_.z;
    unsigned char* MPI_h_postition_idx_ = (unsigned char*)NULL;
    unsigned char* MPI_h_materials_idx_ = (unsigned char*)NULL;
    uint3 MPI_voxelization_dim_ = make_uint3(0,0,0);
    unsigned char voxelization_result;
    switch(voxelization_type){
        case 2:
            voxelization_result = voxelizeGeometrySurfToHost(
                                 vertices_, triangles_, materials_, num_triangles,
                                 num_vertices, no_of_used_materials, dX,
                                 &MPI_h_postition_idx_, &MPI_h_materials_idx_,
                                 &MPI_voxelization_dim_,
                                 1, //Add a layer of air cells around
                                 VOX_CONSERVATIVE,
                                 make_uint3(32, 4, 1), // voxelize_block_size
                                 -1, //MPI_partition_indexing_.at(MPI_rank).at(0),
                                 -1  //MPI_partition_indexing_.at(MPI_rank).back()
                                 );
            log_msg<LOG_DEBUG>(L"V&V - Partial CONSERVATIVE voxelization "
              "done... Dimensions of [%d,%d,%d]. Vox_res = %d")
                  %(int)MPI_voxelization_dim_.x %(int)MPI_voxelization_dim_.y
                    %(int)MPI_voxelization_dim_.z %(int)voxelization_result;
            break;
        default:
            //Remove support for solid now-partial voxelization is more tedious
            log_msg<LOG_ERROR>(L"V&V FDTD_MPI - only surface conservative  "
                    "voxelization supported a.t.m.!");
            exit(EXIT_FAILURE);
    }
    // Voxelization checks:
    if (voxelization_result != 0){
        log_msg<LOG_ERROR>(L"V&V FDTD_verif - Voxelization process with errors "
            "(voxelization_result = %d)! Additional checks should be done "
        "here - see Python voxelization_helper.deal_with_voxelization_errors()")
                    %voxelization_result;
        exit(EXIT_FAILURE);
    }
    // Check that number of GPU partitions:
    check_GPU_partitions(GPU_partitions , (mesh_size_t)MPI_voxelization_dim_.z);
    // Since GPU_partitions depends only on dX, append only the first len(dX_vector):
    if (GPU_partitions_vector.size() < dX_vector.size()){
        GPU_partitions_vector.push_back(GPU_partitions);
    }
    // Set the admittance for the walls:
    // material_specific_admittances =  {1.0, 0.0, Y_x}
    //  For 'air', 'default material', 'walls_of_box'
    // First two are set from the BOX_1_28m_RIGID_python.hdf5 -> set the Y_x:
    material_coefficients_single[2*20] = Y_xyz;
    if (use_ExactSolution_flag)
        log_msg<LOG_INFO>(L"dX = %E ==================== Starting EXACT "
                "solution, Y_xyz = %E ==================== ") %dX %Y_xyz;
    else
        log_msg<LOG_INFO>(L"dX = %E ==================== Starting MANUFACTURED "
                "solution, Y_xyz = %E ==================== ") %dX %Y_xyz;

    log_msg<LOG_INFO>(L"dX = %E  Checking voxelization consistency and inner "
            "air volume relevant data...") %dX;
    mesh_size_t mem_size = (mesh_size_t)mesh_voxelization_dim_.x *
        (mesh_size_t)mesh_voxelization_dim_.y*(mesh_size_t)mesh_voxelization_dim_.z;

    unsigned int start_voxel_idx, size_x, size_y, size_z;
    check_voxelization_consistency_MMS(
                        MPI_h_postition_idx_, // On host
                        mesh_voxelization_dim_,
                        dX,
                        L_xyz,
                        start_voxel_idx,
                        size_x,
                        size_y,
                        size_z
                        );
    log_msg<LOG_INFO>(L"dX = %E ->  Done check. start_voxel_idx = %u, "
        "[size_x,size_y,size_z] = [%u,%u,%u]\nSetting up sources, cuda mesh, "
        "then initial conditions...") %dX %start_voxel_idx %size_x %size_y
                %size_z;
    SimulationParameters* parameter_ptr__ = new SimulationParameters();
    double phi_xyz = M_PI / 4.0;
    std::vector<unsigned int> used_dev_list;

    if (use_ExactSolution_flag){
        set_up_Parameters_MES_single(
                            parameter_ptr__,
                            mesh_voxelization_dim_,
                            MPI_h_postition_idx_, // On host
                            fs,
                            lambda_sim,
                            c_sound,
                            dX,
                            dT,
                            total_steps,
                            CUDA_steps, // should be total_steps - 2
                            L_xyz
                            );
        log_msg<LOG_INFO>(L"dX = %E ->  MES sources done. Set the cuda_mesh:")
                %dX;
        the_mesh->setDouble(false);
        the_mesh->setupMesh<float>(no_of_unique_materials,
                                   material_coefficients_single,
                                   parameter_ptr__->getParameterPtr(),
                                   mesh_voxelization_dim_,
                                   block_size__,
                                   0);//  0: forward-difference, 1: centered
        log_msg<LOG_INFO>(L"dX = %E ->  Cuda mesh done. Setting up initial "
            "conditions MES:\n=======================================") %dX;
        used_dev_list = set_up_Initial_conditions_MES_single(
                the_mesh,
                MPI_h_postition_idx_,
                MPI_h_materials_idx_,
                mesh_voxelization_dim_,
                GPU_partitions,
                start_voxel_idx,
                size_x,
                size_y,
                size_z,
                c_sound,
                dX,
                dT,
                P0,
                L_xyz,
                n_xyz);
    } else {
        set_up_Parameters_MMS_single(
                                    parameter_ptr__,
                                    mesh_voxelization_dim_,
                                    MPI_h_postition_idx_, // On host
                                    fs,
                                    lambda_sim,
                                    c_sound,
                                    dX,
                                    dT,
                                    total_steps,
                                    CUDA_steps, // should be total_steps - 2
                                    P0,
                                    Y_xyz,
                                    L_xyz,
                                    n_xyz,
                                    phi_xyz,
                                    test_only_interior_flag
                                    );
        log_msg<LOG_INFO>(L"dX = %E ->  MMS sources done. Set the "
                "cuda_mesh:") %dX;
        the_mesh->setDouble(false);
        the_mesh->setupMesh<float>(no_of_unique_materials,
                                   material_coefficients_single,
                                   parameter_ptr__->getParameterPtr(),
                                   mesh_voxelization_dim_,
                                   block_size__,
                                   0);//  0: forward-difference, 1: centered
        log_msg<LOG_INFO>(L"dX = %E ->  Cuda mesh done. Setting up initial "
            "conditions MMS:\n=======================================") %dX;
        used_dev_list = set_up_Initial_conditions_MMS_single(
                the_mesh,
                MPI_h_postition_idx_,
                MPI_h_materials_idx_,
                mesh_voxelization_dim_,
                GPU_partitions,
                start_voxel_idx,
                size_x,
                size_y,
                size_z,
                c_sound,
                dX,
                dT,
                P0,
                Y_xyz,
                L_xyz,
                n_xyz,
                phi_xyz);
    }
    log_msg<LOG_INFO>(L"dX = %E ->   Sources done. Launching FDTD3d..."
            "\n=======================================") %dX;
    float * return_values = new float[(mesh_size_t)parameter_ptr__->getNumSteps() *
                                      (mesh_size_t)parameter_ptr__->getNumReceivers()];
////////////////////////////////////////////////////////////////////////////////
////////////////////////      FDTD UPDATE     //////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
    float time = launchFDTD3d(the_mesh,
                        parameter_ptr__,
                        return_values,
                        interruptCallback_,
                        progressCallback_
                        );
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
//////////////////////      FDTD UPDATE  end    ////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
    log_msg<LOG_INFO>(L"dX = %E -> FDTD3d DONE. Retrieving the final calculated mesh...."
            "\n---------------------------------------------") %dX;
    float* final_pressure_field_h = new float[mem_size];
    get_full_final_pressureToHost(the_mesh, final_pressure_field_h);
    log_msg<LOG_INFO>(L"Last pressure value retrieved.");
    // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
    //log_msg<LOG_TRACE>(L" BEFORE FORCING: max( |final_pressure| ) = %f")
    //        %get_abs_max_value(final_pressure_field_h, mem_size);
    // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
    log_msg<LOG_TRACE>(L"CUDA_STEPS = %u, total_steps = %u\nmax(final_p_field) = %f")
            %CUDA_steps %total_steps %get_abs_max_value(final_pressure_field_h, mem_size);
    if(not use_ExactSolution_flag)
    {
        log_msg<LOG_INFO>(L"dX = %E -> Applying last forcing term to MMS:") %dX;
        // Calculate the latest step contained in p_new. Here is an example to
        //get the formula for CUDA_steps =3:
        // t_idx:                 /------ 0 ------\  /------- 1 -----\
        // Cuda steps:             0  ->  1 -> 2  -> 0  -> 1  -> 2  -> 0
        // Time index:   0*dT  1*dT  3*dT  4*dT  5*dT  6*dT  7*dT  8*dT
        unsigned int step_p_new = (0+1)*CUDA_steps + 1;
        double t_p_new = 1.0*( step_p_new )*dT;
        double t_forcing = 1.0*( step_p_new - 1 )*dT;
        // Apply forcing term to the last pressure value:
        mesh_size_t size_XY = (mesh_size_t)mesh_voxelization_dim_.x*
                (mesh_size_t)mesh_voxelization_dim_.y;
        for(unsigned int idx_z =start_voxel_idx;
                idx_z < start_voxel_idx+ size_z; idx_z++)
            for(unsigned int idx_y =start_voxel_idx;
                    idx_y < start_voxel_idx+ size_y; idx_y++)
                for(unsigned int idx_x =start_voxel_idx;
                        idx_x < start_voxel_idx + size_x; idx_x++)
                {
                    // Apply forcing to the inner field:
                    final_pressure_field_h[ (mesh_size_t)idx_z*size_XY +
                                (mesh_size_t)idx_y*mesh_voxelization_dim_.x +
                                (mesh_size_t)idx_x]
                        += get_Forcing_BOX_ALL_Walls_abs_Point(
                            MPI_h_postition_idx_,
                            mesh_voxelization_dim_.x,
                            mesh_voxelization_dim_.y,
                            mesh_voxelization_dim_.z,
                            lambda_sim,
                            idx_x - start_voxel_idx,        // N_x
                            idx_y - start_voxel_idx,        // N_y
                            idx_z - start_voxel_idx,        // N_z
                            idx_z,
                            start_voxel_idx,
                            dX,
                            dT,
                            t_forcing,
                            P0,
                            Y_xyz,
                            n_xyz,
                            L_xyz,
                            phi_xyz,
                            c_sound);
                }
        // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
        //log_msg<LOG_TRACE>(L" AFTER FORCING: max( |final_pressure| ) = %f")
        //        %get_abs_max_value(final_pressure_field_h, mem_size);
        // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG

        if (test_only_interior_flag)
        {
            log_msg<LOG_INFO>(L"dX = %E -> Setting the outer values from "
                                    "manufactured solution:") %dX;
            // Setting the outer field:
            for(unsigned int idx_x = 0; idx_x< Nx; idx_x ++)
                for(unsigned int idx_y = 0; idx_y< Ny; idx_y ++)
                    for(unsigned int idx_z = 0; idx_z< Nz; idx_z ++)
                    {
                        // The outer hard sources (index 0 or Nx/Ny/Nz)
                        if (
                            (idx_x*idx_y*idx_z == 0)
                            ||
                            ((idx_x-Nx+1)*(idx_y-Ny+1)*(idx_z-Nz+1) == 0)
                            )
                        {
                            final_pressure_field_h[
                                ((mesh_size_t)idx_z+start_voxel_idx)*size_XY +
                                ((mesh_size_t)idx_y+start_voxel_idx)*mesh_voxelization_dim_.x +
                                ((mesh_size_t)idx_x+start_voxel_idx)] =
                              get_Manufactured_solution_BOX_ALL_Walls_abs_Point(
                              idx_x,        // N_x
                              idx_y,        // N_y
                              idx_z,        // N_z
                              dX,
                              P0,
                              t_sim,        // t
                              Y_xyz, n_xyz, L_xyz, phi_xyz, c_sound);
                        }
                    }
            // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
            //log_msg<LOG_TRACE>(L" AFTER FORCING and HS: max( |final_pressure| ) = %f")
            //        %get_abs_max_value(final_pressure_field_h, mem_size);
            // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
        }
    }
    free(MPI_h_postition_idx_);
    free(MPI_h_materials_idx_);
    log_msg<LOG_INFO>(L"Last sample forcing done. Calculating analytical matrix:");
    float* final_continuous_solution = new float[(mesh_size_t)size_x*
                                                 (mesh_size_t)size_y*
                                                 (mesh_size_t)size_z];
    if (use_ExactSolution_flag){
        get_Exact_solution_BOX_Rigid(
                final_continuous_solution,
                size_x,        // N_x
                size_y,        // N_y
                size_z,        // N_z
                dX,
                t_sim,        // t
                P0,
                n_xyz,    // n_x
                n_xyz,    // n_y
                n_xyz,    // n_z
                L_xyz,    // L_x
                L_xyz,    // L_y
                L_xyz,    // L_z
                c_sound);
        log_msg<LOG_TRACE>(L"max(P_ES) = %E at t = %f [us]")
        %get_abs_max_value(final_continuous_solution, (mesh_size_t)size_x*size_y*size_z)
        %(t_sim*1E6);
    }else{
        get_Manufactured_solution_BOX_ALL_Walls_abs(
                final_continuous_solution,
                size_x,    // N_x
                dX,
                t_sim,    // t
                P0,
                Y_xyz,
                n_xyz,
                L_xyz,
                phi_xyz,
                c_sound
                );
        log_msg<LOG_TRACE>(L"max(P_MS) = %E at t = %f [us]")
            %get_abs_max_value(final_continuous_solution, (mesh_size_t)size_x*size_y*size_z)
            %(t_sim*1E6);
    }

    // Now get the L2 norm:
    log_msg<LOG_INFO>(L"Last sample forcing done. Calculating L2 norm:");
    double L2_norm = get_L2_norm_single(
            final_pressure_field_h,
            mesh_voxelization_dim_,
            start_voxel_idx,
            size_x,
            size_y,
            size_z,
            final_continuous_solution,
            dX);
    log_msg<LOG_CONFIG>(L"Got L2 error for dX = %E --> %E") %dX %L2_norm;
    L2_norm_vector.push_back(L2_norm);

    // Local (wrt dX) clean-up:
    delete[] final_pressure_field_h;
    delete[] return_values;
    delete parameter_ptr__;
    delete the_mesh;
} // END dX_vector loop
} // END Y_x_vector loop
} // END lambda_vector loop
    //char* number_ = new char[32];
    temp_string = "L2_norm_vector = {";
    for (std::vector<double>::const_iterator i = L2_norm_vector.begin();
            i != L2_norm_vector.end(); ++i)
    {
        sprintf(number_, "%E", *i);
        temp_string = temp_string + number_ + ",";
    }
    temp_string = temp_string + "}\ndX_vector = {";
    for (std::vector<double>::const_iterator i = dX_vector.begin();
                i != dX_vector.end(); ++i)
    {
        sprintf(number_, "%f", *i);
        temp_string = temp_string + std::string(number_) + ",";
    }
    temp_string = temp_string + "} \n";

    log_msg<LOG_CONFIG>(L" %s\n ******************************************"
            "***************") %temp_string.c_str();
    ///////////////////////////////////////////////////////////////////////////
    // Writing L2 norm:
    ///////////////////////////////////////////////////////////////////////////
    write_L2_norm(  dX_vector,
                    lambda_sim_vector,
                    c_sound_vector,
                    Y_x_vector,
                    L2_norm_vector,
                    total_steps_vector,
                    GPU_partitions_vector,
                    t_sim,
                    n_xyz,
                    GPU_partitions_input,
                    mesh_file,
                    (boost::asio::ip::host_name() + "_code_V_" +
                    std::string(use_ExactSolution_flag ? "MES" : "MMS")).c_str(),
                    voxelization_type,
                    (int)use_ExactSolution_flag,
                    (int)test_only_interior_flag,
                    -1, // MPI_size
                    -1 // MPI_rank
                );
    log_msg<LOG_DEBUG>(L" V&V - Dumping hdf5 data complete");

    // Clean-up:
    //delete[] python_data_file; // Seg. fault with the new constant char...
    delete[] material_coefficients_single;
    delete[] vertices_;
    delete[] triangles_;
    delete[] materials_;
    delete[] mesh_file;
    log_msg<LOG_INFO>(L" ------======= Code Verification (BOX) - end OK. ======"
            "------");
    app.close();
    return(0);
}

