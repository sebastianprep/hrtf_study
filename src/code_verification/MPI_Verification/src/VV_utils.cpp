///////////////////////////////////////////////////////////////////////////////
//
// This file is released under the MIT License. See
// http://www.opensource.org/licenses/mit-license.php
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//  Author: Sebastian Prepelita, Aalto University
//  Created on: May 30, 2016
//      - updates to mesh_size_t & full HOST voxelization @2017
//
///////////////////////////////////////////////////////////////////////////////
/*
 * VV_utils.cpp
 *
 *    File  with helper function to set-up a simulation for a code verification
 *    study.
 *
 */

#include "VV_utils.h"
#include <algorithm>

// hdf5 stuff:
#include "hdf5.h"
#include "hdf5_hl.h"
#include "../MPI/MPI_Utils.h"

void check_voxelization_consistency_MMS(
                    const unsigned char* h_position_ptr__, // On host
                    uint3 voxelization_dim,
                    double dX,
                    double L_xyz,
                    unsigned int &OUT_start_voxel_idx,
                    unsigned int &OUT_size_x,
                    unsigned int &OUT_size_y,
                    unsigned int &OUT_size_z
                    ){
    // Calculate theoretical number of voxels:
    if ( (L_xyz/dX) >= (double)UINT_MAX || L_xyz<0 || dX<0){
        log_msg<LOG_ERROR>(L"unsigned int overflow - L_xyz = %f, dX = %f"
                "[VV_tuils.cpp - check_voxelization_consistency_MMS()] @line 34."
                ) %L_xyz %dX;
        exit(EXIT_FAILURE);
    }
    unsigned int Nx_theory = (unsigned int)(L_xyz/dX);
    unsigned int Ny_theory = (unsigned int)(L_xyz/dX);
    unsigned int Nz_theory = (unsigned int)(L_xyz/dX);

    if (voxelization_dim.x < Nx_theory){ // Only < since there is zero padding..
        log_msg<LOG_ERROR>(L"The voxelized space is too small in X direction. "
        "Wrong mesh file? [VV_uils.cpp - check_voxelization_consistency_MMS()]");
        exit(EXIT_FAILURE);
    }
    if (voxelization_dim.y < Ny_theory){
        log_msg<LOG_ERROR>(L"The voxelized space is too small in Y direction. "
        "Wrong mesh file? [VV_uils.cpp - check_voxelization_consistency_MMS()]");
        exit(EXIT_FAILURE);
    }
    if (voxelization_dim.z < Nz_theory){
        log_msg<LOG_ERROR>(L"The voxelized space is too small in Z direction. "
        "Wrong mesh file? [VV_uils.cpp - check_voxelization_consistency_MMS()]");
        exit(EXIT_FAILURE);
    }
    // Find first air voxel:
    // Better to start from the middle (there are going to be some airgaps at lower voxels):
    mesh_size_t start_voxel_idx =
            (mesh_size_t)( (voxelization_dim.z -2 )/2.0); //The 2 is due to an
                           //air layer around the voxelized geometry by default.
    mesh_size_t size_XY = (mesh_size_t)voxelization_dim.x *(mesh_size_t)voxelization_dim.y;
    while ( h_position_ptr__[start_voxel_idx*size_XY + start_voxel_idx*
                             voxelization_dim.x + start_voxel_idx] !=0 ){
        if(start_voxel_idx == 0){// Avoid a seg_fault here...
          log_msg<LOG_ERROR>(L"The minimal voxel was reached ([z,y,z]=[0,0,0])"
                  " without finding a boundary! Something is wrong (bdries "
                  "likely missing from voxelization)...");
          exit(EXIT_FAILURE);
        }
        start_voxel_idx -= 1;
    }
    start_voxel_idx += 1; // Goes one more..
    // Now check that this is a corner:
    if ((h_position_ptr__[(start_voxel_idx-1)*size_XY + start_voxel_idx*
                             voxelization_dim.x + start_voxel_idx] != 0) ||
        (h_position_ptr__[start_voxel_idx*size_XY + (start_voxel_idx-1)*
                             voxelization_dim.x + start_voxel_idx] != 0) ||
        (h_position_ptr__[start_voxel_idx*size_XY + start_voxel_idx*
                             voxelization_dim.x + (start_voxel_idx-1)]) != 0
        ){
        int v1=(int)h_position_ptr__[(start_voxel_idx-1)*size_XY +
                      start_voxel_idx*voxelization_dim.x + start_voxel_idx];
        int v2=(int)h_position_ptr__[start_voxel_idx*size_XY +
                 (start_voxel_idx-1)*voxelization_dim.x + start_voxel_idx];
        int v3=(int)h_position_ptr__[start_voxel_idx*size_XY +
                 start_voxel_idx*voxelization_dim.x + (start_voxel_idx-1)];
        log_msg<LOG_ERROR>(L"The starting voxel is not a corner! Neighbors"
            " are [%d,%d,%d].[VV_uils.cpp - "
            "check_voxelization_consistency_MMS()]") %(v1) %(v2) %(v3);
        exit(EXIT_FAILURE);
    }
    // Also check value:
    if (h_position_ptr__[start_voxel_idx*size_XY + start_voxel_idx*
                         voxelization_dim.x + start_voxel_idx] != (134-3)){
        log_msg<LOG_ERROR>(L"The starting voxel does not have a corner value! "
         "pos[__] is %d. [VV_uils.cpp - check_voxelization_consistency_MMS()]")
           %((int)h_position_ptr__[start_voxel_idx*size_XY + start_voxel_idx*
                         voxelization_dim.x + start_voxel_idx]);
        exit(EXIT_FAILURE);
    }
    // Now go in all directions and check until the first solid voxel is found:
    // X dir:
    mesh_size_t temp_idx = start_voxel_idx;
    while (h_position_ptr__[start_voxel_idx*size_XY + start_voxel_idx*
                             voxelization_dim.x + temp_idx
                            ]!= 0){
        temp_idx += 1;
    }
    if ( (temp_idx - start_voxel_idx) > UINT_MAX){
        log_msg<LOG_ERROR>(L"unsigned int overflow - "
            "[VV_tuils.cpp - check_voxelization_consistency_MMS()] @line 91.");
        exit(EXIT_FAILURE);
    }
    OUT_size_x = (unsigned int)(temp_idx - start_voxel_idx);
    // Y dir:
    temp_idx = start_voxel_idx;
    while (h_position_ptr__[start_voxel_idx*size_XY + temp_idx*
                             voxelization_dim.x + start_voxel_idx]!= 0){
        temp_idx += 1;
    }
    if ( (temp_idx - start_voxel_idx) > UINT_MAX){
        log_msg<LOG_ERROR>(L"unsigned int overflow - "
            "[VV_tuils.cpp - check_voxelization_consistency_MMS()] @line 102.");
        exit(EXIT_FAILURE);
    }
    OUT_size_y = (unsigned int)(temp_idx - start_voxel_idx);
    // Z dir:
    temp_idx = start_voxel_idx;
    while (h_position_ptr__[temp_idx*size_XY + start_voxel_idx*
                             voxelization_dim.x + start_voxel_idx]!= 0){
        temp_idx += 1;
    }
    if ( (temp_idx - start_voxel_idx) > UINT_MAX){
        log_msg<LOG_ERROR>(L"unsigned int overflow - "
            "[VV_tuils.cpp - check_voxelization_consistency_MMS()] @line 115.");
        exit(EXIT_FAILURE);
    }
    OUT_size_z = (unsigned int)(temp_idx - start_voxel_idx);
    // Compare the size:
    if (OUT_size_x != Nx_theory){
        log_msg<LOG_ERROR>(L"The number of found air voxels (%u) is incorrect ("
            "theoretically it should be %u) in X direction."
            "[VV_tuils.cpp - check_voxelization_consistency_MMS()]")
                    %(OUT_size_x) %Nx_theory;
        exit(EXIT_FAILURE);
    }
    if (OUT_size_y != Ny_theory){
        log_msg<LOG_ERROR>(L"The number of found air voxels (%u) is incorrect ("
            "theoretically it should be %u) in Y direction."
            "[VV_tuils.cpp - check_voxelization_consistency_MMS()]")
                    %(OUT_size_y) %Ny_theory;
        exit(EXIT_FAILURE);
    }
    if (OUT_size_z != Nz_theory){
        log_msg<LOG_ERROR>(L"The number of found air voxels (%u) is incorrect ("
            "theoretically it should be %u) in Y direction."
            "[VV_tuils.cpp - check_voxelization_consistency_MMS()]")
                    %(OUT_size_z) %Nz_theory;
        exit(EXIT_FAILURE);
    }
    if ( start_voxel_idx > UINT_MAX){
        log_msg<LOG_ERROR>(L"unsigned int overflow - "
                "[VV_tuils.cpp - check_voxelization_consistency_MMS()] @line 143.");
        exit(EXIT_FAILURE);
    }
    OUT_start_voxel_idx = (unsigned int)start_voxel_idx;
}

void check_GPU_partitions(
        unsigned int &GPU_partitions,
        mesh_size_t dim_z,
        int MPI_rank
        )
{
    if(GPU_partitions == 0 || dim_z == 0){
        log_msg<LOG_ERROR>(L"[R%02d] check_GPU_partitions()@l161 - "
                "GPU_partitions (%u) or dim_z (%llu) is zero!")
                %MPI_rank %GPU_partitions %dim_z;;
        exit(EXIT_FAILURE);
    }
    long GPU_partitions_ret = GPU_partitions;
    while ( GPU_partitions_ret > 0 && (double)dim_z/(double)GPU_partitions_ret <= 2.0){
        GPU_partitions_ret = GPU_partitions_ret - 1;
    }
    if (GPU_partitions_ret < 1)
    {
        log_msg<LOG_ERROR>(L"[R%02d] check_GPU_partitions()@l171 - dim_z (%llu) is too "
            "small to contain at least one updatable z-slice! Although the "
            "code might get verified, it is considered a failed code verification "
            "exercise!") %MPI_rank  %dim_z;
        exit(EXIT_FAILURE);
    }
    if (GPU_partitions_ret != GPU_partitions){
        log_msg<LOG_CONFIG>(L"[R%02d] check_GPU_partitions() - GPU_partitions decremented "
            "from %u to %d! This is to account for small dim_z for the "
            "current host (%llu).") %MPI_rank  %GPU_partitions %GPU_partitions_ret  %dim_z;
        GPU_partitions = GPU_partitions_ret;
    } else {
        log_msg<LOG_DEBUG>(L"[R%02d] check_GPU_partitions() - GPU_partitions (%u) "
            "is OK  for given dim_z (%llu). No updated necessary.")
                %MPI_rank %GPU_partitions %dim_z;
    }
};

void set_up_Parameters_MMS_single(
                    SimulationParameters* parameter_ptr__,
                    uint3 voxelization_dim,
                    unsigned char* h_position_ptr__, // On host
                    unsigned int fs,
                    double lambda_,
                    double c_sound,
                    double dX,
                    double dT,
                    unsigned int total_steps,
                    unsigned int CUDA_steps, // should be total_steps - 2
                    double P0,
                    double Y_xyz,
                    double L_xyz,
                    int n_xyz,
                    double phi_xyz,
                    bool test_only_interior_flag
                    )
{
    parameter_ptr__->setSpatialFs(fs);
    parameter_ptr__->setLambda(lambda_);
    parameter_ptr__->setC(c_sound);
    parameter_ptr__->setNumSteps(CUDA_steps);
    parameter_ptr__->setOctave(0);

    parameter_ptr__->setAddPaddingToElementIdx(false);

    unsigned int start_voxel_idx=0, size_x=0, size_y=0, size_z=0;
    check_voxelization_consistency_MMS(h_position_ptr__, voxelization_dim,
            dX, L_xyz, start_voxel_idx, size_x, size_y, size_z);
    log_msg<LOG_DEBUG>(L"V&V Checks passed! The inside air box: "
            "\n\tstart_voxel_idx = %u \n\size_x = %u \n\size_y = %u"
            "\n\size_z = %u") %start_voxel_idx %size_x %size_y %size_z;
    unsigned int Nx = (unsigned int)(L_xyz/dX), Ny = (unsigned int)(L_xyz/dX),
            Nz = (unsigned int)(L_xyz/dX);
    // Some additional checks:
    if (Nx != size_x){
        log_msg<LOG_ERROR>(L"Nx (%u) is not the same as size_x (%u)."
          "[VV_tuils.cpp - set_up_Parameters_MMS_single()] @l191") %Nx %size_x;
        exit(EXIT_FAILURE);
    }
    if (Ny != size_y){
        log_msg<LOG_ERROR>(L"Ny (%u) is not the same as size_y (%u)."
          "[VV_tuils.cpp - set_up_Parameters_MMS_single()] @l196") %Ny %size_y;
        exit(EXIT_FAILURE);
    }
    if (Nz != size_z){
        log_msg<LOG_ERROR>(L"Nz (%u) is not the same as size_z (%u)."
          "[VV_tuils.cpp - set_up_Parameters_MMS_single()] @l203") %Nz %size_z;
        exit(EXIT_FAILURE);
    }
    ///////////////////////////////////////////////////////////////////////////
    // SOURCES:
    ///////////////////////////////////////////////////////////////////////////
    // In the CUDA mesh, the source is added from sample #1 (to p_current).Thus:
    //    -) the first computation is done on initial conditions (p_0 and p_1)
    // --> get p_2 (so first source term is 0)
    //    -) p_3 is not calculated based on p_2 and p_1. The forcing needs to be
    // applied to p_2 --> for this calculation forcing is != --> second force
    // term is != and equals the forcing at p_2 ( t=2*dT )
    //    -) from p_4 on, just apply forcing...
    //     Thus, source_term = [0.0, forcing(2*dT), forcing(3*dT), ... ]
    std::vector<float> source_data_vector(total_steps);
    memset(&source_data_vector[0], 0, sizeof(source_data_vector[0]) *
            source_data_vector.size());
    unsigned int total_sources = 0, total_SS = 0, total_HS = 0;
    if (test_only_interior_flag)
    {
        log_msg<LOG_DEBUG>(L"V&V : Testing only interior - HARD sources on the "
                "outer domain; SOFT sources inside the domain. [Nx,Ny,Nz] = "
                "[%u,%u,%u]") %Nx %Ny %Nz;
        // Add sources:
        for(unsigned int idx_x = 0; idx_x< Nx; idx_x ++)
            for(unsigned int idx_y = 0; idx_y< Ny; idx_y ++)
                for(unsigned int idx_z = 0; idx_z< Nz; idx_z ++)
                {
                    if (
                            (idx_x*idx_y*idx_z == 0)
                            ||
                            ((idx_x-Nx+1)*(idx_y-Ny+1)*(idx_z-Nz+1) == 0)
                        ){
                        // The outer hard sources (index 0 or Nx/Ny/Nz):
                        Source s( dX*( start_voxel_idx + idx_x), // x [m]
                                  dX*( start_voxel_idx + idx_y), // y [m]
                                  dX*( start_voxel_idx + idx_z), // z [m]
                                  SRC_HARD, // source_type
                                  DATA, // input type = DATA
                                  0 // group ?!
                                  );
                        s.setInputDataIdx(total_sources);
                        parameter_ptr__->addSource_no_logging( s );
/*
//log_msg<LOG_TRACE>( L"Source added [%d,%d,%d]. Getting MMS time vector at "
 * "[%d,%d,%d], from sample %d to sample %d (total_samples = %d). CUDA_steps ="
 * " %d. total_steps = %d" )
//        %(start_voxel_idx + idx_x) %(start_voxel_idx + idx_y)
 * %(start_voxel_idx + idx_z) %idx_x %idx_y %idx_z %1 %total_steps
 * %(total_steps-1+1) %CUDA_steps %total_steps;
// Get data for the HARD sources -> use MMS
 */
                        get_Manufactured_solution_BOX_ALL_Walls_abs_TimeVector(
                                &source_data_vector[0],
                                idx_x, idx_y, idx_z,
                                1,                     // t_start_sample
                                total_steps-1,         //t_end_sample
                                dX, dT, P0, Y_xyz, n_xyz, L_xyz, phi_xyz,
                                c_sound);
                        //log_msg<LOG_TRACE>( L"Done! Adding input data..." );
                        parameter_ptr__->addInputData( source_data_vector );
                        //log_msg<LOG_TRACE>( L"Input data added..." );
                        total_HS += 1;
                    }
                    else
                    {
                        // The interior soft sources:
                        Source s( dX*( start_voxel_idx + idx_x), // x [m]
                                  dX*( start_voxel_idx + idx_y), // y [m]
                                  dX*( start_voxel_idx + idx_z), // z [m]
                                  SRC_SOFT, // source_type
                                  DATA, // input type = DATA
                                  0 // group ?!
                                );
                        s.setInputDataIdx(total_sources);
                        parameter_ptr__->addSource_no_logging( s );
                        // Get data - SOFT sources --> forcing
                        get_Forcing_BOX_ALL_Walls_abs_TimeVector(
                                &source_data_vector[0],
                                h_position_ptr__,
                                voxelization_dim.x,
                                voxelization_dim.y,
                                voxelization_dim.z,
                                lambda_,
                                idx_x, idx_y, idx_z, start_voxel_idx + idx_z,
                                start_voxel_idx,
                                0,                         // t_start_sample
                                total_steps - 2,         // t_end_sample
                                dX, dT, P0, Y_xyz, n_xyz, L_xyz, phi_xyz,
                                c_sound);
                        //First is zero, then it starts from 1.
                        source_data_vector[0] = 0.0f;
                        parameter_ptr__->addInputData( source_data_vector );
                        total_SS += 1;
                    }
                    total_sources += 1;
                }

    } else
    {
        // TESTING BOUNDARY IMPLEMENTATION TOO!
        log_msg<LOG_DEBUG>(L"V&V : Testing all solution, including boundaries - "
                "only SOFT sources inside the domain. [Nx,Ny,Nz] = "
                "[%u,%u,%u]") %Nx %Ny %Nz;
        // Add sources:
        for(unsigned int idx_x = 0; idx_x< Nx; idx_x ++)
            for(unsigned int idx_y = 0; idx_y< Ny; idx_y ++)
                for(unsigned int idx_z = 0; idx_z< Nz; idx_z ++){
                    // Soft source:
                    Source s( dX*( start_voxel_idx + idx_x), // x [m]
                              dX*( start_voxel_idx + idx_y), // y [m]
                              dX*( start_voxel_idx + idx_z), // z [m]
                              SRC_SOFT, // source_type
                              DATA, // input type = DATA
                              0 // group ?!
                            );
                    s.setInputDataIdx(total_sources);
                    parameter_ptr__->addSource_no_logging( s );
                    // Get data - SOFT sources --> forcing
                    get_Forcing_BOX_ALL_Walls_abs_TimeVector(
                            &source_data_vector[0],
                            h_position_ptr__,
                            voxelization_dim.x,
                            voxelization_dim.y,
                            voxelization_dim.z,
                            lambda_,
                            idx_x, idx_y, idx_z, start_voxel_idx + idx_z,
                            start_voxel_idx,
                            0,                         // t_start_sample
                            total_steps - 2,         // t_end_sample
                            dX, dT, P0, Y_xyz, n_xyz, L_xyz, phi_xyz,
                            c_sound);
                    //First is zero, then it starts from 1.
                    source_data_vector[0] = 0.0f;
                    parameter_ptr__->addInputData( source_data_vector );
                    total_SS += 1;
                    total_sources += 1;
                }
    }
    ///////////////////////////////////////////////////////////////////////////
    // Receivers - no receivers:
    ///////////////////////////////////////////////////////////////////////////
    unsigned int total_receivers = 0;
    log_msg<LOG_INFO>(L"V&V MMS - added a total of %u sources (%u HARD and %u "
      "SOFT) #receivers = %u \n===================================")
        %parameter_ptr__->getNumSources() %total_HS %total_SS
            %parameter_ptr__->getNumReceivers();
}

void set_up_Parameters_MES_single(
                    SimulationParameters* parameter_ptr__,
                    uint3 voxelization_dim,
                    const unsigned char* h_position_ptr__, // On host
                    unsigned int fs,
                    double lambda_,
                    double c_sound,
                    double dX,
                    double dT,
                    unsigned int total_steps,
                    unsigned int CUDA_steps, // should be total_steps - 2
                    double L_xyz
                    ){
    parameter_ptr__->setSpatialFs(fs);
    parameter_ptr__->setLambda(lambda_);
    parameter_ptr__->setC(c_sound);
    parameter_ptr__->setNumSteps(CUDA_steps);
    parameter_ptr__->setOctave(0);

    parameter_ptr__->setAddPaddingToElementIdx(false);

    unsigned int start_voxel_idx=0, size_x=0, size_y=0, size_z=0;
    check_voxelization_consistency_MMS(h_position_ptr__, voxelization_dim,
            dX, L_xyz, start_voxel_idx, size_x, size_y, size_z);
    log_msg<LOG_DEBUG>(L"V&V Checks passed! The inside air box: "
            "\n\tstart_voxel_idx = %u \n\size_x = %u \n\size_y = %u"
            "\n\size_z = %u") %start_voxel_idx %size_x %size_y %size_z;
    unsigned int Nx = (unsigned int)(L_xyz/dX), Ny = (unsigned int)(L_xyz/dX),
            Nz = (unsigned int)(L_xyz/dX);
    // Some additional checks:
    if (Nx != size_x){
        log_msg<LOG_ERROR>(L"Nx (%u) is not the same as size_x (%u)."
          "[VV_tuils.cpp - set_up_Parameters_MMS_single()] @l388") %Nx %size_x;
        exit(EXIT_FAILURE);
    }
    if (Ny != size_y){
        log_msg<LOG_ERROR>(L"Ny (%u) is not the same as size_y (%u)."
          "[VV_tuils.cpp - set_up_Parameters_MMS_single()] @l395") %Ny %size_y;
        exit(EXIT_FAILURE);
    }
    if (Nz != size_z){
        log_msg<LOG_ERROR>(L"Nz (%u) is not the same as size_z (%u)."
          "[VV_tuils.cpp - set_up_Parameters_MMS_single()] @l398") %Nz %size_z;
        exit(EXIT_FAILURE);
    }
    ///////////////////////////////////////////////////////////////////////////
    // SOURCES - No sources
    ///////////////////////////////////////////////////////////////////////////
    unsigned int total_HS = 0, total_SS = 0, total_sources = 0;
    ///////////////////////////////////////////////////////////////////////////
    // Receivers - No receivers
    ///////////////////////////////////////////////////////////////////////////
    unsigned int total_receivers = 0;
    log_msg<LOG_INFO>(L"V&V MES - added a total of %u sources (%u HARD and %u "
      "SOFT) #receivers = %u \n===================================")
        %parameter_ptr__->getNumSources() %total_HS %total_SS
            %parameter_ptr__->getNumReceivers();
}


void set_convergence_parameters(
                    double &t_sim,
                    double &c_sound,
                    std::vector<double> dX_vector,
                    double lambda_sim,
                    const unsigned int C
                    ){
    log_msg<LOG_DEBUG>(L"V&V Setting convergence parameters for t_sim = %f, "
        " and lambda_sim = %f (C=%d)") %t_sim %lambda_sim %C;
    std::vector<double>::iterator result;
    result = std::max_element(dX_vector.begin(), dX_vector.end());// <algorithm>
    double dX_max = *result;
    // Fix c_sound for all simulations:
    c_sound = 1.0*lambda_sim*dX_max*C; // 340.48 ( for lambda = 0.5 )
    if (lambda_sim!= 0.5 && C == 4250){
        log_msg<LOG_WARNING>(L"V&V set_convergence_parameters() sets a c_sound "
            "designed for lambda = 0.5. Sound speed will be out-of-the ordinary"
            " - just adjust the input constant C");
    }
    double dT_max = dX_max*lambda_sim/c_sound;
    //Rescale simulation time to fit an integer dT_max:
    t_sim = 1.0*((int) round(t_sim / dT_max)*dT_max);

    double fs_max = c_sound / dX_max /lambda_sim;
    log_msg<LOG_DEBUG>(L"V&V Parameters done: new t_sim = %f, "
    "fs_max = %f, dX_max = %f, c_sound = %f") %t_sim %fs_max %dX_max %c_sound;
}

std::vector<unsigned int> set_up_mesh_Partitions(
        CudaMesh* the_mesh,
        unsigned char* h_position_idx,
        unsigned char* h_material_idx,
        unsigned int GPU_partitions,
        unsigned int dev_start_id){
    int* device_count = new int[1];
    cudaGetDeviceCount(device_count);
    // again, for debugging purposes, in case 2 partitions are used on a single
    // device, use device_index_1 = 0
    unsigned int dev_idx_0 = 0, dev_idx_1 = 1;
    std::vector<unsigned int> v_;
    if ( *device_count == 1 && GPU_partitions > 1)
    {
        std::string dev_list_str;
        dev_list_str = dev_list_str + "[";
        for (unsigned int dev_idx_ = 0; dev_idx_<GPU_partitions; dev_idx_++ )
        {
            v_.push_back( dev_idx_ );
            dev_list_str = dev_list_str + "," + std::to_string(dev_idx_);
        }
        the_mesh->makePartitionFromHost(GPU_partitions,
                                        h_position_idx,
                                        h_material_idx,
                                        v_);
        log_msg<LOG_FINE>(L"VV_utils.cpp : set_up_mesh_Partitions() - "
            "Making forced partition [HOST] on %u devices done. dev_list = %s")
                %GPU_partitions %dev_list_str.c_str();
    }
    else
    {
        if (*device_count != GPU_partitions)
        {
            if (*device_count < GPU_partitions ||
                    (dev_start_id + GPU_partitions ) > *device_count )
            {
                log_msg<LOG_ERROR>(L"VV_utils.cpp@474 set_up_mesh_Partitions()"
                        " - Number of requested GPU partitions (%u) starting from DEV_%u is "
                        "larger than the number of available GPU devices (%d)!")
                         %GPU_partitions %dev_start_id %(*device_count);
                exit(EXIT_FAILURE);;
            } else
            {
                // Construct a device list based on dev_start_id:
                std::string dev_list_str;
                dev_list_str = dev_list_str + "[";
                for(unsigned int dev_id_ = dev_start_id; dev_id_ <
                                (dev_start_id+GPU_partitions); dev_id_++)
                {
                    v_.push_back( dev_id_ );
                    dev_list_str = dev_list_str + "," + std::to_string(dev_id_);
                }
                dev_list_str = dev_list_str + "]";
                the_mesh->makePartitionFromHost(GPU_partitions,
                                                h_position_idx,
                                                h_material_idx,
                                                v_);
                log_msg<LOG_FINE>(L"VV_utils.cpp : set_up_mesh_Partitions() - "
                    "Making forced partition [HOST] on %u devices done (out of %d): "
                    "dev_list = %s.") %GPU_partitions %(*device_count) %dev_list_str.c_str();
            }

        } else {
            std::string dev_list_str;
            dev_list_str = dev_list_str + "[";
            for(unsigned int dev_id_ = 0; dev_id_ <GPU_partitions; dev_id_++)
            {
                v_.push_back( dev_id_ );
                dev_list_str = dev_list_str + "," + std::to_string(dev_id_);
            }
            dev_list_str = dev_list_str + "]";
            the_mesh->makePartitionFromHost(GPU_partitions,
                                            h_position_idx,
                                            h_material_idx);
            log_msg<LOG_FINE>(L"VV_utils.cpp : set_up_mesh_Partitions() - "
                "Making normal partitions (%u partitions on %d devices) on HOST done."
                " dev_list = %s.")
                 %GPU_partitions %(*device_count) %dev_list_str.c_str();
        }
    }
    std::vector< std::vector <mesh_size_t> > partitioning =
                            the_mesh->getPartitionIndexing(the_mesh->getNumberOfPartitions() ,
                                                           the_mesh->getDimZ() );
    // Do extra checks to assure irrelevant tests are not done:
    // This might not be considered an error...
    for(int part_idx = 0; part_idx<partitioning.size(); part_idx++ )
    {
        if(partitioning.at(part_idx).size()<3){
            log_msg<LOG_ERROR>(L"VV_utils.cpp@529 set_up_mesh_Partitions()"
              " - PARTITION %d has only %d z-slices. This partition will not "
              "get any updates!") %part_idx %partitioning.at(part_idx).size();
        debugging_print_partion_indexing_BE(partitioning);
        }
    }
    delete device_count;
    return v_;
}

std::vector<unsigned int> set_up_Initial_conditions_MMS_single(
        CudaMesh* the_mesh,
        unsigned char* h_position_idx,
        unsigned char* h_material_idx,
        uint3 voxelization_dim,
        unsigned int GPU_partitions,
        unsigned int start_voxel_idx,
        unsigned int size_x,
        unsigned int size_y,
        unsigned int size_z,
        double c_sound,
        double dX,
        double dT,
        double P0,
        double Y_xyz,
        double L_xyz,
        int n_xyz,
        double phi_xyz,
        unsigned int dev_start_id){
    // First, set-up the partitions (i.e., push Vox data):
    std::vector<unsigned int> dev_list = set_up_mesh_Partitions(
                                                the_mesh,
                                                h_position_idx,
                                                h_material_idx,
                                                GPU_partitions,
                                                dev_start_id);
    log_msg<LOG_DEBUG>(L"VV_utils_MPI.set_up_Initial_conditions_MMS_single()"
               " - Done setting up mesh partitions (Exact solution).");
    //////////////////////////////////// Init pressure meshes...
    mesh_size_t mem_size = (mesh_size_t)voxelization_dim.x *
            (mesh_size_t)voxelization_dim.y*(mesh_size_t)voxelization_dim.z;
    // Allocate temporary pressure pointer:
    float* p_current = new float[mem_size];
    memset(p_current, 0, mem_size*sizeof(float));
    float* p_old = new float[mem_size];
    memset(p_old, 0, mem_size*sizeof(float));
    ///////////////////////////////////// Calculating MS:
    mesh_size_t size_XY = (mesh_size_t)voxelization_dim.x*
                            (mesh_size_t)voxelization_dim.y;
    for(unsigned int idx_z =start_voxel_idx;
            idx_z < start_voxel_idx+ size_z; idx_z++)
        for(unsigned int idx_y =start_voxel_idx;
                idx_y < start_voxel_idx+ size_y; idx_y++)
            for(unsigned int idx_x =start_voxel_idx;
                    idx_x < start_voxel_idx + size_x; idx_x++)
            {
                *(p_old + (mesh_size_t)idx_z*size_XY + (mesh_size_t)idx_y*voxelization_dim.x + idx_x) =
                        get_Manufactured_solution_BOX_ALL_Walls_abs_Point(
                                idx_x - start_voxel_idx,        // N_x
                                idx_y - start_voxel_idx,        // N_y
                                idx_z - start_voxel_idx,        // N_z
                                dX, P0,
                                0.0*dT,            // t
                                Y_xyz, n_xyz, L_xyz, phi_xyz, c_sound);
                *(p_current+ (mesh_size_t)idx_z*size_XY + (mesh_size_t)idx_y*voxelization_dim.x + idx_x) =
                        get_Manufactured_solution_BOX_ALL_Walls_abs_Point(
                                idx_x - start_voxel_idx,        // N_x
                                idx_y - start_voxel_idx,        // N_y
                                idx_z - start_voxel_idx,        // N_z
                                dX, P0,
                                1.0*dT,        // t
                                Y_xyz, n_xyz, L_xyz, phi_xyz, c_sound);
            }
    push_initial_pressure_fields(the_mesh, p_current, p_old);
    // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
    //log_msg<LOG_TRACE>(L"Set initial conditions: max(p_old) = %f \n "
    //    "max(p_current) = %f, mem_size = %llu") %get_abs_max_value(p_old, mem_size)
    //            %get_abs_max_value(p_current, mem_size) %mem_size;
    // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
    delete[] p_current;
    delete[] p_old;
    log_msg<LOG_DEBUG>(L"VV_utils_MPI: Done pushing the initial conditions"
                "from host to device. Initial conditions MMS done!");
    return dev_list;
}

std::vector<unsigned int> set_up_Initial_conditions_MES_single(
        CudaMesh* the_mesh,
        unsigned char* h_position_idx,
        unsigned char* h_material_idx,
        uint3 voxelization_dim,
        unsigned int GPU_partitions,
        unsigned int start_voxel_idx,
        unsigned int size_x,
        unsigned int size_y,
        unsigned int size_z,
        double c_sound,
        double dX,
        double dT,
        double P0,
        double L_xyz,
        int n_xyz,
        unsigned int dev_start_id){
    // First, set-up the partitions (i.e., push Vox data):
    std::vector<unsigned int> dev_list = set_up_mesh_Partitions(
                                                    the_mesh,
                                                    h_position_idx,
                                                    h_material_idx,
                                                    GPU_partitions,
                                                    dev_start_id);
    log_msg<LOG_DEBUG>(L"VV_utils_MPI.set_up_Initial_conditions_MES_single()"
           " - Done setting up mesh partitions (Exact solution).");
    //////////////////////////////////// Init pressure meshes...
    mesh_size_t mem_size = (mesh_size_t)voxelization_dim.x *
            (mesh_size_t)voxelization_dim.y*(mesh_size_t)voxelization_dim.z;
    // Allocate temporary pressure pointer:
    float* p_current = new float[mem_size];
    memset(p_current, 0, mem_size*sizeof(float));
    float* p_old = new float[mem_size];
    memset(p_old, 0, mem_size*sizeof(float));
    ///////////////////////////////////// Calculating MS:
    mesh_size_t size_XY = (mesh_size_t)voxelization_dim.x *
                            (mesh_size_t)voxelization_dim.y;
    for(unsigned int idx_z =start_voxel_idx;
            idx_z < start_voxel_idx+ size_z; idx_z++)
        for(unsigned int idx_y =start_voxel_idx;
                idx_y < start_voxel_idx+ size_y; idx_y++)
            for(unsigned int idx_x =start_voxel_idx;
                    idx_x < start_voxel_idx + size_x; idx_x++)
            {
                *(p_old + (mesh_size_t)idx_z*size_XY + (mesh_size_t)idx_y*voxelization_dim.x + idx_x) =
                        (float)get_Exact_solution_BOX_Rigid_Point(
                                idx_x-start_voxel_idx,        // N_x
                                idx_y-start_voxel_idx,        // N_y
                                idx_z-start_voxel_idx,        // N_z
                                dX,
                                0.0*dT,        // t
                                P0,
                                n_xyz,        // n_x
                                n_xyz,        // n_y
                                n_xyz,        // n_z
                                L_xyz,        // L_x
                                L_xyz,        // L_y
                                L_xyz,        // L_z
                                c_sound);
                *(p_current+ (mesh_size_t)idx_z*size_XY + (mesh_size_t)idx_y*voxelization_dim.x + idx_x) =
                        (float)get_Exact_solution_BOX_Rigid_Point(
                                idx_x-start_voxel_idx,        // N_x
                                idx_y-start_voxel_idx,        // N_y
                                idx_z-start_voxel_idx,        // N_z
                                dX,
                                1.0*dT,        // t
                                P0,
                                n_xyz,        // n_x
                                n_xyz,        // n_y
                                n_xyz,        // n_z
                                L_xyz,        // L_x
                                L_xyz,        // L_y
                                L_xyz,        // L_z
                                c_sound);
            }
    // General function for any #GPU partition:
    if (dev_list.size() != the_mesh->getNumberOfPartitions() ||
                dev_list.size()!= GPU_partitions){
        log_msg<LOG_ERROR>(L"set_up_Initial_conditions_MES_single()@731 :"
           " - Number of requested GPU partitions in the mes (%u) is different "
             "than the size of the device_list (%d) or requested partitions (%u)!")
               %the_mesh->getNumberOfPartitions() %(int)dev_list.size() %GPU_partitions;
        exit(EXIT_FAILURE);
    }
    log_msg<LOG_DEBUG>(L"VV_utils_MPI.set_up_Initial_conditions_MES_single()"
           " - Pushing initial conditions... #N_partitions = %u") %the_mesh->getNumberOfPartitions();
    push_initial_pressure_fields(the_mesh, p_current, p_old);
    // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
    //log_msg<LOG_TRACE>(L"Set initial conditions: max(p_old) = %f \n "
    //    "max(p_current) = %f, mem_size = %llu") %get_abs_max_value(p_old, mem_size)
    //            %get_abs_max_value(p_current, mem_size) %mem_size;
    // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
    delete[] p_current;
    delete[] p_old;
    log_msg<LOG_DEBUG>(L"VV_utils_MPI: Done pushing the initial conditions"
                    "from host to device. Initial conditions MES done!");
    return dev_list;
}


void read_HDF5_Scene_Data(const char *file_name,
            // Non-array data:
            int *num_triangles, int* num_vertices, int* num_materials,
            int* no_of_used_materials, int* no_of_unique_materials,
            long* total_material_coeff_size_,
            char *mesh_file,
            // arrays:
            double*& material_coefficients__,
            float*& vertices_,
            unsigned int*& triangles_,
            unsigned char*& materials_
            )
{
    // Load up hdf5 file:
    hid_t       file_id;   // file identifier
    herr_t      status;
    file_id = H5Fopen(file_name, H5F_ACC_RDONLY, H5P_DEFAULT);

    // Mesh array properties:
    H5LTread_dataset_int(file_id, "/num_triangles", num_triangles);
    H5LTread_dataset_int(file_id, "/num_vertices", num_vertices);
    H5LTread_dataset_int(file_id, "/num_materials", num_materials);

    H5LTread_dataset_int(file_id, "/no_of_used_materials",
            no_of_used_materials);
    H5LTread_dataset_int(file_id, "/no_of_unique_materials",
            no_of_unique_materials);

    // Array sizes:
    H5LTread_dataset_long(file_id, "/total_material_coeff_size_",
            total_material_coeff_size_);

    // Allocationg and reading arrays:
    material_coefficients__ = new double[(*total_material_coeff_size_)];
    vertices_ = new float[3*(*num_vertices)];
    triangles_ = new unsigned int[3*(*num_triangles)];
    materials_ = new unsigned char[(*num_materials)];

    H5LTread_dataset_double(file_id, "/material_coefficients__",
            material_coefficients__);
    H5LTread_dataset_float(file_id, "/vertices_", vertices_);

    // Read the unsigned ones (no function for these dudes):
    hid_t dataset_id = H5Dopen2(file_id, "/triangles_", H5P_DEFAULT);
    H5Dread(dataset_id, H5T_NATIVE_UINT, H5S_ALL, H5S_ALL, H5P_DEFAULT, triangles_);
    dataset_id = H5Dopen2(file_id, "/materials_", H5P_DEFAULT);
    H5Dread(dataset_id, H5T_NATIVE_UCHAR, H5S_ALL, H5S_ALL, H5P_DEFAULT, materials_);

    // Read mesh file:
    H5LTread_dataset_string(file_id, "/mesh_file", mesh_file);
    status = H5Fclose(file_id);
};

double get_L2_norm_single(
        float* FDTD_matrix,
        uint3 voxelization_dim,
        unsigned int start_voxel_idx,
        unsigned int size_x,
        unsigned int size_y,
        unsigned int size_z,
        float* analytical_matrix,
        double dX)
{
    if (dX <= 0.0){
        log_msg<LOG_ERROR>(L"The grid spacing is negative. This makes no sense!"
                " [VV_tuils.cpp - get_L2_norm_single()] @l835");
        exit(EXIT_FAILURE);
    }
    double result = 0.0, temp = 0.0;
    mesh_size_t size_XY_FDTD = (mesh_size_t)voxelization_dim.x*(mesh_size_t)voxelization_dim.y;
    mesh_size_t size_XY_analytical = (mesh_size_t)size_x*(mesh_size_t)size_y;
    double dV = dX*dX*dX;
    for(mesh_size_t idx_z =0; idx_z < size_z; idx_z++)
        for(mesh_size_t idx_y =0; idx_y < size_y; idx_y++)
            for(mesh_size_t idx_x = 0;idx_x < size_x; idx_x++)
            {
                temp = fabs(
                        FDTD_matrix[(idx_z+start_voxel_idx)*size_XY_FDTD+
                                   (idx_y+start_voxel_idx)*voxelization_dim.x+
                                   (idx_x+start_voxel_idx)] -
                       analytical_matrix[idx_z*size_XY_analytical+
                                         idx_y*size_x+
                                         idx_x]
                          );
                result = result + temp*temp;
            }
    result = sqrt(dV*result);
    //double result_normalized = sqrt(result / (1.0*size_XY_analytical*size_z));
    return result;
};

void write_L2_norm( std::vector<double> dX_vector,
                    std::vector<double> lambda_sim_vector,
                    std::vector<double> c_sound_vector,
                    std::vector<double> Y_x_vector,
                    std::vector<double> L2_norm_vector,
                    std::vector<unsigned int> total_steps_vector,
                    std::vector<unsigned int> GPU_partitions_vector,
                    double t_sim,
                    int n_xyz,
                    unsigned int GPU_partitions,
                    const char* mesh_file,
                    const char* file_string,
                    char voxelization_type,
                    int use_ExactSolution_flag,
                    int test_only_interior_flag,
                    int MPI_size,
                    int MPI_rank
                  ){
    // Basic checks:
    if (lambda_sim_vector.size() != c_sound_vector.size() ){
        log_msg<LOG_ERROR>(L"The size of lambda_vector (%d) is different than "
                "the sound speed vector (%d)") %lambda_sim_vector.size()
                                %c_sound_vector.size();
        exit(EXIT_FAILURE);
    }
    if (lambda_sim_vector.size()*Y_x_vector.size()*dX_vector.size() !=
            L2_norm_vector.size() ){
        log_msg<LOG_ERROR>(L"The size of L2_norm_vector (%d) is not as expected!"
                " - lambda_sim_vector*Y_x_vector*dX_vector [%d*%d*%d]")
        %L2_norm_vector.size() %lambda_sim_vector.size() %Y_x_vector.size()
                        %dX_vector.size();
        exit(EXIT_FAILURE);
    }
    if (L2_norm_vector.size() != total_steps_vector.size() ){
        log_msg<LOG_ERROR>(L"The size of L2_norm_vector (%d) is different than "
                "the size of total steps vector (%d)") %L2_norm_vector.size()
                                %total_steps_vector.size();
        exit(EXIT_FAILURE);
    }
    if (dX_vector.size() != GPU_partitions_vector.size() ){
        log_msg<LOG_ERROR>(L"The size of dX_vector (%d) is different than "
                "the size of GPU_partitions_vector (%d)")
                %dX_vector.size() %GPU_partitions_vector.size();
        exit(EXIT_FAILURE);
    }

    // FIRST, the HDF5 creation:
    hid_t       file_id;   /* file identifier */
    herr_t      status;

    file_id=H5Fcreate((std::string(file_string) + ".hdf5").c_str(),H5F_ACC_TRUNC,
        H5P_DEFAULT, H5P_DEFAULT); // H5F_ACC_TRUNC means the file will be overwritten
    // Basically says one dimension of value 1; for 2 dims: dims[2]; dims[0]=Nx;
    //   dims[1] = Ny
    hsize_t dims[1]={1};
    unsigned int temp = dX_vector.size();
    // Write sizes:
    H5LTmake_dataset(file_id, "/len_dX_vector",1, dims, H5T_NATIVE_INT, &temp);
    temp = lambda_sim_vector.size();
    H5LTmake_dataset(file_id, "/len_lambda_sim_vector", 1, dims, H5T_NATIVE_INT,
            &temp);
    temp = Y_x_vector.size();
    H5LTmake_dataset(file_id, "/len_Y_x_vector",1, dims, H5T_NATIVE_INT, &temp);
    temp = L2_norm_vector.size();
    H5LTmake_dataset(file_id, "/len_L2_norm_vector", 1, dims, H5T_NATIVE_INT,
            &temp);
    H5LTmake_dataset(file_id, "/MPI_size", 1, dims, H5T_NATIVE_INT, &MPI_size);
    H5LTmake_dataset(file_id, "/MPI_rank", 1, dims, H5T_NATIVE_INT, &MPI_rank);
    H5LTmake_dataset(file_id, "/t_sim", 1, dims, H5T_NATIVE_DOUBLE, &t_sim);
    H5LTmake_dataset(file_id, "/n_xyz", 1, dims, H5T_NATIVE_INT, &n_xyz);
    H5LTmake_dataset(file_id, "/GPU_partitions", 1, dims, H5T_NATIVE_UINT,
            &GPU_partitions);
    H5LTmake_dataset(file_id, "/voxelization_type", 1, dims, H5T_NATIVE_CHAR,
                &voxelization_type);
    H5LTmake_dataset(file_id, "/use_ExactSolution_flag", 1, dims,
            H5T_NATIVE_INT, &use_ExactSolution_flag);
    H5LTmake_dataset(file_id, "/test_only_interior_flag", 1, dims,
                H5T_NATIVE_INT, &test_only_interior_flag);

    // Writing vectors:
    dims[0]={(hsize_t)dX_vector.size()};
    H5LTmake_dataset(file_id, "/dX_vector", 1, dims,
            H5T_NATIVE_DOUBLE, &dX_vector[0]);
    H5LTmake_dataset(file_id, "/GPU_partitions_vector", 1, dims,
            H5T_NATIVE_UINT, &GPU_partitions_vector[0]);
    dims[0]={(hsize_t)lambda_sim_vector.size()};
    H5LTmake_dataset(file_id, "/lambda_sim_vector", 1, dims,
            H5T_NATIVE_DOUBLE, &lambda_sim_vector[0]);
    dims[0]={(hsize_t)c_sound_vector.size()};
    H5LTmake_dataset(file_id, "/c_sound_vector", 1, dims,
            H5T_NATIVE_DOUBLE, &c_sound_vector[0]);
    dims[0]={(hsize_t)Y_x_vector.size()};
    H5LTmake_dataset(file_id, "/Y_x_vector", 1, dims,
            H5T_NATIVE_DOUBLE, &Y_x_vector[0]);
    dims[0]={(hsize_t)L2_norm_vector.size()};
    H5LTmake_dataset(file_id, "/L2_norm_vector", 1, dims,
            H5T_NATIVE_DOUBLE, &L2_norm_vector[0]);
    dims[0]={(hsize_t)total_steps_vector.size()};
    H5LTmake_dataset(file_id, "/total_steps_vector", 1, dims,
                H5T_NATIVE_UINT, &total_steps_vector[0]);
    H5LTmake_dataset_string(file_id, "/mesh_file", mesh_file);
    // Create writing time:
    H5LTmake_dataset_string(file_id, "/writing_Timestamp",
            Logger(LOG_DEBUG, L"Writing hdf5 timestamp!").getTimeStampString().c_str());
    // Close file:
    status = H5Fclose(file_id);
}

void get_full_final_pressureToHost(
        CudaMesh* the_mesh,
        float* final_pressure_field_h
        )
{
    // General function for any #GPU partition:
    for (unsigned int partition_idx = 0; partition_idx <
            the_mesh->getNumberOfPartitions(); partition_idx ++)
    {
        float* d_P = the_mesh->getPressurePtrAt(partition_idx);
        mesh_size_t mem_size_partition =
                the_mesh->getNumberOfElementsAt(partition_idx);
        mesh_size_t offset =
                the_mesh->partition_indexing_.at(partition_idx).at(0)*
                                                the_mesh->getDimXY();
        copyDeviceToHost(mem_size_partition, final_pressure_field_h +
                offset, d_P, the_mesh->getDeviceAt(partition_idx));
    }
}


void push_initial_pressure_fields(
        CudaMesh* the_mesh,
        float* current_pressure_field_h,
        float* past_pressure_field_h
        )
{
    for (unsigned int partition_idx = 0; partition_idx <
            the_mesh->getNumberOfPartitions(); partition_idx ++)
    {
        float* d_P = the_mesh->getPressurePtrAt(partition_idx);
        float* d_P_past = the_mesh-> getPastPressurePtrAt(partition_idx);
        mesh_size_t mem_size_partition = the_mesh->getNumberOfElementsAt(partition_idx);
        mesh_size_t offset =
                the_mesh->partition_indexing_.at(partition_idx).at(0)*
                the_mesh->getDimXY();
        cudaSetDevice(the_mesh->getDeviceAt(partition_idx));
        // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
        //log_msg<LOG_FINE>(L"P_idx = %u (mesh_DEV_ID = %u). Copy HOST to "
        //    "DEV P_current = %p!\nNum_elems = %llu, offset = %llu; FULL_z = %u, z_start = %u ")
        //        "z_end = %u [z_size = %u]" %partition_idx %the_mesh->getDeviceAt(partition_idx)
        //        %d_P %mem_size_partition %offset %voxelization_dim.z
        //        %the_mesh->partition_indexing_.at(partition_idx).at(0)
        //        %the_mesh->partition_indexing_.at(partition_idx).back()
        //        %the_mesh->partition_indexing_.at(partition_idx).size();
        // DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG DEBUG
        copyHostToDevice(mem_size_partition, d_P, current_pressure_field_h + offset,
                            the_mesh->getDeviceAt(partition_idx));
        copyHostToDevice(mem_size_partition, d_P_past, past_pressure_field_h + offset,
                            the_mesh->getDeviceAt(partition_idx));
    }

}


///////////////////////////////////////////////////////////////////////////////
void debugging_print_partion_indexing_BE(
        std::vector< std::vector <mesh_size_t> > partition_indexing_,
        int MPI_rank,
        const char* msg_ ){
    int no_of_partition_size = partition_indexing_.size();
    char partition_printout[no_of_partition_size*30 + 30], number_[10];
    partition_printout[0] = '\0'; number_[0] = '\0';
    if (MPI_rank != -1){
        sprintf(number_, "[R%02d] ", MPI_rank);
        strcat(partition_printout, number_);
    }
    for(int p_idx = 0; p_idx< no_of_partition_size; p_idx++ ){
      strcat(partition_printout, "PARTITION_");
      sprintf(number_, "%d", p_idx);
      strcat(partition_printout, number_);
      strcat(partition_printout, " : [");
      sprintf(number_, "%llu", partition_indexing_.at(p_idx).at(0));
      strcat(partition_printout, number_);
      strcat(partition_printout, "-");
      sprintf(number_, "%llu", partition_indexing_.at(p_idx).back());
      strcat(partition_printout, number_);
      strcat(partition_printout, "] - #");
      sprintf(number_, "%d", partition_indexing_.at(p_idx).size());
      strcat(partition_printout, number_);
      strcat(partition_printout, "\n");
    }
    log_msg<LOG_DEBUG>(L"\t@ DEBUG_PRINTOUT_PARTION Indexing %s "
      "(size %d):\n%s") %msg_ %no_of_partition_size %partition_printout;
}
