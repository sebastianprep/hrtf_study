///////////////////////////////////////////////////////////////////////////////
//
// This file is released under the MIT License. See
// http://www.opensource.org/licenses/mit-license.php
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//  Author: Sebastian Prepelita, Aalto University
//  Created on: May 30, 2016
//      - updates to mesh_size_t & full HOST voxelization @2017
//
///////////////////////////////////////////////////////////////////////////////
/*
 * VV_utils.h
 *
 *    File with helper function to set-up a simulation for a code verification
 *    study.
 *
 */

#ifndef V_V_VV_UTILS_H_
#define V_V_VV_UTILS_H_

#include "continuous_solutions.h"
#include <vector>
#include "base/SimulationParameters.h"
#include "kernels/cudaMesh.h"

/*/////////////////////////////////////////////////////////////////////////////
 * Function checks that the inner air volume is as intended inside the voxelized
 * matrix (h_position_ptr__) - (i.e., a cube of L_xyz side).
 *
 * \param h_position_ptr__ [IN]: The voxelization result
 *                 see voxelizationUtils.h
 * \param voxelization_dim [IN]: The dimensions resulting from the voxelizaiton
 *                 process - see voxelizationUtils.h
 * \param dX [IN]: The grid spacing.
 * \param dX [IN]: The side of the room cube [m].
 * \param OUT_start_voxel_idx [OUT]: At which voxel does the inner air volume
 *             start.
 * \param OUT_size_x [OUT]: What is the size in voxels of the inner air volume
 *                 (x direction)\
 * \param OUT_size_y [OUT]: What is the size in voxels of the inner air volume
 *                 (y direction)
 * \param OUT_size_z [OUT]: What is the size in voxels of the inner air volume
 *                 (zy direction)
 */
void check_voxelization_consistency_MMS(
                    const unsigned char* h_position_ptr__, // On host
                    uint3 voxelization_dim,
                    double dX,
                    double L_xyz,
                    unsigned int &OUT_start_voxel_idx,
                    unsigned int &OUT_size_x,
                    unsigned int &OUT_size_y,
                    unsigned int &OUT_size_z
                    );

/*/////////////////////////////////////////////////////////////////////////////
 * Function checks that the number of GPU partitions is small enough such that
 * no GPU partitions contain <=2 z-slices. This can easily happen for the
 * largest dX values in the convergence study and when a large number of GPU
 * devices and MPI_nodes (function also called from MPI functions) are used.
 *
 *      If GPU_partitions is too large relative to dim_z, it will be
 *    decremented. If GPU_partitions becomes smaller than 1, an error will
 *    be thrown.
 *
 *      Note the function DOES NOT check if the number of GPU partitions is
 *      larger than available GPU devices. This is checked in
 *      set_up_mesh_Partitions().
 *
 * \param GPU_partitions [IN/OUT]: The required number of GPU partitions per
 *                 current host.
 * \param dim_z [IN]: The z-dimension resulting from the voxelizaiton
 *                 process - see voxelizationUtils.h
 * \param MPI_rank [IN]: The MPI_rank. Usued only for logging. For non-MPI
 *                 code verification, just ignore this one.
 *
 */
void check_GPU_partitions(
        unsigned int &GPU_partitions,
        mesh_size_t dim_z,
        int MPI_rank = 0
        );

/*/////////////////////////////////////////////////////////////////////////////
 * Function sets some simulation parameters for a smooth convergence: such that
 *  the sampling frequency ends up an integer for all grid spacings dX.
 *
 *    Function can be used with both the exact and manufactured solution.
 *
 * \param t_sim [IN/OUT]: The simulation time in [s]. It is overwritten with
 *             the closest integer multiplier of the maximum sampling time.
 * \param c_sound [OUT]: The speed of sound for the convergence simulations.
 * \param dX_vector [IN]: The grid spacing vector in [m]
 * \param lambda_sim [IN]: The Courant number
 * \param C [IN]: An integer constant used to determine an integer sampling
 *             frequency. The 4250 was designed for a lambda_sim = 0.5.
 */
void set_convergence_parameters(
                    double &t_sim,
                    double &c_sound,
                    std::vector<double> dX_vector,
                    double lambda_sim,
                    const unsigned int C = 4250
                    );

/*/////////////////////////////////////////////////////////////////////////////
 * Function sets the sources for the SimulationParameters* to be used in the
 *  simulation for a Manufactured Solution (MS) in a box with absorbing
 *  boundaries. The voxelization result h_position_ptr__ is also used to check
 *  for consistency.
 *      See continous_solutions.h for information about the MS.
 *
 * \param parameter_ptr__ [OUT]: Parameter pointer where the sources will be
 *         placed.
 * \param voxelization_dim [IN]: The dimensions from the voxelization process.
 * \param h_position_ptr__ [IN]: The b_id from a voxelization process.
 * \param fs [IN]: The sampling frequency.
 * \param lambda_ [IN]: The courant number.
 * \param c_sound [IN]: The sound speed.
 * \param dX [IN]: The grid spacing [m]
 * \param dT [IN]: The sampling time [s] . Should be = 1.0/fs
 * \param total_steps [IN]: Total discrete steps including intial conditions.
 * \param CUDA_steps [IN]: Total simulated steps (should be total_steps - 2).
 * \param P0 [IN]: MS amplitude
 * \param Y_xyz [IN]: MS specific acoustic admittance
 * \param L_xyz [IN]: MS room dimensions in [m]
 * \param n_xyz [IN]: MS mode number
 * \param phi_xyz [IN]: MS spatial mode phase
 * \param test_only_interior_flag [IN]: Boolean: if true, no boundary update is
 *         tested: HS with the MS are placed on the outer edges instead. Otherwise,
 *         all the update is tested.
 */
void set_up_Parameters_MMS_single(
                    SimulationParameters* parameter_ptr__,
                    uint3 voxelization_dim,
                    unsigned char* h_position_ptr__, // On host
                    unsigned int fs,
                    double lambda_,
                    double c_sound,
                    double dX,
                    double dT,
                    unsigned int total_steps,
                    unsigned int CUDA_steps, // should be total_steps - 2
                    double P0,
                    double Y_xyz,
                    double L_xyz,
                    int n_xyz,
                    double phi_xyz,
                    bool test_only_interior_flag
                    );

/*/////////////////////////////////////////////////////////////////////////////
 * Function sets the sources for the SimulationParameters* to be used in the
 *  simulation for an Exact Solution (ES) in a rigid box.The voxelization result
 *   h_position_ptr__ is also used to check for consistency.
 *      See continous_solutions.h for information about the ES.
 *
 * \param parameter_ptr__ [OUT]: Parameter pointer where the sources will be
 *         placed.
 * \param voxelization_dim [IN]: The dimensions from the voxelization process.
 * \param h_position_ptr__ [IN]: The b_id from a voxelization process.
 * \param fs [IN]: The sampling frequency.
 * \param lambda_ [IN]: The courant number.
 * \param c_sound [IN]: The sound speed.
 * \param dX [IN]: The grid spacing [m]
 * \param dT [IN]: The sampling time [s] . Should be = 1.0/fs
 * \param total_steps [IN]: Total discrete steps including intial conditions.
 * \param CUDA_steps [IN]: Total simulated steps (should be total_steps - 2).
 * \param L_xyz [IN]: ES room dimensions in [m]
 */
void set_up_Parameters_MES_single(
                    SimulationParameters* parameter_ptr__,
                    uint3 voxelization_dim,
                    const unsigned char* h_position_ptr__, // On host
                    unsigned int fs,
                    double lambda_,
                    double c_sound,
                    double dX,
                    double dT,
                    unsigned int total_steps,
                    unsigned int CUDA_steps, // should be total_steps - 2
                    double L_xyz
                    );

/*/////////////////////////////////////////////////////////////////////////////
 * Helper function that sets-up the cudamesh by pushing the voxelization data.
 *  The partitions are made based on available GPU devices on HOST,
 *  GPU_partitions and dev_start_id.
 *
 * \param the_mesh [IN/OUT]: The CUDA mesh where initial voxelization data is
 *                          pushed.
 *
 * \param h_position_idx[IN]: Pointer to the position index matrix on HOST
 *               coming from a voxelization process. Will be pushed to CudaMesh.
 * \param h_material_idx[IN]: Pointer to the material index matrix on HOST
 *               coming from a voxelization process. Will be pushed to CudaMesh.
 *
 * \param GPU_partitions [IN]: The number of GPU_partitions used by ParallelFDTD.
 *
 * \param dev_start_id [IN]: This is for testing on a signle node and tells the
 *      starting device_id that will be used (until dev_start_id+GPU_partitions).
 *          E.g., if dev_start_id = 2 and GPU_partitions = 2, then the node
 *          will use [dev_2, dev_3]. You might change this based on MPI_rank.
 *
 * \returns The list with used GPU devices.
 *
 *//////////////////////////////////////////////////////////////////////////////
std::vector<unsigned int> set_up_mesh_Partitions(
    CudaMesh* the_mesh,
    unsigned char* h_position_idx,
    unsigned char* h_material_idx,
    unsigned int GPU_partitions,
    unsigned int dev_start_id = 0);

/*/////////////////////////////////////////////////////////////////////////////
 * Function sets the initial conditions for a code verification task - MMS (Met
 *  hod of Manufactured Solution).
 *
 *    To be used with set_up_Parameters_MMS_single() function.
 *
 *  Call this AFTER the_mesh->setupMesh()! The partitioning is done inside here.
 *
 * \param the_mesh [IN/OUT]: The CUDA mesh where initial conditions are placed.
 * \param voxelization_dim [IN]: The voxelization dimensions.
 *
 * \param h_position_idx[IN]: Pointer to the position index matrix on HOST
 *               coming from a voxelization process. Will be pushed to CudaMesh.
 * \param h_material_idx[IN]: Pointer to the material index matrix on HOST
 *               coming from a voxelization process. Will be pushed to CudaMesh.
 *
 *
 * \param GPU_partitions [IN]: The number of GPU_partitions.
 *
 * \param start_voxel_idx [IN]: where the inside air starts ( coming from
 *             check_voxelization_consistency_MMS() function)
 * \param size_x [IN]: The size of inner air in voxels in x direction ( coming
 *             from check_voxelization_consistency_MMS() function)
 * \param size_y [IN]: The size of inner air in voxels in y direction ( coming
 *             from check_voxelization_consistency_MMS() function)
 * \param size_z [IN]: The size of inner air in voxels in z direction ( coming
 *             from check_voxelization_consistency_MMS() function)
 *
 * \param c_sound [IN]: The sound speed [m/s] - used in MS.
 * \param dX [IN]: The grid spacing [m] - used in MS.
 * \param dT [IN]: The Courant number - used in MS.
 * \param Y_xyz [IN]: The spec. acoustic admittance on walls - used in MS.
 * \param L_xyz [IN]: The room side [m] - used in MS.
 * \param n_xyz [IN]: The mode number - used in MS.
 * \param phi_xyz [IN]: The MS modal phase - used in MS.
 *
 * \param dev_start_id [IN]: This is for testing on a signle node and tells the
 *      starting device_id that will be used (until dev_start_id+GPU_partitions).
 *          E.g., if dev_start_id = 2 and GPU_partitions = 2, then the node
 *          will use [dev_2, dev_3]. You might change this based on MPI_rank.
 *
 * \returns The list with used GPU devices.
 */
std::vector<unsigned int> set_up_Initial_conditions_MMS_single(
        CudaMesh* the_mesh,
        unsigned char* h_position_idx,
        unsigned char* h_material_idx,
        uint3 voxelization_dim,
        unsigned int GPU_partitions,
        unsigned int start_voxel_idx,
        unsigned int size_x,
        unsigned int size_y,
        unsigned int size_z,
        double c_sound,
        double dX,
        double dT,
        double P0,
        double Y_xyz,
        double L_xyz,
        int n_xyz,
        double phi_xyz,
        unsigned int dev_start_id = 0);

/*/////////////////////////////////////////////////////////////////////////////
 * Function sets the initial conditions for a code verification task - MES (Met
 *  hod of Exact Solution).
 *
 *    To be used with set_up_Parameters_MES_single() function.
 *
 *  Call this AFTER the_mesh->setupMesh()! The partitioning is done inside here.
 *
 * \param the_mesh [IN/OUT]: The CUDA mesh where initial conditions are placed.
 * \param voxelization_dim [IN]: The voxelization dimensions.
 *
 * \param h_position_idx[IN]: Pointer to the position index matrix on HOST
 *               coming from a voxelization process. Will be pushed to CudaMesh.
 * \param h_material_idx[IN]: Pointer to the material index matrix on HOST
 *               coming from a voxelization process. Will be pushed to CudaMesh.
 *
 * \param GPU_partitions [IN]: The number of GPU_partitions.
 *
 * \param start_voxel_idx [IN]: where the inside air starts ( coming from
 *             check_voxelization_consistency_MMS() function)
 * \param size_x [IN]: The size of inner air in voxels in x direction ( coming
 *             from check_voxelization_consistency_MMS() function)
 * \param size_y [IN]: The size of inner air in voxels in y direction ( coming
 *             from check_voxelization_consistency_MMS() function)
 * \param size_z [IN]: The size of inner air in voxels in z direction ( coming
 *             from check_voxelization_consistency_MMS() function)
 *
 * \param c_sound [IN]: The sound speed [m/s] - used in MS.
 * \param dX [IN]: The grid spacing [m] - used in MS.
 * \param dT [IN]: The Courant number - used in MS.
 * \param L_xyz [IN]: The room side [m] - used in MS.
 * \param n_xyz [IN]: The mode number - used in MS.
 *
 * \param dev_start_id [IN]: This is for testing on a single node and tells the
 *      starting device_id that will be used (until dev_start_id+GPU_partitions).
 *          E.g., if dev_start_id = 2 and GPU_partitions = 2, then the node
 *          will use [dev_2, dev_3]. You might change this based on MPI_rank.
 *
 * \returns The list with used GPU devices.
 */
std::vector<unsigned int> set_up_Initial_conditions_MES_single(
        CudaMesh* the_mesh,
        unsigned char* h_position_idx,
        unsigned char* h_material_idx,
        uint3 voxelization_dim,
        unsigned int GPU_partitions,
        unsigned int start_voxel_idx,
        unsigned int size_x,
        unsigned int size_y,
        unsigned int size_z,
        double c_sound,
        double dX,
        double dT,
        double P0,
        double L_xyz,
        int n_xyz,
        unsigned int dev_start_id = 0);

/*/////////////////////////////////////////////////////////////////////////////
Function used to read the scene data - prepared in an .hdf5 file instead of
.obj due to python scripting.

    Function needs the hdf5 and hdf5_hl files/libraries.

! The non-array data needs to be created.
! The array data should not be allocated (e.g., '= 0') except mesh_file.
    Remember to delete[] them !
    Remember to allocate mesh_file.

param file_name [IN]: The .hdf5 file that needs to be read.
The following parameters come from the MPI_IO.write_mesh_file_hdf5() function.

param num_triangles [OUT]: Total number of triangles from the mesh.
param num_vertices [OUT]: Total number of vertices from the mesh. Should be
    3*num_triangles.
param num_materials [OUT]: Number of materials. Should be 3*num_triangles
    since the vector comes from Python as a 1D array.

param no_of_used_materials [OUT]: Number of used materials.
param no_of_unique_materials [OUT]: Total number of materials.
                            Should be >= no_of_used_materials
param total_material_coeff_size_ [OUT]: Size of material_coefficients__ 1D
                                    array.
param mesh_file [OUT]: A string with the mesh file. Allocate string beforehand!
Arrays:
param material_coefficients__ [OUT]: Material coefficients 1D vector
    (admittances). This is with 19 zeros between each value to be fed directly
    to Parallel_FDTD library.
        Ex: adm_1, 0,0..0, adm_2, 0,0..0, adm3, ... where size(0,0..0) = 19.

param vertices_ [OUT]: Vertices 1D vector. Size is 3*num_vertices.
param triangles_ [OUT]: Triangles 1D vector. Size is 3*num_triangles.
param materials_ [OUT]: Materials 1D vector. Size is num_materials and NOT
                        3*num_materials. Same size as triangles_
*/
void read_HDF5_Scene_Data(const char *file_name,
            // Non-array data:
            int *num_triangles, int* num_vertices, int* num_materials,
            int* no_of_used_materials, int* no_of_unique_materials,
            long* total_material_coeff_size_,
            char *mesh_file,
            // arrays:
            double*& material_coefficients__,
            float*& vertices_,
            unsigned int*& triangles_,
            unsigned char*& materials_
            );

/*/////////////////////////////////////////////////////////////////////////////
 * Function evaluates the global discrete L2 norm for all grid-points. Note that
 *    it is assumed a regular grid spacing! For non-regular grid spacing, the
 *    norm is different!
 *
 *    Note that the size of the analytical_matrix is assumed to be
 *            [size_x,size_y,size_z].
 *
 *  References:
 *    [1] LeVeque, R. J., "Finite Volume Methods for Hyperbolic Problems",
 *           Cambridge University Press, Cambridge ; New York, (2002), p140
 *    [2] Salari K., Knupp P., "Code Verification by the Method of Manufactured
 *           Solutions", SAND2000-1444, Sandia National Laboratories, Albuquerque,
 *           NM 87185, (2000)
 *
 * \param FDTD_matrix [IN]: The pressure (plus forcing if MMS) calculated from
 *         the FDTD udpate.
 * \param voxelization_dim [IN]: The voxelization dimensions
 *         (see kernels\voxelizationUtils.h).
 * \param start_voxel_idx [IN]: where the inside air starts ( coming from
 *             check_voxelization_consistency_MMS() function)
 * \param size_x [IN]: The size of inner air in voxels in x direction ( coming
 *             from check_voxelization_consistency_MMS() function)
 * \param size_y [IN]: The size of inner air in voxels in y direction ( coming
 *             from check_voxelization_consistency_MMS() function)
 * \param size_z [IN]: The size of inner air in voxels in z direction ( coming
 *             from check_voxelization_consistency_MMS() function)
 * \param analytical_matrix [IN]: The matrix with the analytical result (be it
 *             ES or MS) - assumed size is [size_x,size_y,size_z].
 *
 * \param dX [IN]: The grid spacing [m].
 *
 */
double get_L2_norm_single(
        float* FDTD_matrix,
        uint3 voxelization_dim,
        unsigned int start_voxel_idx,
        unsigned int size_x,
        unsigned int size_y,
        unsigned int size_z,
        float* analytical_matrix,
        double dX);


/*/////////////////////////////////////////////////////////////////////////////
 * Fast function to get the maximum absolute value in a float array.
 *
 * \param vector [IN]: The input array.
 * \param total_size [IN]: The total size of the input array.
 *
 */
inline double get_abs_max_value(float* &vector,
                mesh_size_t total_size){
    double max_value = -1.0;
    for(mesh_size_t i=0; i<total_size; i++){
        if( fabs(*(vector+i)) > max_value){
            max_value = fabs(*(vector+i));
        }
    }
    return max_value;
}

/*/////////////////////////////////////////////////////////////////////////////
 * Fast function to get the maximum absolute value in a double array.
 *
 * \param vector [IN]: The input array.
 * \param total_size [IN]: The total size of the input array.
 *
 */
inline double get_abs_max_value_double(double* &vector,
                mesh_size_t total_size){
    double max_value = -1.0;
    for(mesh_size_t i=0; i<total_size; i++){
        if( fabs(*(vector+i)) > max_value){
            max_value = fabs(*(vector+i));
        }
    }
    return max_value;
}

/*/////////////////////////////////////////////////////////////////////////////
 * Fast function to print a matrix:
 *
 * \param vector [IN]: The input array.
 * \param total_size [IN]: The total size of the input array.
 *
 * Logs at trace level....
 *
 */
template <typename T>
void debug_LOG_matrix_values(T* &matrix,
                unsigned int size_x,
                unsigned int size_y,
                unsigned int size_z)
{
    mesh_size_t size_XY = (mesh_size_t)size_x*(mesh_size_t)size_y;
    for(mesh_size_t idx_z =0; idx_z < size_z; idx_z++)
        for(mesh_size_t idx_y =0; idx_y < size_y; idx_y++)
            for(mesh_size_t idx_x =0; idx_x < size_x; idx_x++)
            {
                log_msg<LOG_TRACE>(L"P[%llu,%llu,%llu] = %f") %idx_x %idx_y %idx_z
                            %*(matrix + idx_z*size_XY + idx_y*size_x + idx_x);
            }
}

/*/////////////////////////////////////////////////////////////////////////////
 * Function used to write relevant data after a code verification convergence
 * study.
 *
 * \param dX_vector [IN]: The grid spacings used in the code verification study.
 * \param lambda_sim_vector [IN]: The lambdas used.
 * \param c_sound_vector [IN]: The sound speed for each lambda. Should have the
 *         same size as lambda_sim_vector.
 * \param Y_x_vector [IN]: The used impedance values.
 * \param L2_norm_vector [IN]: The L2 norm results. It is organized as follows:
 *     First by lambda, then by Y_x, then by dX:
 *         [lambda_1_L2_dX1_Yx1, lambda_1_L2_dX2_Yx1..., lambda_1_L2_dXn_Yx1 ,
 *            lambda_2_L2_dX1_Yx1, lambda_2_L2_dX2_Yx1..., lambda_2_L2_dXn_Yx1
 *                ...
 *                lambda_m_L2_dX1_Yx1, lambda_m_L2_dX2_Yx1..., lambda_m_L2_dXn_Yx1]
 *
 * \param total_steps_vector [IN]: The total number of steps for each
 *         simulation. Same size as L2_norm_vector.
 *             Note that the total_steps includes the step at t=0. Thus, the number
 *             of samples dividing t_sim is (total_steps - 1) - this is Nt and it
 *             doubles every every halving of the grid spacing dX.
 *
 * \param GPU_partitions_vector [IN]: The number of GPU partitions (i.e., CUDA devices)
 *           used for each grid. This may differ from GPU_partitions if too many CUDA
 *           devices are required.
 *
 * \param n_xyz [IN]: The x/y/z mode number.
 *
 * \param t_sim [IN]: Simulation time in [s].
 * \param GPU_partitions [IN]: The number of GPU partitions requested in the
 *         simulation (as input to the program).
 * \param mesh_file [IN]: The mesh .obj file used for the voxelization process.
 * \param file_string [IN]: The name of the hdf5 file that will be created.
 *             Don't use any extension in the name.
 *
 * \param char voxelization_type [IN]: The type of voxelization:
 *                 0 - solid
 *                 1 - surface_6_separating
 *                 2 - surface_conservative
 *
 *     \param use_ExactSolution_flag [IN]: If 1, the exact solution is used. If 0,
 *                 the manufactured solution is used.
 *
 *     \param test_only_interior_flag [IN]: If 1, the interior is tested: HS are
 *     placed next to the boundaries. If 0, the boundary updates are also tested.
 *
 * \param MPI_size [IN]: Used for MPI programs. The total #n of MPI nodes used.
 * \param MPI_rank [IN]: Used for MPI programs. The MPI rank that writes.
 *
 *  The datasets:
        One number:
            /len_dX_vector             - int
            /len_lambda_sim_vector     - int
            /len_Y_x_vector            - int
            /len_L2_norm_vector        - int
            /MPI_size                  - int
            /MPI_rank                  - int
            /t_sim                     - double
            /n_xyz                     - int
            /GPU_partitions            - unsigned int
            /voxelization_type         - char
            /use_ExactSolution_flag    - int
            /test_only_interior_flag   - int

         Vectors:
            /mesh_file                 - const char*
            /dX_vector                 - double[len_dX_vector]
            /lambda_sim_vector         - double[len_lambda_sim_vector]
            /c_sound_vector            - double[len_lambda_sim_vector]
            /Y_x_vector                - double[len_Y_x_vector]
            /L2_norm_vector            - double[len_L2_norm_vector]
            /total_steps_vector        - unsigned int [len_L2_norm_vector]
            /GPU_partitions_vector     - unsigned int [len_dX_vector]
 *
 **/
void write_L2_norm( std::vector<double> dX_vector,
                    std::vector<double> lambda_sim_vector,
                    std::vector<double> c_sound_vector,
                    std::vector<double> Y_x_vector,
                    std::vector<double> L2_norm_vector,
                    std::vector<unsigned int> total_steps_vector,
                    std::vector<unsigned int> GPU_partitions_vector,
                    double t_sim,
                    int n_xyz,
                    unsigned int GPU_partitions,
                    const char* mesh_file,
                    const char* file_string,
                    char voxelization_type,
                    int use_ExactSolution_flag,
                    int test_only_interior_flag,
                    int MPI_size,
                    int MPI_rank
                  );

/*////////////////////////////////////
 * Small helper function that gets a
 * string instead of 1 or 0.
 */
inline
std::string get_bool_string(bool in){
    if (in)
        return std::string("true");
    else
        return std::string("false");
}


/*/////////////////////////////////////////////////////////////////////////////
 * Function retrieves the full FINAL pressure matrix from devices to host,
 * single precision.
 *
 * \param the_mesh [IN]: The CUDA mesh where pressure data resides.
 * \param final_pressure_field_h [OUT]: An allocated float pointer
 *      ( of size the_mesh->getDimXY() * the_mesh->getDimZ() ) on host where
 *      all the data will be copied back.
 *
 */
void get_full_final_pressureToHost(
        CudaMesh* the_mesh,
        float* final_pressure_field_h
        );

/*/////////////////////////////////////////////////////////////////////////////
 * Function pushes the initial conditions (i.e., pressure values at the first
 * two time samples) to the ParallelFDTD CudaMesh class(single precision).
 *  For MPI initial data, the initial conditions are sliced to the part of the
 *  domain available to the current node.
 *
 *  the_mesh->makePartitionFromHost() need to be called before!
 *      In this study functions, this means a call to set_up_mesh_Partitions()
 *      from VV_utils.
 *
 * \param the_mesh [IN/OUT]: The CUDA mesh where pressure data resides.
 *
 * \param current_pressure_field_h [OUT]: An allocated and populated with P[t=0]
 *  float pointer ( of size the_mesh->getDimXY() * the_mesh->getDimZ() ) on
 *   host. All this data will be pushed inside the_mesh, depending on #partitions.
 *
 * \param past_pressure_field_h [OUT]: An allocated and populated with P[t=dT]
 *  float pointer ( of size the_mesh->getDimXY() * the_mesh->getDimZ() ) on
 *   host. All this data will be pushed inside the_mesh, depending on #partitions.
 *
 *
 */
void push_initial_pressure_fields(
        CudaMesh* the_mesh,
        float* current_pressure_field_h,
        float* past_pressure_field_h
        );

/*/////////////////////////////////////////////////////////////////////////////
Function logs the partition indexing: each partion and the start and end
    z-values.

    Log level is LOG_DEBUG.

/param partition_indexing_[IN]: the partition to be printed.
/param MPI_rank[IN]: Mpi rank. If -1, no MPI rank will be displayed.
/param msg_[IN]: Some extra message in the logger print.
*/
void debugging_print_partion_indexing_BE(
        std::vector< std::vector <mesh_size_t> > partition_indexing_,
        int MPI_rank = -1,
        const char* msg_ = "");

#endif /* V_V_VV_UTILS_H_ */

