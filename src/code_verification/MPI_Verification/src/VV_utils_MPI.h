///////////////////////////////////////////////////////////////////////////////
//
// This file is released under the MIT License. See
// http://www.opensource.org/licenses/mit-license.php
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//  Author: Sebastian Prepelita, Aalto University
//  Created on: Jun 8, 2016
//      - updates to mesh_size_t & full HOST voxelization @2017
//
///////////////////////////////////////////////////////////////////////////////
/*
 * VV_utils_MPI.h
 *
 * File with helper function to set-up a simulation for a code verification
 * study - using MPI functions.
 *
 * Different header than VV_utils.h such that the MPI libraries would not be
 * needed for the VV_utils.h functions.
 *
 *
 */

#ifndef V_V_VV_UTILS_MPI_H_
#define V_V_VV_UTILS_MPI_H_

#include "continuous_solutions.h"
#include <vector>
#include "base/SimulationParameters.h"
#include "kernels/cudaMesh.h"

// MES:
/*/////////////////////////////////////////////////////////////////////////////
 * Function sets the sources for the SimulationParameters* to be used in the
 *  simulation for an Exact Solution (ES) in a rigid box. Note the call to
 *  check_voxelization_consistency_MMS should be done in MAIN.
 *
 *  See continous_solutions.h for information about the ES.
 *
 *  MPI version - adds only those sources in the MPI domain.
 *
 * \param parameter_ptr__ [OUT]: Parameter pointer where the sources will be
 * placed.
 *
 * \param MPI_voxelization_dim [IN]: The dimensions from the voxelization
 * process, but for the current MPI domain.
 * \param FULL_voxelization_dim [IN]: The dimensions from the voxelization
 * process, but for the FULL domain.
 *
 * \param fs [IN]: The sampling frequency.
 * \param lambda_ [IN]: The courant number.
 * \param c_sound [IN]: The sound speed.
 * \param dX [IN]: The grid spacing [m]
 * \param dT [IN]: The sampling time [s] . Should be = 1.0/fs
 * \param total_steps [IN]: Total discrete steps including intial conditions.
 * \param CUDA_steps [IN]: Total simulated steps (should be total_steps - 2).
 * \param L_xyz [IN]: ES room dimensions in [m]
 *
 * \param start_voxel_idx [IN]: where the inside air starts ( coming from
 * check_voxelization_consistency_MMS() function) - FULL voxelization
 * \param size_x [IN]: The size of inner air in voxels in x direction ( coming
 * from check_voxelization_consistency_MMS() function) - FULL voxelization
 * \param size_y [IN]: The size of inner air in voxels in y direction ( coming
 * from check_voxelization_consistency_MMS() function) - FULL voxelization
 * \param size_z [IN]: The size of inner air in voxels in z direction ( coming
 * from check_voxelization_consistency_MMS() function) - FULL voxelization
 *
 * \param MPI_rank [IN]: The current MPI rank of MPI domain.
 * \param MPI_partition_indexing_ [IN]: Partitioning of domain in Z direction
Comes from getPartitionIndexing() in cudaMesh.h
 */
void set_up_Parameters_MES_single_MPI(
    SimulationParameters* parameter_ptr__,
    uint3 MPI_voxelization_dim,
    uint3 FULL_voxelization_dim,
    unsigned int fs,
    double lambda_,
    double c_sound,
    double dX,
    double dT,
    unsigned int total_steps,
    unsigned int CUDA_steps, // should be total_steps - 2
    double L_xyz,
    unsigned int start_voxel_idx,
    unsigned int size_x,
    unsigned int size_y,
    unsigned int size_z,
    int MPI_rank,
    const std::vector< std::vector< mesh_size_t >> &MPI_partition_indexing_);


/*/////////////////////////////////////////////////////////////////////////////
 * Function sets the initial conditions for a code verification task - MES (Met
 *  hod of Exact Solution).
 *
 * To be used with set_up_Parameters_MES_single() function.
 *
 *  Call this AFTER the_mesh->setupMesh()! The partitioning is done inside here.
 *
 *  MPI version - sets the initial conditions in the MPI domain.
 *
 * \param the_mesh [IN/OUT]: The CUDA mesh where initial conditions are placed.
 *
 * \param MPI_h_position_idx [IN]: The b_id from a voxelization process for
 *          the current MPI rank. Will be pushed to SimulationParameters.
 * \param MPI_h_material_idx [IN]: The mat_id from a voxelization process for
 *          the current MPI rank. Will be pushed to SimulationParameters.
 *
 * \param MPI_voxelization_dim [IN]: The dimensions from the voxelization
 * process, but for the current MPI domain.
 * \param FULL_voxelization_dim [IN]: The dimensions from the voxelization
 * process, but for the FULL domain.
 *
 * \param GPU_partitions [IN]: The number of GPU_partitions.
 *
 * \param start_voxel_idx [IN]: where the inside air starts ( coming from
 * check_voxelization_consistency_MMS() function)
 * \param size_x [IN]: The size of inner air in voxels in x direction ( coming
 * from check_voxelization_consistency_MMS() function)
 * \param size_y [IN]: The size of inner air in voxels in y direction ( coming
 * from check_voxelization_consistency_MMS() function)
 * \param size_z [IN]: The size of inner air in voxels in z direction ( coming
 * from check_voxelization_consistency_MMS() function)
 *
 * \param c_sound [IN]: The sound speed [m/s] - used in MS.
 * \param dX [IN]: The grid spacing [m] - used in MS.
 * \param dT [IN]: The Courant number - used in MS.
 * \param L_xyz [IN]: The room side [m] - used in MS.
 * \param n_xyz [IN]: The mode number - used in MS.
 *
 * \param MPI_rank [IN]: The current MPI rank of MPI domain.
 * \param MPI_partition_indexing_ [IN]: Partitioning of domain in Z direction
 * Comes from getPartitionIndexing() in cudaMesh.h
 *
 * \param dev_start_id [IN]: This is for testing on a single node and tells the
 *      starting device_id that will be used (until dev_start_id+GPU_partitions).
 *          E.g., if dev_start_id = 2 and GPU_partitions = 2, then the node
 *          will use [dev_2, dev_3]. You might change this based on MPI_rank.
 *
 * \returns The list with used GPU devices.
 */
std::vector<unsigned int> set_up_Initial_conditions_MES_single_MPI(
    CudaMesh* the_mesh,
    unsigned char* MPI_h_position_idx,
    unsigned char* MPI_h_material_idx,
    uint3 MPI_voxelization_dim,
    uint3 FULL_voxelization_dim,
    unsigned int GPU_partitions,
    unsigned int start_voxel_idx,
    unsigned int size_x,
    unsigned int size_y,
    unsigned int size_z,
    double c_sound,
    double dX,
    double dT,
    double P0,
    double L_xyz,
    int n_xyz,
    int MPI_rank,
    const std::vector< std::vector< mesh_size_t >> &MPI_partition_indexing_,
    unsigned int dev_start_id = 0);

/*/////////////////////////////////////////////////////////////////////////////
 * Function sets the sources for the SimulationParameters* to be used in the
 *  simulation for a Manufactured Solution (MS) in a box with absorbing
 *  boundaries. Note the call to check_voxelization_consistency_MMS should be
 *  done in MAIN.
 *
 *  See continous_solutions.h for information about the MS.
 *
 *  MPI version - adds only those sources in the MPI domain.
 *
 * \param MPI_h_postition_idx_ [IN]: The h_position_idx_ in HOST memory.
 * Used for the scaling of the soft source.
 *
 * \param mem_size [IN]: The total amount of memory in MPI_d_postition_idx_.
 *
 * \param parameter_ptr__ [OUT]: Parameter pointer where the sources will be
 * placed.
 *
 * \param MPI_voxelization_dim [IN]: The dimensions from the voxelization
 * process, but for the current MPI domain.
 * \param FULL_voxelization_dim [IN]: The dimensions from the voxelization
 * process, but for the FULL domain.
 *
 * \param fs [IN]: The sampling frequency.
 * \param lambda_ [IN]: The Courant number = c_sound*dT/dX.
 * \param c_sound [IN]: The sound speed.
 * \param dX [IN]: The grid spacing [m]
 * \param dT [IN]: The sampling time [s] . Should be = 1.0/fs
 * \param total_steps [IN]: Total discrete steps including intial conditions.
 * \param CUDA_steps [IN]: Total simulated steps (should be total_steps - 2).
 * \param P0 [IN]: MS amplitude
 * \param Y_xyz [IN]: MS specific acoustic admittance
 * \param L_xyz [IN]: MS room dimensions in [m]
 * \param n_xyz [IN]: MS mode number
 * \param phi_xyz [IN]: MS spatial mode phase
 * \param test_only_interior_flag [IN]: Boolean: if true, no boundary update is
 * tested: HS with the MS are placed on the outer edges instead. Otherwise,
 * all the update is tested.
 *
 * \param start_voxel_idx [IN]: where the inside air starts ( coming from
 * check_voxelization_consistency_MMS() function) - FULL voxelization
 * \param size_x [IN]: The size of inner air in voxels in x direction ( coming
 * from check_voxelization_consistency_MMS() function) - FULL voxelization
 * \param size_y [IN]: The size of inner air in voxels in y direction ( coming
 * from check_voxelization_consistency_MMS() function) - FULL voxelization
 * \param size_z [IN]: The size of inner air in voxels in z direction ( coming
 * from check_voxelization_consistency_MMS() function) - FULL voxelization
 *
 * \param MPI_rank [IN]: The current MPI rank of MPI domain.
 * \param MPI_partition_indexing_ [IN]: Partitioning of domain in Z direction
Comes from getPartitionIndexing() in cudaMesh.h
 */
void set_up_Parameters_MMS_single_MPI(
    unsigned char* MPI_h_postition_idx_, // on host
    mesh_size_t mem_size,
    SimulationParameters* parameter_ptr__,
    uint3 MPI_voxelization_dim,
    uint3 FULL_voxelization_dim,
    unsigned int fs,
    double lambda_,
    double c_sound,
    double dX,
    double dT,
    unsigned int total_steps,
    unsigned int CUDA_steps, // should be total_steps - 2
    double P0,
    double Y_xyz,
    double L_xyz,
    int n_xyz,
    double phi_xyz,
    bool test_only_interior_flag,
    unsigned int start_voxel_idx,
    unsigned int size_x,
    unsigned int size_y,
    unsigned int size_z,
    int MPI_rank,
    const std::vector< std::vector< mesh_size_t >> &MPI_partition_indexing_);

/*/////////////////////////////////////////////////////////////////////////////
 * Function sets the initial conditions for a code verification task - MMS (Met
 *  hod of Manufactured Solution).
 *
 * To be used with set_up_Parameters_MMS_single() function.
 *
 *  Call this AFTER the_mesh->setupMesh()! The partitioning is done inside here.
 *
 * \param the_mesh [IN/OUT]: The CUDA mesh where initial conditions are placed.
 *
 * \param MPI_h_position_idx [IN]: The b_id from a voxelization process for
 *          the current MPI rank. Will be pushed to SimulationParameters.
 * \param MPI_h_material_idx [IN]: The mat_id from a voxelization process for
 *          the current MPI rank. Will be pushed to SimulationParameters.
 *
 * \param MPI_voxelization_dim [IN]: The dimensions from the voxelization
 * process, but for the current MPI domain.
 * \param FULL_voxelization_dim [IN]: The dimensions from the voxelization
 * process, but for the FULL domain.
 *
 * \param GPU_partitions [IN]: The number of GPU_partitions.
 *
 * \param start_voxel_idx [IN]: where the inside air starts ( coming from
 * check_voxelization_consistency_MMS() function)
 * \param size_x [IN]: The size of inner air in voxels in x direction ( coming
 * from check_voxelization_consistency_MMS() function)
 * \param size_y [IN]: The size of inner air in voxels in y direction ( coming
 * from check_voxelization_consistency_MMS() function)
 * \param size_z [IN]: The size of inner air in voxels in z direction ( coming
 * from check_voxelization_consistency_MMS() function)
 *
 * \param c_sound [IN]: The sound speed [m/s] - used in MS.
 * \param dX [IN]: The grid spacing [m] - used in MS.
 * \param dT [IN]: The Courant number - used in MS.
 * \param Y_xyz [IN]: The spec. acoustic admittance on walls - used in MS.
 * \param L_xyz [IN]: The room side [m] - used in MS.
 * \param n_xyz [IN]: The mode number - used in MS.
 * \param phi_xyz [IN]: The MS modal phase - used in MS.
 *
 * \param dev_start_id [IN]: This is for testing on a single node and tells the
 *      starting device_id that will be used (until dev_start_id+GPU_partitions).
 *          E.g., if dev_start_id = 2 and GPU_partitions = 2, then the node
 *          will use [dev_2, dev_3]. You might change this based on MPI_rank.
 *
 * \returns The list with used GPU devices.
 */
std::vector<unsigned int> set_up_Initial_conditions_MMS_single_MPI(
    CudaMesh* the_mesh,
    unsigned char* MPI_h_position_idx,
    unsigned char* MPI_h_material_idx,
    uint3 MPI_voxelization_dim,
    uint3 FULL_voxelization_dim,
    unsigned int GPU_partitions,
    unsigned int start_voxel_idx,
    unsigned int size_x,
    unsigned int size_y,
    unsigned int size_z,
    double c_sound,
    double dX,
    double dT,
    double P0,
    double Y_xyz,
    double L_xyz,
    int n_xyz,
    double phi_xyz,
    int MPI_rank,
    const std::vector< std::vector< mesh_size_t >> &MPI_partition_indexing_,
    unsigned int dev_start_id = 0);


/*/////////////////////////////////////////////////////////////////////////////
 * Function evaluates the global discrete L2-norm^2 for all grid-points inside
 * the MPI sub-domain: only the points in the sub-domain are considered in the
 * norm. This means points between MPI_partition_Z_start_idx and
 * MPI_partition_Z_end_idx.
 * Note that it is assumed a regular grid spacing! For non-regular grid
 * spacing, the norm is different!
 *
 *Note that the size of the analytical_matrix is assumed to be
 *[size_x,size_y, some_MPI_size_z].
 *
 *  References:
 *    [1] LeVeque, R. J., "Finite Volume Methods for Hyperbolic Problems",
 *       Cambridge University Press, Cambridge ; New York, (2002), p140
 *    [2] Salari K., Knupp P., "Code Verification by the Method of Manufactured
 *       Solutions", SAND2000-1444, Sandia National Laboratories, Albuquerque,
 *       NM 87185, (2000)
 *
 * \param MPI_FDTD_matrix [IN]: The pressure (plus forcing if MMS) calculated from
 * the FDTD udpate inside an MPI subdomain.
 * \param MPI_voxelization_dim [IN]: The voxelization dimensions of the MPI sub-
 * domain (see kernels\voxelizationUtils.h).
 * \param start_voxel_idx [IN]: where the inside air starts ( coming from
 * check_voxelization_consistency_MMS() function)
 * \param size_x [IN]: The size of inner air in voxels in x direction ( coming
 * from check_voxelization_consistency_MMS() function)
 * \param size_y [IN]: The size of inner air in voxels in y direction ( coming
 * from check_voxelization_consistency_MMS() function)
 * \param size_z [IN]: The size of inner air in voxels in z direction ( coming
 * from check_voxelization_consistency_MMS() function)
 * \param analytical_matrix [IN]: The matrix with the analytical result (be it
 * ES or MS) - assumed size is [size_x,size_y,size_z].
 *
 * \param dX [IN]: The grid spacing [m].
 *
 * \param MPI_partition_Z_start_idx [IN]: Where the relevant MPI domain index
 * starts.
 * \param MPI_partition_Z_end_idx [IN]: Where the relevant MPI domain index
 *ends.
 * \param MPI_rank [IN]: The current MPI rank of MPI domain.
 * \param MPI_partition_indexing_ [IN]: Partitioning of domain in Z direction
 *Comes from getPartitionIndexing() in cudaMesh.h
 *
 */
double get_L2_square_norm_single_MPI(
float* MPI_FDTD_matrix,
uint3 MPI_voxelization_dim,
unsigned int start_voxel_idx,
unsigned int size_x,
unsigned int size_y,
unsigned int size_z,
float* analytical_matrix,
double dX,
unsigned int MPI_partition_Z_start_idx,
unsigned int MPI_partition_Z_end_idx,
int MPI_rank,
const std::vector< std::vector< mesh_size_t >> &MPI_partition_indexing_
);

#endif /* V_V_VV_UTILS_MPI_H_ */
