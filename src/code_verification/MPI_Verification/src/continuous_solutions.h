///////////////////////////////////////////////////////////////////////////////
//
// This file is released under the MIT License. See
// http://www.opensource.org/licenses/mit-license.php
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//  Author: Sebastian Prepelita, Aalto University
//  Created on: May 30, 2016
//      - updates to mesh_size_t @2017
//
///////////////////////////////////////////////////////////////////////////////
/*
 * continuous_solutions.h
 *
 *    Various continuous functions necessary for MES (Method of Exact Solution)
 *    and MMS (Method of Manufactured Solution).
 *
 */
#include "logger.h"
#include "kernels/cudaUtils.h"

#ifndef V_V_CONTINUOUS_SOLUTIONS_H_
#define V_V_CONTINUOUS_SOLUTIONS_H_

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

/*/////////////////////////////////////////////////////////////////////////////
 * Function evaluates a manufacture solution for a cubic box with all walls
 * absorbing of specific acoustic impedance of Y_xyz.
 *
 *     See [] for more information.
 *
 * The current function calculates the manufactured solution for an entire
 * matrix at the given time t.
 *
 * Function should be used with get_Forcing_BOX_ALL_Walls_abs() for the forcing
 * term.
 *
 * Note that FDTD pressure updates are considered at the center of the cells.
 * For MMS calculated for a time period and fixed location, see function
 * get_Manufactured_solution_BOX_ALL_Walls_abs_TimeVector().
 *
 *  Function logs only errors.
 *
 * \param p_MMS [OUT]: The matrix where the result will be overwritten. The
 *         matrix needs to have a Fortran style indexing.
 * \param N_x [IN]: The number of voxels per dimension. Should be L_xyz/dX.
 * \param dX [IN]: The grid spacing [m].
 * \param t [IN]: The continuous time when the MS (Manufactured Solution) is
 *         evaluated. Given in [seconds].
 * \param P0 [IN]: The amplitude of the MS.
 * \param Y_xyz [IN]: The specific acoustic admittance for all the walls.
 * \param n_xyz [IN]: The spatial mode number in x/y/z direction (see MS )
 * \param L_xyz [IN]: The side of the room cube [m].
 * \param phi_xyz [IN]:  The phase of the spatial mode in x/y/z directions
 *                 [radians]. pi/4 is chosen for ease of calculation.
 * \param c_sound: The sound speed in [m/s].
 */
template <typename T>
void get_Manufactured_solution_BOX_ALL_Walls_abs(
                        T* p_MMS,
                        unsigned int N_x,
                        double dX,
                        double t,
                        double P0,
                        double Y_xyz,
                        int n_xyz = 1,
                        double L_xyz = 1.0,
                        double phi_xyz = M_PI/4.0,
                        double c_sound = 343.39
                        ){
    // Basic checks:
    if (Y_xyz < 0.0){
        log_msg<LOG_ERROR>(L"Admittance cannot be negative! [continuous "
            "solutions - get_Manufactured_solution_BOX_ALL_Walls_abs()]");
        exit(EXIT_FAILURE);
    }
    if (c_sound < 0.0){
        log_msg<LOG_ERROR>(L"Speed of sound cannot be negative! [continuous "
            "solutions - get_Manufactured_solution_BOX_ALL_Walls_abs()]");
        exit(EXIT_FAILURE);
    }

    if (Y_xyz > 1.0){
        log_msg<LOG_WARNING>(L"! You are using an unphysical admittance! "
            "Admittance should be at most 1.0 !");
    }

    // k_n:
    double k_n = (c_sound / Y_xyz) * ( ((double)n_xyz*M_PI - 2.0*phi_xyz ) / L_xyz ) *
            tan(phi_xyz);
    double x_ = 0.0, y_ = 0.0, z_ = 0.0;

    unsigned int N_y = N_x, N_z = N_x;
    // Set up matrix:
    mesh_size_t size_XY = (mesh_size_t)N_x*(mesh_size_t)N_y;
    for(mesh_size_t idx_z =0; idx_z < N_z; idx_z++)
        for(mesh_size_t idx_y =0; idx_y < N_y; idx_y++)
            for(mesh_size_t idx_x =0; idx_x < N_x; idx_x++)
            {
                x_ = dX * (0.5 + idx_x);
                y_ = dX * (0.5 + idx_y);
                z_ = dX * (0.5 + idx_z);
                p_MMS[ idx_z*size_XY + idx_y*(mesh_size_t)N_x + idx_x] = P0 * exp(-k_n * t) *
                    cos(x_*( (double)n_xyz*M_PI - 2.0*phi_xyz )/L_xyz + phi_xyz ) *
                    cos(y_*( (double)n_xyz*M_PI - 2.0*phi_xyz )/L_xyz + phi_xyz ) *
                    cos(z_*( (double)n_xyz*M_PI - 2.0*phi_xyz )/L_xyz + phi_xyz );
            }
};

/*/////////////////////////////////////////////////////////////////////////////
 * Function evaluates the forcing corresponding to a manufactured solution for
 * a cubic box with all walls absorbing of specific acoustic impedance of Y_xyz
 * (see function get_Manufactured_solution_BOX_ALL_Walls_abs() ).
 *
 *     See [] for more information.
 *
 * The current function calculates the forcing for an entire
 * matrix at the given time t.
 *
 * Function should be used with get_Forcing_BOX_ALL_Walls_abs() for the forcing
 * term.
 *
 * Note that FDTD pressure updates are considered at the center of the cells.
 * For MMS calculated for a time period and fixed location, see function
 * get_Manufactured_solution_BOX_ALL_Walls_abs_TimeVector().
 *
 *  Function logs only errors.
 *
 * \param f_MMS [OUT]: The matrix where the result will be overwritten. The
 *         matrix needs to have a Fortran style indexing.
 *
 * \param MPI_h_postition_idx_[IN]: The position idx matrix on the host.
 * \param MPI_voxelization_dim_x[IN]: The voxelization dimensions - x direction.
 * \param MPI_voxelization_dim_y[IN]: The voxelization dimensions - y direction.
 * \param MPI_voxelization_dim_z[IN]: The voxelization dimensions - z direction.
 * \param lambda_sim[IN]: The Courant number for the simulation.
 *
 * \param N_x [IN]: The number of voxels per dimension. Should be L_xyz/dX.
 *
 * Indexing IN parameters used to get the number of boundary neighbors.
 * \param MPI_temp_z_idx[IN]: The MPI z_index (where the source will be placed).
 *                 This should include the start_voxel_idx.
 * \param start_voxel_idx[IN]: The start voxel where the interior is.
 *
 * \param dX [IN]: The grid spacing [m].
 * \param dT [IN]: The sampling time  = 1/fs [s].
 * \param t [IN]: The continuous time when the MS (Manufactured Solution) is
 *         evaluated. Given in [seconds].
 * \param P0 [IN]: The amplitude of the MS.
 * \param Y_xyz [IN]: The specific acoustic admittance for all the walls.
 * \param n_xyz [IN]: The spatial mode number in x/y/z direction (see MS )
 * \param L_xyz [IN]: The side of the room cube [m].
 * \param phi_xyz [IN]:  The phase of the spatial mode in x/y/z directions
 *                 [radians]. pi/4 is chosen for ease of calculation.
 * \param c_sound: The sound speed in [m/s].
 */
template <typename T>
void get_Forcing_BOX_ALL_Walls_abs(
                    T* f_MMS,
                    unsigned char * MPI_h_postition_idx_,
                    unsigned int MPI_voxelization_dim_x,
                    unsigned int MPI_voxelization_dim_y,
                    unsigned int MPI_voxelization_dim_z,
                    double lambda_sim,
                    unsigned int N_x,
                    unsigned int MPI_temp_z_idx,
                    unsigned int start_voxel_idx,
                    double dX,
                    double dT,
                    double t,
                    double P0,
                    double Y_xyz,
                    int n_xyz = 1,
                    double L_xyz = 1.0,
                    double phi_xyz = M_PI/4,
                    double c_sound = 343.39){
    // Basic checks:
    if (Y_xyz < 0.0){
        log_msg<LOG_ERROR>(L"Admittance cannot be negative! [continuous "
            "solutions - get_Forcing_BOX_ALL_Walls_abs()]");
        exit(EXIT_FAILURE);
    }
    if (c_sound < 0.0){
        log_msg<LOG_ERROR>(L"Speed of sound cannot be negative! [continuous "
            "solutions - get_Forcing_BOX_ALL_Walls_abs()]");
        exit(EXIT_FAILURE);
    }
    if (Y_xyz > 1.0){
        log_msg<LOG_WARNING>(L"! You are using an unphysical admittance! "
            "Admittance should be at most 1.0 !");
    }

    // k_n:
    double k_n = (c_sound / Y_xyz)*( ((double)n_xyz*M_PI - 2.0*phi_xyz ) / L_xyz )*
            tan(phi_xyz);
    double x_ = 0.0, y_ = 0.0, z_ = 0.0;
    unsigned int N_y = N_x, N_z = N_x;

    // Forcing constant and scaling:
    double forcing_constant = (k_n*k_n+3.0*(c_sound*((double)n_xyz*M_PI-2.0*phi_xyz)
            /L_xyz)*(c_sound*((double)n_xyz*M_PI - 2.0*phi_xyz)/L_xyz) );
    // Apply the discrete scaling:[applied twice? Error...]
    //forcing_constant = dT*dT*forcing_constant;

    //////////////////////////////
    // Apply the discrete scaling:
    //////////////////////////////////
    forcing_constant = dT*dT*forcing_constant;
    // Apply scaling based on voxelization (this one contains the voxelization
    //boundaries compared to size_XY below-to be used with MPI_h_postition_idx_)
    mesh_size_t MPI_size_XY = (mesh_size_t)MPI_voxelization_dim_x*
                                (mesh_size_t)MPI_voxelization_dim_y;
    // Getting number of boundary neighbors:
    unsigned char Nb;

    // Set up matrix:
    mesh_size_t size_XY = (mesh_size_t)N_x * (mesh_size_t)N_y;
    for(mesh_size_t idx_z =0; idx_z < N_z; idx_z++)
        for(mesh_size_t idx_y =0; idx_y < N_y; idx_y++)
            for(mesh_size_t idx_x =0; idx_x < N_x; idx_x++){
                x_ = dX * (0.5 + idx_x);
                y_ = dX * (0.5 + idx_y);
                z_ = dX * (0.5 + idx_z);
                Nb = 134 - MPI_h_postition_idx_[(mesh_size_t)MPI_temp_z_idx*MPI_size_XY +
                              ((mesh_size_t)start_voxel_idx+N_y)*MPI_voxelization_dim_x +
                              ((mesh_size_t)start_voxel_idx+N_x)];
    // The voxel is not a solid AND the voxel is not just air (avoids 1.0*...)
                if ((Nb != 134) && (Nb != 0)){
                    f_MMS[ idx_z*size_XY + idx_y*N_x + idx_x]= P0*exp(-k_n * t)*
                        cos(x_*( (double)n_xyz*M_PI - 2.0*phi_xyz )/L_xyz + phi_xyz ) *
                        cos(y_*( (double)n_xyz*M_PI - 2.0*phi_xyz )/L_xyz + phi_xyz ) *
                        cos(z_*( (double)n_xyz*M_PI - 2.0*phi_xyz )/L_xyz + phi_xyz ) *
                        forcing_constant / (1.0 + lambda_sim/2.0*Nb*Y_xyz);
                }
                else
                    f_MMS[ idx_z*size_XY + idx_y*N_x + idx_x]= P0*exp(-k_n * t)*
                        cos(x_*( (double)n_xyz*M_PI - 2.0*phi_xyz )/L_xyz + phi_xyz ) *
                        cos(y_*( (double)n_xyz*M_PI - 2.0*phi_xyz )/L_xyz + phi_xyz ) *
                        cos(z_*( (double)n_xyz*M_PI - 2.0*phi_xyz )/L_xyz + phi_xyz ) *
                        forcing_constant;
            }
}


/*/////////////////////////////////////////////////////////////////////////////
 * Function evaluates the forcing corresponding to a manufactured solution for
 * a point (in time and space) for a cubic box with all walls absorbing of
 * specific acoustic impedance of Y_xyz (see function
 * get_Manufactured_solution_BOX_ALL_Walls_abs() ).
 *
 *    See [] for more information.
 *
 *  The current function calculates the exact solution for a single discrete
 *   point [N_x+0.5, N_y+0.5, N_z+0.5]*dX.
 *
 *  Note that FDTD pressure updates are considered at the center of the cells.
 *
 *  Function logs only errors.
 *
 * \param MPI_h_postition_idx_[IN]: The position idx matrix on the host.
 * \param MPI_voxelization_dim_x[IN]: The voxelization dimensions - x direction.
 * \param MPI_voxelization_dim_y[IN]: The voxelization dimensions - y direction.
 * \param MPI_voxelization_dim_z[IN]: The voxelization dimensions - z direction.
 * \param lambda_sim[IN]: The Courant number for the simulation.
 *
 * \param N_x [IN]: The voxel index in x direction where sol is evaluated.
 * \param N_y [IN]: The voxel index in y direction where sol is evaluated.
 * \param N_z [IN]: The voxel index in z direction where sol is evaluated.
 *
 * Indexing IN parameters used to get the number of boundary neighbors.
 * \param MPI_temp_z_idx[IN]: The MPI z_index (where the source will be placed).
 *                 This should include the start_voxel_idx.
 * \param start_voxel_idx[IN]: The start voxel where the interior is.
 *
 * \param dX [IN]: The grid spacing [m].
 * \param dT [IN]: The sampling time [s].
 * \param t [IN]: The continuous time when the MS (Manufactured Solution) is
 *         evaluated. Given in [seconds].
 * \param P0 [IN]: The amplitude of the MS.
 * \param Y_xyz [IN]: The specific acoustic admittance for all the walls.
 * \param n_xyz [IN]: The spatial mode number in x/y/z direction (see MS )
 * \param L_xyz [IN]: The side of the room cube [m].
 * \param phi_xyz [IN]:  The phase of the spatial mode in x/y/z directions
 *                 [radians]. pi/4 is chosen for ease of calculation.
 * \param c_sound: The sound speed in [m/s].
 */
inline double get_Forcing_BOX_ALL_Walls_abs_Point(
                    unsigned char * MPI_h_postition_idx_,
                    unsigned int MPI_voxelization_dim_x,
                    unsigned int MPI_voxelization_dim_y,
                    unsigned int MPI_voxelization_dim_z,
                    double lambda_sim,
                    unsigned int N_x,
                    unsigned int N_y,
                    unsigned int N_z,
                    unsigned int MPI_temp_z_idx,
                    unsigned int start_voxel_idx,
                    double dX,
                    double dT,
                    double t,
                    double P0,
                    double Y_xyz,
                    int n_xyz = 1,
                    double L_xyz = 1.0,
                    double phi_xyz = M_PI/4,
                    double c_sound = 343.39){
    // k_n:
    double k_n = (c_sound / Y_xyz)*( ((double)n_xyz*M_PI - 2.0*phi_xyz ) / L_xyz )*
            tan(phi_xyz);
    double x_ = 0.0, y_ = 0.0, z_ = 0.0;

    // Forcing constant and scaling:
    double forcing_constant = ( k_n*k_n + 3.0*(c_sound*((double)n_xyz*M_PI-2.0*phi_xyz)
            /L_xyz)*(c_sound*((double)n_xyz*M_PI - 2.0*phi_xyz)/L_xyz) );
    //////////////////////////////
    // Apply the discrete scaling:
    //////////////////////////////////
    forcing_constant = dT*dT*forcing_constant;
    // Apply scaling based on voxelization:
    mesh_size_t size_XY = (mesh_size_t)MPI_voxelization_dim_x *
                            (mesh_size_t)MPI_voxelization_dim_y;
    // Getting number of boundary neighbors:
    unsigned char Nb = 134 - MPI_h_postition_idx_[(mesh_size_t)MPI_temp_z_idx*size_XY +
                              ((mesh_size_t)start_voxel_idx+N_y)*(mesh_size_t)MPI_voxelization_dim_x +
                              ((mesh_size_t)start_voxel_idx+N_x)];
    // The voxel is not a solid AND the voxel is not just air (avoids 1.0*...)
    if ((Nb != 134) && (Nb != 0)){
        forcing_constant = forcing_constant / (1.0 + lambda_sim/2.0*Nb*Y_xyz);
    }

    x_ = dX * (0.5 + N_x);
    y_ = dX * (0.5 + N_y);
    z_ = dX * (0.5 + N_z);

    return P0 * exp(-k_n * t) *
            cos(x_*( (double)n_xyz*M_PI - 2.0*phi_xyz )/L_xyz + phi_xyz ) *
            cos(y_*( (double)n_xyz*M_PI - 2.0*phi_xyz )/L_xyz + phi_xyz ) *
            cos(z_*( (double)n_xyz*M_PI - 2.0*phi_xyz )/L_xyz + phi_xyz ) *
            forcing_constant;
}

/*/////////////////////////////////////////////////////////////////////////////
 * Function evaluates the MS for a cubic box with all walls absorbing of
 * specific acoustic impedance of Y_xyz
 * (see function get_Manufactured_solution_BOX_ALL_Walls_abs() ).
 *
 *    See [] for more information.
 *
 *    Function similar to get_Manufactured_solution_BOX_ALL_Walls_abs(), but it
 *    populates a vector with the manufactured solution (MS) corresponding to the
 *    continuous position [(Nx+0.5)*dX,(Ny+0.5)*dX,(Nz+0.5)*dX] during time
 *    [t_start_sample*dT, t_end_sample*dT].
 *
 *    See get_Manufactured_solution_BOX_ALL_Walls_abs() for more details and
 *    remaining parameters.
 *
 * Note that FDTD pressure updates are considered at the center of the cells.
 *
 *  Function logs only errors.
 *
 * \param p_MMS_TimeVector [OUT]: The vector where the result will be overwritten.
 * \param N_x [IN]: The voxel where solution is evaluated. x dimension.
 * \param N_y [IN]: The voxel where solution is evaluated. y dimension.
 * \param N_z [IN]: The voxel where solution is evaluated. z dimension.
 * \param t_start_sample [IN]: The starting time sample at which the forcing
 *         term is evaluated.
 * \param t_end_sample [IN]: The last time sample at which the forcing
 *         term is evaluated.
 * \param dX [IN]: The grid spacing [m].
 * \param dT [IN]: The sampling time  = 1/fs [s].
 * \param t [IN]: The continuous time when the MS (Manufactured Solution) is
 *         evaluated. Given in [seconds].
 * \param P0 [IN]: The amplitude of the MS.
 * \param Y_xyz [IN]: The specific acoustic admittance for all the walls.
 * \param n_xyz [IN]: The spatial mode number in x/y/z direction (see MS )
 * \param L_xyz [IN]: The side of the room cube [m].
 * \param phi_xyz [IN]:  The phase of the spatial mode in x/y/z directions
 *                 [radians]. pi/4 is chosen for ease of calculation.
 * \param c_sound: The sound speed in [m/s].
 */
template <typename T>
void get_Manufactured_solution_BOX_ALL_Walls_abs_TimeVector(
        T* p_MMS_TimeVector,
        unsigned int N_x,
        unsigned int N_y,
        unsigned int N_z,
        unsigned int t_start_sample,
        unsigned int t_end_sample,
        double dX,
        double dT,
        double P0,
        double Y_xyz,
        int n_xyz = 1,
        double L_xyz = 1.0,
        double phi_xyz = M_PI/4,
        double c_sound = 343.39){
    // Basic checks:
    if (Y_xyz < 0.0){
        log_msg<LOG_ERROR>(L"Admittance cannot be negative! ["
        "continuous_solutions.h @l395 - "
            "get_Manufactured_solution_BOX_ALL_Walls_abs_TimeVector()]");
        exit(EXIT_FAILURE);
    }
    if (c_sound < 0.0){
        log_msg<LOG_ERROR>(L"Speed of sound cannot be negative! ["
        "continuous_solutions.h @l401 - "
            "get_Manufactured_solution_BOX_ALL_Walls_abs_TimeVector()]");
        exit(EXIT_FAILURE);
    }
    if (Y_xyz > 1.0){
        log_msg<LOG_WARNING>(L"! You are using an unphysical admittance! "
            "Admittance should be at most 1.0 !");
    }
    if (t_start_sample > t_end_sample || t_start_sample<0 || t_end_sample < 0)
    {
        log_msg<LOG_ERROR>(L"The time sample limits are wrong! Double check "
          "this! [continuous_solutions.h @l411 - "
            "get_Manufactured_solution_BOX_ALL_Walls_abs_TimeVector()]\n"
               "t_start_sample = %u \n t_end_sample = %u") %t_start_sample
                   %t_end_sample;
        exit(EXIT_FAILURE);
    }
    // k_n:
    double k_n = (c_sound / Y_xyz) * ( ((double)n_xyz*M_PI - 2.0*phi_xyz ) / L_xyz ) *
            tan(phi_xyz);
    double x_ = dX*(0.5 + N_x);
    double y_ = dX*(0.5 + N_y);
    double z_ = dX*(0.5 + N_z);
    double t_ = 0.0;

    // Set up matrix:
    for(unsigned int idx_t =t_start_sample; idx_t <= t_end_sample; idx_t++){
        t_ = dT * idx_t;
        p_MMS_TimeVector[ idx_t - t_start_sample ] = P0 * exp(-k_n * t_) *
                cos(x_*( (double)n_xyz*M_PI - 2.0*phi_xyz )/L_xyz + phi_xyz ) *
                cos(y_*( (double)n_xyz*M_PI - 2.0*phi_xyz )/L_xyz + phi_xyz ) *
                cos(z_*( (double)n_xyz*M_PI - 2.0*phi_xyz )/L_xyz + phi_xyz );
    }
}

/*/////////////////////////////////////////////////////////////////////////////
 * Function evaluates the forcing for a Manufactured Solution (MS) for a cubic
 * box with all walls absorbing of specific acoustic impedance of Y_xyz
 * (see function get_Manufactured_solution_BOX_ALL_Walls_abs() ).
 *
 *    See [] for more information.
 *
 *    Function similar to get_Forcing_BOX_ALL_Walls_abs(), but it
 *    populates a vector with the manufactured solution (MS) corresponding to the
 *    continuous position [(Nx+0.5)*dX,(Ny+0.5)*dX,(Nz+0.5)*dX] during time
 *    [t_start_sample*dT, t_end_sample*dT].
 *
 *    See get_Manufactured_solution_BOX_ALL_Walls_abs() for more details and
 *    remaining parameters.
 *
 *  Function logs only errors.
 *
 * \param f_MMS_TimeVector [OUT]: The vector where the result will be overwritten.
 * \param MPI_h_postition_idx_[IN]: The position idx matrix on the host.
 * \param MPI_voxelization_dim_x[IN]: The voxelization dimensions - x direction.
 * \param MPI_voxelization_dim_y[IN]: The voxelization dimensions - y direction.
 * \param MPI_voxelization_dim_z[IN]: The voxelization dimensions - z direction.
 * \param lambda_sim[IN]: The Courant number for the simulation.
 * \param N_x [IN]: The voxel where solution is evaluated. x dimension.
 * \param N_y [IN]: The voxel where solution is evaluated. y dimension.
 * \param N_z [IN]: The voxel where solution is evaluated. z dimension.
 *
 * Indexing IN parameters used to get the number of boundary neighbors.
 * \param MPI_temp_z_idx[IN]: The MPI z_index (where the source will be placed).
 *                 This should include the start_voxel_idx.
 * \param start_voxel_idx[IN]: The start voxel where the interior is.
 *
 * \param t_start_sample [IN]: The starting time sample at which the forcing
 *         term is evaluated.
 * \param t_end_sample [IN]: The last time sample at which the forcing
 *         term is evaluated.
 * \param dX [IN]: The grid spacing [m].
 * \param dT [IN]: The sampling time  = 1/fs [s].
 * \param t [IN]: The continuous time when the MS (Manufactured Solution) is
 *         evaluated. Given in [seconds].
 * \param P0 [IN]: The amplitude of the MS.
 * \param Y_xyz [IN]: The specific acoustic admittance for all the walls.
 * \param n_xyz [IN]: The spatial mode number in x/y/z direction (see MS )
 * \param L_xyz [IN]: The side of the room cube [m].
 * \param phi_xyz [IN]:  The phase of the spatial mode in x/y/z directions
 *                 [radians]. pi/4 is chosen for ease of calculation.
 * \param c_sound: The sound speed in [m/s].
 */
template <typename T>
void get_Forcing_BOX_ALL_Walls_abs_TimeVector(
        T* f_MMS_TimeVector,
        unsigned char * MPI_h_postition_idx_,
        unsigned int MPI_voxelization_dim_x,
        unsigned int MPI_voxelization_dim_y,
        unsigned int MPI_voxelization_dim_z,
        double lambda_sim,
        unsigned int N_x,
        unsigned int N_y,
        unsigned int N_z,
        unsigned int MPI_temp_z_idx,
        unsigned int start_voxel_idx,
        unsigned int t_start_sample,
        unsigned int t_end_sample,
        double dX,
        double dT,
        double P0,
        double Y_xyz,
        int n_xyz = 1,
        double L_xyz = 1.0,
        double phi_xyz = M_PI/4,
        double c_sound = 343.39){
    // Basic checks:
    if (Y_xyz < 0.0){
        log_msg<LOG_ERROR>(L"Admittance cannot be negative! [ "
          "continuous_solutions.h @l508 - "
            "get_Forcing_BOX_ALL_Walls_abs_TimeVector()]");
        exit(EXIT_FAILURE);
    }
    if (c_sound < 0.0){
        log_msg<LOG_ERROR>(L"Speed of sound cannot be negative! ["
          "continuous_solutions.h @l514 - "
            "get_Forcing_BOX_ALL_Walls_abs_TimeVector()]");
        exit(EXIT_FAILURE);
    }
    if (Y_xyz > 1.0){
        log_msg<LOG_WARNING>(L"! You are using an unphysical admittance! "
            "Admittance should be at most 1.0 !");
    }
    if (t_start_sample > t_end_sample || t_start_sample<0 || t_end_sample < 0)
    {
        log_msg<LOG_ERROR>(L"The time sample limits are wrong! Double check "
          "this! [continuous_solutions.h @l524 - "
            "get_Forcing_BOX_ALL_Walls_abs_TimeVector()]\nt_start_sample = %u"
               " \n t_end_sample = %u") %t_start_sample %t_end_sample;
        exit(EXIT_FAILURE);
    }
    // k_n:
    double k_n = (c_sound / Y_xyz) * ( ((double)n_xyz*M_PI - 2.0*phi_xyz ) / L_xyz ) *
            tan(phi_xyz);
    double x_ = dX*(0.5 + N_x);
    double y_ = dX*(0.5 + N_y);
    double z_ = dX*(0.5 + N_z);
    double t_ = 0.0;

    // Forcing constant and scaling:
    double forcing_constant = ( k_n*k_n + 3.0*(c_sound*(n_xyz*M_PI -
            2.0*phi_xyz)/L_xyz)*(c_sound*(n_xyz*M_PI - 2.0*phi_xyz)/L_xyz) );
    //////////////////////////////
    // Apply the discrete scaling:
    //////////////////////////////////
    forcing_constant = dT*dT*forcing_constant;
    // Apply scaling based on voxelization:
    mesh_size_t size_XY = (mesh_size_t)MPI_voxelization_dim_x *
                            (mesh_size_t)MPI_voxelization_dim_y;
    // Getting number of boundary neighbors:
    unsigned char Nb = 134 - MPI_h_postition_idx_[(mesh_size_t)MPI_temp_z_idx*size_XY +
              ((mesh_size_t)start_voxel_idx+N_y)*(mesh_size_t)MPI_voxelization_dim_x +
                          ((mesh_size_t)start_voxel_idx+N_x)];
    // The voxel is not a solid AND the voxel is not just air (avoids 1.0*...)
    if ((Nb != 134) && (Nb != 0)){
        forcing_constant = forcing_constant / (1.0 + lambda_sim/2.0*Nb*Y_xyz);
    }
    // Set up matrix:
    //////////////////////////////////
    for(unsigned int idx_t =t_start_sample; idx_t < t_end_sample; idx_t++){
        t_ = dT * idx_t;
        f_MMS_TimeVector[ idx_t -t_start_sample ] = P0 * exp(-k_n * t_) *
                cos(x_*( (double)n_xyz*M_PI - 2.0*phi_xyz )/L_xyz + phi_xyz ) *
                cos(y_*( (double)n_xyz*M_PI - 2.0*phi_xyz )/L_xyz + phi_xyz ) *
                cos(z_*( (double)n_xyz*M_PI - 2.0*phi_xyz )/L_xyz + phi_xyz ) *
                forcing_constant;
    }
}

/*/////////////////////////////////////////////////////////////////////////////
 * Function evaluates an exact solution in a box with rigid walls.
 *
 * The solution is:
 *
 *   p(x,y,z,t) = P0*cos(omega_n*t)*cos(n_x*pi*x/Lx)*cos(n_y*pi*y/Ly)*
 *               cos(n_z*pi*z/Lz)
 *
 *     And: omega_n = pi*c_sound* sqrt( (n_x*pi/Lx)^2 + (n_y*pi/Ly)^2 +
 *                 (n_z*pi/Lz)^2 )
 *
 *  The current function calculates the exact solution for an entire matrix at
 *   the given time t.
 *
 *  Note that FDTD pressure updates are considered at the center of the cells.
 *
 *  Function logs only errors.
 *
 * \param p_MES [OUT]: The matrix where the result will be overwritten. The
 *         matrix needs to have a Fortran style indexing.
 * \param N_x [IN]: The number of voxels per dimension (x direction).
 *                 Should be L_x/dX.
 * \param N_y [IN]: The number of voxels per dimension (y direction).
 *                 Should be L_y/dX.
 * \param N_z [IN]: The number of voxels per dimension (z direction).
 *                 Should be L_z/dX.
 * \param dX [IN]: The grid spacing [m].
 * \param t [IN]: The continuous time when the MS (Manufactured Solution) is
 *         evaluated. Given in [seconds].
 * \param P0 [IN]: The amplitude of the MS.
 * \param n_x [IN]: The spatial mode number in x direction (see ES )
 * \param n_y [IN]: The spatial mode number in y direction (see ES )
 * \param n_z [IN]: The spatial mode number in z direction (see ES )
 * \param L_x [IN]: The side of the room in x direction [m].
 * \param L_y [IN]: The side of the room in y direction [m].
 * \param L_z [IN]: The side of the room in z direction [m].
 * \param c_sound: The sound speed in [m/s].
 */
template <typename T>
void get_Exact_solution_BOX_Rigid(
        T* p_MES,
        unsigned int N_x,
        unsigned int N_y,
        unsigned int N_z,
        double dX,
        double t,
        double P0,
        int n_x = 1,
        int n_y = 1,
        int n_z = 1,
        double L_x = 1.0,
        double L_y = 1.0,
        double L_z = 1.0,
        double c_sound = 343.39){
    if (c_sound < 0.0){
        log_msg<LOG_ERROR>(L"Speed of sound cannot be negative! [continuous "
                "solutions - get_Exact_solution_BOX_Rigid()]");
        exit(EXIT_FAILURE);
    }
    // omega_n:
    double omega_n = c_sound * M_PI * sqrt( ((double)n_x/L_x)*((double)n_x/L_x) +
                                             ((double)n_y/L_y)*((double)n_y/L_y) +
                                             ((double)n_z/L_z)*((double)n_z/L_z)
                                           );
    double x_ = 0.0, y_ = 0.0, z_ = 0.0;
    // Set up matrix:
    mesh_size_t size_XY = (mesh_size_t)N_x * (mesh_size_t)N_y;
    for(mesh_size_t idx_z =0; idx_z < N_z; idx_z++)
        for(mesh_size_t idx_y =0; idx_y < N_y; idx_y++)
            for(mesh_size_t idx_x =0; idx_x < N_x; idx_x++)
            {
                x_ = dX * (0.5 + idx_x);
                y_ = dX * (0.5 + idx_y);
                z_ = dX * (0.5 + idx_z);
                p_MES[ idx_z*size_XY + idx_y*N_x + idx_x] = P0 *
                                cos( omega_n * t) *
                                cos(x_* M_PI*n_x/L_x ) *
                                cos(y_* M_PI*n_y/L_y ) *
                                cos(z_* M_PI*n_z/L_z );
            }
}

/*/////////////////////////////////////////////////////////////////////////////
 * Function evaluates an exact solution in a box with rigid walls.
 *
 * The solution is:
 *
 *   p(x,y,z,t) = P0*cos(omega_n*t)*cos(n_x*pi*x/Lx)*cos(n_y*pi*y/Ly)*
 *               cos(n_z*pi*z/Lz)
 *
 *     And: omega_n = pi*c_sound* sqrt( (n_x*pi/Lx)^2 + (n_y*pi/Ly)^2 +
 *                 (n_z*pi/Lz)^2 )
 *
 *  The current function calculates the exact solution for a single discrete
 *   point [N_x+0.5, N_y+0.5, N_z+0.5]*dX.
 *
 *  Note that FDTD pressure updates are considered at the center of the cells.
 *
 *  Function logs only errors.
 *
 * \param N_x [IN]: The voxel index in x direction where sol is evaluated.
 * \param N_y [IN]: The voxel index in y direction where sol is evaluated.
 * \param N_z [IN]: The voxel index in z direction where sol is evaluated.
 *
 * \param dX [IN]: The grid spacing [m].
 * \param t [IN]: The continuous time when the MS (Manufactured Solution) is
 *         evaluated. Given in [seconds].
 * \param P0 [IN]: The amplitude of the MS.
 * \param n_x [IN]: The spatial mode number in x direction (see ES )
 * \param n_y [IN]: The spatial mode number in y direction (see ES )
 * \param n_z [IN]: The spatial mode number in z direction (see ES )
 * \param L_x [IN]: The side of the room in x direction [m].
 * \param L_y [IN]: The side of the room in y direction [m].
 * \param L_z [IN]: The side of the room in z direction [m].
 * \param c_sound: The sound speed in [m/s].
 */
inline double get_Exact_solution_BOX_Rigid_Point(
        unsigned int N_x,
        unsigned int N_y,
        unsigned int N_z,
        double dX,
        double t,
        double P0,
        int n_x = 1,
        int n_y = 1,
        int n_z = 1,
        double L_x = 1.0,
        double L_y = 1.0,
        double L_z = 1.0,
        double c_sound = 343.39){
    // omega_n:
    double omega_n = c_sound * M_PI * sqrt( ((double)n_x/L_x)*((double)n_x/L_x) +
                                             ((double)n_y/L_y)*((double)n_y/L_y) +
                                             ((double)n_z/L_z)*((double)n_z/L_z)
                                           );
    double x_ = dX*(0.5 + N_x);
    double y_ = dX*(0.5 + N_y);
    double z_ = dX*(0.5 + N_z);
    return P0*cos( omega_n * t)*cos(x_* M_PI*n_x/L_x )*cos(y_* M_PI*n_y/L_y )*
                cos(z_* M_PI*n_z/L_z );
}

/*/////////////////////////////////////////////////////////////////////////////
 * Function evaluates a manufacture solution for a cubic box with all walls
 * absorbing of specific acoustic impedance of Y_xyz.
 *
 *     See [] for more information.
 *
 * The current function calculates the exact solution for a single discrete
 *   point [N_x+0.5, N_y+0.5, N_z+0.5]*dX.
 *
 * Note that FDTD pressure updates are considered at the center of the cells.
 * For MMS calculated for a time period and fixed location, see function
 * get_Manufactured_solution_BOX_ALL_Walls_abs_TimeVector().
 *
 *  Function logs only errors.
 *
 * \param N_x [IN]: The voxel index in x direction where sol is evaluated.
 * \param N_y [IN]: The voxel index in y direction where sol is evaluated.
 * \param N_z [IN]: The voxel index in z direction where sol is evaluated.
 *
 * \param dX [IN]: The grid spacing [m].
 * \param t [IN]: The continuous time when the MS (Manufactured Solution) is
 *         evaluated. Given in [seconds].
 * \param P0 [IN]: The amplitude of the MS.
 * \param Y_xyz [IN]: The specific acoustic admittance for all the walls.
 * \param n_xyz [IN]: The spatial mode number in x/y/z direction (see MS )
 * \param L_xyz [IN]: The side of the room cube [m].
 * \param phi_xyz [IN]:  The phase of the spatial mode in x/y/z directions
 *                 [radians]. pi/4 is chosen for ease of calculation.
 * \param c_sound: The sound speed in [m/s].
 */
inline double get_Manufactured_solution_BOX_ALL_Walls_abs_Point(
        unsigned int N_x,
        unsigned int N_y,
        unsigned int N_z,
        double dX,
        double P0,
        double t,
        double Y_xyz,
        int n_xyz = 1,
        double L_xyz = 1.0,
        double phi_xyz = M_PI/4,
        double c_sound = 343.39){
    // k_n:
    double k_n = (c_sound / Y_xyz) * ( ((double)n_xyz*M_PI - 2.0*phi_xyz ) / L_xyz ) *
            tan(phi_xyz);
    double x_ = dX*(0.5 + N_x);
    double y_ = dX*(0.5 + N_y);
    double z_ = dX*(0.5 + N_z);

    // Return value:
    return P0 * exp(-k_n * t) *
            cos(x_*( (double)n_xyz*M_PI - 2.0*phi_xyz )/L_xyz + phi_xyz ) *
            cos(y_*( (double)n_xyz*M_PI - 2.0*phi_xyz )/L_xyz + phi_xyz ) *
            cos(z_*( (double)n_xyz*M_PI - 2.0*phi_xyz )/L_xyz + phi_xyz );
}

#endif /* V_V_CONTINUOUS_SOLUTIONS_H_ */
