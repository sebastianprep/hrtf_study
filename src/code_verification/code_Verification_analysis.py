'''
This file is released under the MIT License. See
http://www.opensource.org/licenses/mit-license.php

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Created on Jun 21, 2018

    Module used to study the results of the code-verification routine (ran on the clusters). For least squares,
    the Statsmodel package[Seabold, Skipper, and Josef Perktold. "Statsmodels: Econometric and statistical 
    modeling with python." Proceedings of the 9th Python in Science Conference. 2010] is used.
    
    Remember for each branch to create a folder structure as:
        Root_dir/
            MES/
            MMS_Boudnaries/
            MMS_interior/
    
    and place in each folder the result from the code_verification_exercise. 
    
    Use figure_1_code_Verification() for the full result or plot_L2_error_MPI() for partial evaluations of code verification.
            
@author: Sebastian Prepelita
    Aalto University School of Science
'''
import warnings
import numpy
import os
import time
import h5py
import re
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib.cm as cmx
import statsmodels.api as sm
from statsmodels.sandbox.regression.predstd import wls_prediction_std

jet = cm = plt.get_cmap('plasma'); # 'viridis', 'inferno', 'plasma' (BEST), 'magma'
cNorm  = colors.Normalize(vmin=0, vmax=1.0);
scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet);

def check_VV_MPI_file_consistency(file_name_, use_ExactSolution_flag, use_ExactSolution_flag_, test_only_interior_flag, test_only_interior_flag_,\
                                      dX_vector, dX_vector_, lambda_sim_vector, lambda_sim_vector_, c_sound_vector, c_sound_vector_, \
                                      Y_x_vector, Y_x_vector_, total_steps_vector, total_steps_vector_, t_sim, t_sim_, \
                                      n_xyz, n_xyz_, GPU_partitions, GPU_partitions_, voxelization_type, voxelization_type_,
                                      mesh_file, mesh_file_, MPI_size, MPI_size_,
                                      GPU_partitions_vector, GPU_partitions_vector_):
    '''
    Function compares for equality all the input variables without '_' with those with the '_' postfix.
    '''
    if use_ExactSolution_flag != use_ExactSolution_flag_:
        raise NameError("use_ExactSolution_flag of file " + file_name_ + " is different (" + str(use_ExactSolution_flag_) + ") than in the first file (" + str(use_ExactSolution_flag) + ")");
    if test_only_interior_flag != test_only_interior_flag_:
        raise NameError("test_only_interior_flag of file " + file_name_ + " is different (" + str(test_only_interior_flag_) + ") than in the first file (" + str(test_only_interior_flag) + ")");
    if not numpy.array_equal( dX_vector, dX_vector_ ):
        raise NameError("dX_vector of file " + file_name_ + " is different (" + str(dX_vector_) + ") than in the first file (" + str(dX_vector) + ")");
    if not numpy.array_equal( GPU_partitions_vector, GPU_partitions_vector_ ):
        warnings.warn("\n\nGPU_partitions_vector of file " + file_name_ + " is different \n " + 
                      str(GPU_partitions_vector_) + "\n than in the first file R0" + str(GPU_partitions_vector) + "\n This can be an error, but can also "+
                      "occur due to non-equal distributions of the FULL voxelization domain.");
    if not numpy.array_equal( lambda_sim_vector, lambda_sim_vector_ ):
        raise NameError("lambda_sim_vector of file " + file_name_ + " is different (" + str(lambda_sim_vector_) + ") than in the first file (" + str(lambda_sim_vector) + ")");    
    if not numpy.array_equal( c_sound_vector, c_sound_vector_ ):
        raise NameError("c_sound_vector of file " + file_name_ + " is different (" + str(c_sound_vector_) + ") than in the first file (" + str(c_sound_vector) + ")");
    if not numpy.array_equal( Y_x_vector, Y_x_vector_ ):
        raise NameError("Y_x_vector of file " + file_name_ + " is different (" + str(Y_x_vector_) + ") than in the first file (" + str(Y_x_vector) + ")");
    if not numpy.array_equal( total_steps_vector, total_steps_vector_ ):
        raise NameError("total_steps_vector of file " + file_name_ + " is different (" + str(total_steps_vector_) + ") than in the first file (" + str(total_steps_vector) + ")");
    if t_sim != t_sim_:
        raise NameError("t_sim of file " + file_name_ + " is different (" + str(t_sim_) + ") than in the first file (" + str(t_sim) + ")");
    if n_xyz != n_xyz_:
        raise NameError("n_xyz of file " + file_name_ + " is different (" + str(n_xyz_) + ") than in the first file (" + str(n_xyz) + ")");
    if GPU_partitions != GPU_partitions_:
        raise NameError("GPU_partitions of file " + file_name_ + " is different (" + str(GPU_partitions_) + ") than in the first file (" + str(GPU_partitions) + ")");
    if voxelization_type != voxelization_type_:
        raise NameError("voxelization_type of file " + file_name_ + " is different (" + str(voxelization_type_) + ") than in the first file (" + str(voxelization_type) + ")");
    if mesh_file != mesh_file_:
        raise NameError("mesh_file of file " + file_name_ + " is different (" + str(mesh_file_) + ") than in the first file (" + str(mesh_file) + ")");
    if MPI_size != MPI_size_:
        raise NameError("MPI_size of file " + file_name_ + " is different (" + str(MPI_size_) + ") than in the first file (" + str(MPI_size) + ")");

def read_CSC_convergence_single_file(file_name, reading_dir = "code_Verif_results"):
    '''
    Function used to read the data dumped by the CSC code validation C++ program.
    
    :param file_name: The file to be read.
    :param reading_dir: The reading directory - default is CSC_results.
    '''
    f = h5py.File (reading_dir + os.sep + file_name, 'r');
    # Get group keys:
    dataset_info = f.items();
    dataset_keys = [];
    for i_ in range ( len(dataset_info)):
        if isinstance(dataset_info[i_][1], h5py.Dataset):
            dataset_keys.append(dataset_info[i_][0]);
    # Reading data:
    MPI_size = f['MPI_size'][0];
    MPI_rank = f['MPI_rank'][0];
    
    dX_vector = f['dX_vector'][...];
    mesh_file = str(f['mesh_file'][...]);
    lambda_sim_vector = f['lambda_sim_vector'][...];
    c_sound_vector = f['c_sound_vector'][...];
    Y_x_vector = f['Y_x_vector'][...];
    L2_norm_vector = f['L2_norm_vector'][...];
    total_steps_vector = f['total_steps_vector'][...];
    t_sim = f['t_sim'][0];
    n_xyz = f['n_xyz'][0];
    GPU_partitions = f['GPU_partitions'][0];
    if "GPU_partitions_vector" in dataset_keys:
        GPU_partitions_vector = f['GPU_partitions_vector'][...];
    else:
        GPU_partitions_vector = None;
    voxelization_type = f['voxelization_type'][0];
    use_ExactSolution_flag = f['use_ExactSolution_flag'][0];
    test_only_interior_flag = f['test_only_interior_flag'][0];
    f.close();
    last_modified_str = str(time.ctime(os.path.getmtime( os.path.join(reading_dir, file_name)))) ;
    return use_ExactSolution_flag, test_only_interior_flag, dX_vector, lambda_sim_vector, c_sound_vector, Y_x_vector, L2_norm_vector,\
        total_steps_vector, t_sim, n_xyz, GPU_partitions, GPU_partitions_vector, voxelization_type, mesh_file, MPI_rank, MPI_size, last_modified_str;

def read_CSC_MPI_convergence_data(reading_dir = "code_Verif_results" + os.sep + "MPI"):
    '''
    Function reads all the files inside reading_dir and gets the packed data.
    Most functionality taken from IO/MPI_IO.py module
    
    :param reading_dir: The directory where all the hdf5 files are read.
    '''
    files_reading_dir = os.listdir(reading_dir);
    files_reading_dir_filtered = [f for f in files_reading_dir if re.match(r'R.*' + '_MPI_code_V_' +'.*\.hdf5', f)];
    read_file_idx = 0;
    ranks = [];
    total_rec_azimuths = [];
    print "\n\tReading V&V data from dir: " + str(reading_dir) + ":\n\t===========================\n"
    for file_name_ in files_reading_dir_filtered:
        print "\t[" + str(read_file_idx) + "] Reading file '" + file_name_ + "'";
        print "\t\t Last Modified: " + str(time.ctime(os.path.getmtime( os.path.join(reading_dir, file_name_)))) ;
        if read_file_idx == 0:
            use_ExactSolution_flag, test_only_interior_flag, dX_vector, lambda_sim_vector, c_sound_vector, Y_x_vector, L2_norm_squared_vector,\
            total_steps_vector, t_sim, n_xyz, GPU_partitions, GPU_partitions_vector, voxelization_type, mesh_file, MPI_rank, MPI_size, _ = read_CSC_convergence_single_file(file_name = file_name_, reading_dir = reading_dir);
            ranks.append(MPI_rank);
            #print "  DEBUG -> L2_norm^2 file_idx = 0: " + str(L2_norm_squared_vector);
        else:
            use_ExactSolution_flag_, test_only_interior_flag_, dX_vector_, lambda_sim_vector_, c_sound_vector_, Y_x_vector_, L2_norm_squared_vector_,\
            total_steps_vector_, t_sim_, n_xyz_, GPU_partitions_, GPU_partitions_vector_, voxelization_type_, mesh_file_, MPI_rank_, MPI_size_, _ = read_CSC_convergence_single_file(file_name = file_name_, reading_dir = reading_dir);
            ranks.append(MPI_rank_);
            # Checks:
            ###########
            check_VV_MPI_file_consistency(file_name_, use_ExactSolution_flag, use_ExactSolution_flag_, test_only_interior_flag, test_only_interior_flag_,\
                                          dX_vector, dX_vector_, lambda_sim_vector, lambda_sim_vector_, c_sound_vector, c_sound_vector_, \
                                          Y_x_vector, Y_x_vector_, total_steps_vector, total_steps_vector_, t_sim, t_sim_, \
                                          n_xyz, n_xyz_, GPU_partitions, GPU_partitions_, voxelization_type, voxelization_type_,
                                          mesh_file, mesh_file_, MPI_size, MPI_size_, GPU_partitions_vector, GPU_partitions_vector_);
            #print "  DEBUG -> L2_norm^2 file_idx = " + str(read_file_idx) + ": " + str(L2_norm_squared_vector_);
            # Add to the L2 norm now:
            L2_norm_squared_vector = L2_norm_squared_vector + L2_norm_squared_vector_;
        read_file_idx = read_file_idx + 1;
    L2_norm_vector_final = numpy.sqrt(L2_norm_squared_vector);
    return use_ExactSolution_flag, test_only_interior_flag, dX_vector, lambda_sim_vector, c_sound_vector, Y_x_vector, L2_norm_vector_final,\
            total_steps_vector, t_sim, n_xyz, GPU_partitions, GPU_partitions_vector, voxelization_type, mesh_file, ranks, MPI_size;

def plot_L2_error_MPI(extra_dir = None, file_name = None, reading_dir = "code_Verif_results" + os.sep + "MPI", verbose_flag = False):
    '''
    Function used to read multiple files with L2 norm from an MPI code run
    and then plot the convergence rates for different lambdas, impedances and dX_vectors.
    
    Function reads all the files inside reading_dir/extra_dir.
    
    :param extra_dir: If not None, then all the files in reading_dir/extra_dir are read.
                Otherwise, only the files inside reading_dir are read.
    :param file_name: If not None, only one file will be read. Otherwise, the entire directory.
    :param reading_dir: The reading directory where file_name is.
    :param verbose_flag: Boolean. If True, the L2 norm values are printed.
    '''
    
    if not extra_dir is None:
        reading_dir = reading_dir + os.sep + extra_dir
    if file_name is not None:
        title_prefix = file_name + " : ";
        use_ExactSolution_flag, test_only_interior_flag, dX_vector, lambda_sim_vector, c_sound_vector, Y_x_vector, L2_norm_squared_vector,\
            total_steps_vector, t_sim, n_xyz, GPU_partitions, GPU_partitions_vector, voxelization_type, mesh_file, ranks, MPI_size,\
                last_modified_str = read_CSC_convergence_single_file(file_name = file_name, reading_dir = reading_dir);
        print "Read file " + file_name + ", last modified = " + last_modified_str
        L2_norm_vector = numpy.sqrt(L2_norm_squared_vector);
    else:
        title_prefix = reading_dir + "\\*.hdf5 : ";
        use_ExactSolution_flag, test_only_interior_flag, dX_vector, lambda_sim_vector, c_sound_vector, Y_x_vector, L2_norm_vector,\
                total_steps_vector, t_sim, n_xyz, GPU_partitions, GPU_partitions_vector, voxelization_type, mesh_file, ranks, MPI_size =  read_CSC_MPI_convergence_data(reading_dir = reading_dir);
    
    print "dX_vector = " + str(dX_vector);
    print "Y_x_vector = " + str(Y_x_vector);
    print "L2_norm_vector = " + str(L2_norm_vector);
    print "------------------------------------------------------"
    print "Used exact solution? " + str( "TRUE" if use_ExactSolution_flag else "FALSE" );
    print "Interior? " + str( "TRUE" if test_only_interior_flag else "FALSE" );
    print "--------------------"
    print "  GPU_partitions = " + str(GPU_partitions);
    print "  GPU_partitions_vector = " + str(GPU_partitions_vector);
    print "  MPI_size = " + str(MPI_size) + "    ( ranks : " + str(ranks) + "  )";
    print "------------------------------------------------------"
    
    dx_vector_ideal = [0.32, 0.16, 0.08, 0.04, 0.02, 0.01, 0.005];
    ideal_1st_oder = numpy.float64(dx_vector_ideal);
    ideal_2nd_oder = numpy.power(ideal_1st_oder,2);
    plt.clf();
    line_1st_order, = plt.loglog(dx_vector_ideal, ideal_1st_oder, color = 'k', label = 'Ideal 1st order');
    line_2nd_order, = plt.loglog(dx_vector_ideal, ideal_2nd_oder, color = '0.60', label = 'Ideal 2nd order');
    
    dX_points = len(dX_vector);
    Y_x_points = len(Y_x_vector);
    # Scale the ideal curves:
    #scaling = L2_norm_vector[0] / ideal_1st_oder[1];
    #ideal_1st_oder = ideal_1st_oder * scaling;
    #scaling = L2_norm_vector[0] / ideal_2nd_oder[1];# +dX_points
    #ideal_2nd_oder = ideal_2nd_oder * scaling;
    ideal_1st_oder = ideal_1st_oder / 1E2;
    ideal_2nd_oder = ideal_2nd_oder / 1E2;
    line_1st_order.set_ydata(ideal_1st_oder);
    line_2nd_order.set_ydata(ideal_2nd_oder);

    
    for lambda_idx in range(len(lambda_sim_vector)):
        for Y_idx in range(len(Y_x_vector)):
            c_sound = c_sound_vector[lambda_idx];
            lambda_sim = lambda_sim_vector[lambda_idx];
            
            Y_x = Y_x_vector[Y_idx];
            L2_error_vector = L2_norm_vector[(lambda_idx*Y_x_points*dX_points + Y_idx*dX_points): ( lambda_idx*Y_x_points*dX_points + (Y_idx+1)*dX_points)];
            if verbose_flag:
                print " lambda | Y_x | dX = " + str(dX_vector);
                print "l=" + str(lambda_sim) + " | Y=" + str(Y_x) + " | L2_norm = " + str(L2_error_vector);
            colorVal = scalarMap.to_rgba(Y_x);
            plt.loglog(dX_vector, L2_error_vector, label = r'$\epsilon_{L2}$ [$Y_{spec}=' + str(Y_x) + ', \lambda = ' + str(numpy.round(lambda_sim,2)) + '$]', color = colorVal, marker="o", markersize=3);
    text_ = "";
    if use_ExactSolution_flag:
        text_ = text_ + "MES";
    else:
        text_ = text_ + "MMS";
        if test_only_interior_flag:
            text_ = text_ + " (interior) "
        else:
            text_ = text_ + " (boundary) "
    plt.title(title_prefix + "Code verif - " + text_ + "; single prec. " + r"; $n_{xyz} = " + str(n_xyz) + "$ ; $t_{sim} = " + str(numpy.round(t_sim*1E3,2)) + "$ [ms].");
    plt.xlabel('dX [m]');
    plt.ylabel('Global Error at time = ' + str(numpy.round(t_sim*1E3,2)) + " [ms]");
    plt.xlim(numpy.min(dx_vector_ideal), numpy.max(dx_vector_ideal));
    plt.grid(True, which="both");
    plt.ylim(1E-8, 1.0);
    plt.xlim((1-0.1)*1E-2, 0.2);
    plt.legend(loc='best', borderaxespad=1., fancybox=True, shadow=True, fontsize= 9);
    plt.show();



# The final code-verification plot:

def figure_1_code_Verification(show_interior_plot = True, verbose_flag = False, reading_dir = "..\\"):
    '''
    Function that plots convergence for the code verification
    
    :param show_interior_plot: Boolean. If True, the interior (2nd order) plot is done. Otherwise, the boundary plot is done.
    :param verbose_flag: Boolean. If True, a lot of prints are done with dX vector, Y_x vector etc.
    '''
    dx_vector_ideal = [0.32, 0.16, 0.08, 0.04, 0.02, 0.01, 0.005];
    ideal_1st_oder = numpy.float64(dx_vector_ideal);
    ideal_2nd_oder = numpy.power(ideal_1st_oder,2);
    
    plt.figure(1, figsize=(6, 6), dpi=80, facecolor='w', edgecolor='k');
    axes = plt.gca();
    
    lengend_lines = [];
    lengend_texts = [];
    
    line_1st_order, = plt.loglog(dx_vector_ideal, ideal_1st_oder, color = 'k', label = r'$1^{st}$ order', linewidth = 2.5);
    lengend_lines.append( line_1st_order );
    lengend_texts.append( r'Ideal $1^{st}$ order' );
    line_2nd_order, = plt.loglog(dx_vector_ideal, ideal_2nd_oder, color = '0.55', label = r'$2^{nd}$ order', linewidth = 2.5);
    lengend_lines.append( line_2nd_order );
    lengend_texts.append( r'Ideal $2^{nd}$ order' );
    
    #markers = ['o','v','^','<','>','s','+','*'];
    markers = ['o','o','o','o','o','o','o','s','*'];
    # Colormap:
    #total_no_lines = 8;
    jet = cm = plt.get_cmap('plasma'); # 'viridis', 'inferno', 'plasma' (BEST), 'magma'
    cNorm  = colors.Normalize(vmin=0, vmax=1.0);
    scalarMap = cmx.ScalarMappable(norm=cNorm, cmap=jet);
    
    #===========================================================================
    #------------------------------------------  First, the MES in a rigid box:
    #===========================================================================
    print "\nREADING MES MPI files.................\n"
        
    reading_dir_MES = reading_dir + os.sep + "MES"; 
    
    use_ExactSolution_flag, test_only_interior_flag, dX_vector, lambda_sim_vector, c_sound_vector, Y_x_vector, L2_norm_vector,\
        total_steps_vector, t_sim, n_xyz, GPU_partitions, GPU_partitions_vector, voxelization_type, mesh_file, ranks, MPI_size =  read_CSC_MPI_convergence_data(reading_dir = reading_dir_MES);
    dX_points = len(dX_vector);
    Y_x_points = len(Y_x_vector);
    
    # Linear regression:
    X = sm.add_constant(numpy.log10(dX_vector));
    
    if verbose_flag:
        print "\n"
        print "\t MES dX_vector = " + str(dX_vector);
        print "\t MES Y_x_vector = " + str(Y_x_vector);
        print "\t MES L2_norm_vector = " + str(L2_norm_vector);
        print "\t MES ------------------------------------------------------"
        print "\t MES Used exact solution? " + str( "TRUE" if use_ExactSolution_flag else "FALSE" );
        print "\t MES Interior? " + str( "TRUE" if test_only_interior_flag else "FALSE" );
        print "\t MES --------------------"
        print "\t MES   GPU_partitions = " + str(GPU_partitions);
        print "\t MES   GPU_partitions_vector = " + str(GPU_partitions_vector);
        print "\t MES   MPI_size = " + str(MPI_size) + "    ( ranks : " + str(ranks) + "  )";
        print "\t MES ------------------------------------------------------"
    str_OLS = "";
    line_idx = 0;
    for lambda_idx in range(len(lambda_sim_vector)):
        for Y_idx in range(len(Y_x_vector)):
            c_sound = c_sound_vector[lambda_idx];
            lambda_sim = lambda_sim_vector[lambda_idx];
            
            Y_x = Y_x_vector[Y_idx];
            L2_error_vector = L2_norm_vector[(lambda_idx*Y_x_points*dX_points + Y_idx*dX_points): ( lambda_idx*Y_x_points*dX_points + (Y_idx+1)*dX_points)];
            if verbose_flag:
                print "\t\t------"
                print "\t\tdX_points = " + str(dX_points)
                print "\t\tY_x_points = " + str(Y_x_points)
                print "\t\tFROM sample " + str((lambda_idx*Y_x_points*dX_points + Y_idx*dX_points)) + " to sample " + str(( lambda_idx*Y_x_points*dX_points + (Y_idx+1)*dX_points));
                print "\t\t lambda | Y_x | dX = " + str(dX_vector);
                print "\t\tl=" + str(lambda_sim) + " | Y=" + str(Y_x) + " | L2_norm = " + str(L2_error_vector);
            ##################################################
            # Fit (Ordinary Least Squares) and summary:
            model = sm.OLS(numpy.log10(L2_error_vector), X);
            results = model.fit();
            prstd, iv_l, iv_u = wls_prediction_std(results);
            model_params = results.params;
            slope_ = model_params[1];
            rsquared_adj_ = results.rsquared_adj;
            str_OLS = str_OLS + "\t beta = "  + str(Y_x) + ' \t q_obs = ' + str(numpy.round(slope_,2)) + '\t R^2 =' + str(numpy.round(rsquared_adj_,3)) + '\n'; 
            
            #label_text = r'$\epsilon_{l^2}$ [$Y_{b}=' + str(Y_x) + '$]';# + ', \lambda = ' + str(numpy.round(lambda_sim,2))
            label_text = r'$\beta=' + str(Y_x) + '$';# + ' ($q_{O}=' + str(numpy.round(slope_,2)) + '$)';  
            # Color:
            colorVal = scalarMap.to_rgba(Y_x);
            line, = plt.loglog(dX_vector, L2_error_vector, color = colorVal, marker=markers[line_idx], markersize=4, linewidth = 1.8);
            
            lengend_lines.append( line );
            lengend_texts.append( label_text );
            
            line_idx = line_idx + 1;
            
    # Scale the ideal curves:
    ###################################################
    #scaling = L2_norm_vector[4] / ideal_1st_oder[5];# Last point
    scaling = L2_norm_vector[0] / ideal_1st_oder[1];# First point
    scaling = scaling  + 7*1E-3;
    ideal_1st_oder = ideal_1st_oder * scaling;
    #scaling = L2_norm_vector[4] / ideal_2nd_oder[5];# +dX_points
    scaling = L2_norm_vector[0] / ideal_2nd_oder[1];# First point
    scaling = scaling + 3*1E-2;
    ideal_2nd_oder = ideal_2nd_oder * scaling;
    line_1st_order.set_ydata(ideal_1st_oder);
    line_2nd_order.set_ydata(ideal_2nd_oder);
    ##########################################################
    #===========================================================================
    #------------------------------------------  Second, the MMS Interior
    #===========================================================================
    if show_interior_plot:
        reading_dir_MES = reading_dir + os.sep + "MMS_Interior"; 
        
        use_ExactSolution_flag, test_only_interior_flag, dX_vector, lambda_sim_vector, c_sound_vector, Y_x_vector, L2_norm_vector,\
            total_steps_vector, t_sim, n_xyz, GPU_partitions, GPU_partitions_vector, voxelization_type, mesh_file, ranks, MPI_size =  read_CSC_MPI_convergence_data(reading_dir = reading_dir_MES);
        dX_points = len(dX_vector);
        Y_x_points = len(Y_x_vector);
        if verbose_flag:
            print "\n"
            print "\t MMSi dX_vector = " + str(dX_vector);
            print "\t MMSi Y_x_vector = " + str(Y_x_vector);
            print "\t MMSi L2_norm_vector = " + str(L2_norm_vector);
            print "\t MMSi ------------------------------------------------------"
            print "\t MMSi Used exact solution? " + str( "TRUE" if use_ExactSolution_flag else "FALSE" );
            print "\t MMSi Interior? " + str( "TRUE" if test_only_interior_flag else "FALSE" );
            print "\t MMSi --------------------"
            print "\t MMSi   GPU_partitions = " + str(GPU_partitions);
            print "\t MMSi   GPU_partitions_vector = " + str(GPU_partitions_vector);
            print "\t MMSi   MPI_size = " + str(MPI_size) + "    ( ranks : " + str(ranks) + "  )";
            print "\t MMSi ------------------------------------------------------"
        
        for lambda_idx in range(len(lambda_sim_vector)):
            for Y_idx in range(len(Y_x_vector)):
                c_sound = c_sound_vector[lambda_idx];
                lambda_sim = lambda_sim_vector[lambda_idx];
                
                Y_x = Y_x_vector[Y_idx];
                L2_error_vector = L2_norm_vector[(lambda_idx*Y_x_points*dX_points + Y_idx*dX_points): ( lambda_idx*Y_x_points*dX_points + (Y_idx+1)*dX_points)];
                if verbose_flag:
                    print "\t\t------"
                    print "\t\tdX_points = " + str(dX_points)
                    print "\t\tY_x_points = " + str(Y_x_points)
                    print "\t\tFROM sample " + str((lambda_idx*Y_x_points*dX_points + Y_idx*dX_points)) + " to sample " + str(( lambda_idx*Y_x_points*dX_points + (Y_idx+1)*dX_points));
                    print "\t\t lambda | Y_x | dX = " + str(dX_vector);
                    print "\t\tl=" + str(lambda_sim) + " | Y=" + str(Y_x) + " | L2_norm = " + str(L2_error_vector);
                ##################################################
                # Fit (Ordinary Least Squares) and summary:
                model = sm.OLS(numpy.log10(L2_error_vector), X);
                results = model.fit();
                prstd, iv_l, iv_u = wls_prediction_std(results);
                model_params = results.params;
                slope_ = model_params[1];
                rsquared_adj_ = results.rsquared_adj;
                str_OLS = str_OLS + "\t beta = "  + str(Y_x) + ' \t q_obs = ' + str(numpy.round(slope_,2)) + '\t R^2 =' + str(numpy.round(rsquared_adj_,3)) + '\n';
                
                #label_text = r'$\epsilon_{l^2}$ [$\beta=' + str(Y_x) + '$]';# + ', \lambda = ' + str(numpy.round(lambda_sim,2))
                label_text = r'$\beta=' + str(Y_x) + '$';# + ' ($q_{O}=' + str(numpy.round(slope_,2)) + '$)'; 
                if Y_x == 1.0:
                    line_back, = plt.loglog(dX_vector, L2_error_vector, color = '0.3', marker=None);
                # Color:
                colorVal = scalarMap.to_rgba(Y_x);
                line, = plt.loglog(dX_vector, L2_error_vector, color = colorVal, marker=markers[line_idx], markersize=4, alpha = 1.0);
                line.set_dashes([line_idx*2, line_idx*0.9]);
                if Y_x == 1.0:
                    lengend_lines.append( (line_back, line) );
                else:
                    lengend_lines.append( line );
                lengend_texts.append( label_text );
                line_idx = line_idx + 1;
            
    #===========================================================================
    #------------------------------------------  Second, the MMS Boundary
    #===========================================================================
    else:
        reading_dir_MES = reading_dir + os.sep + "MMS_Boundaries"; 
         
        use_ExactSolution_flag, test_only_interior_flag, dX_vector, lambda_sim_vector, c_sound_vector, Y_x_vector, L2_norm_vector,\
            total_steps_vector, t_sim, n_xyz, GPU_partitions, GPU_partitions_vector, voxelization_type, mesh_file, ranks, MPI_size =  read_CSC_MPI_convergence_data(reading_dir = reading_dir_MES);
        dX_points = len(dX_vector);
        Y_x_points = len(Y_x_vector);
        if verbose_flag:
            print "\n"
            print "\t MMS bdry dX_vector = " + str(dX_vector);
            print "\t MMS bdry Y_x_vector = " + str(Y_x_vector);
            print "\t MMS bdry L2_norm_vector = " + str(L2_norm_vector);
            print "\t MMS bdry ------------------------------------------------------"
            print "\t MMS bdry Used exact solution? " + str( "TRUE" if use_ExactSolution_flag else "FALSE" );
            print "\t MMS bdry Interior? " + str( "TRUE" if test_only_interior_flag else "FALSE" );
            print "\t MMS bdry --------------------"
            print "\t MMS bdry   GPU_partitions = " + str(GPU_partitions);
            print "\t MMS bdry   GPU_partitions_vector = " + str(GPU_partitions_vector);
            print "\t MMS bdry   MPI_size = " + str(MPI_size) + "    ( ranks : " + str(ranks) + "  )";
            print "\t MMS bdry ------------------------------------------------------"
                 
        for lambda_idx in range(len(lambda_sim_vector)):
            for Y_idx in range(len(Y_x_vector)):
                c_sound = c_sound_vector[lambda_idx];
                lambda_sim = lambda_sim_vector[lambda_idx];
                 
                Y_x = Y_x_vector[Y_idx];
                L2_error_vector = L2_norm_vector[(lambda_idx*Y_x_points*dX_points + Y_idx*dX_points): ( lambda_idx*Y_x_points*dX_points + (Y_idx+1)*dX_points)];
                if verbose_flag:
                    print "\t\t------"
                    print "\t\tdX_points = " + str(dX_points)
                    print "\t\tY_x_points = " + str(Y_x_points)
                    print "\t\tFROM sample " + str((lambda_idx*Y_x_points*dX_points + Y_idx*dX_points)) + " to sample " + str(( lambda_idx*Y_x_points*dX_points + (Y_idx+1)*dX_points));
                    print "\t\t lambda | Y_x | dX = " + str(dX_vector);
                    print "\t\tl=" + str(lambda_sim) + " | Y=" + str(Y_x) + " | L2_norm = " + str(L2_error_vector);
                ##################################################
                # Fit (Ordinary Least Squares) and summary:
                model = sm.OLS(numpy.log10(L2_error_vector), X);
                results = model.fit();
                prstd, iv_l, iv_u = wls_prediction_std(results);
                model_params = results.params;
                slope_ = model_params[1];
                rsquared_adj_ = results.rsquared_adj;
                str_OLS = str_OLS + "\t beta = "  + str(Y_x) + ' \t q_obs = ' + str(numpy.round(slope_,2)) + '\t R^2 =' + str(numpy.round(rsquared_adj_,3)) + '\n';
                
                #label_text = r'$\epsilon_{L2}$ [$Y_{spec}=' + str(Y_x) + '$]';# + ', \lambda = ' + str(numpy.round(lambda_sim,2))
                label_text = r'$\beta=' + str(Y_x) + '$';# + ' ($q_{O}=' + str(numpy.round(slope_,2)) + '$)'; 
                if Y_x == 1.0:
                    line_back, = plt.loglog(dX_vector, L2_error_vector, color = '0.3', marker=None);
                # Color:
                colorVal = scalarMap.to_rgba(Y_x);
                line, = plt.loglog(dX_vector, L2_error_vector, color = colorVal, marker=markers[line_idx], markersize=4);
                line.set_dashes([line_idx*2, line_idx*0.9]);
                
                if Y_x == 1.0:
                    lengend_lines.append( (line_back, line) );
                else:
                    lengend_lines.append( line );
                lengend_texts.append( label_text );
                
                line_idx = line_idx + 1;
            
    print "==============================================================="
    print "=============== Ordinary Least Squares (R2 adjusted) =========="
    print "==\t DIR: " + str(reading_dir) + "\t=="
    print "==============================================================="
    print str_OLS
    print "==============================================================="
    text_ = "";
    if use_ExactSolution_flag:
        text_ = text_ + "MES";
    else:
        text_ = text_ + "MMS";
        if test_only_interior_flag:
            text_ = text_ + " (interior) ";
        else:
            text_ = text_ + " (boundary) ";
            
    #plt.title("Code verif - " + text_ + "; single prec. " + r"; $n_{xyz} = " + str(n_xyz) + "$ ; $t_{sim} = " + str(numpy.round(t_sim*1E3,2)) + "$ [ms].");
    plt.xlabel(r'$\Delta X$ [m]');
    plt.ylabel(r'$\|\epsilon\|_2$', fontsize=17);
    plt.xlim(numpy.min(dx_vector_ideal), numpy.max(dx_vector_ideal));
    plt.grid(True, which="both", color = 'grey');
    #    mpl.rcParams['grid.color'] = 'k'
    if show_interior_plot:
        1==1;
        #leg = plt.legend(lengend_lines, lengend_texts, loc='best', borderaxespad=1., fancybox=True, shadow=False, fontsize= 9.5, ncol = 2);
        #leg.get_frame().set_alpha(0.6);
    else:
        leg = plt.legend(lengend_lines, lengend_texts, loc='best', borderaxespad=1., fancybox=True, shadow=False, fontsize= 14.0, ncol = 2);
        leg.get_frame().set_alpha(0.9);
    plt.xlim(0.009,0.17);
    plt.ylim(3*1E-8, 7*1E-2);
    
    plt.subplots_adjust(left=0.1, right=0.995, top=0.995, bottom=0.05);
    axes.xaxis.set_label_coords(0.45, -0.025);
    axes.yaxis.set_label_coords(-0.055, 0.45);
    plt.show();

if __name__ == '__main__':
    print "Code Verification - Python, nf-HRTFs -----> START"
    #===========================================================================
    # AWS, surface_vox_host:
    #===========================================================================
    # Fig 1a): First part: 2nd order convergence:
    figure_1_code_Verification(show_interior_plot = True, reading_dir = "Results" + os.sep + "MPI_AWS_surface_vox_host");
    # Fig 1b): Second part: 1st order convergence:
    figure_1_code_Verification(show_interior_plot = False, reading_dir = "Results" + os.sep + "MPI_AWS_surface_vox_host");
                       
    #===========================================================================
    # AWS, asyncMPI:
    #===========================================================================
    # Fig 1a): First part: 2nd order convergence:
    figure_1_code_Verification(show_interior_plot = True, reading_dir = "Results" + os.sep + "MPI_AWS_asyncMPI");
    # Fig 1b): Second part: 1st order convergence:
    figure_1_code_Verification(show_interior_plot = False, reading_dir = "Results" + os.sep + "MPI_AWS_asyncMPI");
    
    print "END --------> Code Verification - Python, nf-HRTFs"