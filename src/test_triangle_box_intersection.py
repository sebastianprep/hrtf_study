'''
This file is released under the MIT License. See
http://www.opensource.org/licenses/mit-license.php

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Created on Nov 17, 2014

    A Python 2 script which interactively tests the triangle-voxel intersection rules for a voxelization study.

    Uploaded online (self-contained) in August 2020.

@author: Sebastian Prepelita, 2014
'''

import math
import numpy
import os

import wx

#Fancy plotting:
from mayavi import mlab
from tvtk.tools import visual
from tvtk.api import tvtk
from numbers import Number
from traits.api import HasTraits, Range, Instance, on_trait_change
from traitsui.api import View, Item, Group
from mayavi.core.api import PipelineBase
from mayavi.core.ui.api import MayaviScene, SceneEditor, MlabSceneModel

class Point_3D(object):
    '''
    Helper class for classes Bounding_box and KEMAR_Bounding_Boxes
    '''
    #x = numpy.float64(0.0);
    #y = numpy.float64(0.0);
    #z = numpy.float64(0.0);
    def __init__(self, x_ = 0.0, y_ = 0.0, z_ = 0.0):
        '''Defines x and y variables'''
        self.x = numpy.float64(x_);
        self.y = numpy.float64(y_);
        self.z = numpy.float64(z_);
    def __str__(self):
        return "P[%s,%s,%s] "%(self.x, self.y, self.z);
    
    def __add__(self, other):#Add two points
        return Point_3D(self.x + other.x, self.y + other.y, self.z + other.z);
    
    def __sub__(self, other):#subtract two points
        return Point_3D(self.x - other.x, self.y - other.y, self.z - other.z);
    
    def __mul__(self, other):
        if isinstance(other,Number):# multiply by number
            return Point_3D(self.x * other, self.y * other, self.z * other);
        else: # multiply by same class
            return Point_3D(self.x * other.x, self.y * other.y, self.z * other.z);
    def __rmul__(self, other):
        if isinstance(other,Number):
            return Point_3D(self.x * other, self.y * other, self.z * other);
        else:
            return Point_3D(self.x * other.x, self.y * other.y, self.z * other.z);
        
    def __div__(self,other):
        if isinstance(other,Number):# div by number
            return Point_3D(self.x / other, self.y / other, self.z / other);
        else: # div by same class
            return Point_3D(self.x / other.x, self.y / other.y, self.z / other.z);
    def __rdiv__(self,other):
        if isinstance(other,Number):
            return Point_3D( other / self.x, other / self.y, other / self.z);
        else:
            return Point_3D( other.x / self.x, other.y / self.y, other.z / self.z);
        
    def getXYZ_list(self):
        '''
        Function returns a list with the coordinates.
        '''
        return [self.x, self.y, self.z];
    
    def getXYZ(self):
        '''
        Function returns a numpy float32 array with the coordinates.
        '''
        return numpy.float32( self.getXYZ_list() );
    
    # Functions using numpy functionality:
    def dot(self, other):
        '''
        Function does a dot product with another point_3D class.
        '''
        return numpy.dot(self.getXYZ(), other.getXYZ());
    
    def cross(self, other):
        '''
        Function does a cross product with another Point_3D class. Returns another Point_3D class.
        '''
        numpy_cross = numpy.cross(self.getXYZ(), other.getXYZ());
        return Point_3D( numpy_cross[0], numpy_cross[1], numpy_cross[2]);
    
    def normalized(self):
        '''
        Function returns the normalized version of the vector.
        
        #Note that for many loops (like going through all triangles), use np.sqrt(x.dot(x)) for perfomance : http://stackoverflow.com/questions/9171158/how-do-you-get-the-magnitude-of-a-vector-in-numpy
        '''
        temp = self.getXYZ() / numpy.linalg.norm(self.getXYZ());
        return Point_3D( temp[0], temp[1], temp[2]);
        
    def normalize(self):
        '''
        Function normalizez the x, y, z coordinates.
        '''
        temp = self.normalized();
        self.x = temp.x;
        self.y = temp.y;
        self.z = temp.z;
        
    def get_XY_point(self):
        '''
        Function retrieves a Point_2D class with x and y coordinates.
        '''
        return Point_2D(self.x, self.y);
    
    def get_YX_point(self):
        '''
        Function retrieves a Point_2D class with y and x coordinates.
        '''
        return Point_2D(self.y, self.x);
    
    def get_XZ_point(self):
        '''
        Function retrieves a Point_2D class with x and z coordinates.
        '''
        return Point_2D(self.x, self.z);
    
    def get_ZX_point(self):
        '''
        Function retrieves a Point_2D class with z and x coordinates.
        '''
        return Point_2D(self.z, self.x);
    
    def get_YZ_point(self):
        '''
        Function retrieves a Point_2D class with y and z coordinates.
        '''
        return Point_2D(self.y, self.z);
    
    def get_ZY_point(self):
        '''
        Function retrieves a Point_2D class with z and y coordinates.
        '''
        return Point_2D(self.z, self.y);

class Point_2D(object):
    '''
    2D version of Point_3D.
    '''
    #x = numpy.float64(0.0);
    #y = numpy.float64(0.0);
        
    def __init__(self, x_ = 0.0, y_ = 0.0):
        '''Defines x and y variables'''
        self.x = numpy.float64(x_);
        self.y = numpy.float64(y_);
        
    def __str__(self):
        return "P[%s,%s] "%(self.x, self.y);
    
    def __add__(self, other):#Add two points
        return Point_2D(self.x + other.x, self.y + other.y);
    
    def __sub__(self, other):#subtract two points
        return Point_2D(self.x - other.x, self.y - other.y);
    
    def __mul__(self, other):
        if isinstance(other,Number):# multiply by number
            return Point_2D(self.x * other, self.y * other);
        else: # multiply by same class
            return Point_2D(self.x * other.x, self.y * other.y);
    def __rmul__(self, other):
        if isinstance(other,Number):
            return Point_2D(self.x * other, self.y * other);
        else:
            return Point_2D(self.x * other.x, self.y * other.y);
        
    def __div__(self,other):
        if isinstance(other,Number):# div by number
            return Point_2D(self.x / other, self.y / other );
        else: # div by same class
            return Point_2D(self.x / other.x, self.y / other.y );
    def __rdiv__(self,other):
        if isinstance(other,Number):
            return Point_2D( other / self.x, other / self.y );
        else:
            return Point_2D( other.x / self.x, other.y / self.y );
        
    def getXY_list(self):
        '''
        Function returns a list with the coordinates.
        '''
        return [self.x, self.y];
    
    def getXY(self):
        '''
        Function returns a numpy float32 array with the coordinates.
        '''
        return numpy.float32( self.getXY_list() );
    
    # Functions using numpy functionality:
    def dot(self, other):
        '''
        Function does a dot product with another point_3D class.
        '''
        return numpy.dot(self.getXY(), other.getXY());
    
    def cross(self, other):
        '''
        Function does a cross product with another Point_3D class. Returns another Point_3D class.
        '''
        numpy_cross = numpy.cross(self.getXY(), other.getXY());
        return Point_2D( numpy_cross[0], numpy_cross[1]);
    
    def normalized(self):
        '''
        Function returns the normalized version of the vector.
        
        #Note that for many loops (like going through all triangles), use np.sqrt(x.dot(x)) for perfomance : http://stackoverflow.com/questions/9171158/how-do-you-get-the-magnitude-of-a-vector-in-numpy
        '''
        len_ = numpy.linalg.norm(self.getXY());
        if len_ == 0.0:
            return Point_2D( 0.0, 0.0);
        else:
            temp = self.getXY() / len_;
            return Point_2D( temp[0], temp[1]);
        
    def normalize(self):
        '''
        Function normalizez the x, y, z coordinates.
        '''
        temp = self.normalized();
        self.x = temp.x;
        self.y = temp.y;

def get_2D_normals(triangle_v0, triangle_v1, triangle_v2, normalize = False):
    vertices_array = [triangle_v0, triangle_v1, triangle_v2];
    e0 = triangle_v1 - triangle_v0;
    e1 = triangle_v2 - triangle_v1;
    e2 = triangle_v0 - triangle_v2;
    
    # Tri normal:
    triangle_normal = e0.cross(-1.0 * e2);
    edge_XY_normals = [Point_2D(), Point_2D(), Point_2D()]; # Edges
    edge_ZX_normals = [Point_2D(), Point_2D(), Point_2D()]; # Edges
    edge_YZ_normals = [Point_2D(), Point_2D(), Point_2D()]; # Edges
    for i in range(3):
        ########### Normals:
        # XY:
        #C++: data.ne[i] = tri[i].y - tri[(i + 1)%3].y, tri[(i + 1)%3].x - tri[i].x        
        edge_XY_normals[i] = Point_2D( vertices_array[i].y - vertices_array[(i + 1)%3].y, vertices_array[(i + 1)%3].x - vertices_array[i].x);
        if triangle_normal.z < 0.0:
            edge_XY_normals[i] = -1.0*edge_XY_normals[i];
        # XZ :
        #C++ (voxelizer.cu, line 2839) : data.ne[i] = make_float2( tri[i].x - tri[(i + 1)%3].x, tri[(i + 1)%3].z - tri[i].z );
        edge_ZX_normals[i] = Point_2D( vertices_array[i].x - vertices_array[(i + 1)%3].x, vertices_array[(i + 1)%3].z - vertices_array[i].z);
        if triangle_normal.y < 0.0:
            edge_ZX_normals[i] = -1.0*edge_ZX_normals[i];    
        # YZ:
        #C++: data.ne[i] = make_float2( tri[i].z - tri[(i + 1)%3].z, tri[(i + 1)%3].y - tri[i].y );
        edge_YZ_normals[i] = Point_2D( vertices_array[i].z - vertices_array[(i + 1)%3].z, vertices_array[(i + 1)%3].y - vertices_array[i].y);
        if triangle_normal.x < 0.0:
            edge_YZ_normals[i] = -1.0*edge_YZ_normals[i];
        if normalize:
            edge_XY_normals[i].normalize();
            edge_ZX_normals[i].normalize();
            edge_YZ_normals[i].normalize();
    return edge_XY_normals, edge_ZX_normals, edge_YZ_normals


def cube_triangle_intersection(voxel_index_X, voxel_index_Y, voxel_index_Z, dX, triangle_v0, triangle_v1, triangle_v2):
    '''
    Function calculates the intersection between a triangle given by the vertices (v0,v1,v2) and a voxel given in voxel coordinates.
    
    Assume voxels start at voxel_index_X*dX and end at  (voxel_index_X+1)*dX, so that the voxel center is at dX/2.
    
    The test is based on [1].
    
    :param voxel_index_X: The voxel coordinate (in voxels), X direction.
    :param voxel_index_Y: The voxel coordinate (in voxels), Y direction.
    :param voxel_index_Z: The voxel coordinate (in voxels), Z direction.
    :param dX: the spacial sampling distance [m]
    :param triangle_v0: Point_3D - The [x,y,z] coordinates of first vertex of triangle [m]
    :param triangle_v1: Point_3D - The [x,y,z] coordinates of second vertex of triangle [m]
    :param triangle_v2: Point_3D - The [x,y,z] coordinates of last vertex of triangle [m]
    '''
    # Easier to work with x, y, z -> convert to Point_3D:
    # Build a vector with all the vertices:
    ###################################################################
    #    PREPARE DATA:
    ###################################################################
    # Data for plane intersection
    #################################
    vertices_array = [triangle_v0, triangle_v1, triangle_v2];
    e0 = triangle_v1 - triangle_v0;
    e1 = triangle_v2 - triangle_v1;
    e2 = triangle_v0 - triangle_v2;
    edge_array = [e0, e1, e2];
    voxel_min_corner_WORLD = Point_3D(voxel_index_X*dX, voxel_index_Y*dX, voxel_index_Z*dX);
    
    # Tri normal:
    triangle_normal = e0.cross(-1.0 * e2);
    #triangle_normal.normalize(); # No need for this
    triangle_normal_mask = (triangle_normal.getXYZ()*10).clip(min = 0, max = 1.0); # Clipping to zero: http://stackoverflow.com/questions/3391843/how-to-transform-negative-elements-to-zero-without-a-loop
                                        # Example: if tri_normal is [-0.07546648  0.17837532  0.98106425], it will result in a mask of [ 0.  1.  1.]
    # Convert to Point_3D:
    triangle_normal_mask = Point_3D(triangle_normal_mask[0], triangle_normal_mask[1], triangle_normal_mask[2]);
    
    ##################################################
    # XY, XZ, YZ planar edge normals and distances:
    ##################################################
    edge_XY_normals = [Point_2D(), Point_2D(), Point_2D()]; # Edges
    edge_XY_distances = [0.0, 0.0, 0.0]; # Distances
    edge_ZX_normals = [Point_2D(), Point_2D(), Point_2D()]; # Edges
    edge_ZX_distances = [0.0, 0.0, 0.0]; # Distances
    edge_YZ_normals = [Point_2D(), Point_2D(), Point_2D()]; # Edges
    edge_YZ_distances = [0.0, 0.0, 0.0]; # Distances
    
    for i in range(3):
        ########### Normals:        
        edge_XY_normals[i] = Point_2D( vertices_array[i].y - vertices_array[(i + 1)%3].y, vertices_array[(i + 1)%3].x - vertices_array[i].x);
        if triangle_normal.z < 0.0:
            edge_XY_normals[i] = -1.0*edge_XY_normals[i];
        edge_ZX_normals[i] = Point_2D( vertices_array[i].x - vertices_array[(i + 1)%3].x, vertices_array[(i + 1)%3].z - vertices_array[i].z);
        if triangle_normal.y < 0.0:
            edge_ZX_normals[i] = -1.0*edge_ZX_normals[i];    
        edge_YZ_normals[i] = Point_2D( vertices_array[i].z - vertices_array[(i + 1)%3].z, vertices_array[(i + 1)%3].y - vertices_array[i].y);
        if triangle_normal.x < 0.0:
            edge_YZ_normals[i] = -1.0*edge_YZ_normals[i];
        ########### Distances:
        edge_XY_distances[i] = -(edge_XY_normals[i].x * vertices_array[i].x + edge_XY_normals[i].y * vertices_array[i].y) + max( 0.0, dX * edge_XY_normals[i].x ) + max( 0.0, dX * edge_XY_normals[i].y );
        edge_ZX_distances[i] = -(edge_ZX_normals[i].x * vertices_array[i].z + edge_ZX_normals[i].y * vertices_array[i].x) + max( 0.0, dX * edge_ZX_normals[i].x ) + max( 0.0, dX * edge_ZX_normals[i].y );
        edge_YZ_distances[i] = -(edge_YZ_normals[i].x * vertices_array[i].y + edge_YZ_normals[i].y * vertices_array[i].z) + max( 0.0, dX * edge_YZ_normals[i].x ) + max( 0.0, dX * edge_YZ_normals[i].y );    
    #############################################################################################################################
    #                                                            TESTS:
    ############################################################################################################################# 
    ##############
    # Phase 1: test plane of triangle overlaps voxel:
    ##################################################
        # Critical point:
    max_point = Point_3D(dX, dX, dX); # A better name would better be delta_p since max_point would be voxel_min_corner_WORLD + Point_3D(dX, dX, dX)
    critical_point = triangle_normal_mask * max_point;
        # Distances in plane-intersect test:
        
    d_1 = triangle_normal.dot(critical_point - triangle_v0);
    d_2 = triangle_normal.dot(max_point - critical_point - triangle_v0);
        # Compute intersection test:
    n_dot_p = triangle_normal.dot(voxel_min_corner_WORLD);
    tri_plane_intersects_voxel = (n_dot_p + d_1)*(n_dot_p + d_2) <= 0;
    
    ##############
    # Phase 2: 2D overlap tests in XY, ZX, YZ planes:
    ##################################################
    testResult_XY = True;
    testResult_ZX = True;
    testResult_YZ = True;
    for i in range(3):
        testResult_XY &= ((edge_XY_normals[i].dot(voxel_min_corner_WORLD.get_XY_point()) + edge_XY_distances[i]) >= 0.0);
        testResult_ZX &= ((edge_ZX_normals[i].dot(voxel_min_corner_WORLD.get_ZX_point()) + edge_ZX_distances[i]) >= 0.0);
        testResult_YZ &= ((edge_YZ_normals[i].dot(voxel_min_corner_WORLD.get_YZ_point()) + edge_YZ_distances[i]) >= 0.0);
    return tri_plane_intersects_voxel & testResult_XY & testResult_ZX & testResult_YZ

def cube_triangle_intersection_6_separating(voxel_index_X, voxel_index_Y, voxel_index_Z, dX, triangle_v0, triangle_v1, triangle_v2):
    '''
    Function calculates the intersection between a triangle given by the vertices (v0,v1,v2) and a voxel given in voxel coordinates.
    
    Assume voxels start at voxel_index_X*dX and end at  (voxel_index_X+1)*dX, so that the voxel center is at dX/2.
    
    The test is based on [1].
    
    :param voxel_index_X: The voxel coordinate (in voxels), X direction.
    :param voxel_index_Y: The voxel coordinate (in voxels), Y direction.
    :param voxel_index_Z: The voxel coordinate (in voxels), Z direction.
    :param dX: the spacial sampling distance [m]
    :param triangle_v0: Point_3D - The [x,y,z] coordinates of first vertex of triangle [m]
    :param triangle_v1: Point_3D - The [x,y,z] coordinates of second vertex of triangle [m]
    :param triangle_v2: Point_3D - The [x,y,z] coordinates of last vertex of triangle [m]
    '''
    # Easier to work with x, y, z -> convert to Point_3D:
    # Build a vector with all the vertices:
    ###################################################################
    #    PREPARE DATA:
    ###################################################################
    # Data for plane intersection
    #################################
    vertices_array = [triangle_v0, triangle_v1, triangle_v2];
    e0 = triangle_v1 - triangle_v0;
    e1 = triangle_v2 - triangle_v1;
    e2 = triangle_v0 - triangle_v2;
    edge_array = [e0, e1, e2];
    voxel_min_corner_WORLD = Point_3D(voxel_index_X*dX, voxel_index_Y*dX, voxel_index_Z*dX);
    dX_over_2 = 0.5 * dX;
    
    # Tri normal:
    triangle_normal = e0.cross(-1.0 * e2);
    #triangle_normal = -1.0 * triangle_normal;
    triangle_normal.normalize(); # No need for this
    triangle_normal_mask = (triangle_normal.getXYZ()*10).clip(min = 0, max = 1.0); # Clipping to zero: http://stackoverflow.com/questions/3391843/how-to-transform-negative-elements-to-zero-without-a-loop
                                        # Example: if tri_normal is [-0.07546648  0.17837532  0.98106425], it will result in a mask of [ 0.  1.  1.]
    # Convert to Point_3D:
    triangle_normal_mask = Point_3D(triangle_normal_mask[0], triangle_normal_mask[1], triangle_normal_mask[2]);
    
    ##################################################
    # XY, XZ, YZ planar edge normals and distances:
    ##################################################
    edge_XY_normals = [Point_2D(), Point_2D(), Point_2D()]; # Edges
    edge_XY_distances = [0.0, 0.0, 0.0]; # Distances
    edge_ZX_normals = [Point_2D(), Point_2D(), Point_2D()]; # Edges
    edge_ZX_distances = [0.0, 0.0, 0.0]; # Distances
    edge_YZ_normals = [Point_2D(), Point_2D(), Point_2D()]; # Edges
    edge_YZ_distances = [0.0, 0.0, 0.0]; # Distances
    for i in range(3):
        ########### Normals:        
        edge_XY_normals[i] = Point_2D( vertices_array[i].y - vertices_array[(i + 1)%3].y, vertices_array[(i + 1)%3].x - vertices_array[i].x);
        if triangle_normal.z < 0.0:
            edge_XY_normals[i] = -1.0*edge_XY_normals[i];
        edge_ZX_normals[i] = Point_2D( vertices_array[i].x - vertices_array[(i + 1)%3].x, vertices_array[(i + 1)%3].z - vertices_array[i].z);
        if triangle_normal.y < 0.0:
            edge_ZX_normals[i] = -1.0*edge_ZX_normals[i];    
        edge_YZ_normals[i] = Point_2D( vertices_array[i].z - vertices_array[(i + 1)%3].z, vertices_array[(i + 1)%3].y - vertices_array[i].y);
        if triangle_normal.x < 0.0:
            edge_YZ_normals[i] = -1.0*edge_YZ_normals[i];
        ########### MODIFIED edge Distances:
        edge_XY_distances[i] = (edge_XY_normals[i].x * (dX_over_2 - vertices_array[i].x) + edge_XY_normals[i].y * (dX_over_2 - vertices_array[i].y) ) + dX_over_2 * max( abs(edge_XY_normals[i].x), abs(edge_XY_normals[i].y) );
        edge_ZX_distances[i] = (edge_ZX_normals[i].x * (dX_over_2 - vertices_array[i].z) + edge_ZX_normals[i].y * (dX_over_2 - vertices_array[i].x) ) + dX_over_2 * max( abs(edge_ZX_normals[i].x), abs(edge_ZX_normals[i].y) );
        edge_YZ_distances[i] = (edge_YZ_normals[i].x * (dX_over_2 - vertices_array[i].y) + edge_YZ_normals[i].y * (dX_over_2 - vertices_array[i].z) ) + dX_over_2 * max( abs(edge_YZ_normals[i].x), abs(edge_YZ_normals[i].y) );    
    #############################################################################################################################
    #                                                            TESTS:
    ############################################################################################################################# 
    ##############
    # Phase 1: test plane of triangle overlaps voxel:
    ##################################################
    max_point = Point_3D(dX, dX, dX); # A better name would better be delta_p since max_point would be voxel_min_corner_WORLD + Point_3D(dX, dX, dX)
    tri_normal_abs = Point_3D(abs(triangle_normal.x), abs(triangle_normal.y), abs(triangle_normal.z));
    normal_dominant_value = max(max(tri_normal_abs.x, tri_normal_abs.y), tri_normal_abs.z);
    # MODIFIED distances:
    d_1 = triangle_normal.dot(0.5 * max_point - triangle_v0) + dX_over_2 * normal_dominant_value;
    d_2 = triangle_normal.dot(0.5 * max_point - triangle_v0) - dX_over_2 * normal_dominant_value;
        # Compute intersection test:
    n_dot_p = triangle_normal.dot(voxel_min_corner_WORLD);
    tri_plane_intersects_voxel = (n_dot_p + d_1)*(n_dot_p + d_2) <= 0;
    ##############
    # Phase 2: 2D overlap tests in XY, ZX, YZ planes:
    ##################################################
    testResult_XY = True;
    testResult_ZX = True;
    testResult_YZ = True;
    for i in range(3):
        testResult_XY &= ((edge_XY_normals[i].dot(voxel_min_corner_WORLD.get_XY_point()) + edge_XY_distances[i]) >= 0.0);
        testResult_ZX &= ((edge_ZX_normals[i].dot(voxel_min_corner_WORLD.get_ZX_point()) + edge_ZX_distances[i]) >= 0.0);
        testResult_YZ &= ((edge_YZ_normals[i].dot(voxel_min_corner_WORLD.get_YZ_point()) + edge_YZ_distances[i]) >= 0.0);
    return tri_plane_intersects_voxel & testResult_XY & testResult_ZX & testResult_YZ

class TestTriangle(HasTraits):
    
    dX = 0.0297383021013; # dX = 0.1;
            # Some good starting values: triangle almost touches one edge
#     triangle_v0 = (0.1, -0.010000000000000002, 0.17068);
#     triangle_v1 = (0.17999999999999997, 0.13128000000000004, 0.07876);
#     triangle_v2 = (-0.019800000000000012, 0.010559999999999986, 0.13096);
    
    # 6-separating test #1 (a 2D diagonal triangle that is big enough to cover all voxels):
#     triangle_v0 = (0.0, 0.0, -2*dX);
#     triangle_v1 = (0.0, 0.0, 4*dX);
#     triangle_v2 = (5*dX, 5*dX, dX);
    
    # 6-separating test #1 (a 3D diagonal triangle that is big enough to cover all voxels):
#     triangle_v0 = (0.0, 0.0, -4*dX);
#     triangle_v1 = (0.0, 2*dX, 4*dX);
#     triangle_v2 = (3*dX, 3*dX, 3*dX);
    
    # 6-separating test #2 ( parallel triangle)
#     triangle_v0 = (0.0, dX, -2*dX);
#     triangle_v1 = (0.0, dX, 4*dX);
#     triangle_v2 = (5*dX, dX, dX);
    
    # 6-separating NOT WORKING (dX = 0.1):
#     triangle_v0 = (0.016399999999999998, 0.09539999999999993, 0.1482799999999999);
#     triangle_v1 = (0.17999999999999997, 0.10228000000000001, 0.11515999999999998);
#     triangle_v2 = (0.016599999999999976, 0.07236000000000006, 0.10556000000000001);
    
    # ANOTHER 6-SEP failing one:
#     triangle_v0 = (-0.0076000000000000095, 0.09539999999999993, 0.09227999999999986);
#     triangle_v1 = (0.17999999999999997, 0.08047999999999997, 0.08235999999999993);
#     triangle_v2 = (-0.0074000000000000316, 0.08336000000000007, 0.07855999999999999);

    # Weird 6-SEP fail:
    triangle_v0 = (0.0, 0.0, 0.0);
    triangle_v1 = (0.0, 3.00000024, 0.0);
    triangle_v2 = (3.00000024, 3.00000024, 0.0);
    
    intersection_test = '6-separating';# '6-separating' ; 'conservative'
    
    ######################################################################################################
    ##########                            RANGES:
    #######################
    ######################################################################################################
    ranges = numpy.int16([1,1,1])*1;
    ######################################################################################################
    range_x = ranges[0]; range_y = ranges[1]; range_z = ranges[2]; # In voxel coordinates...
    voxel_range_x = range(range_x);
    voxel_range_y = range(range_y);
    voxel_range_z = range(range_z);
    # Configs:
    show_mid_box = True;
    show_tri_plane_int_volumes = False;
    print_vertices = False;
    
    #DEEBUG:
    scale_factors = [2.5,0.1, 4.0];
    normalize_edge_normals = True;
    proj_color = (0.0, 1.0, 0.0);
    show_XY_proj = True;
    show_XY_normals = True;
    
    show_YZ_proj = True;
    show_YZ_proj_lines = False;
    show_YZ_normals = True;
    
    show_ZX_proj = True;
    show_ZX_normals = True;
    
    ###############################
    # Critical points - 6-separating
    ####################################
    dX_over_2 = 0.5*dX;
    colors_ = [(0.576, 0.827, 0.282),(0.0, 0.0, 1.0),(0.827, 0.294, 0.282)]; # For edge_0, edge_1, edge_2
    critical_point_size = dX / 30;
    sizes_ = [0.5 * critical_point_size, 1.0*critical_point_size, 1.5*critical_point_size];
    opacity_ = [1.0, 0.8, 0.6];
    print "[X, Y, Z]: Res=               Plane & XY & ZX & YZ"
    
    boxes = [];
    for x_ in voxel_range_x:
        for y_ in voxel_range_y:
            for z_ in voxel_range_z:
                boxes.append([x_, y_, z_]);
    feedback_sphere_list = [];
    
    verts = [triangle_v0, triangle_v1, triangle_v2];
    vert_sizes = [dX/20, dX/20, dX/20];
    tri_x, tri_y, tri_z = zip(*verts);
    #Convert to lsit:
    tri_x_reference = list(tri_x);
    tri_y_reference = list(tri_y);
    tri_z_reference = list(tri_z);
    tri_x_list = list(tri_x);
    tri_y_list = list(tri_y);
    tri_z_list = list(tri_z);
    
    feedback_red = 0.15;
    feedback_green = 0.0;
    
    selected_point_id = 0;
    
    # Manipulation useful variables:
    move_all_dX = dX/100.0;
    
    range__ = 10.0;
    n_x = Range(-range__*dX, range__*dX, 0.0, );#mode='spinner')
    n_y = Range(-range__*dX, range__*dX, 0.0, );#mode='spinner')
    n_z = Range(-range__*dX, range__*dX, 0.0, );#mode='spinner')
    scene = Instance(MlabSceneModel, ());
    triangle_vertices_ = Instance(PipelineBase);
        
    # When the scene is activated, or when the parameters are changed, we
    # update the plot.
    @on_trait_change('n_x,n_y,n_z,scene.activated')
    def update_plot(self):
        self.tri_x_list[self.selected_point_id] = self.tri_x_reference[self.selected_point_id] + self.n_x;
        self.tri_y_list[self.selected_point_id] = self.tri_y_reference[self.selected_point_id] + self.n_y;
        self.tri_z_list[self.selected_point_id] = self.tri_z_reference[self.selected_point_id] + self.n_z;
        
        #x, y, z, t = curve(self.n_x, self.n_y);
        if self.triangle_vertices_ is None:
            #Origin:
            self.scene.mlab.points3d([0.0], [0.0], [0.0], [0.02], opacity=1.0, color= (0.0, 0.0, 0.0), scale_factor = 1.0, mode = '2dcross');
            self.scene.mlab.orientation_axes();
            self.scene.mlab.orientation_axes();
            self.scene.mlab.title( 'dX = ' + str(self.dX) + ' ; box@[0-'+ str(self.range_x) +', 0-' + str(self.range_y) + ', 0-' + str(self.range_z) +']' + '\nIntersection test: ' + self.intersection_test + "\nUse mouse or '.'-',' to select vertices; use 8/2 to move trianlge +/- "+str(self.move_all_dX)+"*Y" , height=10 , size = 0.12, opacity = 1.0, color = (0, 0, 0));
            # Boxes:
            for box_idx_ in range(len(self.boxes)):
                # The box:
                cells_x = [1.0*self.boxes[box_idx_][0] * self.dX + self.dX/2];
                cells_y = [1.0*self.boxes[box_idx_][1] * self.dX + self.dX/2];
                cells_z = [1.0*self.boxes[box_idx_][2] * self.dX + self.dX/2];
                cells_sizes = [self.dX];
                pts  = self.scene.mlab.points3d(cells_x, cells_y, cells_z, cells_sizes, color= (1.0,0.0,0.0), opacity = 1.00, scale_factor=1.0, mode = 'cube');
                pts.actor.property.representation = 'wireframe';
                if self.show_mid_box:
                    #show 8 boxes:
                    cells_x_ = [1.0*cells_x[0] - self.dX/4, 1.0*cells_x[0] + self.dX/4, 1.0*cells_x[0] - self.dX/4, 1.0*cells_x[0] + self.dX/4];
                    cells_y_ = [1.0*cells_y[0] - self.dX/4, 1.0*cells_y[0] - self.dX/4, 1.0*cells_y[0] + self.dX/4, 1.0*cells_y[0] + self.dX/4];
                    cells_z_ = [1.0*cells_z[0] - self.dX/4, 1.0*cells_z[0] - self.dX/4, 1.0*cells_z[0] - self.dX/4, 1.0*cells_z[0] - self.dX/4];
                    cells_z_2 = [1.0*cells_z[0] + self.dX/4, 1.0*cells_z[0] + self.dX/4, 1.0*cells_z[0] + self.dX/4, 1.0*cells_z[0] + self.dX/4];
                    cells_sizes_ = [self.dX/2, self.dX/2, self.dX/2, self.dX/2];
                    pts_  = self.scene.mlab.points3d(cells_x_*2, cells_y_*2, cells_z_+cells_z_2, cells_sizes_*2, color= (0.0,0.0,0.0), opacity = 0.56, scale_factor=1.0, line_width = 0.2, mode = 'cube');
                    pts_.actor.property.representation = 'wireframe';
                if self.show_tri_plane_int_volumes:
                    tube_radius_ = 0.0005;
                    col_ = (0.0,1.0,1.0);
                    # z= cst
                    self.scene.mlab.plot3d([ cells_x[0] + self.dX/2, cells_x[0], cells_x[0] - self.dX/2, cells_x[0], cells_x[0] + self.dX/2 ], 
                                           [ cells_y[0], cells_y[0]+ self.dX/2, cells_y[0], cells_y[0] - self.dX/2, cells_y[0] ], 
                                           [ cells_z[0], cells_z[0], cells_z[0], cells_z[0], cells_z[0]], 
                                           color=col_, tube_radius=tube_radius_);# tube_radius=10.
                    self.scene.mlab.plot3d( [ cells_x[0], cells_x[0], cells_x[0], cells_x[0], cells_x[0]],
                                            [ cells_y[0] + self.dX/2, cells_y[0], cells_y[0] - self.dX/2, cells_y[0], cells_y[0] + self.dX/2 ], 
                                               [ cells_z[0], cells_z[0]+ self.dX/2, cells_z[0], cells_z[0] - self.dX/2, cells_z[0] ],  
                                               color=col_, tube_radius=tube_radius_);# tube_radius=10.
            # Feedback spheres:
            for box_idx_ in range(len(self.boxes)):
                ori_ = 1.0;
                self.feedback_sphere_list.append( self.scene.mlab.quiver3d([self.boxes[box_idx_][0]*self.dX + ori_*self.dX/2], [self.boxes[box_idx_][1]*self.dX + ori_*self.dX/2], [self.boxes[box_idx_][2]*self.dX + ori_+self.dX/2 - 1.0], -0.3, 0.0, 0.0, opacity=1.0, scale_factor = self.dX/2, mode = 'sphere', colormap='jet', vmin=-0.2, vmax = 0.2) );
                #self.feedback_sphere = self.scene.mlab.quiver3d([1.0], [1.0], [1.0], [1.0],[1.0],[1.0],scale_factor = self.dX/10, opacity=1.0, mode = 'sphere', colormap='jet', vmin=-0.2, vmax = 0.2);
                self.feedback_sphere_list[box_idx_].glyph.color_mode = 'color_by_scalar'
                    
            # Selection:
            self.outline = mlab.outline(line_width=3);
            self.outline.outline_mode = 'cornered';
            
            # Meshes:
            triangle_v0 = Point_3D(self.triangle_v0[0], self.triangle_v0[1], self.triangle_v0[2]);
            triangle_v1 = Point_3D(self.triangle_v1[0], self.triangle_v1[1], self.triangle_v1[2]);
            triangle_v2 = Point_3D(self.triangle_v2[0], self.triangle_v2[1], self.triangle_v2[2]);
            edge_XY_normals, edge_ZX_normals, edge_YZ_normals = get_2D_normals(triangle_v0, triangle_v1, triangle_v2, normalize = self.normalize_edge_normals);
            # Triangle mesh:
            self.tri_mesh = mlab.triangular_mesh(self.tri_x, self.tri_y, self.tri_z, [(0,1,2)], 
                                    #representation='wireframe', 
                                    color = (0, 0, 1), opacity = 1.0, line_width = 3.0);
            if self.show_XY_proj:
                self.tri_mesh_XY = mlab.triangular_mesh( self.tri_x, self.tri_y, (0.0, 0.0, 0.0), [(0,1,2)], 
                                        representation='wireframe', 
                                        color = self.proj_color, opacity = 1.0, line_width = 3.0);
            if self.show_YZ_proj:
                self.tri_mesh_YZ = mlab.triangular_mesh( (0.0, 0.0, 0.0), self.tri_y, self.tri_z, [(0,1,2)], 
                                        representation='wireframe', 
                                        color = self.proj_color, opacity = 1.0, line_width = 3.0);
                if self.show_YZ_proj_lines:
                    # Lines for point testing:
                    self.cnts = 100.0*self.dX;
                    # Edge_0:
                    edge_0_direction = triangle_v1.get_YZ_point() - triangle_v0.get_YZ_point(); # draw line from v1 in edge_0 direction and then from v0 in -edge_0 direction
                    self.YZ_e0 = mlab.plot3d([0, 0], [self.tri_y[0] - self.cnts * edge_0_direction.x, self.tri_y[1] + self.cnts * edge_0_direction.x], [self.tri_z[0] -  self.cnts * edge_0_direction.y, self.tri_z[1] + self.cnts * edge_0_direction.y ],  color=self.colors_[0], tube_radius=0.001*self.dX, opacity = .4);
                    edge_1_direction = triangle_v2.get_YZ_point() - triangle_v1.get_YZ_point(); 
                    self.YZ_e1 = mlab.plot3d([0, 0], [self.tri_y[1] - self.cnts * edge_1_direction.x, self.tri_y[2] + self.cnts * edge_1_direction.x], [self.tri_z[1] -  self.cnts * edge_1_direction.y, self.tri_z[2] + self.cnts * edge_1_direction.y ],  color=self.colors_[1], tube_radius=0.001*self.dX, opacity = .4);
                    edge_2_direction = triangle_v0.get_YZ_point() - triangle_v2.get_YZ_point(); 
                    self.YZ_e2 = mlab.plot3d([0, 0], [self.tri_y[0] - self.cnts * edge_2_direction.x, self.tri_y[2] + self.cnts * edge_2_direction.x], [self.tri_z[0] -  self.cnts * edge_2_direction.y, self.tri_z[2] + self.cnts * edge_2_direction.y ],  color=self.colors_[2], tube_radius=0.001*self.dX, opacity = .4);
            if self.show_ZX_proj:
                self.tri_mesh_ZX = mlab.triangular_mesh( self.tri_x, (0.0, 0.0, 0.0), self.tri_z, [(0,1,2)], 
                                        representation='wireframe', 
                                        color = self.proj_color, opacity = 0.9, line_width = 1.0);
                
            if self.show_XY_normals:
                self.tri_normals_XY = mlab.quiver3d( ( self.tri_x[0] + 0.5*(self.tri_x[1]- self.tri_x[0]), self.tri_x[1] + 0.5*(self.tri_x[2]- self.tri_x[1]), self.tri_x[2] + 0.5*(self.tri_x[0]- self.tri_x[2]) ), # in the middle:
                                                     ( self.tri_y[0] + 0.5*(self.tri_y[1]- self.tri_y[0]), self.tri_y[1] + 0.5*(self.tri_y[2]- self.tri_y[1]), self.tri_y[2] + 0.5*(self.tri_y[0]- self.tri_y[2]) ), # in the middle:
                                                     (0.0, 0.0, 0.0),
                                                     [edge_XY_normals[0].x * self.scale_factors[0], edge_XY_normals[1].x* self.scale_factors[1], edge_XY_normals[2].x* self.scale_factors[2]], 
                                                     [edge_XY_normals[0].y * self.scale_factors[0], edge_XY_normals[1].y* self.scale_factors[1], edge_XY_normals[2].y* self.scale_factors[2]],
                                                     (0.0, 0.0, 0.0), 
                                                     scale_factor=self.dX / 5);#color = (0.0,0.0,1.0),
            if self.show_YZ_normals:
                self.tri_normals_YZ = mlab.quiver3d( (0.0, 0.0, 0.0), 
                                                     ( self.tri_y[0] + 0.5*(self.tri_y[1]- self.tri_y[0]), self.tri_y[1] + 0.5*(self.tri_y[2]- self.tri_y[1]), self.tri_y[2] + 0.5*(self.tri_y[0]- self.tri_y[2]) ), # in the middle:
                                                     ( self.tri_z[0] + 0.5*(self.tri_z[1]- self.tri_z[0]), self.tri_z[1] + 0.5*(self.tri_z[2]- self.tri_z[1]), self.tri_z[2] + 0.5*(self.tri_z[0]- self.tri_z[2]) ), # in the middle:
                                                     (0.0, 0.0, 0.0), 
                                                     #[Green, Blue, Red]
                                                     [edge_YZ_normals[0].x * self.scale_factors[0], edge_YZ_normals[1].x *self.scale_factors[1], edge_YZ_normals[2].x * self.scale_factors[2]], 
                                                     [edge_YZ_normals[0].y * self.scale_factors[0], edge_YZ_normals[1].y *self.scale_factors[1], edge_YZ_normals[2].y * self.scale_factors[2]], 
                                                     scale_factor=self.dX / 5);
                # Show critical points:
                self.critical_points_YZ = [];
                for box_idx_ in range(len(self.boxes)): # For each box... easier
                    box_center_x = 1.0*self.boxes[box_idx_][0] * self.dX + self.dX/2;
                    box_center_y = 1.0*self.boxes[box_idx_][1] * self.dX + self.dX/2;
                    box_center_z = 1.0*self.boxes[box_idx_][2] * self.dX + self.dX/2;
                    for edge_index in range(3):
                        edge_YZ_normals_abs = Point_2D(abs(edge_YZ_normals[edge_index].x),  abs(edge_YZ_normals[edge_index].y));
                        # edge
                        if edge_YZ_normals_abs.x >= edge_YZ_normals_abs.y:
                            self.critical_points_YZ.append( self.scene.mlab.points3d( numpy.float32( 0.0 ), numpy.float32( box_center_y + self.dX_over_2 * numpy.sign(edge_YZ_normals[edge_index].x) ), numpy.float32( box_center_z ), numpy.float32( self.sizes_[edge_index] ), color= self.colors_[edge_index], opacity = self.opacity_[edge_index], scale_factor=1.0, mode = 'sphere') );
                        else:
                            self.critical_points_YZ.append( self.scene.mlab.points3d( numpy.float32( 0.0 ), numpy.float32( box_center_y ), numpy.float32( box_center_z +self.dX_over_2 * numpy.sign(edge_YZ_normals[edge_index].y) ), numpy.float32( self.sizes_[edge_index] ), color= self.colors_[edge_index], opacity = self.opacity_[edge_index], scale_factor=1.0, mode = 'sphere') );
                #dX_over_2 * max( abs(edge_YZ_normals[i].x), abs(edge_YZ_normals[i].y) )
                
            if self.show_ZX_normals:
                self.tri_normals_ZX = mlab.quiver3d( ( self.tri_x[0] + 0.5*(self.tri_x[1]- self.tri_x[0]), self.tri_x[1] + 0.5*(self.tri_x[2]- self.tri_x[1]), self.tri_x[2] + 0.5*(self.tri_x[0]- self.tri_x[2]) ), # in the middle:
                                                     (0.0, 0.0, 0.0), 
                                                     ( self.tri_z[0] + 0.5*(self.tri_z[1]- self.tri_z[0]), self.tri_z[1] + 0.5*(self.tri_z[2]- self.tri_z[1]), self.tri_z[2] + 0.5*(self.tri_z[0]- self.tri_z[2]) ), # in the middle:
                                                     [edge_ZX_normals[0].y* self.scale_factors[0], edge_ZX_normals[1].y* self.scale_factors[1], edge_ZX_normals[2].y* self.scale_factors[2]],
                                                     (0.0, 0.0, 0.0), 
                                                     [edge_ZX_normals[0].x* self.scale_factors[0], edge_ZX_normals[1].x* self.scale_factors[1], edge_ZX_normals[2].x* self.scale_factors[2]], 
                                                     scale_factor=self.dX / 5);
            #quiver3d(u, v, w, ...)  
            # Triangle points:
            self.triangle_vertices_ = self.scene.mlab.points3d(self.tri_x, self.tri_y, self.tri_z, self.vert_sizes, opacity=1.0, color= (0.0, 0.0, 0.0), scale_factor = 0.2, mode = 'sphere');
            
            # Glyph:
            self.glyph_points = self.triangle_vertices_.glyph.glyph_source.glyph_source.output.points.to_array();
            # Picker:
            self.picker = self.scene.mayavi_scene.on_mouse_pick(self.picker_callback_);
            self.picker.tolerance = 0.1;
            # Key press:
            self.scene.interactor.add_observer('KeyPressEvent', self.on_key_press);
            
            self.update_outline_box();
            self.intersect_all_voxels();
        else:
            self.scene.disable_render = True; # Cool trick to speed things up!
            self.triangle_v0 = (self.tri_x_list[0], self.tri_y_list[0], self.tri_z_list[0]);
            self.triangle_v1 = (self.tri_x_list[1], self.tri_y_list[1], self.tri_z_list[1]);
            self.triangle_v2 = (self.tri_x_list[2], self.tri_y_list[2], self.tri_z_list[2]);
            print '_'
            ### Print stuff:
            if self.print_vertices:
                print 'v0: ' + str(self.triangle_v0);
                print 'v1: ' + str(self.triangle_v1);
                print 'v2: ' + str(self.triangle_v2);
            ###########################################
            self.intersect_all_voxels();
            
            self.tri_mesh.mlab_source.set(x= self.tri_x_list, y=self.tri_y_list, z=self.tri_z_list);
            
            self.update_meshes();
                                                         
            self.triangle_vertices_.mlab_source.set(x=self.tri_x_list, y=self.tri_y_list, z=self.tri_z_list, sizes=self.vert_sizes);
            self.update_outline_box();
            self.scene.disable_render = False; # Cool trick to speed things up!
            #self.scene.mlab.draw();
    
    
    # The layout of the dialog created
    view = View(Item('scene', editor=SceneEditor(scene_class=MayaviScene),
                     height=700, width=700, show_label=False),
                Group(
                        '_', 'n_x', 'n_y', 'n_z',
                     ),
                resizable=True,
                );
                
    def update_meshes(self):
        triangle_v0 = Point_3D(self.tri_x_list[0], self.tri_y_list[0], self.tri_z_list[0]);
        triangle_v1 = Point_3D(self.tri_x_list[1], self.tri_y_list[1], self.tri_z_list[1]);
        triangle_v2 = Point_3D(self.tri_x_list[2], self.tri_y_list[2], self.tri_z_list[2]);
        edge_XY_normals, edge_ZX_normals, edge_YZ_normals = get_2D_normals(triangle_v0, triangle_v1, triangle_v2, normalize = self.normalize_edge_normals);
        
        if self.show_XY_proj:
            self.tri_mesh_XY.mlab_source.set( x=self.tri_x_list, y=self.tri_y_list, z=(0.0, 0.0, 0.0)); 
        if self.show_YZ_proj:
            self.tri_mesh_YZ.mlab_source.set(x= (0.0,0.0,0.0), y=self.tri_y_list, z=self.tri_z_list);
            if self.show_YZ_proj_lines:
                # Edge show:
                edge_0_direction = triangle_v1.get_YZ_point() - triangle_v0.get_YZ_point(); # draw line from v1 in edge_0 direction and then from v0 in -edge_0 direction
                self.YZ_e0.mlab_source.set(x=[0, 0], y=[self.tri_y_list[0] - self.cnts * edge_0_direction.x, self.tri_y_list[1] + self.cnts * edge_0_direction.x], z=[self.tri_z_list[0] -  self.cnts * edge_0_direction.y, self.tri_z_list[1] + self.cnts * edge_0_direction.y ]);
                edge_1_direction = triangle_v2.get_YZ_point() - triangle_v1.get_YZ_point(); 
                self.YZ_e1.mlab_source.set(x=[0, 0], y=[self.tri_y_list[1] - self.cnts * edge_1_direction.x, self.tri_y_list[2] + self.cnts * edge_1_direction.x], z=[self.tri_z_list[1] -  self.cnts * edge_1_direction.y, self.tri_z_list[2] + self.cnts * edge_1_direction.y ]);
                edge_2_direction = triangle_v0.get_YZ_point() - triangle_v2.get_YZ_point(); 
                self.YZ_e2.mlab_source.set(x=[0, 0], y=[self.tri_y_list[0] - self.cnts * edge_2_direction.x, self.tri_y_list[2] + self.cnts * edge_2_direction.x], z=[self.tri_z_list[0] -  self.cnts * edge_2_direction.y, self.tri_z_list[2] + self.cnts * edge_2_direction.y ]);
        if self.show_ZX_proj:
            self.tri_mesh_ZX.mlab_source.set( x=self.tri_x_list, y=(0.0, 0.0, 0.0), z=self.tri_z_list );
        if self.show_YZ_normals:
            self.tri_normals_YZ.mlab_source.reset( x=(0.0, 0.0, 0.0), 
                                                   y= ( self.tri_y_list[0] + 0.5*(self.tri_y_list[1]- self.tri_y_list[0]), self.tri_y_list[1] + 0.5*(self.tri_y_list[2]- self.tri_y_list[1]), self.tri_y_list[2] + 0.5*(self.tri_y_list[0]- self.tri_y_list[2]) ), # in the middle:
                                                   z= ( self.tri_z_list[0] + 0.5*(self.tri_z_list[1]- self.tri_z_list[0]), self.tri_z_list[1] + 0.5*(self.tri_z_list[2]- self.tri_z_list[1]), self.tri_z_list[2] + 0.5*(self.tri_z_list[0]- self.tri_z_list[2]) ), # in the middle:
                                                   u= (0.0, 0.0, 0.0), #(1.0, 1.0, 1.0), (0.0, 0.0, 0.0),
                                                   v= [edge_YZ_normals[0].x* self.scale_factors[0], edge_YZ_normals[1].x* self.scale_factors[1], edge_YZ_normals[2].x* self.scale_factors[2]], 
                                                   w= [edge_YZ_normals[0].y* self.scale_factors[0], edge_YZ_normals[1].y* self.scale_factors[1], edge_YZ_normals[2].y* self.scale_factors[2]]
                                                   );
            # Show critical points:
            idx__ = 0;
            for box_idx_ in range(len(self.boxes)): # For each box... easier
                box_center_x = 1.0*self.boxes[box_idx_][0] * self.dX + self.dX/2;
                box_center_y = 1.0*self.boxes[box_idx_][1] * self.dX + self.dX/2;
                box_center_z = 1.0*self.boxes[box_idx_][2] * self.dX + self.dX/2;
                for edge_index in range(3):
                    edge_YZ_normals_abs = Point_2D(abs(edge_YZ_normals[edge_index].x),  abs(edge_YZ_normals[edge_index].y));
                    # edge
                    if edge_YZ_normals_abs.x >= edge_YZ_normals_abs.y:
                        self.critical_points_YZ[idx__].mlab_source.set( x=numpy.float32( 0.0 ), y=numpy.float32( box_center_y + self.dX_over_2 * numpy.sign(edge_YZ_normals[edge_index].x) ), z=numpy.float32( box_center_z )) ;
                    else:
                        self.critical_points_YZ[idx__].mlab_source.set( x=numpy.float32( 0.0 ), y=numpy.float32( box_center_y ), z=numpy.float32( box_center_z +self.dX_over_2 * numpy.sign(edge_YZ_normals[edge_index].y) )) ;
                    idx__ += 1;# Easier like this - shortcut thinking is bliss
                    
        if self.show_XY_normals:
            self.tri_normals_XY.mlab_source.reset( x=( self.tri_x_list[0] + 0.5*(self.tri_x_list[1]- self.tri_x_list[0]), self.tri_x_list[1] + 0.5*(self.tri_x_list[2]- self.tri_x_list[1]), self.tri_x_list[2] + 0.5*(self.tri_x_list[0]- self.tri_x_list[2]) ), # in the middle: 
                                                   y=( self.tri_y_list[0] + 0.5*(self.tri_y_list[1]- self.tri_y_list[0]), self.tri_y_list[1] + 0.5*(self.tri_y_list[2]- self.tri_y_list[1]), self.tri_y_list[2] + 0.5*(self.tri_y_list[0]- self.tri_y_list[2]) ), # in the middle:
                                                   z=(0.0, 0.0, 0.0),
                                                   u=[edge_XY_normals[0].x* self.scale_factors[0], edge_XY_normals[1].x* self.scale_factors[1], edge_XY_normals[2].x* self.scale_factors[2]],
                                                   v=[edge_XY_normals[0].y* self.scale_factors[0], edge_XY_normals[1].y* self.scale_factors[1], edge_XY_normals[2].y* self.scale_factors[2]], 
                                                   w=(0.0, 0.0, 0.0)
                                                   );
        if self.show_ZX_normals:
            self.tri_normals_ZX.mlab_source.reset( x=( self.tri_x_list[0] + 0.5*(self.tri_x_list[1]- self.tri_x_list[0]), self.tri_x_list[1] + 0.5*(self.tri_x_list[2]- self.tri_x_list[1]), self.tri_x_list[2] + 0.5*(self.tri_x_list[0]- self.tri_x_list[2]) ), # in the middle:
                                                 y=(0.0, 0.0, 0.0), 
                                                 z=( self.tri_z_list[0] + 0.5*(self.tri_z_list[1]- self.tri_z_list[0]), self.tri_z_list[1] + 0.5*(self.tri_z_list[2]- self.tri_z_list[1]), self.tri_z_list[2] + 0.5*(self.tri_z_list[0]- self.tri_z_list[2]) ), # in the middle:
                                                 u=[edge_ZX_normals[0].y* self.scale_factors[0], edge_ZX_normals[1].y* self.scale_factors[1], edge_ZX_normals[2].y* self.scale_factors[2]],
                                                 v=(0.0, 0.0, 0.0), 
                                                 w=[edge_ZX_normals[0].x* self.scale_factors[0], edge_ZX_normals[1].x* self.scale_factors[1], edge_ZX_normals[2].x* self.scale_factors[2]]
                                                 );
                                                   
    def picker_callback_(self,picker):
        """ Picker callback: this get called when on pick events.
        """
        if picker.actor in self.triangle_vertices_.actor.actors:
            # Find which data point corresponds to the point picked:
            # we have to account for the fact that each data point is
            # represented by a glyph with several points
            selected_point_id = picker.point_id/self.glyph_points.shape[0];
            # If the no points have been selected, we have '-1'
            if selected_point_id != -1:
                self.reset_displacement(selected_point_id);
                self.update_outline_box();
                print 'Selected vertex ' +str(self.selected_point_id) + ' !';
                
    def reset_displacement(self, selected_point_id):
        # 1) Update reference
        self.tri_x_reference[self.selected_point_id] = self.tri_x_reference[self.selected_point_id] + self.n_x;
        self.tri_y_reference[self.selected_point_id] = self.tri_y_reference[self.selected_point_id] + self.n_y;
        self.tri_z_reference[self.selected_point_id] = self.tri_z_reference[self.selected_point_id] + self.n_z;
        # 2) reset the displacement
        self.selected_point_id = selected_point_id;
        self.n_x = 0.0;
        self.n_y = 0.0;
        self.n_z = 0.0;
        
    def on_key_press(self, vtk_obj, event):
        print '_'
        #print vtk_obj.GetClassName(), event, vtk_obj.GetKeyCode()
        #print vtk_obj.GetClassName(), event, vtk_obj.GetKeySym()
        if vtk_obj.GetKeyCode() == '.':
            selected_point_id = (self.selected_point_id + 1) %3;
            self.reset_displacement(selected_point_id);
            self.update_outline_box();
            print 'Selected vertex ' +str(self.selected_point_id) + ' !';
        if vtk_obj.GetKeyCode() == ',':
            selected_point_id = (self.selected_point_id - 1) %3;
            self.reset_displacement(selected_point_id);
            self.update_outline_box();
            print 'Selected vertex ' +str(self.selected_point_id) + ' !';
        if vtk_obj.GetKeyCode() == '8':
            self.scene.disable_render = True;
            for i in range(3):
                self.tri_y_list[i] = self.tri_y_list[i] + self.move_all_dX;
            self.triangle_v0 = (self.tri_x_list[0], self.tri_y_list[0], self.tri_z_list[0]);
            self.triangle_v1 = (self.tri_x_list[1], self.tri_y_list[1], self.tri_z_list[1]);
            self.triangle_v2 = (self.tri_x_list[2], self.tri_y_list[2], self.tri_z_list[2]);
            self.intersect_all_voxels();
            self.tri_mesh.mlab_source.set(x= self.tri_x_list, y=self.tri_y_list, z=self.tri_z_list);
            self.update_meshes();
            self.triangle_vertices_.mlab_source.set(x=self.tri_x_list, y=self.tri_y_list, z=self.tri_z_list, sizes=self.vert_sizes);
            self.update_outline_box();
            if self.print_vertices:
                print 'v0: ' + str(self.triangle_v0);
                print 'v1: ' + str(self.triangle_v1);
                print 'v2: ' + str(self.triangle_v2);
            self.scene.disable_render = False;
        if vtk_obj.GetKeyCode() == '2':
            self.scene.disable_render = True;
            for i in range(3):
                self.tri_y_list[i] = self.tri_y_list[i] - self.move_all_dX;
            self.triangle_v0 = (self.tri_x_list[0], self.tri_y_list[0], self.tri_z_list[0]);
            self.triangle_v1 = (self.tri_x_list[1], self.tri_y_list[1], self.tri_z_list[1]);
            self.triangle_v2 = (self.tri_x_list[2], self.tri_y_list[2], self.tri_z_list[2]);
            self.intersect_all_voxels();
            self.tri_mesh.mlab_source.set(x= self.tri_x_list, y=self.tri_y_list, z=self.tri_z_list);
            self.update_meshes();
            self.triangle_vertices_.mlab_source.set(x=self.tri_x_list, y=self.tri_y_list, z=self.tri_z_list, sizes=self.vert_sizes);
            self.update_outline_box();
            if self.print_vertices:
                print 'v0: ' + str(self.triangle_v0);
                print 'v1: ' + str(self.triangle_v1);
                print 'v2: ' + str(self.triangle_v2);
            self.scene.disable_render = False;
        if vtk_obj.GetKeyCode() == '4':
            self.scene.disable_render = True;
            for i in range(3):
                self.tri_x_list[i] = self.tri_x_list[i] + self.move_all_dX;
            self.triangle_v0 = (self.tri_x_list[0], self.tri_y_list[0], self.tri_z_list[0]);
            self.triangle_v1 = (self.tri_x_list[1], self.tri_y_list[1], self.tri_z_list[1]);
            self.triangle_v2 = (self.tri_x_list[2], self.tri_y_list[2], self.tri_z_list[2]);
            self.intersect_all_voxels();
            self.tri_mesh.mlab_source.set(x= self.tri_x_list, y=self.tri_y_list, z=self.tri_z_list);
            self.update_meshes();
            self.triangle_vertices_.mlab_source.set(x=self.tri_x_list, y=self.tri_y_list, z=self.tri_z_list, sizes=self.vert_sizes);
            self.update_outline_box();
            if self.print_vertices:
                print 'v0: ' + str(self.triangle_v0);
                print 'v1: ' + str(self.triangle_v1);
                print 'v2: ' + str(self.triangle_v2);
            self.scene.disable_render = False;
        if vtk_obj.GetKeyCode() == '6':
            self.scene.disable_render = True;
            for i in range(3):
                self.tri_x_list[i] = self.tri_x_list[i] - self.move_all_dX;
            self.triangle_v0 = (self.tri_x_list[0], self.tri_y_list[0], self.tri_z_list[0]);
            self.triangle_v1 = (self.tri_x_list[1], self.tri_y_list[1], self.tri_z_list[1]);
            self.triangle_v2 = (self.tri_x_list[2], self.tri_y_list[2], self.tri_z_list[2]);
            self.intersect_all_voxels();
            self.tri_mesh.mlab_source.set(x= self.tri_x_list, y=self.tri_y_list, z=self.tri_z_list);
            self.update_meshes();
            self.triangle_vertices_.mlab_source.set(x=self.tri_x_list, y=self.tri_y_list, z=self.tri_z_list, sizes=self.vert_sizes);
            self.update_outline_box();
            if self.print_vertices:
                print 'v0: ' + str(self.triangle_v0);
                print 'v1: ' + str(self.triangle_v1);
                print 'v2: ' + str(self.triangle_v2);
            self.scene.disable_render = False;
            
        if vtk_obj.GetKeyCode() == '7':
            self.scene.disable_render = True;
            for i in range(3):
                self.tri_z_list[i] = self.tri_z_list[i] - self.move_all_dX;
            self.triangle_v0 = (self.tri_x_list[0], self.tri_y_list[0], self.tri_z_list[0]);
            self.triangle_v1 = (self.tri_x_list[1], self.tri_y_list[1], self.tri_z_list[1]);
            self.triangle_v2 = (self.tri_x_list[2], self.tri_y_list[2], self.tri_z_list[2]);
            self.intersect_all_voxels();
            self.tri_mesh.mlab_source.set(x= self.tri_x_list, y=self.tri_y_list, z=self.tri_z_list);
            self.update_meshes();
            self.triangle_vertices_.mlab_source.set(x=self.tri_x_list, y=self.tri_y_list, z=self.tri_z_list, sizes=self.vert_sizes);
            self.update_outline_box();
            if self.print_vertices:
                print 'v0: ' + str(self.triangle_v0);
                print 'v1: ' + str(self.triangle_v1);
                print 'v2: ' + str(self.triangle_v2);
            self.scene.disable_render = False;
        if vtk_obj.GetKeyCode() == '9':
            self.scene.disable_render = True;
            for i in range(3):
                self.tri_z_list[i] = self.tri_z_list[i] + self.move_all_dX;
            self.triangle_v0 = (self.tri_x_list[0], self.tri_y_list[0], self.tri_z_list[0]);
            self.triangle_v1 = (self.tri_x_list[1], self.tri_y_list[1], self.tri_z_list[1]);
            self.triangle_v2 = (self.tri_x_list[2], self.tri_y_list[2], self.tri_z_list[2]);
            self.intersect_all_voxels();
            self.tri_mesh.mlab_source.set(x= self.tri_x_list, y=self.tri_y_list, z=self.tri_z_list);
            self.update_meshes();
            self.triangle_vertices_.mlab_source.set(x=self.tri_x_list, y=self.tri_y_list, z=self.tri_z_list, sizes=self.vert_sizes);
            self.update_outline_box();
            if self.print_vertices:
                print 'v0: ' + str(self.triangle_v0);
                print 'v1: ' + str(self.triangle_v1);
                print 'v2: ' + str(self.triangle_v2);
            self.scene.disable_render = False;
                 
    def update_outline_box(self):
        '''
        Function used to update the selected vertex all the time...
        '''
        x, y, z = self.tri_x_list[self.selected_point_id], self.tri_y_list[self.selected_point_id], self.tri_z_list[self.selected_point_id];
        # Move the outline to the data point.
        div_ = 30;
        self.outline.bounds = (x-self.dX/div_, x+self.dX/div_,
                          y-self.dX/div_, y+self.dX/div_,
                          z-self.dX/div_, z+self.dX/div_);
            
    def intersect_all_voxels(self):
        '''
        Function that goes through all the voxels and intersects with the triangle and colors the feedback spheres accordingly.
        '''
        # Convert to Point_3D
        triangle_v0 = Point_3D(self.triangle_v0[0], self.triangle_v0[1], self.triangle_v0[2]);
        triangle_v1 = Point_3D(self.triangle_v1[0], self.triangle_v1[1], self.triangle_v1[2]);
        triangle_v2 = Point_3D(self.triangle_v2[0], self.triangle_v2[1], self.triangle_v2[2]);

        for box_idx_ in range(len(self.boxes)):
            if self.intersection_test == 'conservative':
                triangle_intersects_voxel = cube_triangle_intersection(self.boxes[box_idx_][0], 
                                                                       self.boxes[box_idx_][1], 
                                                                       self.boxes[box_idx_][2], 
                                                                       self.dX, 
                                                                       triangle_v0, 
                                                                       triangle_v1, 
                                                                       triangle_v2);
            # Cpp 6-separating version:
            if self.intersection_test == '6-separating':
                triangle_intersects_voxel = cube_triangle_intersection_6_separating(self.boxes[box_idx_][0], 
                                                                       self.boxes[box_idx_][1], 
                                                                       self.boxes[box_idx_][2], 
                                                                       self.dX, 
                                                                       triangle_v0, 
                                                                       triangle_v1, 
                                                                       triangle_v2);
                #print "Box " + str(box_idx_) + " [" + str(self.boxes[box_idx_][0])+ "," + str(self.boxes[box_idx_][1]) + "," + str(self.boxes[box_idx_][2]) + "] - " + str(triangle_intersects_voxel);
            if triangle_intersects_voxel:
                self.feedback_sphere_list[box_idx_].mlab_source.set(scalars = self.feedback_green);
            else:
                self.feedback_sphere_list[box_idx_].mlab_source.set(scalars = self.feedback_red);
        
def dump(obj):
  for attr in dir(obj):
    print "obj.%s = %s" % (attr, getattr(obj, attr));
    
if __name__ == '__main__':
    print 'Tri-box start'
    
    # Assume cube, i.e. dX = dY = dZ - use only dX
    test = TestTriangle();
    test.configure_traits();
    
    print 'Tri-box end'